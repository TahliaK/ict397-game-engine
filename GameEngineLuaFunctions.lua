-- EngineLuaFunctions.lua
-- contains wrappers for the C wrappers so its easier
-- to script for our game engine
-- Author: Tahlia Kowald
-- Date: 27/04/2017
-- Version: 1

-- Honestly I need to implement LuaBind for this

Camera -- UserData camera
GameObjects {} --UserData table of - indexed starting @ 1
GameObjectNumber = 0 --number of GameObjects in table

--GameObjects table management
AddGameObject = function(newObject)
	GameObjectNumber++
	GameObjects[GameObjectsNumber] = newObject
end

-- Game_Make3DObject
Game_Make3DObject_Values = function(name, isVisible, isStatic, posX, posY, posZ, rotX, rotY, rotZ, scX, scY, scZ)
	return scZ, scY, scX, rotZ, rotY, rotX, posZ, posY, posX, isStatic, isVisible, name --puts on stack
end

Game_Make3DObject = function(name, isVisible, isStatic, posX, posY, posZ, rotX, rotY, rotZ, scX, scY, scZ)
	Game_Make3DObject_Values(name, isVisible, isStatic, posX, posY, posZ, rotX, rotY, rotZ, scX, scY, scZ)
	c_Make3DObject()
end

-- Game_Load3DModel
Game_Load3DModel_Values = function(GameObject, filename)
	return filename, GameObject
end

Game_Load3DModel = function(GameObject, filename)
