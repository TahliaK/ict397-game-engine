#include "SDL_Sound.h"
#include <cstdlib>
//BOOST_CLASS_EXPORT_IMPLEMENT(SDL_Sound)
#define SDL_SOUND_FADE 0.5f 
//length of sound fade time in ms

//SDL Mixer documentation link so I don't lose it:
//https://www.libsdl.org/projects/SDL_mixer/docs/SDL_mixer.html#SEC55

/* SDL_SOUND_SFXInfoManager ------------------*/

int SDL_Sound_SFXInfoManager::numberOfInits = 0;
std::vector<SDL_SFXItem> SDL_Sound_SFXInfoManager::sfxList = std::vector<SDL_SFXItem>(10, SDL_SFXItem());
std::vector<SDL_MusItem> SDL_Sound_SFXInfoManager::musicList = std::vector<SDL_MusItem>(10, SDL_MusItem());

void SDL_Sound_SFXInfoManager::initialiseSound()
{
	if(!Mix_Init(0))
	{
		Mix_Init(MIX_INIT_FLAC|MIX_INIT_MP3|MIX_INIT_OGG);
		Mix_ChannelFinished(channel_finished);
		std::cout << "[SDL_SOUND_SFXIM] Initialised sound." << std::endl;
	}

	int freq, channels;
	Uint16 format;
	if(Mix_QuerySpec(&freq, &format, &channels) == 0)
	{
		Mix_OpenAudio(MIX_DEFAULT_FREQUENCY, AUDIO_S16SYS, 2, 1024);
		//Audio opens with default frequency (22050 GHz)
		//Standard audio system (signed 16-bit ints with system byte order)
		//Stereo (2) streams of music
		//1024bits in a sound chunk
		std::cout << "[SDL_SOUND_SFXIM] Opened audio stream." << std::endl;
	}

	numberOfInits++;
	std::cout << "[SDL_SOUND_SFXIM] ItemCount:" << numberOfInits << std::endl;
}

void SDL_Sound_SFXInfoManager::closeSound()
{
	numberOfInits--;
	
	if(numberOfInits == 0)
	{
		std::cout << "[SDL_SOUND_SFXIM] Closed sound." << std::endl;
		int i, frequency, channels;
		Uint16 format;
		for(i = 0; i < Mix_QuerySpec(&frequency, &format, &channels); i++)
		{
			Mix_CloseAudio();
		}
		while(Mix_Init(0))
		{
			Mix_Quit();
		}

		//free memory used for loaded Sounds
		for(i = 0; i < sfxList.size(); i++)
		{
			if(sfxList[i].sfx_ptr != nullptr)
			{
				Mix_FreeChunk(sfxList[i].sfx_ptr);
				sfxList[i] = SDL_SFXItem();
			}
		}
		for(i = 0; i < musicList.size(); i++)
		{
			if(musicList[i].mus_ptr != nullptr)
			{
				Mix_FreeMusic(musicList[i].mus_ptr);
				musicList[i] = SDL_MusItem();
			}
		}

		sfxList.resize(10);
		musicList.resize(10);
	}
}

int SDL_Sound_SFXInfoManager::loadSound(std::string filename, int soundType)
{
	int soundIndex, soundID;
	if(!isLoaded(filename, soundType, soundIndex)) //is not already loaded
	{
		soundIndex = firstFreeIndex(soundType);
		if(soundIndex != -1)
		{
			if(soundType == SOUNDTYPE_MUSIC)
			{
				musicList[soundIndex].mus_ptr = Mix_LoadMUS(filename.c_str());
				if(musicList[soundIndex].mus_ptr != NULL)
				{
					musicList[soundIndex].filename = filename;
					musicList[soundIndex].numUsers++;
					soundID = IndexToID(soundIndex, soundType);
				}
				else
				{
					musicList[soundIndex].mus_ptr = nullptr;
					soundID = SOUNDFILE_NOTFOUND;
				}
			}
			else
			{
				sfxList[soundIndex].sfx_ptr = Mix_LoadWAV(filename.c_str());
				if(sfxList[soundIndex].sfx_ptr != NULL)
				{
					sfxList[soundIndex].filename = filename;
					sfxList[soundIndex].numUsers++;
					soundID = IndexToID(soundIndex, soundType);
				}
				else
				{
					sfxList[soundIndex].sfx_ptr = nullptr;
					soundID = SOUNDFILE_NOTFOUND;
				}
			}
			std::cout << "[SDL_SOUND_SFXIM] Loaded sound: " << filename  << "\tAssigned ID: "<< soundID << std::endl;
		}
		else
		{
			soundID = SOUNDLOAD_FAILED;
		}
	}
	else //is already loaded
	{
		soundID = IndexToID(soundIndex, soundType);
		if(soundType == SOUNDTYPE_MUSIC)
		{
			musicList[soundIndex].numUsers++;
		}
		else
		{
			sfxList[soundIndex].numUsers++;
		}
	}

	switch(soundID)
	{
	case SOUNDFILE_NOTFOUND:
		std::cout << "[SDL SOUND ERROR]: Sound load failed in SDL_Mixer (Is the filename right?)" << std::endl;
		break;
	case SOUNDLOAD_FAILED:
		std::cout << "[SDL SOUND ERROR]: Sound load failed due to memory error" << std::endl;
		break;
	}

	return soundID;
}

void SDL_Sound_SFXInfoManager::unloadSound(std::string filename, int soundType)
{
	std::cout << "[SDL_SOUND_SFXIM] Unloaded sound: " << filename << std::endl;
	int soundIndex = -1;
	if(isLoaded(filename, soundType, soundIndex))
	{
		if(soundType == SOUNDTYPE_MUSIC)
		{
			if(--musicList[soundIndex].numUsers == 0)
			{
				Mix_FreeMusic(musicList[soundIndex].mus_ptr);
				musicList[soundIndex] = SDL_MusItem();
			}
		}
		else
		{
			if(--sfxList[soundIndex].numUsers == 0)
			{
				if(sfxList[soundIndex].channel != -1)
				{
					Mix_HaltChannel(sfxList[soundIndex].channel);
				}

				Mix_FreeChunk(sfxList[soundIndex].sfx_ptr);
				sfxList[soundIndex] = SDL_SFXItem();
				}
		}
	}
}

int SDL_Sound_SFXInfoManager::playSound(int soundID, int times, float fadeIn)
{
	std::cout << "[SDL_SOUND_SFXIM] Set playing sound: " << soundID << std::endl;
	int channelNum = -1;
	try
	{
		if(fadeIn == 0.0f)
		{
			if(isMusic(soundID))
				Mix_FadeInMusic(musicList.at(IDToIndex(soundID)).mus_ptr, times, SDL_SOUND_FADE);
			else
			{
				channelNum = Mix_FadeInChannel(-1, sfxList.at(IDToIndex(soundID)).sfx_ptr, times, SDL_SOUND_FADE);
				//need to change it so that the SDL_Sound component decides fade time

				sfxList.at(IDToIndex(soundID)).channel = channelNum;
			}
		}
		else
		{
			if(isMusic(soundID))
			{
				std::cout << "[SDL_SOUND_SFXIM] Playing sound: " << soundID << std::endl;
				int out = Mix_PlayMusic(musicList.at(IDToIndex(soundID)).mus_ptr, times);
				if(out != 0)
					std::cout << "[SDL_SOUND_SFXIM] Playback error: " << soundID << std::endl;
			}
			else
			{
				channelNum = Mix_PlayChannel(-1, sfxList.at(IDToIndex(soundID)).sfx_ptr, times);
				sfxList.at(IDToIndex(soundID)).channel = channelNum;
			}
		}
	}
	catch(std::out_of_range)
	{
		//soundID is not valid
	}

	return channelNum;
}

void SDL_Sound_SFXInfoManager::stopSound(int soundID, float fadeOut)
{
	try{
		if(!isMusic(soundID))
		{
			if(fadeOut == 0.0f)
			{
				Mix_HaltChannel(sfxList.at(IDToIndex(soundID)).channel);
				sfxList.at(IDToIndex(soundID)).channel = -1;
			}
			else
			{
				Mix_FadeOutChannel(sfxList.at(IDToIndex(soundID)).channel, fadeOut);
				sfxList.at(IDToIndex(soundID)).channel = -1;
			}
		}
		else
		{
			if(fadeOut == 0.0f)
			{
				Mix_HaltMusic();
			}
			else
			{
				Mix_FadeOutMusic(fadeOut);
			}
		}

		std::cout << "[SDL_SOUND_SFXIM] Stopped sound: " << soundID << std::endl;
	}
	catch(std::out_of_range)
	{
		//invalid soundID
	}
}

void SDL_Sound_SFXInfoManager::channel_finished(int channel)
{
	bool updateComplete = false;
	for(int i = 0; i < sfxList.size(); i++)
	{
		if(sfxList[i].channel == channel)
		{
			sfxList[i].channel = -1;
			updateComplete = true;
		}

		if(updateComplete)
			break;
	}
}

bool SDL_Sound_SFXInfoManager::isValidID(int soundID)
{
	bool valid = true;
	try{
		if(isMusic(soundID))
		{
			if(musicList.at(IDToIndex(soundID)).mus_ptr == nullptr)
			{
				valid = false;
			}
		}
		else //is SFX
		{
			if(sfxList.at(IDToIndex(soundID)).sfx_ptr == nullptr)
			{
				valid = false;
			}
		}
	}
	catch(std::out_of_range)
	{
		valid = false;
	}

	return valid;
}

bool SDL_Sound_SFXInfoManager::isMusic(int soundID)
{
	return soundID < 0;
}

bool SDL_Sound_SFXInfoManager::isLoaded(std::string filename, int soundType, int &index)
{
	bool found = false;
	if(soundType == SOUNDTYPE_MUSIC)
	{
		for(int i = 0; i < musicList.size(); i++)
		{
			if(musicList[i].filename == filename)
			{
				found = true;
				index = i;
			}

			if(found)
				break;
		}
	}
	else
	{
		for(int i = 0; i < sfxList.size(); i++)
		{
			if(sfxList[i].filename == filename)
			{
				found = true;
				index = i;
			}

			if(found)
				break;
		}
	}

	return found;
}

int SDL_Sound_SFXInfoManager::IDToIndex(int soundID)
{
	if(soundID < 0)
	{
		std::cout << "Music ToIndex conversion, input: " << soundID;
		soundID+=1;
		soundID = soundID * -1;
		std::cout << "    output: " << soundID << std::endl;
	}

	return soundID;
}

int SDL_Sound_SFXInfoManager::IndexToID(int soundIndex, int soundType)
{
	if(soundType == SOUNDTYPE_MUSIC)
	{
		std::cout << "Music ID/Index conversion, input: " << soundIndex;
		soundIndex = soundIndex * -1;
		soundIndex -= 1;
		std::cout << "    output: " << soundIndex << std::endl;
	}

	return soundIndex;
}

int SDL_Sound_SFXInfoManager::firstFreeIndex(int soundType)
{
	int soundIndex = -1;
	try{
		if(soundType == SOUNDTYPE_MUSIC)
		{
			for(int i = 0; i < musicList.size(); i++)
			{
				if(musicList[i].mus_ptr == nullptr)
				{
					soundIndex = i;
				}

				if(soundIndex != -1)
					break;
			}

			if(soundIndex == -1)
			{
				soundIndex = musicList.size();
				musicList.resize(musicList.size() + 10, SDL_MusItem());
			}
		}
		else
		{
			for(int i = 0; i < sfxList.size(); i++)
			{
				if(sfxList[i].sfx_ptr == nullptr)
				{
					soundIndex = i;
				}

				if(soundIndex != -1)
					break;
			}

			if(soundIndex == -1)
			{
				soundIndex = sfxList.size();
				sfxList.resize(sfxList.size() + 10, SDL_SFXItem());
			}

		}
	}
	catch(length_error e)
	{
		std::cerr << "[SDL SOUND ERROR]: First Free Index error:" << e.what() << std::endl;
		soundIndex = -1;
	}

	return soundIndex;
}

/* -------------- SDL_SOUND ----------- */

SDL_Sound::SDL_Sound() : Sound()
{
	Initialize();
	Reflection();
	
	m_name = "Sound (SDL)";
	m_filename = "Sounds\\";
	m_soundType = SOUNDTYPE_SFX;
	m_isPlaying = 0;
	m_repeat = false;
	m_fadeInTime = 0.0f;
	m_fadeOutTime = 0.0f;
	m_loopNumber = 0;
}

SDL_Sound::~SDL_Sound()
{
	Clear();
}

void SDL_Sound::Initialize()
{
	static bool l_inList = false;
	if (!l_inList) {
		l_inList = true;
		SDL_Sound* l_pointer = new SDL_Sound;
		m_derivedClasses.push_back(l_pointer);
	}
	
	SDL_Sound_SFXInfoManager::initialiseSound();
}

void SDL_Sound::Start() {}

void SDL_Sound::Update() {} //no update, SDL_Mixer handles constant stuff

void SDL_Sound::Clear()
{
	SDL_Sound_SFXInfoManager::closeSound();
}

bool SDL_Sound::SetFile(boost::any file)
{
	try{
		m_filename = boost::any_cast<std::string>(file);
		return true;
	}
	catch(boost::bad_any_cast)
	{
		std::cout << "SDL_Sound::SetFile bad boost::any_cast" << std::endl;
	}
	return false;

}

bool SDL_Sound::SetIsMusic(boost::any isMusic)
{
	try{
		if(boost::any_cast<bool>(isMusic))
		{
			m_soundType = SOUNDTYPE_MUSIC;
		}
		else
		{
			m_soundType = SOUNDTYPE_SFX;
		}
		return true;
	}
	catch(boost::bad_any_cast)
	{
		std::cout << "SDL_Sound::SetIsMusic bad boost::any_cast" << std::endl;
	}
	return false;
}

bool SDL_Sound::SetPlaying(boost::any isPlaying)
{
	try{
		bool temp_isPlaying = boost::any_cast<bool>(isPlaying);
		updateIsPlaying(temp_isPlaying);
		return true;
	}
	catch(boost::bad_any_cast)
	{
		std::cout << "SDL_Sound::SetPlaying bad boost::any_cast" << std::endl;
	}
	return false;
}

bool SDL_Sound::SetRepeat(boost::any isRepeating)
{
	try{
		m_repeat = boost::any_cast<bool>(isRepeating);
		return true;
	}
	catch(boost::bad_any_cast)
	{
		std::cout << "SDL_Sound::SetRepeat bad boost::any_cast" << std::endl;
	}
	return false;
}

bool SDL_Sound::SetLoopNum(boost::any loops)
{
	try{
		if(boost::any_cast<int>(loops) < 0)
		{
			m_repeat = true;
			m_loopNumber = -1;
		}
		else
		{
			m_loopNumber = boost::any_cast<int>(loops);
			m_repeat = false;
		}
		return true;
	}
	catch(boost::bad_any_cast)
	{
		std::cout << "[SDL SOUND ERROR] SetLoopNum bad any cast" << std::endl;
	}
	return false;
}

bool SDL_Sound::SetFadeInTime(boost::any fadeIn)
{
	try{
		m_fadeInTime = boost::any_cast<int>(fadeIn);
		return true;
	}
	catch(boost::bad_any_cast)
	{
		std::cout << "SDL_Sound::SetFadeInTime bad boost::any_cast" << std::endl;
	}
	return false;
}

bool SDL_Sound::SetFadeOutTime(boost::any fadeOut)
{
	try{
		m_fadeOutTime = boost::any_cast<int>(fadeOut);
	}
	catch(boost::bad_any_cast)
	{
		std::cout << "SDL_Sound::SetFadeOutTime bad boost::any_cast" << std::endl;
	}
	return false;
}

std::string SDL_Sound::GetFile() {return m_filename;}
bool SDL_Sound::GetIsMusic()
{
	if(m_soundType == SOUNDTYPE_MUSIC)
	{
		return true;
	}

	return false;
}
bool SDL_Sound::GetPlaying()
{
	if(m_isPlaying)
	{
		return true;
	}
	return false;
}

bool SDL_Sound::GetRepeat()
{
	if(m_repeat)
	{
		return true;
	}
	return false;
}

int SDL_Sound::GetLoopNum()
{
	return m_loopNumber;
}

int SDL_Sound::GetFadeInTime()
{
	return m_fadeInTime;
}

int SDL_Sound::GetFadeOutTime()
{
	return m_fadeOutTime;
}

/*int SDL_Sound::LoadSound(std::string filename, int soundType)
{
	return SDL_Sound_SFXInfoManager::loadSound(filename, soundType);
}

bool SDL_Sound::PlaySound(int soundID, int times, bool fadeIn)
{
	bool success = true;
	if(isValidID(soundID))
	{
		if(isMusic(soundID))
		{
			if(fadeIn)
			{
				Mix_FadeInMusic(loadedMUS[IDToIndex(soundID)], times, SDL_SOUND_FADE);
			}
			else
			{
				Mix_PlayMusic(loadedMUS[IDToIndex(soundID)], times);
			}
		}
		else //is SFX
		{
			if(isValidID(soundID))
			{
				if(fadeIn)
				{
					loadedSFX[IDToIndex(soundID)].channel = Mix_FadeInChannel(-1, loadedSFX[IDToIndex(soundID)].sfx_ptr, times, SDL_SOUND_FADE);
				}
				else
				{
					loadedSFX[IDToIndex(soundID)].channel = Mix_PlayChannel(-1, loadedSFX[IDToIndex(soundID)].sfx_ptr, times);
				}
			}
		} //end else isMusic
	}//end if isValid
	else
	{
		std::cout << "[ERROR]: Non-existent SoundID" << std::endl;
		success = false;
	}

	return success;

	return true;
}*/

Component* SDL_Sound::NewComponent() const
{
	Component* l_pointer = new SDL_Sound;
	return l_pointer;
}

void SDL_Sound::Reflection()
{
	m_Variables.push_back("Sound file:");

	m_Setters.push_back(boost::bind(&SDL_Sound::SetFile, this, _1));
	m_Getters.push_back(boost::bind(&SDL_Sound::GetFile, this));

	m_Variables.push_back("Is Music Track:");
	m_Setters.push_back(boost::bind(&SDL_Sound::SetIsMusic, this, _1));
	m_Getters.push_back(boost::bind(&SDL_Sound::GetIsMusic, this));

	m_Variables.push_back("Repeat:");
	m_Setters.push_back(boost::bind(&SDL_Sound::SetRepeat, this, _1));
	m_Getters.push_back(boost::bind(&SDL_Sound::GetRepeat, this));

	m_Variables.push_back("Loop Number: ");
	m_Setters.push_back(boost::bind(&SDL_Sound::SetLoopNum, this, _1));
	m_Getters.push_back(boost::bind(&SDL_Sound::GetLoopNum, this));

	m_Variables.push_back("Fade in time:");
	m_Setters.push_back(boost::bind(&SDL_Sound::SetFadeInTime, this, _1));
	m_Getters.push_back(boost::bind(&SDL_Sound::GetFadeInTime, this));

	m_Variables.push_back("Fade out time:");
	m_Setters.push_back(boost::bind(&SDL_Sound::SetFadeOutTime, this, _1));
	m_Getters.push_back(boost::bind(&SDL_Sound::GetFadeOutTime, this));
	
	m_Variables.push_back("Is Playing:");
	m_Setters.push_back(boost::bind(&SDL_Sound::SetPlaying, this, _1));
	m_Getters.push_back(boost::bind(&SDL_Sound::GetPlaying, this));

}

void SDL_Sound::updateIsPlaying(bool isPlaying)
{
	m_isPlaying = isPlaying;
	if(m_repeat){ m_loopNumber = -1;}
	if(isPlaying)
	{
		m_soundID = SDL_Sound_SFXInfoManager::loadSound(m_filename, m_soundType);
		SDL_Sound_SFXInfoManager::playSound(m_soundID, m_loopNumber, m_fadeInTime);
	}
	else
	{
		SDL_Sound_SFXInfoManager::stopSound(m_soundID, m_fadeOutTime);
	}

}

