#include "LuaScript.h"
#define FUNCTIONCALL_ERROR_HANDLED -1
#define FUNCTIONCALL_ERROR_UNHANDLED -2
#define FUNCTIONCALL_NOFUNCTION -3

bool LuaScript::s_inList = false;

LuaScript::LuaScript() {
	Reflection();
	m_multiple = true;
	m_name = "Lua Script";
	m_fileName = "Lua/";
	if (!s_inList) {
		s_inList = true;
		LuaScript* l_pointer = new LuaScript;
		m_derivedClasses.push_back(l_pointer);
	}
	updatingScript = true;
}
Component*  LuaScript::NewComponent() const {
	Component* l_pointer = new LuaScript;
	return l_pointer;
}

bool LuaScript::SetFileName(boost::any var) {
	try {
		m_fileName = boost::any_cast<string> (var);
		Clear();
		Start();
		updatingScript = true; //prevents disabling of updating while typing in script name
	}
	catch (boost::bad_any_cast& /*bac*/) {
		return false;
	}
	return true;
}

std::string LuaScript::GetFileName() {
	return m_fileName;
}

void LuaScript::Start() {
	m_state = luaL_newstate();
	if (!m_state) 
	{
		std::cout << "[ERROR]: " << lua_tostring(m_state, -1) << endl;
	}
	luaL_openlibs(m_state);

	lua_register(m_state, "cpp_Multiply", cpp_Multiply);

	if (m_fileName.compare("") != 0) {
		if (luaL_dofile(m_state, m_fileName.c_str()))			cout << "[ERROR]: " << lua_tostring(m_state, -1) << endl;
	}

	runBasicFunction(m_state, "start");
}

int LuaScript::Multiply(int p_1, int p_2) {
	return p_1*p_2;
}

int LuaScript::cpp_Multiply(lua_State* p_state) {
	int l_paramAmount = lua_gettop(p_state);
	if (l_paramAmount < 2 || !lua_isnumber(p_state, -1) || !lua_isnumber(p_state, -2)) {
		cout << "[ERROR] invalid parameters" << endl;
		return 0;
	}
	int result = Multiply((int)lua_tonumber(p_state, -1), (int)lua_tonumber(p_state, -2));
	lua_pushnumber(p_state, result);
	return 1;
}

void LuaScript::Update(){ //confirmed - runs every frame
	if(updatingScript)
	{
		lua_settop(m_state, 0);
		int functionCall = runBasicFunction(m_state, "update");

		//if unhandled error or function doesn't exist, ceases to call update
		if(functionCall == FUNCTIONCALL_ERROR_UNHANDLED || 
			functionCall == FUNCTIONCALL_NOFUNCTION)
		{
			updatingScript = false;
			cout << "Functioncall Error handler fired" << endl;
		}
	}
}

void LuaScript::Clear() {
	runBasicFunction(m_state, "clear");
	lua_close(m_state);
}

void LuaScript::Reflection() {
	m_Variables.push_back("Script file");
	m_Setters.push_back(boost::bind(&LuaScript::SetFileName, this, _1));
	m_Getters.push_back(boost::bind(&LuaScript::GetFileName, this));
}

int LuaScript::runBasicFunction(lua_State * p_state, std::string functionName){
	lua_getglobal(m_state, functionName.c_str());
	int success = 0;
	if(lua_isfunction(p_state, -1))
	{
		int error = lua_pcall(m_state, 0, 0, 0);
		if(error != 0)
		{
			cout << "[ERROR]: " << m_fileName << " Function call:";
			switch(error)
			{
			case LUA_ERRRUN:
				cout << " Runtime error." << endl;
				success = FUNCTIONCALL_ERROR_HANDLED;
				break;
			case LUA_ERRMEM:
				cout << " Memory allocation error." << endl;
				success = FUNCTIONCALL_ERROR_UNHANDLED;
				break;
			case LUA_ERRERR:
				cout << " Runtime error + error handler error." << endl;
				success = FUNCTIONCALL_ERROR_UNHANDLED;
				break;
			}
		}
	}
	else 
	{
		cout << "[SCRIPT] " << m_fileName << " no update function found" << endl;
		success = FUNCTIONCALL_NOFUNCTION;
	}

	return success;
}