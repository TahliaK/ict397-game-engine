#include "WindowClass.h"
#include <map>

using namespace std;

static map<int, PROCPTR> procs;

WindowClass::WindowClass(HINSTANCE hInst, LPCSTR caption)
{
	/*Caption = (LPSTR)malloc(strlen(caption) + 1);
	strcpy(Caption, caption);
	WindowClass::hInst = hInst;
	WindowClass::nextID = 101;

	wc.lpszClassName = Caption;
	wc.lpfnWndProc = WndProc;
	wc.style = CS_OWNDC | CS_VREDRAW | CS_HREDRAW;
	wc.hInstance = hInst;
	wc.hIcon = LoadIcon(NULL, IDI_APPLICATION);
	wc.hCursor = LoadCursor(NULL, IDC_ARROW);
	wc.hbrBackground = (HBRUSH) (COLOR_BACKGROUND);
	wc.lpszMenuName = NULL;
	wc.cbClsExtra = 0;
	wc.cbWndExtra = 0;

	if(!RegisterClass(&wc)) throw WindowException();
	hWnd = CreateWindow(Caption, Caption, WS_OVERLAPPEDWINDOW, CW_USEDEFAULT, CW_USEDEFAULT, 500, 350, NULL, NULL, hInst, NULL);

	if(hWnd == NULL) throw WindowException();*/
}

LRESULT CALLBACK WindowClass::WndProc(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
	if(procs.find(msg) != procs.end())
	{
		procs[msg](wParam, lParam);
		return 0;
	}
	return DefWindowProc(hWnd, msg, wParam, lParam);
}

void WindowClass::Show(int nCmdShow)
{
	ShowWindow(hWnd, nCmdShow);
}

void WindowClass::Update(void)
{
	UpdateWindow(hWnd);
}

WPARAM WindowClass::MessageLoop(void)
{
	MSG msg;
	while(GetMessage(&msg, NULL, 0, 0))
	{
		TranslateMessage(&msg);
		DispatchMessage(&msg);
	}
	return msg.wParam;
}

HWND WindowClass::getHandle(void)
{
	return hWnd;
}

HINSTANCE WindowClass::getInstance(void)
{
	return hInst;
}

int WindowClass::registerProcedure(UINT msg, PROCPTR procedure)
{
	procs[msg] = procedure;
	return 0;
}

int WindowClass::getFreeID()
{
	return nextID++;
}

long WindowClass::getWidth(void)
{
	RECT rect;
	GetClientRect(hWnd, &rect);
	return rect.right;
}

long WindowClass::getHeight(void)
{
	RECT rect;
	GetClientRect(hWnd, &rect);
	return rect.bottom;
}