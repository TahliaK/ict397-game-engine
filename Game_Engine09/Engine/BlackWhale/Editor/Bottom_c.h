#ifndef Bottom_c_H
#define Bottom_c_H
#include "Controller.h"
#include "Controls.h"

namespace Editor {

	/*
	* @class Bottom_c
	* @brief Used as a controller for a window with two tabs and allows to
	* switch between the tabs.
	* @author Olesia Kochergina
	* @date 03/03/2017
	*/
	class Bottom_c : public Controller {
	public:

		/*
		* @brief Default c-tor.
		*/
		Bottom_c() {};

		/*
		* @brief Destructor.
		*/
		~Bottom_c() {};

		/*
		* @brief Initializes internal controls.
		* @param info - contains information about the window being created.
		* @return Returns 0 to indicate that the message is processed, if -1 then the window is destroyed.
		*/
		int Create(CREATESTRUCT* info);

		/*
		* @brief Destroys the window and its controls.
		* @return Returns 0 to indicate that the message is processed.
		*/
		int Destroy(void);

		/*
		* @brief Switches between the tabs.
		* @param id - ID of the control sending the message.
		* @param lParam - pointer to NMHDR which contains the notification code and other information.
		* @return Can be ignored.
		*/
		int Notify(int id, LPARAM lParam);

		/*
		* @brief Sets the first window to the Console tab and the second one to the Project tab.
		* @param handles - vector of window handles.
		*/
		void SetDialogHandles(std::vector<HWND> handles);
	private:

		/// Tab controller
		TabControl bottom;

		///handles to console and project windows.
		vector<HWND> m_handles;
	};

}
#endif Bottom_c_H