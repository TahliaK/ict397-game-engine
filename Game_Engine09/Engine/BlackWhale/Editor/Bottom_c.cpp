#include "Bottom_c.h"

#include <exception>
#include <cstdlib>
#include <string>
#include <sstream>

#include "../../resource.h"

using namespace Editor;

void Bottom_c::SetDialogHandles(std::vector<HWND> p_handles) {
	m_handles = p_handles;
	bottom.AddTab(m_handles.at(0), "Console", 0, IDD_FORM_CONSOLE);
	bottom.AddTab(m_handles.at(1), "Project", 1, IDD_FORM_PROJECT);
	bottom.ShowTab(0, 1);
}


int Bottom_c::Create(CREATESTRUCT* /*info*/) {
	bottom = TabControl(handle, IDC_TAB_BO_1, true);
	return 0;
}

int Bottom_c::Destroy() { 
	m_handles.clear(); 
	::DestroyWindow(handle);
	return 0; 
}

int Bottom_c::Notify(int id, LPARAM lParam) {
	NMHDR* nmhdr = (NMHDR*)lParam;
	if(nmhdr->code == TCN_SELCHANGE)
	{
		IFORSI(0, bottom.GetSize()) {
			bottom.ShowTab(i, 0);
		}
		bottom.ShowTab(bottom.GetSelected(), 1);
	}
	return 0;
}


