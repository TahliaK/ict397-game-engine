#include <exception>
#include <cstdlib>
#include <string>
#include <sstream>

#include "Compmenu_c.h"
#include "../../resource.h"
#include "../Component.h"

using namespace Editor;

Compmenu_c::Compmenu_c() {}

int Compmenu_c::Destroy() {
	m_editbox.Destroy();
	return 0;
}

int Compmenu_c::Create(CREATESTRUCT* info) {
	m_itemIndex = LB_ERR;
	m_listbox = ListBox(handle, IDC_LIST_COMPMENU);
	AddComponents("", false);
	m_editbox = EditBox(handle, IDC_EDIT_COMPMENU);
	return 0;
}

int Compmenu_c::Command(WPARAM wParam, LPARAM msg, HWND hwnd) {
	if (handle != GetForegroundWindow()) {
		::ShowWindow(handle, false);
	}
	switch (LOWORD(wParam)) {

	case IDC_EDIT_COMPMENU:
	{
		char input[100];
		GetDlgItemText((HWND)hwnd, LOWORD(wParam), input, sizeof(input));
		string in(input);
		AddComponents(in, true);
	}
	break;
	case IDC_LIST_COMPMENU:
		if (HIWORD(wParam) == LBN_SELCHANGE) {
		}
		else if (HIWORD(wParam) == LBN_DBLCLK) {
			int l_selIndex = m_listbox.GetSelectedIndex();
			if (l_selIndex != LB_ERR) {
				m_itemIndex = GetComponentIndex(m_listbox.GetSelectedString());
				//SendMessage(m_setHandle,WM_COMMAND,MAKEWPARAM(IDC_BUTTON_ADD,BN_CLICKED),LPARAM(GetDlgItem(m_setHandle,IDC_BUTTON_ADD)));
				SendMessage(m_setHandle, WM_COMMAND, MAKEWPARAM(IDC_BUTTON_ADD, BN_CLICKED), 0);
				::ShowWindow(handle, false);//hide window
				SetDlgItemText((HWND)hwnd, IDC_EDIT_COMPMENU, "");//reset the search editbox
			}
		}
		else if (HIWORD(wParam) == EN_SETFOCUS) {
		}
		else if (HIWORD(wParam) == EN_KILLFOCUS) {
			char input[100];
			GetDlgItemText((HWND)hwnd, LOWORD(wParam), input, sizeof(input));
			string in(input);
			float l_number = 0;
			bool l_newValue = false;
			try {
				l_number = std::stof(in);
				char l_char[30];
				sprintf_s(l_char, "%f", l_number);
				l_newValue = true;
			}
			catch (exception e) {
				cout << "[ERROR] Compmenu_c.cpp: " << e.what() << endl;
			}
			char l_char[30];
			sprintf_s(l_char, "%f", l_number);
			SetDlgItemText(hwnd, LOWORD(wParam), (LPCSTR)l_char);
		}
		break;
	}
	return 0;
}

void Compmenu_c::AddComponents(std::string p_name, bool p_compare) {
	m_listbox.ResetContent();
	IFORSI(0, Component::GetDerivedClasses().size()) {
		string tempInfo(typeid(*Component::GetDerivedClasses().at(i)).name());
		tempInfo = tempInfo.substr(tempInfo.find(' ') + 1);
		if (!p_compare || (tempInfo.substr(0, p_name.size()).compare(p_name) == 0)) {
			m_listbox.AddString(tempInfo);
		}
	}
}

int Compmenu_c::GetComponentIndex(std::string p_name) {
	IFORSI(0, Component::GetDerivedClasses().size()) {
		string tempInfo(typeid(*Component::GetDerivedClasses().at(i)).name());
		tempInfo = tempInfo.substr(tempInfo.find(' ') + 1);
		if (tempInfo.substr(0, p_name.size()).compare(p_name) == 0) {
			return i;
		}
	}
	return LB_ERR;
}

int Compmenu_c::GetSelectedComponent(void) const {
	return m_itemIndex;
}

void Compmenu_c::SetSelectedComponent(int p_index) {
	m_itemIndex = p_index;
}

void Compmenu_c::SetSettingsHandle(HWND handle) {
	m_setHandle = handle;
}