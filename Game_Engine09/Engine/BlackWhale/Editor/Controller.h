#ifndef WIN_CONTROLLER_H
#define WIN_CONTROLLER_H

#include <windows.h>
#include <iostream>
#include <vector>
#include "../Scene.h"
#include "Controls.h"

using namespace std;

namespace Editor {

	///Forward declaration
	class ControllerGL;
	///Forward declaration
	class DialogWindow;

	/*
	* @brief Base class for all major windows to identify
	* unique behaviour for the windows.
	* @author Olesia Kochergina
	* @date 20/02/2017
	*/
	class Controller
	{
	public:

		/*
		* @brief Default c-tor.
		*/
		Controller();

		/*
		* @brief Destructor.
		*/
		virtual ~Controller();

		/*
		* @brief Sets the window handle.
		* @param handle - handle of the window.
		*/
		void SetHandle(HWND handle);

		/*
		* @brief Returns the window's handle.
		* @return the window;s handle.
		*/
		HWND GetHandle(void) const;

		/*
		* @brief Called when the window is closed by WM_CLOSE message received from the message loop.
		* @return Returns 0 to indicate that the message is processed.
		*/
		virtual int Close(void);

		/*
		* @brief Called by WM_COMMAND message received from the message loop.
		* @param wParam - ID of the control
		* @param msg -
		* @param hwnd - handle to the window
		* @return Returns 0 to indicate that the message is processed.
		*/
		virtual int Command(WPARAM wParam, LPARAM msg, HWND hwnd);

		/*
		* @brief Called by WM_CONTEXTMENU message received from the message loop.
		* @param handle - handle to the window in which the user right-clicked the mouse.
		* @param x - horizontal position of the cursor.
		* @param y - vertical position of the cursor.
		*/
		virtual void ContextMenu(HWND handle, int x, int y);

		/*
		* @brief Called by WM_CREATE message received from the message loop.
		* @param info - contains information about the window being created.
		* @return Returns 0 to indicate that the message is processed, if -1 then the window is destroyed.
		*/
		virtual int Create(CREATESTRUCT* info);

		/*
		* @brief Called by WM_DESTROY message received from the message loop.
		* @return Returns 0 to indicate that the message is processed.
		*/
		virtual int Destroy(void);

		/*
		* @brief Called by WM_ENABLE message received from the message loop.
		* @param flag - true means that the window is enabled, else disabled.
		* @return Returns 0 to indicate that the message is processed.
		*/
		virtual int Enable(bool flag);

		/*
		* @brief Called by WM_ERASEBKGND message received from the message loop.
		* @param hdc - a handle to the device context.
		* @return Returns 0 if the background is not erased, else non-zero.
		*/
		virtual int EraseBkgnd(HDC hdc);

		/*
		* @brief Called by WM_HSCROLL message received from the message loop.
		* @param wParam - position of the scroll box and scroll bar value.
		* @param lParam - handle to the scroll bar.
		* @return Returns 0 to indicate that the message is processed.
		*/
		virtual int HorizontalScroll(WPARAM wParam, LPARAM lParam);

		/*
		* @brief Called by WM_VSCROLL message received from the message loop.
		* @param wParam - position of the scroll box and scroll bar value.
		* @param lParam - handle to the scroll bar.
		* @return Returns 0 to indicate that the message is processed.
		*/
		virtual int VerticalScroll(WPARAM wParam, LPARAM lParam);

		/*
		* @brief Called by WM_MOUSEHOVER message received from the message loop.
		* @param state - IDs of virtual keys that are down.
		* @param x - x-axis position of the cursor, relative to upper-left corner of the client area.
		* @param y - y-axis position of the cursor, relative to upper-left corner of the client area.
		* @return Returns 0 to indicate that the message is processed.
		*/
		virtual int MouseHover(int state, int x, int y);

		/*
		* @brief Called by WM_MOUSELEAVE message received from the message loop.
		* @return Returns 0 to indicate that the message is processed.
		*/
		virtual int MouseLeave(void);

		/*
		* @brief Called by WM_MOUSEWHEEL message received from the message loop.
		* @param state - virtual keys that are down.
		* @param delta - distance of the wheel is rotated.
		* @param x - x-axis position of the pointer, relative to the upper-left corner of the screen.
		* @param y - y-axis position of the pointer, relative to the upper-left corner of the screen.
		* @return Returns 0 to indicate that the message is processed.
		*/
		virtual int MouseWheel(int state, int delta, int x, int y);

		/*
		* @brief Called by WM_NOTIFY message received from the message loop.
		* @param id - ID of the control sending the message.
		* @param lParam - pointer to NMHDR which contains the notification code and other information.
		* @return Can be ignored.
		*/
		virtual int Notify(int id, LPARAM lParam);

		/*
		* @brief Called by WM_PAINT message received from the message loop.
		* @return Returns 0 to indicate that the message is processed.
		*/
		virtual int Paint(void);

		/*
		* @brief Called by WM_RBUTTONDOWN message received from the message loop.
		* @param state - indicates whether virtual keys are down or not.
		* @param x - x-axis position of the cursor, relative to upper-left corner of the client area.
		* @param y - y-axis position of the cursor, relative to upper-left corner of the client area.
		* @return Returns 0 to indicate that the message is processed.
		*/
		virtual int RightButtonDown(WPARAM state, int x, int y);

		/*
		* @brief Called by WM_RBUTTONUP message received from the message loop.
		* @param state - indicates whether virtual keys are down or not.
		* @param x - x-axis position of the cursor, relative to upper-left corner of the client area.
		* @param y - y-axis position of the cursor, relative to upper-left corner of the client area.
		* @return Returns 0 to indicate that the message is processed.
		*/
		virtual int RightButtonUp(WPARAM state, int x, int y);

		/*
		* @brief Called by WM_SIZE message received from the message loop.
		* @param width - new width of the client area.
		* @param height - new height of the client area.
		* @param wParam - type of resizing
		* @return Returns 0 to indicate that the message is processed.
		*/
		virtual int Resize(int width, int height, WPARAM wParam);

		/*
		* @brief Called by WM_TIMER message received from the message loop.
		* @param id - timer ID
		* @param lParam - pointer to callback function
		* @return Returns 0 to indicate that the message is processed.
		*/
		virtual int Timer(WPARAM id, LPARAM lParam);

		/*
		* @brief Called by WM_CTLCOLORSTATIC, WM_CTLCOLORDLG messages received from the message loop.
		* @param hdc - handle to the device context.
		* @return Returns handle to a brush.
		*/
		virtual int SetColor(HDC hdc);

		/*
		* @brief Sets the program's 3D Graphics Controller.
		* @param glCtrl - reference to the graphics controller.
		*/
		static void SetGraphicsController(ControllerGL& glCtrl);

		/*
		* @brief Sets the program's scene chooser dialog.
		* param sceneDialog - reference to the scene dialog.
		*/
		static void SetSceneDlg(DialogWindow& sceneDialog);

		/*
		* @brief Cleans up static variables.
		*/
		static void Clear();

	protected:

		///Controller of the graphics window
		static ControllerGL m_glCtrl;

		///Window handle to map window to controller
		HWND handle;

	private:

		///scene chooser dialog window
		static DialogWindow s_sceneDlg;

		///static brush to change colors of all windows.
		static HBRUSH s_brush;
	};
}
#endif  CONTROLLER_H
