#include <exception>
#include <cstdlib>
#include <string>
#include <sstream>

#include "Settings_c.h"
#include "../../resource.h"
#include "Hierarchy_c.h"
#include "../SceneManager.h"

using namespace Editor;
unsigned int Separator::s_ids = 10480;
unsigned int TextBox::s_ids = 9480;
unsigned int EditBox::s_ids = 8480;
unsigned int CheckBox::s_ids = 7480;
GameObject* Settings_c::m_selected = NULL;

Settings_c::Settings_c() {

}


int Settings_c::Destroy() {	
	ClearWindow();
	::DestroyWindow(handle);
	return 0;
}


int Settings_c::Create(CREATESTRUCT* info) {
	m_selected = NULL;
	Update();

	return 0;
}

int Settings_c::Command(WPARAM wParam, LPARAM msg, HWND hwnd) {
	switch (LOWORD(wParam)) {
	case IDC_EDIT_IN_NAME:
	{
		char input[100];
		GetDlgItemText((HWND)hwnd, LOWORD(wParam), input, sizeof(input));
		string in(input);
		if (m_selected) {
			m_selected->SetName(in);
			//Hierarchy_c::Update();
		}
	}
	break;
	case IDC_CHECK_IN_STATIC:
		if (m_selected)
			m_selected->SetStatic(IsDlgButtonChecked(handle, IDC_CHECK_IN_STATIC));
		break;
	case IDC_CHECK_IN_VISIBLE:
		if (m_selected)
			m_selected->SetVisible(IsDlgButtonChecked(handle, IDC_CHECK_IN_VISIBLE));
		break;
	case IDC_EDIT_IN_PX:
	case IDC_EDIT_IN_PY:
	case IDC_EDIT_IN_PZ:
	case IDC_EDIT_IN_RX:
	case IDC_EDIT_IN_RY:
	case IDC_EDIT_IN_RZ:
	case IDC_EDIT_IN_SX:
	case IDC_EDIT_IN_SY:
	case IDC_EDIT_IN_SZ:
		if (HIWORD(wParam) == EN_KILLFOCUS) {
			char input[100];
			GetDlgItemText((HWND)hwnd, LOWORD(wParam), input, sizeof(input));
			string in(input);
			float l_number = 0;
			bool l_newValue = false;
			try {
				l_number = std::stof(in);
				char l_char[30];
				sprintf_s(l_char, "%f", l_number);
				l_newValue = true;
			}
			catch (exception e) {
				cout << "[ERROR] Settings_c.cpp: " << e.what() << endl;
			}
			char l_char[30];
			sprintf_s(l_char, "%f", l_number);
			SetDlgItemText(hwnd, LOWORD(wParam), (LPCSTR)l_char);
			if (l_newValue && m_selected) {
				if (LOWORD(wParam) == IDC_EDIT_IN_PX) {
					m_selected->GetTransform()->position.x = l_number;
				}
				else if (LOWORD(wParam) == IDC_EDIT_IN_PY) {
					m_selected->GetTransform()->position.y = l_number;
				}
				else if (LOWORD(wParam) == IDC_EDIT_IN_PZ) {
					m_selected->GetTransform()->position.z = l_number;
				}
				else if (LOWORD(wParam) == IDC_EDIT_IN_RX) {
					m_selected->GetTransform()->rotation.x = l_number;
				}
				else if (LOWORD(wParam) == IDC_EDIT_IN_RY) {
					m_selected->GetTransform()->rotation.y = l_number;
				}
				else if (LOWORD(wParam) == IDC_EDIT_IN_RZ) {
					m_selected->GetTransform()->rotation.z = l_number;
				}
				else if (LOWORD(wParam) == IDC_EDIT_IN_SX) {
					m_selected->GetTransform()->scale.x = l_number;
				}
				else if (LOWORD(wParam) == IDC_EDIT_IN_SY) {
					m_selected->GetTransform()->scale.y = l_number;
				}
				else if (LOWORD(wParam) == IDC_EDIT_IN_SZ) {
					m_selected->GetTransform()->scale.z = l_number;
				}
			}
		}
		break;
	case IDC_BUTTON_ADD:
		if (HIWORD(wParam) == BN_CLICKED) {
			if (((Compmenu_c*)m_compmenu->GetController())->GetSelectedComponent() >= 0) {
				Component* l_temp = Component::GetDerivedClasses().at(((Compmenu_c*)m_compmenu->GetController())->GetSelectedComponent())->NewComponent();
				HWND l_addHandle = GetDlgItem(handle, IDC_BUTTON_ADD);
				if (l_temp) {
					if (m_selected)
						AddComponentEvent(l_temp, l_addHandle, m_selected->GetComponents().size());
					else {
						AddComponentEvent(l_temp, l_addHandle, -1);
					}
					((Compmenu_c*)m_compmenu->GetController())->SetSelectedComponent(LB_ERR);
					cout << SceneManagement::SceneManager::GetActive()->AddComponent(m_selected, l_temp) << endl;
					ClearWindow();
					Update();
				}
				else {
					cout << "[ERROR] NULL pointer when trying to add a new component" << endl;
				}
			}
			else {
				m_compmenu->Show(true);
				RECT l_setRect;
				GetWindowRect(handle, &l_setRect);
				RECT  l_menuRect;
				GetWindowRect(m_compmenu->GetHandle(), &l_menuRect);
				//center of this rect
				int l_cX = (l_setRect.right + l_setRect.left) / 2;
				int l_cY = (l_setRect.top + l_setRect.bottom) / 2;

				int l_sideX = (l_menuRect.right - l_menuRect.left);
				int l_sideY = (l_menuRect.bottom - l_menuRect.top);
				MoveWindow(m_compmenu->GetHandle(), l_cX - l_sideX, l_cY - l_sideY, l_sideX, l_sideY, true);
			}
		}
		break;
	default:
		if (HIWORD(wParam) == EN_KILLFOCUS) {
			IFORSI(0, m_editbox.size()) {
				if (m_editbox.at(i).GetHandle() == (HWND)msg) {
					std::string str(m_editbox.at(i).GetText());
					cout << str << endl;
					if (m_setters.find((HWND)msg) != m_setters.end()) {
						if (m_selected) {
							bool l_validInput = false;
							boost::any var_any = m_selected->GetComponents().at(m_setters[(HWND)msg].compIndex)->GetGetters().at(m_setters[(HWND)msg].varIndex)();
							std::string l_previousText;
							if (var_any.type() == typeid(string)) {
								l_previousText = boost::any_cast<string>(var_any);
								l_validInput = m_selected->GetComponents().at(m_setters[(HWND)msg].compIndex)->GetSetters().at(m_setters[(HWND)msg].varIndex)(str);
							}
							else if (var_any.type() == typeid(float)) {
								l_previousText = std::to_string(boost::any_cast<float>(var_any));
								float l_number = 0;
								try {
									l_number = std::stof(str);
									char l_char[30];
									sprintf_s(l_char, "%f", l_number);
									l_validInput = m_selected->GetComponents().at(m_setters[(HWND)msg].compIndex)->GetSetters().at(m_setters[(HWND)msg].varIndex)(l_number);

								}
								catch (exception e) {
									std::cout << "[ERROR] Settings_c.cpp: " << e.what() << endl;
								}
							}
							else if (var_any.type() == typeid(Vector3)) {
								float l_number;
								Vector3 l_var(boost::any_cast<Vector3> (var_any));
								l_previousText = std::to_string(l_var[m_setters[(HWND)msg].elementIndex]);
								try {
									l_number = std::stof(str);
									char l_char[30];
									sprintf_s(l_char, "%f", l_number);
									
									l_var.Set(m_setters[(HWND)msg].elementIndex, l_number);
									l_validInput = m_selected->GetComponents().at(m_setters[(HWND)msg].compIndex)->GetSetters().at(m_setters[(HWND)msg].varIndex)(l_var);

								}
								catch (exception e) {
									std::cout << "[ERROR] Settings_c.cpp: " << e.what() << endl;
								}
							}
							else if (var_any.type() == typeid(int)) {	
								l_previousText = std::to_string(boost::any_cast<int>(var_any));
								int l_number = 0;
								try {
									l_number = std::stoi(str);
									char l_char[30];
									sprintf_s(l_char, "%i", l_number);
									l_validInput = m_selected->GetComponents().at(m_setters[(HWND)msg].compIndex)->GetSetters().at(m_setters[(HWND)msg].varIndex)(l_number);
								}
								catch (exception e) {
									std::cout << "[ERROR] Settings_c.cpp: " << e.what() << endl;
								}
							}
							else if (var_any.type() == typeid(RGBA)) {
								RGBA l_var(boost::any_cast<RGBA> (var_any));
								l_previousText = std::to_string(l_var[m_setters[(HWND)msg].elementIndex]);
								float l_number;
								try {
									l_number = std::stof(str);
									char l_char[30];
									sprintf_s(l_char, "%f", l_number);
									l_var.Set(m_setters[(HWND)msg].elementIndex, l_number);
									l_validInput = m_selected->GetComponents().at(m_setters[(HWND)msg].compIndex)->GetSetters().at(m_setters[(HWND)msg].varIndex)(l_var);
								}
								catch (exception e) {
									std::cout << "[ERROR] Settings_c.cpp: " << e.what() << endl;
								}
							}
							if (!l_validInput) {
								m_editbox.at(i).SetText(l_previousText);
								cout << "[ERROR] Invalid Input: resetting value." << endl;
							}
							return 1;
						}
					}

					break;
				}
			}
		}
		if (HIWORD(wParam) == BN_CLICKED) {
			IFORSI(0, m_checkbox.size()) {
				if (m_checkbox.at(i).GetHandle() == (HWND)msg) {

					if (m_boolSetters.find((HWND)msg) != m_boolSetters.end()) {
						if (m_selected) {
							bool l_validInput = false;
							boost::any var_any = m_selected->GetComponents().at(m_boolSetters[(HWND)msg].compIndex)->GetGetters().at(m_boolSetters[(HWND)msg].varIndex)();
							if (var_any.type() == typeid(bool)) {
								try {
									m_checkbox.at(i).Check(!m_checkbox.at(i).isChecked());
									l_validInput = m_selected->GetComponents().at(m_boolSetters[(HWND)msg].compIndex)->GetSetters().at(m_boolSetters[(HWND)msg].varIndex)(m_checkbox.at(i).isChecked());

									cout << l_validInput << endl;
								}
								catch (exception e) {
									std::cout << "[ERROR] Settings_c.cpp: " << e.what() << endl;
								}
								if (!l_validInput) {
									//invalid input (bool)
								}
								return 1;
							}

						}
					}
				}
			}
		}
		break;
	}
	return 0;
}

void Settings_c::AddComponentEvent(Component* p_component, const HWND& p_parent, int p_compIndex) {
	RECT l_addPosition;
	POINT p;

	m_textbox.push_back(TextBox(handle, true));
	::GetWindowRect(p_parent, &l_addPosition);
	p.x = l_addPosition.left;
	p.y = l_addPosition.top;
	ScreenToClient(handle, &p);

	string l_comp_str(typeid(*p_component).name());
	string l_class_name(l_comp_str.substr(l_comp_str.find(" ") + 1));//remove "class "
	LPCSTR l_comp_name = l_class_name.c_str();
	m_textbox.push_back(TextBox(handle, true));
	m_textbox.back().Create(0, p.y, l_comp_name);

	MoveWindow(p_parent, p.x, p.y + 30, l_addPosition.right - l_addPosition.left, l_addPosition.bottom - l_addPosition.top, TRUE);

	IFORSI(0, p_component->GetGetters().size()) {

		//window rp_componentt of the ADD COMPONENT button relative to the screen
		::GetWindowRect(p_parent, &l_addPosition);
		p.x = l_addPosition.left;
		p.y = l_addPosition.top;
		//converting screen coordinates to client based points
		ScreenToClient(handle, &p);
		int l_offset = 30;
		Vector3 l_tempV;
		RGBA l_tempC;
		bool l_tempB = false;
		boost::any var_any = p_component->GetGetters().at(i)();
		string curStr(var_any.type().name());
		string g;
		int l_choice = 2;
		try {
			if (var_any.type() == typeid(bool)) {
				//g = string(std::to_string(boost::any_cast<bool>(p_component->GetGetters().at(i)())));
				g = p_component->GetVariables().at(i);
				if (g.substr(0, 2).compare("m_") == 0)
					g = g.substr(2);
				l_tempB = boost::any_cast<bool>(var_any);
				l_choice = 0;
			}
			else if (var_any.type() == typeid(int)) {
				g = string(std::to_string((int)boost::any_cast<int>(var_any)));
				l_choice = 1;
			}
			else if (var_any.type() == typeid(float)) {
				g = string(std::to_string((float)boost::any_cast<float>(var_any)));
				l_choice = 1;
			}
			else if (var_any.type() == typeid(Vector3)) {
				l_tempV = boost::any_cast<Vector3>(var_any);
				l_choice = 3;
			}
			else if (var_any.type() == typeid(RGBA)) {
				l_tempC = boost::any_cast<RGBA>(var_any);
				l_choice = 4;
			}
			else if (var_any.type() == typeid(string)) {
				g = string(boost::any_cast<string>(var_any));
				l_choice = 5;
			}
			else {
				l_choice = 2;
			}
		}
		catch (boost::bad_any_cast& /*bac*/) {
			std::cout << "[ERROR] BAD CAST in Settings_c" << std::endl;
			//std::cout << "problem:\n" << bac.what() << endl;
		}
		LPCSTR temp = g.c_str();

		if (l_choice == 0) {
			m_checkbox.push_back(CheckBox(handle, true));
			m_checkbox.back().Create(0, p.y, temp);
			m_checkbox.back().Check(l_tempB);
			//might give invalid results
			if (p_compIndex > 0) {
				m_element l_elem(p_compIndex, i, -1);
				m_boolSetters[m_checkbox.back().GetHandle()] = l_elem;
			}
		}
		else if (l_choice == 3) {//Vector3
			string l_str(p_component->GetVariables().at(i));
			if (l_str.substr(0, 2).compare("m_") == 0)
				l_str = l_str.substr(2);
			LPCSTR temp1 = l_str.c_str();
			m_textbox.push_back(TextBox(handle, true));
			m_textbox.back().Create(0, p.y, temp1);
			MoveWindow(p_parent, p.x, p.y + l_offset, l_addPosition.right - l_addPosition.left, l_addPosition.bottom - l_addPosition.top, TRUE);
			string l_type[3] = { "X","Y","Z" };
			int l_lineOffset = 0;
			JFORSI(0, 3) {
				LPCSTR l_send = l_type[j].c_str();
				m_textbox.push_back(TextBox(handle, true));
				m_textbox.back().Create(j * 10 + l_lineOffset, p.y + l_offset, l_send);
				string l_t(std::to_string(l_tempV[j]));
				l_t = l_t.substr(0, l_t.size() - 2);
				l_send = l_t.c_str();
				m_editbox.push_back(EditBox(handle, true));
				m_editbox.back().Create((j * 10) + 15 + l_lineOffset, p.y + l_offset, l_send);
				l_lineOffset += 80;
				if (p_compIndex > 0) {
					m_element l_elem(p_compIndex, i, j);
					m_setters[m_editbox.back().GetHandle()] = l_elem;
				}

			}
			l_offset += 30;
		}
		else if (l_choice == 4) {//RGBA
			string l_str(p_component->GetVariables().at(i));
			if (l_str.substr(0, 2).compare("m_") == 0)
				l_str = l_str.substr(2);
			LPCSTR temp1 = l_str.c_str();
			m_textbox.push_back(TextBox(handle, true));
			m_textbox.back().Create(0, p.y, temp1);
			MoveWindow(p_parent, p.x, p.y + l_offset, l_addPosition.right - l_addPosition.left, l_addPosition.bottom - l_addPosition.top, TRUE);
			string l_type[4] = { "R","G","B","A" };
			int l_lineOffset = 0;
			JFORSI(0, 4) {
				LPCSTR l_send = l_type[j].c_str();
				m_textbox.push_back(TextBox(handle, true));

				m_textbox.back().Create(j * 10 + l_lineOffset, p.y + l_offset, l_send);
				string l_t(std::to_string(l_tempC[j]));
				l_t = l_t.substr(0, l_t.size() - 4);
				l_send = l_t.c_str();
				m_editbox.push_back(EditBox(handle, true));
				m_editbox.back().Create((j * 10) + 15 + l_lineOffset, p.y + l_offset, l_send);
				l_lineOffset += 60;
				if (p_compIndex > 0) {
					m_element l_elem(p_compIndex, i, j);
					m_setters[m_editbox.back().GetHandle()] = l_elem;
				}
			}
			l_offset += 30;
		}
		else {
			string l_str(p_component->GetVariables().at(i));
			if (l_str.substr(0, 2).compare("m_") == 0)
				l_str = l_str.substr(2);
			LPCSTR temp1 = l_str.c_str();
			m_textbox.push_back(TextBox(handle, true));
			m_textbox.back().Create(0, p.y, temp1);

			m_editbox.push_back(EditBox(handle, true));
			m_editbox.back().Create(100, p.y, temp);
			if (p_compIndex > 0) {
				m_element l_elem(p_compIndex, i, -1);
				m_setters[m_editbox.back().GetHandle()] = l_elem;
			}
		}
		MoveWindow(p_parent, p.x, p.y + l_offset, l_addPosition.right - l_addPosition.left, l_addPosition.bottom - l_addPosition.top, TRUE);

	}
	//add a component separator
	m_separator.push_back(Separator(handle, true));
	::GetWindowRect(p_parent, &l_addPosition);
	p.x = l_addPosition.left;
	p.y = l_addPosition.top;
	ScreenToClient(handle, &p);
	m_separator.back().Create(0, p.y - 10, 275, p.y);
}

int Settings_c::VerticalScroll(WPARAM wParam, LPARAM lParam) {
	HWND l_bar = GetDlgItem(handle, IDC_SCROLLBAR_SETTINGS);
	int l_curPos = GetScrollPos(l_bar, SB_CTL);
	SCROLLINFO si;
	si.cbSize = sizeof(si);
	si.fMask = SIF_ALL;
	GetScrollInfo(l_bar, SB_VERT, &si);
	//cout << si.nPos << "  " << si.nTrackPos << "  " << si.nMin << "  " << si.nMax << "  " << endl;
	// check if the message comming from trackbar
	HWND trackbarHandle = (HWND)lParam;
	//std::cout << "VSCROLL " << HIWORD(wParam) << endl;
	switch (LOWORD(wParam)) {
	case SB_TOP:
		l_curPos = 0;
		break;

	case SB_LINEUP:
		if (l_curPos > 0)
			l_curPos--;
		break;

	case SB_THUMBPOSITION:
		l_curPos = HIWORD(wParam);
		break;

	case SB_THUMBTRACK:
		l_curPos = HIWORD(wParam);
		break;

	case SB_LINEDOWN:
		if (l_curPos < 240)
			l_curPos++;
		std::cout << "f" << endl;
		break;

	case SB_BOTTOM:
		l_curPos = 240;
		break;

	case SB_ENDSCROLL:
		break;
	}
	SetScrollPos(l_bar, SB_CTL, l_curPos, TRUE);
	return 0;
}


int Settings_c::Notify(int id, LPARAM lParam) {
	return 0;
}

void Settings_c::ClearWindow() {
	IFORSI(0, m_checkbox.size()) {
		m_checkbox.at(i).Destroy();
	}
	IFORSI(0, m_textbox.size()) {
		m_textbox.at(i).Destroy();
	}
	IFORSI(0, m_separator.size()) {
		m_separator.at(i).Destroy();
	}
	IFORSI(0, m_editbox.size()) {
		m_editbox.at(i).Destroy();
	}
	m_editbox.clear();
	m_textbox.clear();
	m_checkbox.clear();
	m_separator.clear();
	m_setters.clear();
	HWND l_addHandle = GetDlgItem(handle, IDC_BUTTON_ADD);
	RECT l_addPosition;
	::GetWindowRect(l_addHandle, &l_addPosition);
	POINT p;
	p.x = l_addPosition.left;
	p.y = l_addPosition.top;
	//converting screen coordinates to client based points
	ScreenToClient(handle, &p);
	MoveWindow(l_addHandle, p.x, 156, l_addPosition.right - l_addPosition.left, l_addPosition.bottom - l_addPosition.top, TRUE);
}

void Settings_c::Update() {
	if (m_selected) {
		CheckDlgButton(handle, IDC_CHECK_IN_STATIC, m_selected->GetStatic());
		CheckDlgButton(handle, IDC_CHECK_IN_VISIBLE, m_selected->GetVisible());
		char l_char[50];
		strcpy(l_char, m_selected->GetName().c_str());
		SetDlgItemText(handle, IDC_EDIT_IN_NAME, (LPCSTR)l_char);
		sprintf_s(l_char, "%f", m_selected->GetTransform()->position.x);
		SetDlgItemText(handle, IDC_EDIT_IN_PX, (LPCSTR)l_char);
		sprintf_s(l_char, "%f", m_selected->GetTransform()->position.y);
		SetDlgItemText(handle, IDC_EDIT_IN_PY, (LPCSTR)l_char);
		sprintf_s(l_char, "%f", m_selected->GetTransform()->position.z);
		SetDlgItemText(handle, IDC_EDIT_IN_PZ, (LPCSTR)l_char);
		sprintf_s(l_char, "%f", m_selected->GetTransform()->rotation.x);
		SetDlgItemText(handle, IDC_EDIT_IN_RX, (LPCSTR)l_char);
		sprintf_s(l_char, "%f", m_selected->GetTransform()->rotation.y);
		SetDlgItemText(handle, IDC_EDIT_IN_RY, (LPCSTR)l_char);
		sprintf_s(l_char, "%f", m_selected->GetTransform()->rotation.z);
		SetDlgItemText(handle, IDC_EDIT_IN_RZ, (LPCSTR)l_char);
		sprintf_s(l_char, "%f", m_selected->GetTransform()->scale.x);
		SetDlgItemText(handle, IDC_EDIT_IN_SX, (LPCSTR)l_char);
		sprintf_s(l_char, "%f", m_selected->GetTransform()->scale.y);
		SetDlgItemText(handle, IDC_EDIT_IN_SY, (LPCSTR)l_char);
		sprintf_s(l_char, "%f", m_selected->GetTransform()->scale.z);
		SetDlgItemText(handle, IDC_EDIT_IN_SZ, (LPCSTR)l_char);

		//excluding the transform
		IFORSI(1, m_selected->GetComponents().size()) {
			HWND l_addHandle = GetDlgItem(handle, IDC_BUTTON_ADD);
			AddComponentEvent(m_selected->GetComponents().at(i), l_addHandle, i);
		}
	}
	else {
		CheckDlgButton(handle, IDC_CHECK_IN_STATIC, false);
		CheckDlgButton(handle, IDC_CHECK_IN_VISIBLE, false);
		char l_char = '\0';
		SetDlgItemText(handle, IDC_EDIT_IN_NAME, (LPCSTR)l_char);
		SetDlgItemText(handle, IDC_EDIT_IN_PX, (LPCSTR)l_char);
		SetDlgItemText(handle, IDC_EDIT_IN_PY, (LPCSTR)l_char);
		SetDlgItemText(handle, IDC_EDIT_IN_PZ, (LPCSTR)l_char);
		SetDlgItemText(handle, IDC_EDIT_IN_RX, (LPCSTR)l_char);
		SetDlgItemText(handle, IDC_EDIT_IN_RY, (LPCSTR)l_char);
		SetDlgItemText(handle, IDC_EDIT_IN_RZ, (LPCSTR)l_char);
		SetDlgItemText(handle, IDC_EDIT_IN_SX, (LPCSTR)l_char);
		SetDlgItemText(handle, IDC_EDIT_IN_SY, (LPCSTR)l_char);
		SetDlgItemText(handle, IDC_EDIT_IN_SZ, (LPCSTR)l_char);
	}
	//should change it to just hiding the dialog window
	/*ShowWindow(GetDlgItem(handle,IDC_CHECK_IN_STATIC),l_display);
	ShowWindow(GetDlgItem(handle,IDC_CHECK_IN_VISIBLE),l_display);
	ShowWindow(GetDlgItem(handle, IDC_EDIT_IN_NAME),l_display);
	ShowWindow(GetDlgItem(handle, IDC_EDIT_IN_PX),l_display);
	ShowWindow(GetDlgItem(handle, IDC_EDIT_IN_PY),l_display);
	ShowWindow(GetDlgItem(handle, IDC_EDIT_IN_PZ),l_display);
	ShowWindow(GetDlgItem(handle, IDC_EDIT_IN_RX),l_display);
	ShowWindow(GetDlgItem(handle, IDC_EDIT_IN_RY),l_display);
	ShowWindow(GetDlgItem(handle, IDC_EDIT_IN_RZ),l_display);
	ShowWindow(GetDlgItem(handle, IDC_EDIT_IN_SX),l_display);
	ShowWindow(GetDlgItem(handle, IDC_EDIT_IN_SY),l_display);
	ShowWindow(GetDlgItem(handle, IDC_EDIT_IN_SZ),l_display);
	ShowWindow(GetDlgItem(handle, IDC_BUTTON_ADD),l_display);
	ShowWindow(GetDlgItem(handle, IDC_COMBO_IN_LAYER),l_display);*/
}

void Settings_c::SetCompMenu(DialogWindow* compmenu) {
	m_compmenu = compmenu;
}