#include <exception>
#include <cstdlib>
#include <string>
#include <sstream>
#include <Windows.h>
#include "Toolbox_c.h"
#include "../../resource.h"
#include "../Master.h"

using namespace Editor;
bool Toolbox_c::s_switchMode = false;

Toolbox_c::Toolbox_c(){}


int Toolbox_c::Destroy(){
	::DestroyWindow(handle);
	return 0;
}


int Toolbox_c::Create(CREATESTRUCT* info) {
	return 0;
}

int Toolbox_c::Command(WPARAM wParam, LPARAM msg, HWND hwnd) {
	switch (LOWORD(wParam))
	{
	case IDC_STATIC_PLAY:
		s_switchMode = true;
		//SendDlgItemMessage(handle, IDC_STATIC_PLAY, STM_SETICON,
		//	(WPARAM)(HICON)IDI_ICON_FOLDER, 0);
		break;
	case IDC_STATIC_PAUSE:
	{
		////get location of this exe
		//char BUF[200];
		//GetModuleFileName(NULL, BUF, 200);
		//std::string location(BUF);
		////remove Engine.exe
		//location = location.substr(0, location.find_last_of('\\'));

		//std::string filename("log.txt");
		//std::string file(location + "\\" + filename);
		//cout << file << endl;
		//std::string result("start notepad++.exe " + file + "||start notepad.exe " + file);
		//if (system(result.c_str()) != 0) {
		//	cout << "[ERROR] Unable to open notepad\notepad++." << endl;
		//}
	}
	break;
	case IDC_STATIC_STOP:
		break;
	case IDC_STATIC_MOVE:
		break;
	case IDC_STATIC_RENDER:
		Master::IS_FRAME_RENDER_MODE = !Master::IS_FRAME_RENDER_MODE;
		break;
	case IDC_STATIC_TRANSLATE:
		break;
	case IDC_STATIC_ROTATE:
		break;
	case IDC_STATIC_SCALE:
		break;
	case IDC_EDIT_IN_SZ:
		if (HIWORD(wParam) == EN_KILLFOCUS) {
			char input[100];
			GetDlgItemText((HWND)hwnd, LOWORD(wParam), input, sizeof(input));
			string in(input);
			float l_number = 0;
			bool l_newValue = false;
			try {
				l_number = std::stof(in);
				char l_char[30];
				sprintf_s(l_char, "%f", l_number);
				l_newValue = true;
			}
			catch (exception e) {
				cout << "[ERROR] Toolbox_c.cpp: " << e.what() << endl;
			}
			char l_char[30];
			sprintf_s(l_char, "%f", l_number);
			SetDlgItemText(hwnd, LOWORD(wParam), (LPCSTR)l_char);
		}
		break;
	}

	return 0;
}

int Toolbox_c::Notify(int id, LPARAM lParam) {
	return 0;
}


