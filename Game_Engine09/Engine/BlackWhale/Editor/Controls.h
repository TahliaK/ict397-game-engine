/*
* @brief Collection of control windows.
* All control classes: push button, check box, label, text box, tree view.
* @author Olesia Kochergina
* @date 10/02/2017
*/

#ifndef WIN_CONTROLS_H
#define WIN_CONTROLS_H

#include <windows.h>
#include <commctrl.h>                   
#include <vector>
#include <iostream>
#include "../../resource.h"
namespace Editor {
	
	enum { MAX_INDEX = 30000 };

	/*
	* @class ControlBase
	* @brief Base class for all control objects
	*/
	class ControlBase
	{
	public:

		/*
		* @brief Default c-tor.
		*/
		ControlBase() : m_handle(0), m_parent(0), m_id(0) {

		}

		/*
		* @brief Constructor.
		* @param p_parent - parent of this control
		* @param p_id - id of the control
		* @param visible - visibility of the control
		*/
		ControlBase(HWND p_parent, int p_id, bool p_visible = true) : m_handle(GetDlgItem(p_parent, p_id)) {
			Enable(p_visible);
			m_parent = p_parent;
		}

		/*
		* @brief Destructor.
		*/
		virtual ~ControlBase() {
			/*if (m_fontHandle)
				DeleteObject(m_fontHandle);
			m_fontHandle = 0;*/
			//::DestroyWindow(m_handle);
		}

		/*
		* @brief Returns m_handle to the control.
		* @return m_handle to the control.
		*/
		HWND GetHandle() const {
			return m_handle;
		}

		/*
		* @brief Returns handle to the parent of this control.
		* @return handle to the parent.
		*/
		HWND GetParentHandle() const {
			return m_parent;
		}

		/*
		* @brief Sets all control members.
		* @param p_parent - handle to the parent
		* @param p_id - id of the control
		* @param p_visible - visibility of the control (optional)
		*/
		void Set(HWND p_parent, int p_id, bool p_visible = true) {
			m_parent = p_parent;
			m_id = p_id;
			m_handle = GetDlgItem(p_parent, m_id);
			Enable(p_visible);
		}

		/*
		* @brief Changes the caption of the control.
		* @param p_text  - text to be set
		*/
		void SetText(const std::string p_text) const {
			SendMessage(m_handle, WM_SETTEXT, 0, (LPARAM)p_text.c_str());
		}

		/*
		* @brief Shows/Hides the control.
		* @param visible - visibility of the control
		*/
		void Show(const bool p_visible) const {
			ShowWindow(m_handle, p_visible);
		}

		/*
		* @brief Sets focus to the control.
		*/
		void SetFocus(void) const {
			::SetFocus(m_handle);
		}

		/*
		* @brief Enables/Disables the control.
		* @param p_enable - true to enable, else false.
		*/
		void Enable(bool p_enable = true) {
			::EnableWindow(m_handle, p_enable);
		}

		/*
		* @brief Returns the visibility of the control.
		* @return visibility of the control.
		*/
		bool isVisible(void) const {
			return (IsWindowVisible(m_handle) != 0);
		}

	protected:

		///handle to the control
		HWND  m_handle;

		///handle to the parent's control
		HWND  m_parent;

		///id of the control
		int   m_id;
	};

	/*
	* @class Button class
	* @brief Allows to create and manage push button controls.
	*/
	class Button : public ControlBase
	{
	public:

		/*
		* @brief Default c-tor.
		*/
		Button() : ControlBase() {

		}

		/*
		* @brief Constructor.
		* @param m_parent - m_parent of this control
		* @param m_id - m_id of the control
		* @param visible - visibility of the control
		*/
		Button(HWND p_parent, int p_id, bool p_visible = true) : ControlBase(p_parent, p_id, p_visible) {

		}

		/*
		* @brief Destructor.
		*/
		~Button() {

		}

		/*
		* @brief Adds a bitmap to the button.
		* @param p_bitmap - bitmap to be added.
		*/
		void SetImage(HBITMAP p_bitmap) const {
			SendMessage(m_handle, BM_SETIMAGE, (WPARAM)IMAGE_BITMAP, (LPARAM)p_bitmap);
		}

		/*
		* @brief Adds an icon to the button.
		* @param p_icon - icon to be added.
		*/
		void SetImage(HICON p_icon) const {
			SendMessage(m_handle, BM_SETIMAGE, (WPARAM)IMAGE_ICON, (LPARAM)p_icon);
		}
	};

	/*
	* @class Checkbox class
	*/
	class CheckBox : public Button {
	public:

		/*
		* @brief Default c-tor.
		*/
		CheckBox() : Button() {

		}

		/*
		* @brief Constructor.
		* @param m_parent - m_parent of this control
		* @param visible - visibility of the control
		*/
		CheckBox(HWND m_parent, bool visible = true) {
			this->m_parent = m_parent;
			m_id = s_ids++;
			Button(m_parent, m_id);
			Enable(visible);
		}

		/*
		* @brief Destructor.
		*/
		~CheckBox() {
			//Destroy();
		}

		/*
		* @brief Destroys the window.
		*/
		void Destroy() {
			DestroyWindow(m_handle);
		}

		/*
		* @brief Creates a new check box.
		* @param p_posX - x-axis position of the check box.
		* @param p_posY - y-axis position of the check box.
		* @param p_name - name of the check box.
		*/
		void Create(const int p_posX, const int p_posY, const LPCSTR p_name) {
			HINSTANCE hInstance = (HINSTANCE)GetWindowLong(m_parent, GWL_HINSTANCE);
			//LPSTR boxName = "Game Engine";
			//m_box = Window(GetModuleHandle(0),boxName,
			HDC hDc = GetDC(m_parent);
			SIZE box_size;
			std::string l_temp(p_name);
			if (!GetTextExtentPoint32A(hDc, p_name, l_temp.size() + 1, &box_size))
				cout << "[ERROR] Cannot get checkbox size: Controls.h" << endl;
			box_size.cx = box_size.cx + GetSystemMetrics(SM_CXMENUCHECK);
			m_handle = CreateWindowEx(0, "BUTTON", p_name, WS_CHILD | WS_VISIBLE | BS_CHECKBOX | WS_GROUP | BS_LEFTTEXT, p_posX, p_posY, box_size.cx, box_size.cy, m_parent, NULL, hInstance, NULL);
			ReleaseDC(m_handle, hDc);
		}

		/*
		* @brief Allows to change the check box's value.
		* @param value - value of the check box.
		*/
		void Check(bool p_value = true) {
			SendMessage(m_handle, BM_SETCHECK, (WPARAM)p_value, 0);
		}

		/*
		* @brief Checks whether the box is checked or not.
		* @return true - check box is checked, else false.
		*/
		bool isChecked() const {
			return (SendMessage(m_handle, BM_GETCHECK, 0, 0) == BST_CHECKED);
		}

	private:

		///static id counter.
		static unsigned int s_ids;

		///id of the control
		unsigned int m_id;
	};

	/*
	* @class Textbox class
	*/
	class TextBox : public ControlBase
	{
	public:

		/*
		* @brief Default c-tor.
		*/
		TextBox(void) : ControlBase() {

		}

		/*
		* @brief Constructor.
		* @param p_parent - parent of this control
		* @param p_id - id of the control
		* @param p_visible - visibility of the control
		*/
		TextBox(HWND p_parent, int p_id, bool p_visible = true) : ControlBase(p_parent, p_id, p_visible) {

		}

		/*
		* @brief Constructor.
		* @param p_parent - parent of this control
		* @param p_visible - visibility of the control
		*/
		TextBox(HWND p_parent, bool p_visible = true) {
			m_parent = p_parent;
			m_id = s_ids++;
			Enable(p_visible);
			ControlBase(m_parent, m_id);
		}

		/*
		* @brief Destructor
		*/
		~TextBox() {
			//Destroy();
		}

		/*
		* @brief Destroys the window.
		*/
		void Destroy() {
			DestroyWindow(m_handle);
		}

		/*
		* @brief Creates a new text box.
		* @param p_posX - x-axis position of the text box.
		* @param p_posY - y-axis position of the text box.
		* @param p_name - name of the check box.
		*/
		void Create(const int p_posX, const int p_posY, const LPCSTR p_name) {
			HINSTANCE hInstance = (HINSTANCE)GetWindowLong(m_parent, GWL_HINSTANCE);
			HDC hDc = GetDC(m_parent);
			SIZE box_size;
			std::string l_temp(p_name);
			if (!GetTextExtentPoint32A(hDc, p_name, l_temp.size() + 1, &box_size))
				cout << "[ERROR] Cannot get button size: Controls.h" << endl;
			box_size.cx = box_size.cx + GetSystemMetrics(SM_CXMENUCHECK);
			m_handle = CreateWindowEx(0, "STATIC", p_name, WS_CHILD | WS_VISIBLE, p_posX, p_posY, box_size.cx, box_size.cy, m_parent, NULL, hInstance, NULL);
			ReleaseDC(m_handle, hDc);
		}

		/*
		* @brief Changes the text within the text box.
		* @param p_text - text to be set
		*/
		void SetText(std::string p_text) {
			SendMessage(m_handle, WM_SETTEXT, 0, (LPARAM)p_text.c_str());
		}

		/*
		* @brief Returns the text within the text box.
		* @return text within the text box
		*/
		std::string GetText() {
			char l_buffer[100];
			SendMessage(m_handle, WM_GETTEXT, (WPARAM)100, (LPARAM)(LPCSTR)l_buffer);
			return std::string(l_buffer);
		}

		/*
		* @brief Returns the size of the text.
		* @return amount of chars within the text box.
		*/
		int  GetTextLength() const {
			return (int)SendMessage(m_handle, WM_GETTEXTLENGTH, 0, 0);
		}

	private:

		///static id counter
		static unsigned int s_ids;

		///id of this text box
		unsigned int m_id;
	};

	/*
	* @brief Separator class
	*/
	class Separator : public ControlBase
	{
	public:

		/*
		* @brief Default c-tor.
		*/
		Separator(void) : ControlBase() {

		}

		/*
		* @brief Constructor.
		* @param p_parent - parent of this control
		* @param p_id - id of the control
		* @param p_visible - visibility of the control
		*/
		Separator(HWND p_parent, int p_id, bool p_visible = true) : ControlBase(p_parent, p_id, p_visible) {

		}

		/*
		* @brief Constructor.
		* @param p_parent - parent of this control
		* @param p_visible - visibility of the control
		*/
		Separator(HWND p_parent, bool p_visible = true) {
			m_parent = p_parent;
			m_id = s_ids++;
			ControlBase(m_parent, m_id);
		}

		/*
		* @brief Destructor.
		*/
		~Separator() {
			//Destroy();
		}

		/*
		* @brief Destroys the window.
		*/
		void Destroy() {
			DestroyWindow(m_handle);
		}

		/*
		* @brief Creates a new control box.
		* @param p_posX - x-axis position of the control.
		* @param p_posY - y-axis position of the control.
		* @param p_width - width of the separator.
		* @param p_height - height of the separator.
		*/
		void Create(const int p_posX, const int p_posY, const int p_width, const int p_height) {
			HINSTANCE hInstance = (HINSTANCE)GetWindowLong(m_parent, GWL_HINSTANCE);
			m_handle = CreateWindowEx(0, "STATIC", "", WS_CHILD | WS_VISIBLE | SS_ETCHEDHORZ, p_posX, p_posY, p_width, p_height, m_parent, NULL, hInstance, NULL);
		}

	private:

		///static id counter
		static unsigned int s_ids;

		///id of this control
		unsigned int m_id;
	};

	/*
	* @class EditBox class
	*/
	class EditBox : public TextBox
	{
	public:

		/*
		* @brief Default c-tor.
		*/
		EditBox() : TextBox(), m_maxLength(0) {

		}

		/*
		* @brief Constructor.
		* @param p_parent - parent of this control
		* @param p_visible - visibility of the control
		*/
		EditBox(HWND p_parent, bool p_visible = true) {
			m_parent = p_parent;
			m_id = s_ids++;
			TextBox(m_parent, m_id, p_visible);
			m_maxLength = (int)SendMessage(m_handle, EM_GETLIMITTEXT, 0, 0);
		}

		/*
		* @brief Constructor.
		* @param p_parent - parent of this control
		* @param p_id - id of the control
		* @param p_visible - visibility of the control
		*/
		EditBox(HWND p_parent, int p_id, bool p_visible = true) : TextBox(p_parent, p_id, p_visible) {
			m_maxLength = (int)SendMessage(m_handle, EM_GETLIMITTEXT, 0, 0);
		}

		/*
		* @brief Destructor.
		*/
		~EditBox() {
			//Destroy();
		}

		/*
		* @brief Destroys the current window.
		*/
		void Destroy() {
			DestroyWindow(m_handle);
		}

		/*
		* @brief Creates a new edit box window.
		* @param p_posX - position on the x-axis
		* @param p_posY - position on the y-axis
		* @param p_name - name of the control.
		*/
		void Create(const int p_posX, const int p_posY, const LPCSTR p_name) {
			HINSTANCE hInstance = (HINSTANCE)GetWindowLong(m_parent, GWL_HINSTANCE);
			HDC hDc = GetDC(m_parent);
			SIZE box_size;
			std::string l_temp(p_name);
			if (!GetTextExtentPoint32A(hDc, p_name, l_temp.size() + 1, &box_size))
				cout << "[ERROR] Cannot get button size: Controls.h" << endl;
			box_size.cx = box_size.cx + GetSystemMetrics(SM_CXMENUCHECK);
			if (box_size.cx > 170)
				box_size.cx = 170;
			else if (box_size.cx < 70)
				box_size.cx = 50;
			m_handle = CreateWindowEx(0, "EDIT", p_name, WS_CHILD | WS_TABSTOP| WS_VISIBLE | ES_AUTOHSCROLL, p_posX, p_posY, box_size.cx, box_size.cy, m_parent, NULL, hInstance, NULL);
			ReleaseDC(m_handle, hDc);
		}

		/*
		* @brief Sets all member variables.
		* @param p_parent - parent handle.
		* @param p_id - id of the control
		* @param p_visibile - visibility of the control
		*/
		void Set(HWND p_parent, int p_id, bool p_visible = true) {
			ControlBase::Set(p_parent, p_id, p_visible);
			m_maxLength = (int)SendMessage(m_handle, EM_GETLIMITTEXT, 0, 0);
		}

		/*
		* @brief Selects\unselects the whole text.
		* @param p_select - true to select; false to unselect
		*/
		void SelectText(bool p_select = true) {
			if (p_select)
				SendMessage(m_handle, EM_SETSEL, 0, -1);
			else
				SendMessage(m_handle, EM_SETSEL, (WPARAM)-1, 0);
		}

		/*
		* @brief Returns the max number of characters.
		* @return max number of chars.
		*/
		int GetMaxText() const {
			return m_maxLength;
		}

		//static bool isChanged(int code) {
		//	return (code == EN_CHANGE);
		//} // LOWORD(wParam)==m_id && HIWORD(wParam)==EN_CHANGE

	protected:

		///maximum size of the box
		int m_maxLength;
	private:

		///static id counter
		static unsigned int s_ids;

		///id of this control
		unsigned int m_id;
	};

	/*
	* @class ListBox class
	*/
	class ListBox : public ControlBase
	{
	public:
		/*
		* @brief Default c-tor.
		*/
		ListBox() : ControlBase(), m_listCount(0) {

		}

		/*
		* @brief Constructor.
		* @param p_parent - parent of this control
		* @param p_id - id of the control
		* @param p_visible - visibility of the control
		*/
		ListBox(HWND p_parent, int p_id, bool p_visible = true) : ControlBase(p_parent, p_id, p_visible), m_listCount(0) {

		}

		/*
		* @brief Destructor.
		*/
		~ListBox() {

		}

		/*
		* @brief Returns the amount of items.
		* @return number of items.
		*/
		int GetCount() const {
			return m_listCount;
		}

		/*
		* @brief Resets the list box.
		*/
		void ResetContent(void) {
			SendMessage(m_handle, LB_RESETCONTENT, 0, 0);
			m_listCount = 0;
		}

		/*
		* @brief Returns the selected item.
		* @return index of the selected item.
		*/
		int GetSelectedIndex(void) const {
			return SendMessage(m_handle, LB_GETCURSEL, 0, 0);
		}

		/*
		* @brief Returns the content of the selected string.
		* @return the content of the selected item.
		*/
		std::string GetSelectedString(void) {
			char l_buffer[100];
			SendMessage(m_handle, LB_GETTEXT, GetSelectedIndex(), (LPARAM)(LPCSTR)l_buffer);
			return std::string(l_buffer);
		}

		/*
		* @brief Adds a string to the end of the list. If the list is full it will delete the first
		* string first and then add the new one.
		* @param p_name - string to be added.
		*/
		void AddString(std::string p_name) {
			if (m_listCount >= MAX_INDEX)
				DeleteString(0);
			int index = (int)SendMessage(m_handle, LB_ADDSTRING, 0, (LPARAM)p_name.c_str());
			if (index != LB_ERR)
				++m_listCount;
			SendMessage(m_handle, LB_SETTOPINDEX, index, 0);
		}

		/*
		* @brief Inserts a string at the given index.
		* @param str - new string
		* @param index - index.
		* @return true - the string is set, else false.
		*/
		bool InsertString(const wchar_t* p_str, int p_index) {
			if (m_listCount >= MAX_INDEX)
				return false;
			p_index = (int)SendMessage(m_handle, LB_INSERTSTRING, p_index, (LPARAM)p_str);
			if (p_index != LB_ERR)
				++m_listCount;
			else
				return false;
			SendMessage(m_handle, LB_SETTOPINDEX, p_index, 0);
			return true;
		}

		/*
		* @brief Deletes a string at the given position.
		* @param p_index - position of the string.
		*/
		void DeleteString(int p_index) {
			if (SendMessage(m_handle, LB_DELETESTRING, p_index, 0) != LB_ERR)
				--m_listCount;
		}

	private:

		///number of items.
		int m_listCount;
	};

	/*
	* @brief ComboBox class.
	*/
	class ComboBox : public ControlBase
	{
	public:

		/*
		* @brief Default c-tor.
		*/
		ComboBox() : ControlBase() {

		}

		/*
		* @brief Constructor.
		* @param p_parent - parent of this control
		* @param p_id - id of the control
		* @param p_visible - visibility of the control
		*/
		ComboBox(HWND p_parent, int p_id, bool p_visible = true) : ControlBase(p_parent, p_id, p_visible) {

		}

		/*
		* @brief Destructor.
		*/
		~ComboBox() {

		}

		/*
		* @brief Returns the amount of items.
		* @return number of items.
		*/
		int GetCount(void) const {
			return (int)SendMessage(m_handle, CB_GETCOUNT, 0, 0);
		}

		/*
		* @brief Resets the content of the combo box.
		*/
		void ResetContent() {
			SendMessage(m_handle, CB_RESETCONTENT, 0, 0);
		}

		/*
		* @brief Adds a new string at the end of the combo box.
		* @param p_str - the new string.
		*/
		void AddString(const std::string p_str) {
			SendMessage(m_handle, CB_ADDSTRING, 0, (LPARAM)p_str.c_str());
		}

		/*
		* @brief Inserts a new string at the given position into the list.
		* @param p_str - the new string
		* @param p_index - position of the new string.
		*/
		void InsertString(const std::string p_str, int p_index) {
			SendMessage(m_handle, CB_INSERTSTRING, p_index, (LPARAM)p_str.c_str());
		}

		/*
		* @brief Deletes a string at the given index.
		* @param p_index - position of the string.
		*/
		void DeleteString(int p_index) {
			SendMessage(m_handle, CB_DELETESTRING, p_index, 0);
		}

		/*
		* @brief Returns the position of the selected string.
		* @return position of the selected string.
		*/
		int GetSelected() {
			return (int)SendMessage(m_handle, CB_GETCURSEL, 0, 0);
		}

		/*
		* @brief Sets a string at the given index.
		* @param p_index - position of the string to be selected.
		*/
		void SetSelected(int p_index) {
			SendMessage(m_handle, CB_SETCURSEL, p_index, 0);
		}
	};

	/*
	* @class TreeView class
	*/
	class TreeView : public ControlBase
	{
	public:

		/*
		* @brief Default c-tor.
		*/
		TreeView() : ControlBase() {

		}

		/*
		* @brief Constructor.
		* @param p_parent - parent of this control
		* @param p_id - id of the control
		* @param p_visible - visibility of the control
		*/
		TreeView(HWND p_parent, int p_id, bool p_visible = true) : ControlBase(p_parent, p_id, p_visible) {

		}

		/*
		* @brief Destructor.
		*/
		~TreeView() {

		}

		/*
		* @brief Returns a handle to the next item.
		* @param p_item - previous item
		* @param p_flag - type of the item to be returned such as TVGN_CHILD or TVGN_CARET.
		* @return the handle or null.
		*/
		HTREEITEM GetNextItem(HTREEITEM p_item, unsigned int p_flag = TVGN_NEXT) const {
			return (HTREEITEM)SendMessage(m_handle, TVM_GETNEXTITEM, (WPARAM)p_flag, (LPARAM)p_item);
		}

		/*
		* @brief Returns the next item.
		* @param p_item - the current item.
		* @param the next item or null.
		*/
		HTREEITEM GetNext(HTREEITEM p_item) const {
			return GetNextItem(p_item, TVGN_NEXT);
		}

		/*
		* @brief Returns the previous item.
		* @param p_item - the current item.
		* @param the previous item or null.
		*/
		HTREEITEM GetPrevious(HTREEITEM item) const {
			return GetNextItem(item, TVGN_PREVIOUS);
		}

		/*
		* @brief Returns the root item.
		* @param the root item or null.
		*/
		HTREEITEM GetRoot(void) const {
			return GetNextItem(0, TVGN_ROOT);
		}

		/*
		* @brief Returns the parent item.
		* @param p_item - the current item.
		* @param the parent item or null.
		*/
		HTREEITEM GetParent(HTREEITEM p_item) const {
			return GetNextItem(p_item, TVGN_PARENT);
		}  

		/*
		* @brief Returns the first child item.
		* @param p_item - the current item.
		* @param the first child item or null.
		*/
		HTREEITEM GetChild(HTREEITEM item) const {
			return GetNextItem(item, TVGN_CHILD);
		}   

		/*
		* @brief Returns the selected item.
		* @return the selected item.
		*/
		HTREEITEM GetSelected(void) const {
			return GetNextItem(0, TVGN_CARET);
		}     

		/*
		* @brief Returns the target item of a drag and drop operation.
		* @return the target item or null.
		*/
		HTREEITEM GetDropHilight() const {
			return GetNextItem(0, TVGN_DROPHILITE);
		} 

		/*
		* @brief Clears the tree view.
		*/
		void Clear(void) {
			HTREEITEM l_last = GetRoot();
			if (!l_last)
				return;
			HTREEITEM l_previous;
			while (l_last != NULL) {
				l_previous = l_last;
				l_last = GetNext(l_previous);
			}
			while (l_previous != NULL) {
				l_last = GetPrevious(l_previous);
				DeleteItem(l_previous);
				l_previous = l_last;
			}
		}

		/*
		* @brief Sets an item.
		* @param p_tvitem - hItem and mask must be set.
		*/
		void SetItem(TVITEM* p_tvitem) {
			SendMessage(m_handle, TVM_SETITEM, 0, (LPARAM)p_tvitem);
		}

		/*
		* @brief Gets an item.
		* @param p_tvitem - hItem and mask must be set.
		*/
		void GetItem(TVITEM* tvitem) const {
			SendMessage(m_handle, TVM_GETITEM, 0, (LPARAM)tvitem);
		}

		/*
		* @brief Selects an item within the tree view.
		* @param p_item - item to be selected
		* @param p_flag - type of the item such as TVGN_FIRSTVISIBLE or TVGN_CARET.
		*/
		void SelectItem(HTREEITEM p_item, unsigned int p_flag = TVGN_CARET) const {
			SendMessage(m_handle, TVM_SELECTITEM, (WPARAM)p_flag, (LPARAM)p_item);
		}

		/*
		* @brief Inserts a new item into the tree view.
		* @param p_str - text 
		* @param p_parent - parent of the item
		* @param p_insertAfter - item after which it should be inserted
		* @param p_imageIndex - static image 
		* @param p_selectedImageIndex - image when the item is selected
		* @return the item itself.
		*/
		HTREEITEM InsertItem(LPSTR p_str, HTREEITEM p_parent = TVI_ROOT, HTREEITEM p_insertAfter = TVI_LAST, int p_imageIndex = 0, int p_selectedImageIndex = 0) const {
			TVINSERTSTRUCT insertStruct;
			insertStruct.hParent = p_parent;
			insertStruct.hInsertAfter = p_insertAfter;        // m_handle to item or TVI_FIRST, TVI_LAST, TVI_ROOT
			insertStruct.item.mask = TVIF_TEXT | TVIF_IMAGE | TVIF_SELECTEDIMAGE;
			//insertStruct.item.mask = TVIF_TEXT | TVIF_IMAGE | TVIF_SELECTEDIMAGE | TVIF_PARAM;
			insertStruct.item.pszText = p_str;
			insertStruct.item.cchTextMax = sizeof(p_str) / sizeof(p_str[0]);
			insertStruct.item.iImage = p_imageIndex;                       // image index of ImageList
			insertStruct.item.iSelectedImage = p_selectedImageIndex;

			// insert the item
			HTREEITEM hTreeItem = (HTREEITEM)SendMessage(m_handle, TVM_INSERTITEM, 0, (LPARAM)&insertStruct);

			// expand its m_parent
			HTREEITEM hParentItem = GetParent(hTreeItem);
			if (hParentItem)
				Expand(hParentItem);

			return hTreeItem;
		}
				
		/*
		* @brief Deletes an item.
		* @param p_item - the item from the treeview.
		*/
		void DeleteItem(HTREEITEM p_item) const {
			SendMessage(m_handle, TVM_DELETEITEM, 0, (LPARAM)p_item);
		}

		/*
		* @brief Changes an item's label.
		* @param p_item - the item with a new label.
		*/
		void EditLabel(HTREEITEM p_item) const {
			SendMessage(m_handle, TVM_EDITLABEL, 0, (LPARAM)p_item);
		}

		/*
		* @brief Handle of an editbox control of an item where TVN_BEGINLABELEDIT is notified.
		* @return handle to the editbox.
		*/
		HWND GetEditControl(void) const {
			return (HWND)SendMessage(m_handle, TVM_GETEDITCONTROL, 0, 0);
		}

		/*
		* @brief Returns the number of items within the tree.
		* @return number of items
		*/
		int  GetCount(void) const {
			return (int)SendMessage(m_handle, TVM_GETCOUNT, 0, 0);
		}

		/*
		* @brief Expands/Collapses an item.
		* @param p_item - item to be expanded/collapsed
		* @param p_flag - type of operation: TVE_EXPAND / TVE_COLLAPSE / TVE_TOGGLE / TVE_EXPANDPARTIAL / TVE_COLLAPSERESET.
		*/
		void Expand(HTREEITEM p_item, unsigned int p_flag = TVE_EXPAND) const{
			SendMessage(m_handle, TVM_EXPAND, (WPARAM)p_flag, (LPARAM)p_item);
		}
		
		/*
		* @brief Sets indentation.
		* @param p_indent - indentation, where min is 0.
		*/
		void SetIndent(int indent) const {
			SendMessage(m_handle, TVM_SETINDENT, (WPARAM)indent, 0);
		}

		/*
		* @brief Return the current indentation.
		* @return indentation.
		*/
		int GetIndent(void) const {
			return (int)SendMessage(m_handle, TVM_GETINDENT, 0, 0);
		}

		/*
		* @brief Sets an image list.
		* @param p_imageListHandle - handle to the list.
		* @param p_imageListType - TVSIL_NORMAL / TVSIL_STATE
		*/
		void SetImageList(HIMAGELIST p_imageListHandle, int p_imageListType = TVSIL_NORMAL) const {
			SendMessage(m_handle, TVM_SETIMAGELIST, (WPARAM)p_imageListType, (LPARAM)p_imageListHandle);
		}

		/*
		* @brief Returns an image list based on the type.
		* @param p_imageListType - TVSIL_NORMAL / TVSIL_STATE
		*/
		HIMAGELIST GetImageList(int p_imageListType) const {
			return (HIMAGELIST)SendMessage(m_handle, TVM_GETIMAGELIST, (WPARAM)p_imageListType, (LPARAM)0);
		}

		/*
		* @brief Creates a dragging image.
		* @param p_item - item that is being dragged.
		* @param the dragging image
		*/
		HIMAGELIST CreateDragImage(HTREEITEM p_item) const{
			return (HIMAGELIST)SendMessage(m_handle, TVM_CREATEDRAGIMAGE, 0, (LPARAM)p_item);
		}

		/*
		* @brief Retrieves the rect of the item being dragged.
		* @param p_rect - pointer to an empty rectangle.
		* @param p_textOnly - true - rect of the text only
		*/
		void GetItemRect(RECT* p_rect, bool p_textOnly = true) const{
			SendMessage(m_handle, TVM_GETITEMRECT, (WPARAM)p_textOnly, (LPARAM)p_rect);
		}

		/*
		* @brief Retrieves hit test info for a dragging item.
		* @param hitTestInfo - hit test info
		* @return the item
		*/
		HTREEITEM HitTest(TVHITTESTINFO* p_hitTestInfo) const{
			return (HTREEITEM)SendMessage(m_handle, TVM_HITTEST, 0, (LPARAM)p_hitTestInfo);
		}

	};

	/*
	* @brief TabControl class
	*/
	class TabControl : public ControlBase {
	public:
		
		/*
		* @brief Default c-tor.
		*/
		TabControl() : ControlBase() {

		}

		/*
		* @brief Constructor.
		* @param m_parent - m_parent of this control
		* @param m_id - m_id of the control
		* @param visible - visibility of the control
		*/
		TabControl(HWND p_parent, int p_id, bool p_visible = true) : ControlBase(p_parent, p_id, p_visible) {
		
		}
		
		/*
		* @brief Destructor.
		*/
		~TabControl() {
			m_handles.clear();
		}

		/*
		* @brief Adds an image to the tab control.
		* @param p_bitmap - the image to be added.
		*/
		void SetImage(HBITMAP p_bitmap) {
			SendMessage(m_handle, BM_SETIMAGE, (WPARAM)IMAGE_BITMAP, (LPARAM)p_bitmap);
		}

		/*
		* @brief Adds an icon to the tab control.
		* @param p_icon - the image to be added.
		*/
		void SetImage(HICON p_icon) { 
			SendMessage(m_handle, BM_SETIMAGE, (WPARAM)IMAGE_ICON, (LPARAM)p_icon); 
		}

		/*
		* @brief Shows/Hides tabs
		* @param p_index - index of the tab to be modified.
		* @param p_show - true for showing, false for hiding
		*/
		void ShowTab(int p_index, bool p_show) {
			ShowWindow(m_handles.at(p_index), p_show);
		}

		/*
		* @brief Returns the index of the selected tab.
		* @return the index of the tab.
		*/
		int GetSelected() {
			return SendMessage(m_handle, TCM_GETCURSEL, 0, 0);
		}

		/*
		* @brief Returns the amount of tabs.
		* @return number of tabs.
		*/
		int GetSize() {
			return SendMessage(m_handle, TCM_GETITEMCOUNT, 0, 0);
		}

		/*
		* @brief Adds a new tab to the tab control.
		* @param p_handle - handle to a window to be added to the new tab.
		* @param p_name - name of the tab
		* @param p_index - index of the tab.
		* @return returns the tab itself.
		*/
		TCITEM AddTab(HWND p_handle, std::string p_name, int p_index, LPARAM /*lParam*/) {
			m_handles.push_back(p_handle);
			TCITEM l_tab;
			l_tab.mask = TCIF_TEXT | TCIF_PARAM;
			l_tab.pszText = (LPSTR)p_name.c_str();
			l_tab.cchTextMax = 30;
			l_tab.lParam = (LPARAM)p_handle;
			if (!SendMessage(m_handle, TCM_INSERTITEM, p_index, (LPARAM)&l_tab))
				cout << "[ERROR] Cannot insert a tab." << endl;
			ShowWindow(m_handles.at(p_index), SW_HIDE);
			return l_tab;
		}

		/*
		* @brief Sets a tab at a specific index.
		* @param p_handle - handle to a window
		* @param p_name - name of the tab
		* @param p_index - index of the tab
		* @return the tab 
		*/
		TCITEM SetTab(HWND p_handle, LPSTR p_name, int p_index) {
			m_handles.push_back(p_handle);
			TCITEM l_tab;
			l_tab.mask = TCIF_TEXT;
			l_tab.pszText = p_name;
			l_tab.cchTextMax = 30;
			if (!SendMessage(m_handle, TCM_SETITEM, p_index, (LPARAM)&l_tab))
				cout << "[ERROR] Cannot set a tab." << endl;
			return l_tab;
		}

		/*
		* @brief Returns a tab at the specific index.
		* @param p_index - index of the tab
		* @return the tab
		*/
		TCITEM GetTab(unsigned int index) {
			TCITEM l_tab;
			if (index < m_handles.size()) {
				if (!SendMessage(this->m_handle, TCM_GETITEM, index, (LPARAM)&l_tab))
					cout << "[ERROR] Cannot get a tab." << endl;
			}
			return l_tab;
		}
	private:

		///handles to windows.
		std::vector<HWND> m_handles;
	};

	/*
	*
	*/
	class ListControl : public ControlBase {
	public:
		
		/*
		* @brief Default c-tor.
		*/
		ListControl() : ControlBase() {

		}

		/*
		* @brief Constructor.
		* @param m_parent - m_parent of this control
		* @param m_id - m_id of the control
		* @param visible - visibility of the control
		*/
		ListControl(HWND p_parent, int p_id, bool p_visible = true) : ControlBase(p_parent, p_id, p_visible) {

		}

		/*
		* @brief Destructor.
		*/
		~ListControl() {

		}

		/*
		* @brief Adds an image to the list control.
		* @param p_bitmap - the image to be added.
		*/
		void SetImage(HBITMAP p_bitmap) { 
			SendMessage(m_handle, BM_SETIMAGE, (WPARAM)IMAGE_BITMAP, (LPARAM)p_bitmap); 
		}
		
		/*
		* @brief Adds an icon to the list control.
		* @param p_icon - the image to be added.
		*/
		void SetImage(HICON p_icon) { 
			SendMessage(m_handle, BM_SETIMAGE, (WPARAM)IMAGE_ICON, (LPARAM)p_icon); 
		}

		/*
		* @brief Returns the amount of items within the control.
		* @return number of items.
		*/
		int GetSize(void) const{
			return SendMessage(m_handle, LVM_GETITEMCOUNT, 0, 0);
		}

		/*
		* @brief Adds a new item to the list control.
		* @param p_name - name of the item
		* @param p_index - index of the item
		* @return the item itself.
		*/
		LVITEM AddItem(LPSTR p_name, int p_index) {
			LVITEM l_item;
			l_item.mask = LVIF_TEXT;// | LVIF_PARAM;
			l_item.iItem = p_index;
			l_item.iSubItem = 0;
			l_item.pszText = p_name;
			l_item.cchTextMax = 30;
			if (!SendMessage(m_handle, LVM_INSERTITEM, p_index, (LPARAM)&l_item))
				cout << "[ERROR] Cannot insert a tab.";
			return l_item;
		}

		/*
		* @brief Set an item to the list control.
		* @param p_name - name of the item
		* @param p_index - index of the item
		* @return the item itself.
		*/
		LVITEM SetItem(LPSTR p_name, int p_index) {
			LVITEM l_item;
			l_item.mask = TCIF_TEXT;
			l_item.pszText = p_name;
			l_item.cchTextMax = 30;
			if (!SendMessage(m_handle, LVM_SETITEM, p_index, (LPARAM)&l_item))
				cout << "[ERROR] Cannot set a tab." << endl;
			return l_item;
		}

		/*
		* @brief Returns an item at the given index.
		* @param index of the item.
		* @return the item itself.
		*/
		LVITEM GetItem(unsigned int index) {
			LVITEM l_item;
			if (index < m_handles.size()) {
				if (!SendMessage(this->m_handle, LVM_GETITEM, index, (LPARAM)&l_item))
					cout << "[ERROR] Cannot get a tab." << endl;
			}
			return l_item;
		}

		/*
		* @brief Removes all items from the list control.
		* @return true - items are removed.
		*/
		const bool Clear(void) const {
			return (bool)SendMessage(m_handle, LVM_DELETEALLITEMS, 0, 0);
		}

		/*
		* @brief Removes an item at the given index.
		* @param p_index - position of the item.
		* @return true - the item is removed.
		*/
		const bool DeleteItem(int p_index) const {
			return (bool)SendMessage(m_handle, LVM_DELETEITEM, p_index, 0);
		}
	private:

		///handles to items
		std::vector<HWND> m_handles;
	};
}
#endif
