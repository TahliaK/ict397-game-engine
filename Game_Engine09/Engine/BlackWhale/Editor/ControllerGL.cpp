#include "ControllerGL.h"

#include "../Engine.h"
#include "../Master.h"

bool Editor::ControllerGL::s_isCreatedContext = false;

Editor::ControllerGL::ControllerGL(Editor::View* p_view) : m_view(p_view) {

}

int Editor::ControllerGL::Close() {
	::DestroyWindow(handle);
	return 0;
}

int Editor::ControllerGL::Destroy() {
	::wglMakeCurrent(0, 0);
	::ReleaseDC(handle, m_view->getDC());
	return 0;
}

int Editor::ControllerGL::Create(CREATESTRUCT* info) {
	if (!s_isCreatedContext) {
		// Create a OpenGL rendering context
		if (!m_view->createContext(handle, 32, 24, 8, 4))
		{
			//log(L"[ERROR] Failed to Create OpenGL rendering context from ControllerGL::Create(): %d", GetLastError());
			return -1;
		}
		m_view->activateContext();
		s_isCreatedContext = true;
	}
	else {
		// set DC only, and use RC from view 1
		if (!m_view->setContext(handle, m_hglrc, m_pixelFormat)) {
			//log("[ERROR] Failed to Create OpenGL window for screen 2.");
		}
	}
	return 0;
}

int Editor::ControllerGL::Paint() {
	if (Master::STARTED) {
		GraphicsEngine::S_DRAW_CALLS = 0;
		Engine::Update();
	}
	m_view->swapBuffers();
	return 0;
}

int Editor::ControllerGL::Command(int id, int cmd, LPARAM msg) {
	return 0;
}

int Editor::ControllerGL::Resize(int width, int height, WPARAM type) {
	if (Master::STARTED) {
		Engine::ReshapeFunction(width, height);
		Engine::Update();
		return 0;
	}
	else {
		return 1;
	}
}

HGLRC Editor::ControllerGL::GetRC() const {
	return m_view->getRC();
}

int Editor::ControllerGL::GetPixelFormat() const {
	return ::GetPixelFormat(m_view->getDC());
}

void Editor::ControllerGL::SetRC(HGLRC p_rc, int p_pixelFormat) {
	m_hglrc = p_rc;
	m_pixelFormat = p_pixelFormat;
}


