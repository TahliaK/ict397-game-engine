#include "OpenScene_c.h"

#include <exception>
#include <cstdlib>
#include <string>
#include <sstream>

#include "OpenScene_c.h"
#include "../../resource.h"
#include "../Component.h"
#include "../SceneManager.h"
#include "Hierarchy_c.h"

using namespace Editor;

OpenScene_c::OpenScene_c(){

}


int OpenScene_c::Destroy(){
	::DestroyWindow(handle);
	return 0;
}


int OpenScene_c::Create(CREATESTRUCT* info) {
	m_itemIndex = LB_ERR;
	m_listbox = ListBox(handle, IDC_LIST_OPEN_SCENE);
	LoadScenes();
	return 0;
}

int OpenScene_c::GetSelectedScene() const{
	return m_itemIndex;
}

void OpenScene_c::SetSelectedScene(int p_index) {
	m_itemIndex = p_index;
}



void OpenScene_c::LoadScenes() {
	m_listbox.ResetContent();
	char BUF[200];
	GetModuleFileName(NULL, BUF, 200);
	std::string location(BUF);

	//remove Engine.exe
	location = location.substr(0, location.find_last_of('\\'));
	std::string l_path(location + "\\Scenes\\"+"/*.scene");
	//cout << l_path << endl;
	WIN32_FIND_DATA l_fd;
	HANDLE l_hFind = ::FindFirstFile(l_path.c_str(), &l_fd);
	if (l_hFind != INVALID_HANDLE_VALUE) {
		do {
			// read all (real) files in current folder
			// , delete '!' read other 2 default folder . and ..
			if (!(l_fd.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY))
			{
				string l_temp(l_fd.cFileName);
				m_listbox.AddString((LPCSTR)l_temp.substr(0,l_temp.find_last_of('.')).c_str());
			}
		} while (::FindNextFile(l_hFind, &l_fd));
		::FindClose(l_hFind);
	}
}

int OpenScene_c::GetSceneIndex(std::string p_name) {
	IFORSI(0, Component::GetDerivedClasses().size()) {
		string tempInfo(typeid(*Component::GetDerivedClasses().at(i)).name());
		tempInfo = tempInfo.substr(tempInfo.find(' ') + 1);
		if (tempInfo.substr(0, p_name.size()).compare(p_name) == 0) {
			return i;
		}
	}
	return LB_ERR;
}

int OpenScene_c::Command(WPARAM wParam, LPARAM msg, HWND hwnd) {
	if (handle != GetForegroundWindow()) {
		::ShowWindow(handle, false);
	}
	switch (LOWORD(wParam)) {

	//case IDC_EDIT_COMPMENU:
	//{
	//	char input[100];
	//	GetDlgItemText((HWND)hwnd, LOWORD(wParam), input, sizeof(input));
	//	string in(input);
	//	cout << in << endl;
	//	//LoadScenes();
	//}
	//break;
	case IDC_LIST_OPEN_SCENE:
		if (HIWORD(wParam) == LBN_SELCHANGE) {
		}
		else if (HIWORD(wParam) == LBN_DBLCLK) {
			int l_selIndex = m_listbox.GetSelectedIndex();
			if (l_selIndex != LB_ERR) {
				SceneManagement::SceneManager::SetActive(SceneManagement::SceneManager::LoadScene(m_listbox.GetSelectedString()));
				SceneManagement::SceneManager::GetActive()->Start();
				Hierarchy_c::Update();
				::ShowWindow(handle, false);//hide window
			}
		}
		else if (HIWORD(wParam) == EN_KILLFOCUS) {
			char input[100];
			GetDlgItemText((HWND)hwnd, LOWORD(wParam), input, sizeof(input));
			string in(input);
			float l_number = 0;
			bool l_newValue = false;
			try {
				l_number = std::stof(in);
				char l_char[30];
				sprintf_s(l_char, "%f", l_number);
				l_newValue = true;
			}
			catch (exception e) {
				cout << "[ERROR] OpenScene_c.cpp: " << e.what() << endl;
			}
			char l_char[30];
			sprintf_s(l_char, "%f", l_number);
			SetDlgItemText(hwnd, LOWORD(wParam), (LPCSTR)l_char);
		}
		break;
	}

	return 0;
}

int OpenScene_c::Notify(int id, LPARAM lParam){
	return 0;
}