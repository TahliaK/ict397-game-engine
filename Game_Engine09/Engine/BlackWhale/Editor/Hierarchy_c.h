#ifndef HIERARCHY_C_H
#define HIERARCHY_C_H
#include "Controller.h"
#include "Settings_c.h"
namespace Editor {

	/*
	* @class Bottom_c
	* @brief Used as a controller for a window which displays all game
	* objects within the current scene and allows to select and delete them.
	* @author Olesia Kochergina
	* @date 25/02/2017
	*/
	class Hierarchy_c : public Controller {
	public:

		/*
		* @brief Default c-tor.
		*/
		Hierarchy_c();

		/*
		* @brief Destructor.
		*/
		~Hierarchy_c();

		/*
		* @brief Initializes internal controls.
		* @param info - contains information about the window being created.
		* @return Returns 0 to indicate that the message is processed, if -1 then the window is destroyed.
		*/
		int Create(CREATESTRUCT* info);

		/*
		* @brief Destroys the window and its controls.
		* @return Returns 0 to indicate that the message is processed.
		*/
		int Destroy();

		/*
		* @brief Allows to select game objects and edit their names.
		* @param id - ID of the control sending the message.
		* @param lParam - pointer to NMHDR which contains the notification code and other information.
		* @return Can be ignored.
		*/
		int Notify(int id, LPARAM lParam);

		/*
		* @brief Called by WM_COMMAND message received from the message loop.
		* @param wParam - ID of the control
		* @param msg -
		* @param hwnd - handle to the window
		* @return Returns 0 to indicate that the message is processed.
		*/
		int Command(WPARAM wParam, LPARAM msg, HWND hwnd);

		/*
		* @brief Sets the setings window.
		* @param settingsController - pointer to the settings window's controller.
		*/
		void SetCurrentSettings(Settings_c* settingsController);

		/*
		* @brief Refreshes the hierarchy.
		*/
		static void Update();
	private:
		///sequence DRAW-CLICK-CHANGESELECTION  = -1 -> 0 -> 1 -> 2
		int m_itemSelected;

		///pointer to the settings window.
		Settings_c* m_insForm;

		///handle to a hierarchy item which has been modified.
		HWND m_editedHandle;

		///control within the window with available game objects.
		static TreeView m_hierarchy;

		///Hepls to determine what item is selected in the hierarchy using recursion
		struct Item {
			///View-tree item
			HTREEITEM treeItem;
			///Index of a gameobject attached to the item which is based on its position in a vector of children of a particular gameobject
			int goIndex;
		};

		///a vector of all hierarchy items
		static std::vector<Item> m_items;

		/*
		* @brief Recursive method which allows adding gameobjects to the hierarchy.
		* @param parentGO - the current gameobject parent
		* @param parentItem - the current item which will be set as a parent
		*/
		static void AddHierarchyChildren(GameObject* parentGO, const Item& parentItem);

		/*
		* @brief Produces an array of indices to identify the first layer parent of a selected game object.
		* @param child - selected game object.
		* @return array of integers which identify the way to the first layer game object.
		*/
		static vector<int> GetReverse(const Item& child);
	};
}
#endif HIERARCHY_C_H