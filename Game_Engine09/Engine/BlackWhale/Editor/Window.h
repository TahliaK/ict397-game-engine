#ifndef Window_H
#define Window_H

#include <Windows.h>
#include "../Math_s.h"
#include "Controller.h"

namespace Editor {
		class Window {
		public:
			Window() {}
			Window(HINSTANCE hInst, std::string name, HWND hParent, Controller* ctrl);
			~Window();
			HWND Create();
			void Show(bool cmdShow = (bool)SW_SHOWDEFAULT);
			HWND GetHandle();

			void SetClassStyle(UINT style);
			void SetIcon(int ID);
			void SetIconSmall(int ID);
			void SetCursor(int ID);
			void SetBackground(int color);
			void SetMenuName(LPCTSTR name);
			void SetWindowStyle(DWORD style);
			void SetWindowStyleEx(DWORD style);
			void SetWindowRect(const Vector4& rect);
			void SetParent(HWND handle);
			void SetCurrentMenu(HMENU menu);
		private:
			HICON m_LoadIcon(int ID);
			HCURSOR m_LoadCursor(int ID);
			HWND m_handle;
			WNDCLASSEX m_winClass;
			DWORD m_winStyle;
			DWORD m_winStyleEx;
			LPSTR m_title;
			LPSTR m_className;
			Vector4 m_size;
			HWND m_pHandle;
			HMENU m_menuHandle;
			HINSTANCE m_instance;
			Controller *controller;
		};
	}
#endif Window_H

