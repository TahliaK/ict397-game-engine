/*
* @brief Windows Dialog Box class
* @author Olesia Kochergina
* @date 01/01/2017
*/
#ifndef WIN_DIALOG_WINDOW_H
#define WIN_DIALOG_WINDOW_H

#include <windows.h>
#include "Controller.h"
#include "../Math_s.h"
namespace Editor{
    class DialogWindow
    {
    public:
		/*
		* @brief Default Constructor
		*/
		DialogWindow(){};
        // ctor/dtor with minimal args
        // The second param, id should be the template ID of a dialog in resource.rc(IDD_*).
        DialogWindow(HINSTANCE hInst, WORD id, HWND hParent, Controller* ctrl);

        ~DialogWindow();

		/*
		* @brief Creates a new dialog.
		* @return Returns the dialog's handle.
		*/
        HWND Create();

		/*
		* @Changes the visibility of the window.
		* @param show: true - visible, false - invisible
		*/
        void Show(bool show = SW_SHOWDEFAULT);  
        
		/*
		* @brief Returns the handle of this dialog box.
		* @returnthe handle of the dialog box
		*/
		HWND GetHandle(){ return handle; };

        // setters
     	void SetWindowRect(Vector4 rect);

		Controller* GetController(){
			return controller;		
		}
    private:

		///handle to this dialog
        HWND handle;                          

        ///handle to parent window
		HWND parentHandle;                     
        WORD id;
       Vector4 m_rect;                            // window height
        HINSTANCE instance;                     // handle to instance
        Controller *controller;            // pointer to controller
    };
}
#endif
