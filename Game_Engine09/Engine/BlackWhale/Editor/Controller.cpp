#include "Controller.h"
#include "../GameObject.h"
#include "../SceneManager.h"
#include "Hierarchy_c.h"
#include "Settings_c.h"
#include "OpenScene_c.h"
#include "ControllerGL.h"

using namespace Editor;
DialogWindow Controller::s_sceneDlg;
ControllerGL Controller::m_glCtrl;
HBRUSH Controller::s_brush = (HBRUSH)GetStockObject(LTGRAY_BRUSH);

Controller::Controller() : handle(0){
}

Controller::~Controller(){
	::DestroyWindow(handle);
	
}
void Controller::Clear() {
	DeleteObject(s_brush);
}

void Controller::SetGraphicsController(ControllerGL& glCtrl) {
	m_glCtrl = glCtrl;
}

void Controller::SetSceneDlg(DialogWindow& sceneDialog) {
	s_sceneDlg = sceneDialog;
}

int Controller::SetColor(HDC p_hdc) {
	int r = 70, g=70, b=70;

	SetTextColor(p_hdc, RGB(255, 255, 255));
	SetBkMode(p_hdc, TRANSPARENT);

	/*
	LOGBRUSH lb;
	if(GetObject(temp,sizeof(LOGBRUSH),(LPSTR)&lb))
	{
		r = GetRValue(lb.lbColor);
		g = GetGValue(lb.lbColor);
		b = GetBValue(lb.lbColor);
	}else{
	r=b=g=0;
	}
	int returnValue = (LONG)CreateSolidBrush(RGB(r, g, b));*/
	return (int)s_brush;
}

int Controller::Command(WPARAM wParam, LPARAM msg, HWND hwnd) {
	switch (LOWORD(wParam)) {
	case ID_FILE_CREATEEXE:
	{
		return 0;
	}
	case ID_NEW_PROJECT:
		return 0;
	case ID_NEW_SCENE:
	{
		//creates a new scene named "Level" plus the size of the vector of scenes and then makes it the active one
		std::string l_temp = "Level" + std::to_string(SceneManagement::SceneManager::GetTotalAmount());
		SceneManagement::SceneManager::SetActive(SceneManagement::SceneManager::CreateScene(l_temp));
		SceneManagement::SceneManager::GetActive()->Start();
		Hierarchy_c::Update();
		return 0;
	}
	case ID_SAVE_PROJECT:
	{
		int l_active = SceneManagement::SceneManager::GetActiveID();
		IFORSI(0, SceneManagement::SceneManager::GetTotalAmount()) {
			SceneManagement::SceneManager::SetActive(i);
			SceneManagement::SceneManager::SaveActive();
		}
		SceneManagement::SceneManager::SetActive(l_active);
		return 0;
	}
	case ID_SAVE_SCENE:
		SceneManagement::SceneManager::SaveActive();
		return 0;
	case ID_OPEN_PROJECT:
		return 0;
	case ID_OPEN_SCENE:
	{
		s_sceneDlg.Show(true);
		((OpenScene_c*)s_sceneDlg.GetController())->LoadScenes();
		return 0;
	}
	case ID_FILE_EXIT:
		if (MessageBox(NULL, "Would You Like To Save The Current Scene?", "Save?", MB_YESNO | MB_ICONQUESTION) == IDYES) {
			//SceneManagement::SceneManager::SaveActive();
		}
		SendMessage(handle, WM_CLOSE, 0, 0);
		return 0;
	case ID_GAMEOBJECT://will be changed if decide to add more GOs to the menu
	{
		GameObject* l_newGO = new GameObject;
		SceneManagement::SceneManager::GetActive()->GetRootGameObjects().push_back(l_newGO);
		Hierarchy_c::Update();
	}
	return 0;
	default:
		break;
	}
	//disabled because children cannot be saved and there is only one type of GO atm
	/*IFORSI(0, m_menus.size()) {
		if (wParam == (UINT)m_menus.at(i)) {
			if (i == 0) {
				GameObject* l_newGO = new GameObject;
				if (Settings_c::m_selected) {

					Settings_c::m_selected->GetTransform()->AddChild(l_newGO->GetTransform());
				}
				else
				SceneManagement::SceneManager::GetActive()->GetRootGameObjects().push_back(l_newGO);
				Hierarchy_c::Update();
				break;
			}
			else if (GameObject::GetDerivedClasses().size() > i - 1) {
				GameObject* l_newGO = GameObject::GetDerivedClasses().at(i - 1)->NewObject();
				if (Settings_c::m_selected) {
					//disabled because children cannot be saved
					Settings_c::m_selected->GetTransform()->AddChild(l_newGO->GetTransform());
				}
				else
				SceneManagement::SceneManager::GetActive()->GetRootGameObjects().push_back(l_newGO);

				Hierarchy_c::Update();
				break;
			}
		}
	}*/
	//should be removed
	/*IFORSI(0, GetMenuItemCount(l_submenu)) {
		MENUITEMINFO l_iteminfo;
		l_iteminfo.cbSize = sizeof(MENUITEMINFO);
		l_iteminfo.dwTypeData = NULL;
		l_iteminfo.fMask = MIIM_TYPE;
		l_iteminfo.fType = MFT_STRING;
		if (!GetMenuItemInfo(l_submenu, i, true, &l_iteminfo)) {
			cout << "cannot get item" << endl;
		}
		char* l_temp = new char[++l_iteminfo.cch];
		l_iteminfo.dwTypeData = l_temp;
		if (!GetMenuItemInfo(l_submenu, i, true, &l_iteminfo)) {
			cout << GetLastError() << endl;
		}
		else {
			string d(l_iteminfo.dwTypeData);
			cout << d << endl;
		}
		delete l_temp;
	}
	*/
	return 0;
}

void Controller::SetHandle(HWND hwnd) {
	handle = hwnd; 
}

HWND Controller::GetHandle() const{ 
	return handle; 
}

int Controller::Close() { 
	::DestroyWindow(handle); 
	return 0; 
}

void Controller::ContextMenu(HWND /*handle*/, int /*x*/, int /*y*/){ 

}

int Controller::Create(CREATESTRUCT* info) {
	return 0; 
}

int Controller::Destroy() { 
	return 0; 
}

int Controller::Enable(bool /*flag*/) { 
	return 0; 
}

int Controller::EraseBkgnd(HDC /*hdc*/) { 
	return 0; 
}

int Controller::HorizontalScroll(WPARAM /*wParam*/, LPARAM /*lParam*/) { 
	return 0; 
}

int Controller::MouseHover(int /*state*/, int /*x*/, int /*y*/) { 
	return 0; 
}

int Controller::MouseLeave() { 
	return 0; 
}

int Controller::MouseWheel(int /*state*/, int /*delta*/, int /*x*/, int /*y*/) { 
	return 0; 
}

int Controller::Notify(int /*id*/, LPARAM /*lParam*/) { 
	return 0; 
}

int Controller::Paint() { 
	return 0; 
}

int Controller::RightButtonDown(WPARAM /*wParam*/, int /*x*/, int /*y*/) { 
	return 0; 
}

int Controller::RightButtonUp(WPARAM /*wParam*/, int /*x*/, int /*y*/) { 
	return 0; 
}

int Controller::Resize(int /*w*/, int /*h*/, WPARAM /*type*/) { 
	return 0; 
}

int Controller::Timer(WPARAM /*id*/, LPARAM /*lParam*/) { 
	return 0; 
}

int Controller::VerticalScroll(WPARAM /*wParam*/, LPARAM /*lParam*/) { 
	return 0; 
}
