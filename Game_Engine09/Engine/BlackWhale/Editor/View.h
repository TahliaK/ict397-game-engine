#ifndef VIEW_GL_H
#define VIEW_GL_H
#include <windows.h>
namespace Editor {
		class View
		{
		public:
			View();                                       // ctor
			~View();                                      // dtor

			bool createContext(HWND handle, int colorBits, int depthBits, int stencilBits, int msaaSamples);  // Create OpenGL rendering context
			void closeContext(HWND handle);
			void swapBuffers();
			void activateContext();
			bool setContext(HWND handle, HGLRC rc, int pixelFormat);

			HDC getDC() const { return hdc; };
			HGLRC getRC() const { return hglrc; };

		protected:

		private:
			// member functions
			static bool setPixelFormat(HDC hdc, int colorBits, int depthBits, int stencilBits);
			static int findPixelFormat(HDC hdc, int colorbits, int depthBits, int stencilBits); // return best matched format ID
			static int findPixelFormatWithAttributes(int colorbits, int depthBits, int stencilBits, int msaaSamples);

			HDC hdc;                                        // handle to device context
			HGLRC hglrc;                                    // handle to OpenGL rendering context

		};
	}
#endif
