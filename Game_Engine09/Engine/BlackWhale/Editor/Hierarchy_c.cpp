#include <exception>
#include <cstdlib>
#include <string>
#include <sstream>

#include "Hierarchy_c.h"
#include "../../resource.h"
#include "../SceneManager.h"

using namespace Editor;
TreeView Hierarchy_c::m_hierarchy;
vector<Hierarchy_c::Item> Hierarchy_c::m_items;

Hierarchy_c::Hierarchy_c() {

}

Hierarchy_c::~Hierarchy_c() {
	Destroy();
}

int Hierarchy_c::Destroy() {
	m_items.clear();
	m_hierarchy.Clear();
	return 0;
}


int Hierarchy_c::Create(CREATESTRUCT* info) {
	m_hierarchy = TreeView(handle, IDC_TREE_HI_1, true);
	m_itemSelected = -1;
	m_insForm = NULL;
	Update();
	return 0;
}

void Hierarchy_c::SetCurrentSettings(Settings_c* p_form) {
	m_insForm = p_form;
}

void Hierarchy_c::Update() {
	if (!SceneManagement::SceneManager::GetActive())
		return;
	m_items.clear();
	m_hierarchy.Clear();
	Item l_treeItem;
	std::string l_tempName = SceneManagement::SceneManager::GetActive()->GetName();
	LPSTR temp = (LPSTR)(l_tempName.c_str());
	l_treeItem.treeItem = m_hierarchy.InsertItem(temp);
	l_treeItem.goIndex = -1;
	m_items.push_back(l_treeItem);

	IFORSI(0, SceneManagement::SceneManager::GetActive()->GetRootGameObjects().size()) {
		GameObject* l_go = SceneManagement::SceneManager::GetActive()->GetRootGameObjects().at(i);
		std::string str = l_go->GetName();
		LPSTR temp = (LPSTR)(str.c_str());
		l_treeItem.treeItem = m_hierarchy.InsertItem(temp, m_items.at(0).treeItem);
		l_treeItem.goIndex = i;
		m_items.push_back(l_treeItem);
		AddHierarchyChildren(l_go, m_items.back());
	}
}

void Hierarchy_c::AddHierarchyChildren(GameObject* p_go, const Item& p_parent) {
	IFORSI(0, p_go->GetTransform()->GetChildrenCount()) {
		std::string str = p_go->GetTransform()->GetChild(i)->GetGameObject()->GetName();
		LPSTR l_temp = (LPSTR)(str.c_str());
		Item l_treeItem;
		l_treeItem.treeItem = m_hierarchy.InsertItem(l_temp, p_parent.treeItem);
		l_treeItem.goIndex = i;
		m_items.push_back(l_treeItem);
		AddHierarchyChildren(p_go->GetTransform()->GetChild(i)->GetGameObject(), m_items.back());
	}
}

int Hierarchy_c::Command(WPARAM wParam, LPARAM msg, HWND hwnd) {
	switch (LOWORD(wParam)) {
	case EN_UPDATE:
		//cout << "updating" << endl;
	case IDC_TREE_HI_1: {
		//cout << "calling inside" << endl;
	}
	case IDC_EDIT_IN_SZ:
		if (HIWORD(wParam) == EN_KILLFOCUS) {
			char l_input[100];
			GetDlgItemText((HWND)hwnd, LOWORD(wParam), l_input, sizeof(l_input));
			string in(l_input);
			float l_number = 0;
			bool l_newValue = false;
			try {
				l_number = std::stof(in);
				char l_char[30];
				sprintf_s(l_char, "%f", l_number);
				l_newValue = true;
			}
			catch (exception e) {
				cout << "[ERROR] Hierarchy_c.cpp: " << e.what() << endl;
			}
			char l_char[30];
			sprintf_s(l_char, "%f", l_number);
			SetDlgItemText(hwnd, LOWORD(wParam), (LPCSTR)l_char);
		}
		break;
	}

	return 0;
}

vector<int> Hierarchy_c::GetReverse(const Hierarchy_c::Item& child) {
	vector<int> l_index;
	Item l_temp = child;
	while (true) {
		l_index.push_back(l_temp.goIndex);
		l_temp.treeItem = m_hierarchy.GetParent(l_temp.treeItem);
		if (!l_temp.treeItem)
			return l_index;
		IFORSI(0, m_items.size()) {
			if (l_temp.treeItem == m_items.at(i).treeItem) {
				l_temp.goIndex = m_items.at(i).goIndex;
			}
		}
	}
}

int Hierarchy_c::Notify(int id, LPARAM lParam) {
	// first cast lParam to NMHDR* to know what the control is
	NMHDR* nmhdr = (NMHDR*)lParam;
	HWND from = nmhdr->hwndFrom;

	switch (nmhdr->code) {
	case TVN_SELCHANGING:
		if (m_itemSelected == 1 || m_itemSelected == -2)
			m_itemSelected = 2;
		break;
	case TVN_SELCHANGED:
	{
		HTREEITEM l_selected = m_hierarchy.GetSelected();
		//starting index -> [0] - is the scene itself
		int l_startIndex = 1;
		if (l_selected == m_items.at(0).treeItem) {
			m_insForm->ClearWindow();
			Settings_c::m_selected = NULL;
			m_insForm->Update();
		}
		else {
			IFORSI(l_startIndex, m_items.size()) {
				if (l_selected == m_items.at(i).treeItem) {
					vector<int> l_reversed = GetReverse(m_items.at(i));
					if (l_reversed.size() == 2) {
						m_insForm->ClearWindow();
						Settings_c::m_selected = SceneManagement::SceneManager::GetActive()->GetRootGameObjects().at(l_reversed.at(0));
						m_insForm->Update();
					}
					else if (l_reversed.size() == 1) {

					}
					else if (l_reversed.size() > 2) {
						GameObject* l_selectedGO = SceneManagement::SceneManager::GetActive()->GetRootGameObjects().at(l_reversed.at(l_reversed.size() - 2));
						for (int i = l_reversed.size() - 3; i >= 0; i--) {
							int f = l_selectedGO->GetTransform()->GetChildrenCount();
							if (l_selectedGO->GetTransform()->GetChildrenCount() > l_reversed.at(i))
								l_selectedGO = l_selectedGO->GetTransform()->GetChild(l_reversed.at(i))->GetGameObject();
						}
						m_insForm->ClearWindow();
						Settings_c::m_selected = l_selectedGO;
						m_insForm->Update();
					}
				}
			}
		}
		break;
	}
	case TVN_BEGINLABELEDIT:
	{
		m_editedHandle = m_hierarchy.GetEditControl();
		return 0;//to allow editing
	}
	break;
	case TVN_ENDLABELEDIT:
	{
		HTREEITEM l_selected = m_hierarchy.GetSelected();
		NMTVDISPINFO* l_info = (NMTVDISPINFO*)lParam;
		if (!l_info->item.pszText)//otherwise throws null exception if the used doesnt change a label
			break;
		string l_newValue((CHAR*)l_info->item.pszText);

		IFORSI(0, m_items.size()) {
			int l_startIndex = 1;
			if (l_selected == m_items.at(i).treeItem) {
				vector<int> l_reversed = GetReverse(m_items.at(i));
				if (l_reversed.size() == 2) {
					if (SceneManagement::SceneManager::GetActive()->GetRootGameObjects().at(l_reversed.at(0))->SetName(l_newValue)) {//valid name
						l_info->item.hItem = l_selected;
						m_hierarchy.SetItem(&l_info->item);
						m_insForm->ClearWindow();
						Settings_c::m_selected = SceneManagement::SceneManager::GetActive()->GetRootGameObjects().at(l_reversed.at(0));
						m_insForm->Update();
						return 1;//to save new name
					}
				}
				else if (l_reversed.size() == 1) {
					//handle scene name change
				}
				else if (l_reversed.size() > 2) {
					GameObject* l_temp = SceneManagement::SceneManager::GetActive()->GetRootGameObjects().at(l_reversed.at(l_reversed.size() - 2));
					for (int i = l_reversed.size() - 3; i >= 0; i--) {
						int f = l_temp->GetTransform()->GetChildrenCount();
						if (l_temp->GetTransform()->GetChildrenCount() > l_reversed.at(i))
							l_temp = l_temp->GetTransform()->GetChild(l_reversed.at(i))->GetGameObject();
					}
					if (l_temp->SetName(l_newValue)) {//valid name
						l_info->item.hItem = l_selected;
						m_hierarchy.SetItem(&l_info->item);
						m_insForm->ClearWindow();
						Settings_c::m_selected = l_temp;
						m_insForm->Update();
						return 1;//to save new name
					}
				}


				break;
			}
		}
	}
	break;
	case NM_CUSTOMDRAW:
		if (m_itemSelected == -2 || m_itemSelected == -1)
			m_itemSelected = 0;
		break;
	case NM_CLICK:
		if (m_itemSelected == 0)
			m_itemSelected = 1;
		else {
			m_itemSelected = -2;
			m_hierarchy.SelectItem(m_items.at(0).treeItem);
		}
		break;
	case TVN_KEYDOWN:
	{
		LPNMTVKEYDOWN r = (LPNMTVKEYDOWN)lParam;
		if (r->wVKey == VK_DELETE) {
			HTREEITEM l_selected = m_hierarchy.GetSelected();
			//starting index -> [0] - is the scene itself
			int l_startIndex = 1;
			if (l_selected == m_items.at(0).treeItem) {
				/*m_insForm->ClearWindow();
				Settings_c::m_selected = NULL;
				m_insForm->Update();*/
			}
			else {
				IFORSI(l_startIndex, m_items.size()) {
					if (l_selected == m_items.at(i).treeItem) {
						vector<int> l_reversed = GetReverse(m_items.at(i));
						if (l_reversed.size() == 2) {
							m_insForm->ClearWindow();
							SceneManagement::SceneManager::GetActive()->DeleteGameObject(l_reversed.at(0));
							Settings_c::m_selected = NULL;
							m_insForm->Update();
							Update();
						}
						else if (l_reversed.size() == 1) {
							//handle deletion of the active scene
						}
						else if (l_reversed.size() > 2) {
							GameObject* l_parent = NULL;
							GameObject* l_child = SceneManagement::SceneManager::GetActive()->GetRootGameObjects().at(l_reversed.at(l_reversed.size() - 2));
							for (int i = l_reversed.size() - 3; i >= 0; i--) {
								int f = l_child->GetTransform()->GetChildrenCount();
								if (l_child->GetTransform()->GetChildrenCount() > l_reversed.at(i)) {
									l_parent = l_child;
									l_child = l_child->GetTransform()->GetChild(l_reversed.at(i))->GetGameObject();
								}
							}
							m_insForm->ClearWindow();
							if (l_parent) {
								l_parent->GetTransform()->DeleteChild(l_child->GetTransform());
							}
							//delete 	l_child;
							Settings_c::m_selected = NULL;
							m_insForm->Update();
							Update();
						}
					}
				}
			}
		}
	}
	break;
	//case NM_KILLFOCUS:
	//	cout << "KILL FOCUS" << endl;
	//	break;

	//case	NM_RETURN:
	//	cout << "return" << endl;
	//	break;
	//case NM_SETCURSOR:
	//	//cout << "set cursor" << endl;
	//	break;
	//case	NM_SETFOCUS:
	//	cout << "set focus" << endl;
	//	break;
	//case TVN_ASYNCDRAW:
	//	cout << "asyncdraw" << endl;
	//	break;
	//case TVN_BEGINDRAG:
	//	cout << "begindrag" << endl;
	//	break;
	//case TVN_BEGINRDRAG:
	//	cout << "beginrdrag" << endl;
	//	break;
	//case TVN_DELETEITEM:
	//	cout << "deleteitem" << endl;
	//	break;
	//case TVN_GETDISPINFO:
	//	cout << "dispinfo" << endl;
	//	break;
	//case TVN_GETINFOTIP:
	//	cout << "getinfotip" << endl;
	//	break;
	//case TVN_ITEMCHANGED:
	//	cout << "itemchanged" << endl;
	//	break;
	//case TVN_ITEMCHANGING:
	//	cout << "itemchanging" << endl;
	//	break;
	//case TVN_ITEMEXPANDED:
	//	//cout << "itemexpanded" << endl;
	//	break;
	//case TVN_ITEMEXPANDING:
	//	//cout << "itemexpanding" << endl;
	//	break;

	default:
		//Unhandled notifications
		break;
	}

	return 0;
}

