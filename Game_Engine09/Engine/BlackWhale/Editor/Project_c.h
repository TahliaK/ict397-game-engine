#ifndef Project_c_H
#define Project_c_H
#include "Controller.h"
namespace Editor {

	/*
	* @class Project_c
	* @brief NOT IMPLEMENTED
	* @author Olesia Kochergina
	* @date 03/03/2017
	*/
	class Project_c : public Controller {
	public:

		/*
		* @brief Default c-tor.
		*/
		Project_c();

		/*
		* @brief Destructor.
		*/
		~Project_c() {};

		/*
		* @brief Initializes internal controls.
		* @param info - contains information about the window being created.
		* @return Returns 0 to indicate that the message is processed, if -1 then the window is destroyed.
		*/
		int Create(CREATESTRUCT* info);

		/*
		* @brief Destroys the window and its controls.
		* @return Returns 0 to indicate that the message is processed.
		*/
		int Destroy(void);

		/*
		* @brief Identifies selected files within the project folder.
		* @param id - ID of the control sending the message.
		* @param lParam - pointer to NMHDR which contains the notification code and other information.
		* @return Can be ignored.
		*/
		int Notify(int id, LPARAM lParam);

		/*
		* @brief Called by WM_COMMAND message received from the message loop.
		* @param wParam - ID of the control
		* @param msg -
		* @param hwnd - handle to the window
		* @return Returns 0 to indicate that the message is processed.
		*/
		int Command(WPARAM wParam, LPARAM msg, HWND hwnd);
	private:

		/*
		* @brief Reads a directory and adds files to the directory tree view.
		* @param parent - parent directory tree view item.
		* @param dirName - name of the directory.
		* @param fileNames - all names under the directory.
		* @param updateTree - true if the tree needs to be updated.
		*/
		void SetDirectory(HTREEITEM parent, std::string dirName, std::vector<std::string>& fileNames, bool updateTree);

		/**
		* @brief Returns the path to a directory with game assets.
		* @return Path to the directory.
		*/
		std::string GetDirectory();

		///a list of available folders under the project folder
		TreeView m_directory;

		///a list of files under a specific folder.
		ListControl m_items;

		///true - the tree view will be updated.
		bool m_updateTree;

		///adds item to the scene - NOT IMPLEMENTED
		bool m_addItemFromList;
	};
}
#endif Project_c_H