#ifndef Settings_c_H
#define Settings_c_H

#include "Controller.h"
#include "DialogWindow.h"
#include "Compmenu_c.h"
#include <map>
#include "../GameObject.h"
#include "../Component.h"

namespace Editor {

	/*
	* @class Settings_c
	* @brief Used as a controller for a window which allows to view
	* all components of a game object, modify them and add new ones.
	* @author Olesia Kochergina
	* @date 03/03/2017
	*/
	class Settings_c : public Controller {
	public:

		/*
		* @brief Default c-tor.
		*/
		Settings_c();

		/*
		* @brief Destructor.
		*/
		~Settings_c() {};

		/*
		* @brief Called by WM_VSCROLL message received from the message loop.
		* @param wParam - position of the scroll box and scroll bar value.
		* @param lParam - handle to the scroll bar.
		* @return Returns 0 to indicate that the message is processed.
		*/
		int VerticalScroll(WPARAM wParam, LPARAM lParam);

		/*
		* @brief Initializes internal controls.
		* @param info - contains information about the window being created.
		* @return Returns 0 to indicate that the message is processed, if -1 then the window is destroyed.
		*/
		int Create(CREATESTRUCT* info);

		/*
		* @brief Destroys the window and its controls.
		* @return Returns 0 to indicate that the message is processed.
		*/
		int Destroy();

		/*
		* @brief Called by WM_NOTIFY message received from the message loop.
		* @param id - ID of the control sending the message.
		* @param lParam - pointer to NMHDR which contains the notification code and other information.
		* @return Can be ignored.
		*/
		int Notify(int id, LPARAM lParam);

		/*
		* @brief Identifies which scene to load and makes it the current one.
		* @param wParam - ID of the control
		* @param msg -
		* @param hwnd - handle to the window
		* @return Returns 0 to indicate that the message is processed.
		*/
		int Command(WPARAM wParam, LPARAM msg, HWND hwnd);

		/*
		* @brief Updates info within all controls.
		*/
		void Update(void);

		/*
		* @brief Clears the settings dialog window.
		*/
		void ClearWindow(void);

		/*
		* @brief Sets the component window pointer.
		* @param compMenu - pointer to the component menu window.
		*/
		void SetCompMenu(DialogWindow* compMenu);

		///pointer to the currently selected game object
		static GameObject* m_selected;

	private:

		/*
		* @brief Displays all available setters within a component on the screen.
		* @param component - pointer to the component
		* @param parent - reference to the "AddComponent" button
		* @param compIndex - index of this component
		*/
		void AddComponentEvent(Component* component, const HWND& parent, int compIndex);

		///all text boxes within the window
		std::vector<TextBox> m_textbox;

		///all labels within the window
		std::vector<EditBox> m_editbox;

		///all checkboxes within the window
		std::vector<CheckBox> m_checkbox;

		///all horizontal line separators between components
		std::vector<Separator> m_separator;

		///defines a single element within a component
		struct m_element {

			///index of a component
			int compIndex;
			///index of a variable
			int varIndex;
			///index of an element within the variable
			int elementIndex;

			/*
			* @brief Constructor.
			* @param c - component index
			* @param v - variable index
			* @param e - element within th variable
			*/
			m_element(int c, int v, int e) {
				compIndex = c;
				elementIndex = e;
				varIndex = v;
			}

			/*
			* @brief Default c-tor which sets inidices to -1.
			*/
			m_element() {
				compIndex = -1;
				elementIndex = -1;
				varIndex = -1;
			}

			/*
			* @brief Copy c-tor.
			* @param original - the original m_element
			*/
			m_element(const m_element& original) {
				compIndex = original.compIndex;
				elementIndex = original.elementIndex;
				varIndex = original.varIndex;
			}
		};

		///map of setters with their handles
		std::map<HWND, m_element> m_setters;

		///map of boolean setters with their handles
		std::map<HWND, m_element> m_boolSetters;

		///pointer to the component menu window.
		DialogWindow* m_compmenu;

	};
}
#endif Settings_c_H