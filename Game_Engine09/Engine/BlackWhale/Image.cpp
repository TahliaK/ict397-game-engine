#include "Image.h"
#include "Master.h"
#include "VertexBuffer.h"
#include "GraphicsEngine.h"
#include "SceneManager.h"
#include "Input\Input.h"
BOOST_CLASS_EXPORT_IMPLEMENT(Image)

Image::Image() : UserInterface() {
	Reflection();
	Initialize();
	static bool l_inList = false;
	if (!l_inList) {
		l_inList = true;
		Image* l_pointer = new Image;
		m_derivedClasses.push_back(l_pointer);
	}
}

Component* Image::NewComponent() const {
	Component* l_pointer = new Image;
	return l_pointer;
}

void Image::Clear() {
	m_data.Clear();
}

Image::~Image() {
	Clear();
}

void Image::Initialize() {
	m_name = "Image";
	m_material.path = "Resources/GUI/Button.png";
	if (!SetImage(m_material.path))
		cout << "IMAGE TEXTURE ERROR" << endl;
	m_material.ambient = RGBA(1, 1, 1);
}

bool Image::SetImage(boost::any var) {
	try {
		std::string l_temp = boost::any_cast<std::string> (var);
		if (m_material.path.compare(l_temp) == 0)
			return true;
		else
			m_material.path = l_temp;
		//clear
		m_material.texture = GraphicsEngine::LoadTexture(m_material.path, true);
		if (m_material.texture == 0)
			return false;
		return true;
	}
	catch (boost::bad_any_cast& /*bac*/) {
		return false;
	}
}

std::string Image::GetImage(void) const {
	return m_material.path;
}


void Image::Start() {
	Transform l_tr = *m_gameObject->GetTransform();

	m_data.attribLocation[0] = Master::gl.GetALoc(Master::s_TextShader.GetID(), "position");
	m_data.attribLocation[1] = Master::gl.GetALoc(Master::s_TextShader.GetID(), "texCoord");
	m_data.attribLocation[2] = Master::gl.GetALoc(Master::s_TextShader.GetID(), "normal");

	if (m_gameObject) {
		m_gameObject->SetLayer(1);
		m_gameObject->GetTransform()->scale = Vector3(100, 100, 0);
		m_gameObject->GetTransform()->rotation = Vector3(0, 0, 0);
		//z = 0 or 1
		m_gameObject->GetTransform()->position = Vector3(0, 0, 0);

		float minX = GetGameObject()->GetTransform()->position.x;
		float minY = GetGameObject()->GetTransform()->position.y;
		float maxX = GetGameObject()->GetTransform()->position.x + GetGameObject()->GetTransform()->scale.x;
		float maxY = GetGameObject()->GetTransform()->position.y + GetGameObject()->GetTransform()->scale.y;

		//convert to clip coordinates and convert OpenGL clip coordinates to Windows coordinate system (Upper-Left origin)
		float middleX = (float)Master::s_screen[2] / 2;
		float middleY = (float)Master::s_screen[3] / 2;

		float properMinX = (minX / middleX) - 1;
		float properMaxX = (maxX / middleX) - 1;

		float properMinY = ((maxY / middleY) - 1) * -1;
		float properMaxY = ((minY / middleY) - 1) * -1;
		properMinX = 0;
		properMinY = 0;
		properMaxX = 1;
		properMaxY = 1;
		if (m_data.data.vertex.size() != 0) {
			m_data.data.vertex.clear();
			GraphicsEngine::AddPolygon(properMinX, properMinY, properMaxX, properMaxY, m_data.data.vertex, false);
			Master::gl.DeleteBuffers(1, &m_data.VBO);
			Master::gl.DeleteBuffers(1, &m_data.VAO);
			m_data.Clear();
		}
		{
			GraphicsEngine::AddPolygon(properMinX, properMinY, properMaxX, properMaxY, m_data.data.vertex, false);
			GraphicsEngine::AddPolygon(properMinX, properMinX, properMaxX, properMaxY, m_data.data.texture, false);
			m_data.data.normal = vector<Vector3>(m_data.data.vertex.size());
			for (unsigned i = 0; i < m_data.data.vertex.size(); i++)
				m_data.data.vertex.at(i).z = GetGameObject()->GetTransform()->position.z;
		}
		if (!VertexBuffer::CreateVBO(&m_data, Master::s_TextShader.GetID()))
			cout << "ERROR IN IMAGE CLASS: CREATE VBO METHOD" << ENDL;
	}
	*m_gameObject->GetTransform() = l_tr;
	m_material.texture = GraphicsEngine::LoadTexture(m_material.path, true);
}


void Image::Update() {
	if (!m_gameObject || !m_gameObject->GetVisible())
		return;
	UserInterface::Update();
	Master::s_TextShader.Activate();
	if (m_material.texture == 0) {
		SetImage(m_material.path);
	}
	Matrix4 l_modelMatrix(((Camera*)m_camera.GetComponents().at(1))->GetModelMatrix());
	l_modelMatrix = Matrix4::Scale(l_modelMatrix, Vector4(m_gameObject->GetTransform()->scale.x / 50, m_gameObject->GetTransform()->scale.y / 50, m_gameObject->GetTransform()->scale.z, 0));
	l_modelMatrix = Matrix4::Rotate(l_modelMatrix, Vector4(0, 0, m_gameObject->GetTransform()->rotation.z, 0));
	l_modelMatrix = Matrix4::Translate(l_modelMatrix, Vector4(m_gameObject->GetTransform()->position.x / 50 - 1, m_gameObject->GetTransform()->position.y / 50 - 1, 0, 0));


	Master::gl.UniformMatrix4fv(Master::s_TextShader.GetLocation("model"), 1, GRAPHICS_FALSE, l_modelMatrix);
	Master::gl.BindTexture(GRAPHICS_TEXTURE_2D, m_material.texture);
	GraphicsEngine::SwitchToRGB(false);

	Master::gl.Uniform4fv(Master::s_TextShader.GetLocation("color"), 1, m_color.GetColor());// m_material.ambient.GetColor());
	Master::gl.Uniform1i(Master::s_TextShader.GetLocation("isSDF"), false);

	//Master::gl.TexEnvi(GRAPHICS_TEXTURE_ENV,GRAPHICS_TEXTURE_ENV_MODE,GRAPHICS_MODULATE);//affects the image's initial color
	VertexBuffer::RenderVBO(&m_data, 0, m_data.data.vertex.size());
};
void Image::Interactivity() {
	float l_sWidth = Master::s_screen[2] - Master::s_screen[0];
	float l_sHeight = Master::s_screen[3] - Master::s_screen[1];
	float minX = (m_gameObject->GetTransform()->position.x / 100 * l_sWidth);
	float maxX = (m_gameObject->GetTransform()->scale.x / 100 * l_sWidth) + minX;
	float maxY = (100 - m_gameObject->GetTransform()->position.y) / 100 * l_sHeight;
	float minY = (100 - (m_gameObject->GetTransform()->position.y + m_gameObject->GetTransform()->scale.y)) / 100 * l_sHeight;
	if (minX < Input::GetMousePosition()[0] && maxX > Input::GetMousePosition()[0] &&
		minY < Input::GetMousePosition()[1] && maxY > Input::GetMousePosition()[1]) {
		m_mouseOver = true;
	}
	else {
		m_mouseOver = false;
	}
}

void Image::Reflection(void) {
	m_Variables.push_back("Texture");
	m_Setters.push_back(boost::bind(&Image::SetImage, this, _1));
	m_Getters.push_back(boost::bind(&Image::GetImage, this));
}