#include "Camera.h"
#include "Master.h"
BOOST_CLASS_EXPORT_IMPLEMENT(Camera)
#include "FrustumCulling.h"
Camera::Camera() {
	Reflection();
	Initialize();
}

Camera::Camera(const Camera& original) :Component(original) {
	m_lockUpVector = original.m_lockUpVector;
	m_is3Dmode = original.m_is3Dmode;
	m_gameObject->GetTransform()->position = original.m_gameObject->GetTransform()->position;
	view = original.view;
	right = original.right;
	up = original.up;
	m_gameObject->GetTransform()->rotation = original.m_gameObject->GetTransform()->rotation;
	m_model = original.m_model;
	m_view = original.m_view;
	m_projection = original.m_projection;
}

void Camera::Initialize() {
	m_model.Identity();
	m_lockUpVector = false;
	m_is3Dmode = true;
	view = Vector3(0, 0, -1);
	right = Vector3(1, 0, 0);
	up = Vector3(0, 1, 0);

	static bool l_inList = false;
	if (!l_inList) {
		l_inList = true;
		Camera* l_pointer = new Camera;
		m_derivedClasses.push_back(l_pointer);
	}
	if (m_gameObject) {
		m_gameObject->GetTransform()->position = Vector3(0, 0, 0);
		m_gameObject->GetTransform()->rotation = Vector3(0);
	}
}

Component* Camera::NewComponent() const {
	Component* l_pointer = new Camera;
	return l_pointer;
}

void Camera::Start() {
	//	ResetView();
}

void Camera::Rotate(const Vector3 p_rotation) {
	//ResetView();
	for (unsigned i = 0; i < 3; i++) {
		if ((p_rotation[i] > 0 && m_gameObject->GetTransform()->rotation[i] < 0) || (p_rotation[i] < 0 && m_gameObject->GetTransform()->rotation[i] > 0))//to reset the rotation once the direction is changed
			m_gameObject->GetTransform()->rotation.Set(i, 0);

	}
	m_gameObject->GetTransform()->rotation = m_gameObject->GetTransform()->rotation + p_rotation;
	if (m_gameObject->GetTransform()->rotation.y > 360)      /* keep rotation within 360 degrees */
		m_gameObject->GetTransform()->rotation.y = 0.0;
	if (m_gameObject->GetTransform()->rotation.y < -360)     /* keep rotation within 360 degrees */
		m_gameObject->GetTransform()->rotation.y = 0.0;

	if (m_gameObject->GetTransform()->rotation.x > 90)       /* max angle (directly up) */
		m_gameObject->GetTransform()->rotation.x = 90.0;
	if (m_gameObject->GetTransform()->rotation.x < -90)      /* min angle (directly down) */
		m_gameObject->GetTransform()->rotation.x = -90.0;

	if (p_rotation.x != 0) {
		float l_x = p_rotation.x*PIdiv180;
		view = Vector3::Normalize((view * cos(l_x) + up * sin(l_x)));
		if (!m_lockUpVector)
			up = Vector3::Cross(view, right) * -1;
		right = Vector3::Cross(view, up);
	}
	if (p_rotation.y != 0) {
		float l_y = p_rotation.y*PIdiv180;
		view = Vector3::Normalize((view * cos(l_y) - right * sin(l_y)));
		right = Vector3::Cross(view, up);
	}
	/*view = Vector3::Normalize((view * cos(m_gameObject->GetTransform()->rotation.y * PIdiv180) - right * sin(m_gameObject->GetTransform()->rotation.y*PIdiv180)));
	right = Vector3::Cross(view, up);*/
	/*if (p_rotation.x != 0) {
		view = Vector3::Normalize((view * cos(m_gameObject->GetTransform()->rotation.x * PIdiv180) + up * sin(m_gameObject->GetTransform()->rotation.x*PIdiv180)));
		if (!m_lockUpVector)
			up = Vector3::Cross(view, right) * -1;
	}

	if (p_rotation.z != 0) {
		right = Vector3::Normalize((right * cos(m_gameObject->GetTransform()->rotation.z * PIdiv180) + up * sin(m_gameObject->GetTransform()->rotation.z*PIdiv180)));
		if (!m_lockUpVector)
			up = Vector3::Cross(view, right) * -1;
	}*/
	//ResetView();
}


bool Camera::SetLockUp(boost::any var) {
	try {
		m_lockUpVector = boost::any_cast<bool> (var);
	}
	catch (boost::bad_any_cast& /*bac*/) {
		return false;
	}
	return true;
}
bool Camera::GetLockUp(void) const {
	return m_lockUpVector;
}
bool Camera::SetMode(boost::any var) {
	try {
		m_is3Dmode = boost::any_cast<bool> (var);
	}
	catch (boost::bad_any_cast& /*bac*/) {
		return false;
	}
	return true;
}
bool Camera::GetMode(void) const {
	return m_is3Dmode;
}

void Camera::Reflection() {
	m_Variables.push_back("Lock Up Vector?");
	m_Setters.push_back(boost::bind(&Camera::SetLockUp, this, _1));
	m_Getters.push_back(boost::bind(&Camera::GetLockUp, this));

	m_Variables.push_back("Is 3D?");
	m_Setters.push_back(boost::bind(&Camera::SetMode, this, _1));
	m_Getters.push_back(boost::bind(&Camera::GetMode, this));
}

void Camera::Move(const Vector3 p_direction) {
	//ResetView();
	if (p_direction.x != 0) {
		if (!m_lockUpVector)
			m_gameObject->GetTransform()->position = m_gameObject->GetTransform()->position + view * (p_direction.x * -1);
		else {
			Vector3 temp = view * (p_direction.x * -1);
			m_gameObject->GetTransform()->position.x += temp.x;
			m_gameObject->GetTransform()->position.z += temp.z;
		}
	}
	if (p_direction.y != 0)
		m_gameObject->GetTransform()->position = m_gameObject->GetTransform()->position + up * p_direction.y;
	if (p_direction.z != 0)
		m_gameObject->GetTransform()->position = m_gameObject->GetTransform()->position + right * p_direction.z;
	ResetView();
}


void Camera::InvertPitch() {
	//ResetView();
	m_gameObject->GetTransform()->rotation.x = -m_gameObject->GetTransform()->rotation.x;
	view = Vector3::Normalize((view * cos(m_gameObject->GetTransform()->rotation.x * PIdiv180) + up * sin(m_gameObject->GetTransform()->rotation.x*PIdiv180)));
	if (!m_lockUpVector)
		up = Vector3::Cross(view, right) * -1;
	//ResetView();
}

void Camera::Update() {
	//m_model.Identity();	
	ResetProjection();
	ResetView();
}

void Camera::ResetView(void) {
	m_view.Identity();
	if (m_is3Dmode) {
		m_view = LookAt(m_gameObject->GetTransform()->position, Vector3(m_gameObject->GetTransform()->position + view), up);
		m_frustum.LookAt(m_gameObject->GetTransform()->position, Vector3(m_gameObject->GetTransform()->position + view), up);
	}
}

void Camera::ResetProjection(void) {
	m_projection.Identity();
	if (m_is3Dmode) {
		float ratio = 1.0f * ((float)(Master::s_screen[2]) / (Master::s_screen[3]));
		m_projection = Perspective(45, ratio, 0.01, 2500);
		m_frustum.Perspective(45, ratio, 0.01, 2500);
	}
	else {
		m_projection = Ortho(Master::s_screen[0], Master::s_screen[0] + Master::s_screen[2], Master::s_screen[3] + Master::s_screen[1], Master::s_screen[1], -1, 1);
	}
}

bool Camera::Contains(Vector3 p_point) {
	return m_frustum.Contains(p_point);
}
bool Camera::Contains(ColliderAABB* p_box) {
	return m_frustum.Contains(p_box);
}

Matrix4& Camera::GetProjectionMatrix() {
	return m_projection;
}

Matrix4& Camera::GetModelMatrix() {
	return m_model;
}

Matrix4& Camera::GetViewMatrix() {
	return m_view;
}

Vector3 Camera::GetTarget() const {
	Vector3 temp = Vector3(m_gameObject->GetTransform()->position + view);
	return temp;
}

Matrix4 Camera::Ortho(float p_left, float p_right, float p_bottom, float p_top, float p_near, float p_far) {
	Matrix4 l_projection;
	l_projection.rows[0].Set(0, 2 / (p_right - p_left));
	l_projection.rows[1].Set(1, 2 / (p_top - p_bottom));
	l_projection.rows[2].Set(2, 2 / (p_far - p_near));

	l_projection.rows[0].Set(3, -(p_right + p_left) / (p_right - p_left));
	l_projection.rows[1].Set(3, -(p_top + p_bottom) / (p_top - p_bottom));
	l_projection.rows[2].Set(3, -(p_far + p_near) / (p_far - p_near));
	return l_projection;
}

Matrix4 Camera::Perspective(float p_fov, float p_aspect, float p_near, float p_far) {
	p_fov /= 2;

	float l_top = p_near * tan(RADIANS(p_fov));
	float l_bottom = -l_top;
	float l_right = l_top * p_aspect;
	float l_left = -l_right;

	Matrix4 l_projection;
	//l_projection.rows[3].Set(2, -1);
	//l_projection.rows[2].Set(3, -2*p_far*p_near/(p_far-p_near));
	//l_projection.rows[0].Set(2, (l_right+l_left)/(l_right-l_left));
	//l_projection.rows[1].Set(2, (l_top+l_bottom)/(l_top-l_bottom));

	l_projection.rows[3].Set(3, 0);

	l_projection.rows[0].Set(0, 2 * p_near / (l_right - l_left));
	l_projection.rows[1].Set(1, 2 * p_near / (l_top - l_bottom));
	l_projection.rows[2].Set(2, -(p_far + p_near) / (p_far - p_near));

	l_projection.rows[2].Set(0, (l_right + l_left) / (l_right - l_left));
	l_projection.rows[2].Set(1, (l_top + l_bottom) / (l_top - l_bottom));

	l_projection.rows[2].Set(3, -1);
	l_projection.rows[3].Set(2, -2 * p_far*p_near / (p_far - p_near));
	return l_projection;
}

Matrix4 Camera::LookAt(Vector3 pos, Vector3 view, Vector3 up) {
	Vector3 Z = Vector3::Normalize(pos - view);
	Vector3 X = Vector3::Normalize(Vector3::Cross(up, Z));
	Vector3 Y = Vector3::Cross(Z, X);
	Matrix4 Orientation(
		Vector4(X.x, Y.x, Z.x, 0),
		Vector4(X.y, Y.y, Z.y, 0),
		Vector4(X.z, Y.z, Z.z, 0),
		Vector4(0, 0, 0, 1));
	Matrix4 Translation;
	Translation.rows[3] = Vector4(-pos.x, -pos.y, -pos.z, 1);
	return  Translation * Orientation;
}