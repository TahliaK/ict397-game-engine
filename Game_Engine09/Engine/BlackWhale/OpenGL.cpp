#include "OpenGL.h"

#include <GL\glew.h>
#include "GL\glfw3.h"
//#include "GL\gl.h"
#include <GL\freeglut.h>

void OpenGL::ShowCursor(bool show) {
	/*if (!show)
		SDL_ShowCursor(SDL_DISABLE);
	else
		SDL_ShowCursor(SDL_ENABLE);*/
}

void OpenGL::LockCursor(int p_x, int p_y) {
	SetCursorPos(p_x, p_y);
	//glutWarpPointer(p_x, p_y);
	//need to change later
	//SDL_WarpMouseGlobal(p_x, p_y);
}
void OpenGL::Color(RGBA color) {
	glColor4fv(color.GetColor());
}

void OpenGL::RasterPos(Vector3 position) {
	glRasterPos3i(position.x, position.y, position.z);
}
void OpenGL::GetActiveUniform(unsigned int programID, unsigned int location, const int bufSize, int* length, int* size, unsigned int* type, char* name) {
	glGetActiveUniform(programID, location, bufSize, length, size, type, name);
}

void OpenGL::GetActiveAttribute(unsigned int programID, unsigned int location, const int bufSize, int* length, int* size, unsigned int* type, char* name) {
	glGetActiveAttrib(programID, location, bufSize, length, size, type, name);
}
void OpenGL::BitmapString(void* p_font, std::string p_text, Vector3 position) {
	glPushMatrix();
	glScalef(0.1, -0.1, 0.1);
	glTranslatef(0, -500, 0);
	for (unsigned l_index = 0; l_index < p_text.size(); l_index++) {
		if (p_text.at(l_index) == '\n')
			RasterPos(Vector3(position.x, position.y += 13, position.z));
		else
			//glutStrokeCharacter(GLUT_STROKE_ROMAN, p_text.at(l_index));
			glutBitmapCharacter(p_font, p_text.at(l_index));
	}

	glPopMatrix();
	/*char file[50];
	int h = 12;
	int w = 8;
	int TextureName[10];
	for(unsigned l_index = 0; l_index < p_text.size(); l_index++){
	if(p_text.at(l_index) != ' '){
	sprintf(file,"font1_%c.bmp", tolower(p_text[l_index]));
	BindTexture(TEXTURE_2D,TextureName[(int)file]);
	}
	}*/
}


void OpenGL::Disable(unsigned int state) {
	glDisable(state);

}

void OpenGL::Enable(unsigned int state) {
	glEnable(state);
}

void OpenGL::BindTexture(unsigned int target, unsigned int texture) {
	glBindTexture(target, texture);
}


void OpenGL::ActiveTexture(unsigned int ID) {
	glActiveTexture(ID);
}

void  OpenGL::MatrixMode(unsigned int p_mode) {
	glMatrixMode(p_mode);
}
void  OpenGL::LoadIdentity() {
	glLoadIdentity();
}
void  OpenGL::Viewport(int p_x, int p_y, int p_w, int p_h) {
	glViewport(p_x, p_y, p_w, p_h);
}
void  OpenGL::Ortho(float p_left, float p_right, float p_top, float p_bottom, float p_near, float p_far) {
	glOrtho(p_left, p_right, p_top, p_bottom, p_near, p_far);
}
void  OpenGL::Perspective(float FOV, float aspect, float p_near, float p_far) {
	gluPerspective(FOV, aspect, p_near, p_far);
}

void OpenGL::ClearColor(float p_red, float p_blue, float p_green, float p_alpha) {
	glClearColor(p_red, p_green, p_blue, p_alpha);
}
void OpenGL::ClearColor(RGBA p_color) {
	glClearColor(p_color.r, p_color.g, p_color.b, p_color.a);
}
void OpenGL::Clear(unsigned int p_mask) {
	glClear(p_mask);
}

void OpenGL::PushMatrix() {
	glPushMatrix();
}
void OpenGL::PopMatrix() {
	glPopMatrix();
}
void OpenGL::SwapBuffers() {
	glutSwapBuffers();
}
void OpenGL::PostRedisplay() {
	glutPostRedisplay();
}

void OpenGL::Init(int *p_argc, char **p_argv) {
	glutInit(p_argc, p_argv);

	GLint l_result = glewInit();
	if (GLEW_OK != l_result)
	{
		cout << "ERROR: " << glewGetErrorString(l_result) << endl;
	}
}

void OpenGL::InitDisplayMode(unsigned int p_mode) {
	glutInitDisplayMode(p_mode);
}

void OpenGL::SetWindowRect(int p_x, int p_y, int p_w, int p_h) {
	glutInitWindowPosition(p_x, p_y);
	glutInitWindowSize(p_w, p_h);
}

void OpenGL::InitializeWindow(std::string p_windowName) {
	glutCreateWindow(p_windowName.c_str());
}

void OpenGL::EnterMainLoop() {
	glutMainLoop();
}

unsigned int OpenGL::GetError() {
	return glGetError();
}

void OpenGL::Uniform1fv(int location, int size, const float* value) {
	glUniform1fv(location, size, value);
}

void OpenGL::Uniform2fv(int location, int size, const float* value) {
	glUniform2fv(location, size, value);
}

void OpenGL::Uniform4fv(int location, int size, const float* value) {
	glUniform4fv(location, size, value);
}

void OpenGL::Uniform4fv(int location, int size, const Vector4& value) {
	glUniform4fv(location, size, &value.x);
}

void OpenGL::Uniform3fv(int location, int size, const Vector3& value) {
	glUniform3fv(location, size, &value.x);
}

void OpenGL::Uniform3fv(int location, int size, const float* value) {
	glUniform3fv(location, size, value);
}

void OpenGL::UniformMatrix4fv(int location, int count, unsigned char transpose, const float* value) {
	glUniformMatrix4fv(location, count, transpose, value);
}

void OpenGL::UniformMatrix3fv(int location, int count, unsigned char transpose, const float* value) {
	glUniformMatrix3fv(location, count, transpose, value);
}

void OpenGL::UniformMatrix4fv(int location, int count, unsigned char transpose, const Matrix4& value) {
	Matrix4 temp(value);
	glUniformMatrix4fv(location, count, transpose, &temp.Get()[0]);
}



void OpenGL::Uniform1f(int location, float value) {
	glUniform1f(location, value);
}

void OpenGL::Uniform1i(int location, int value) {
	glUniform1i(location, value);
}

void OpenGL::Uniform1iv(int location, int size, const int* value) {
	glUniform1iv(location, size, value);
}


int OpenGL::GetALoc(unsigned int shaderID, std::string attribName) {
	return glGetAttribLocation(shaderID, attribName.c_str());
}

int OpenGL::GetULoc(unsigned int shaderID, std::string uniformName) {
	return glGetUniformLocation(shaderID, uniformName.c_str());
}

std::string OpenGL::GetProgramInfoLog(unsigned int shaderID) {
	int length = 0;
	int amountWritten = 0;
	char* log;
	std::string result;
	GetProgramiv(shaderID, GL_INFO_LOG_LENGTH, &length);

	if (length > 0) {
		log = (char*)malloc(length);
		glGetProgramInfoLog(shaderID, length, &amountWritten, log);
		result = string(log);
		free(log);
	}
	return result;
}

void OpenGL::UseProgram(unsigned int shaderID) {
	glUseProgram(shaderID);
}

void OpenGL::LinkProgram(unsigned int shaderID) {
	glLinkProgram(shaderID);
}

void OpenGL::GetProgramiv(unsigned int shaderID, unsigned int pname, int* param) {
	glGetProgramiv(shaderID, pname, param);
}

void OpenGL::DeleteProgram(unsigned int programID) {
	glDeleteProgram(programID);
}

void OpenGL::AttachShader(unsigned int programID, unsigned int shaderID) {
	glAttachShader(programID, shaderID);
}

void OpenGL::DetachShader(unsigned int programID, unsigned int shaderID) {
	glDetachShader(programID, shaderID);
}

void OpenGL::DeleteShader(unsigned int shaderID) {
	glDeleteShader(shaderID);
}
unsigned int OpenGL::CreateShader(unsigned int shaderType) {
	return glCreateShader(shaderType);
}
void OpenGL::ShaderSource(unsigned int shaderID, int numOfStrings, const char** strings, int* lengthOfStrings) {
	glShaderSource(shaderID, numOfStrings, strings, lengthOfStrings);
}

void OpenGL::CompileShader(unsigned int shaderID) {
	glCompileShader(shaderID);
}

void OpenGL::GetShaderiv(unsigned int shaderID, unsigned int pname, int* param) {
	glGetShaderiv(shaderID, pname, param);
}

std::string OpenGL::GetShaderInfoLog(unsigned int shaderID) {
	int length = 0;
	int amountWritten = 0;
	char* log;
	string result;
	GetShaderiv(shaderID, GL_INFO_LOG_LENGTH, &length);

	if (length > 0) {
		log = (char*)malloc(length);
		glGetShaderInfoLog(shaderID, length, &amountWritten, log);
		result = string(log);
		free(log);
	}
	return result;
}

unsigned int OpenGL::CreateProgram() {
	return glCreateProgram();
}

void OpenGL::DeleteBuffers(int p_amount, unsigned int* p_bufferIndex) {
	glDeleteBuffers(p_amount, p_bufferIndex);
}

void OpenGL::DeleteTextures(int p_amount, unsigned int* p_textureIndex) {
	glDeleteTextures(p_amount, p_textureIndex);
}

void OpenGL::GenFB(int amount, unsigned int* buffer) {
	glGenFramebuffers(amount, buffer);
}
void OpenGL::GenTexture(int amount, unsigned int* buffer) {
	glGenTextures(amount, buffer);
}
void OpenGL::BindRB(unsigned int target, unsigned int buffer) {
	glBindRenderbuffer(target, buffer);
}
void OpenGL::GenRB(int amount, unsigned int* buffer) {
	glGenRenderbuffers(amount, buffer);
}
void OpenGL::RBstorage(unsigned int target, unsigned int internalformat, unsigned int w, unsigned int h) {
	glRenderbufferStorage(target, internalformat, w, h);
}
void OpenGL::FBRB(unsigned int target, unsigned int attachment, unsigned int original, unsigned int buffer) {
	glFramebufferRenderbuffer(target, attachment, original, buffer);
}
void OpenGL::DeleteFB(int amount, unsigned int* buffer) {
	glDeleteFramebuffers(amount, buffer);
}
void OpenGL::DeleteRB(int amount, unsigned int* buffer) {
	glDeleteRenderbuffers(amount, buffer);
}
void OpenGL::BindFB(unsigned int target, unsigned int buffer) {
	glBindFramebuffer(target, buffer);
}
void OpenGL::FBtexture(unsigned int target, unsigned int attachment, unsigned int texture, int level) {
	glFramebufferTexture(target, attachment, texture, level);
}


void OpenGL::TexImage2D(unsigned int target, int level, int internalformat, int width, int height, int border, unsigned int format, unsigned int type, const void *pixels) {
	glTexImage2D(target, level, internalformat, width, height, border, format, type, pixels);
}
void OpenGL::TexParami(unsigned int target, unsigned int pname, int param) {
	glTexParameteri(target, pname, param);
}

void OpenGL::TexParamf(unsigned int target, unsigned int pname, float param) {
	glTexParameteri(target, pname, param);
}

void OpenGL::DrawBuffers(int amount, const unsigned int* buffer) {
	glDrawBuffers(amount, buffer);
}
void OpenGL::DrawBuffer(unsigned int mode) {
	glDrawBuffer(mode);
}

void OpenGL::ReadBuffer(unsigned int attachment) {
	glReadBuffer(attachment);
}

unsigned int OpenGL::CheckFBstatus(unsigned int target) {
	return glCheckFramebufferStatus(target);
}

void OpenGL::TexEnvi(unsigned int target, unsigned int pname, int param) {
	glTexEnvi(target, pname, param);
}
void OpenGL::PolygonMode(unsigned int face, unsigned int mode) {
	glPolygonMode(face, mode);
}

void OpenGL::BindBuffer(unsigned int target, unsigned int buffer) {
	glBindBuffer(target, buffer);
}
void OpenGL::GenVAO(int amount, unsigned int* buffer) {
	glGenVertexArrays(amount, buffer);
}
void OpenGL::BindVAO(unsigned int buffer) {
	glBindVertexArray(buffer);
}
void OpenGL::GenBuffer(int amount, unsigned int* buffer) {
	glGenBuffers(amount, buffer);
}
void OpenGL::BufferSubData(unsigned int target, int offset, int size, const void* data) {
	glBufferSubData(target, offset, size, data);
}

void OpenGL::BufferData(unsigned int target, int size, const void* data, unsigned int usage) {

	glBufferData(target, size, data, usage);
}

void OpenGL::EnableVertexAttribArray(unsigned int index) {
	glEnableVertexAttribArray(index);
}

void OpenGL::VertexAttribPointer(unsigned int index, int size, unsigned int type, unsigned char normalized, int stride, const void* pointer) {
	glVertexAttribPointer(index, size, type, normalized, stride, pointer);
}

void OpenGL::DrawArrays(unsigned int mode, int first, int count) {
	glDrawArrays(mode, first, count);
}

void OpenGL::DrawElements(unsigned int mode, int count, int type, void* indices) {
	glDrawElements(mode, count, type, indices);
}

void OpenGL::FbTexture2D(unsigned int fb, unsigned int attachment, unsigned int textarget, unsigned int texture, int level) {
	glFramebufferTexture2D(fb, attachment, textarget, texture, level);
}

void OpenGL::BlendFunc(unsigned int sfactor, unsigned int dfactor) {
	glBlendFunc(sfactor, dfactor);
}
void OpenGL::AlphaFunc(unsigned int func, float ref) {
	glAlphaFunc(func, ref);
}

void OpenGL::DepthMask(unsigned char flag) {
	glDepthMask(flag);
}
void OpenGL::DepthFunc(unsigned int func) {
	glDepthFunc(func);
}
void OpenGL::CullFace(unsigned int mode) {
	glCullFace(mode);
}