#ifndef MESHRENDERER_H
#define MESHRENDERER_H
#include "Renderer.h"
#include "Component.h"
#include "ShaderProgram.h"
#include "Math_s.h"
#include "Mesh.h"

/*
* @class MeshRenderer
* @brief Used for displaying 3D models within the game world.
* @version 01
* @date 03/04/2017
* @author Olesia Kochergina
*/
class MeshRenderer: public Component{
public:

	/*
	* @brief Default c-tor.
	*/
	MeshRenderer();

	/*
	* @brief Constructor.
	* @param shaderProgram - shader program ID
	* @param mesh - mesh that represents a 3D model.
	*/
	MeshRenderer(unsigned  int shaderProgram, Mesh* mesh);

	/*
	* @brief Constructor.
	* @param shaderProgram - shader program ID
	* @param meshFile - file name of a model.
	*/
	MeshRenderer(unsigned int  shaderProgram, std::string meshFile);

	/*
	* @brief Destructor.
	*/
	~MeshRenderer();

	/*
	* @brief Initializes the object, called when creating the object.
	*/
	void Initialize();
	
	/*
	* @brief Called just before the first update.
	*/
	void Start();

	/*
	* @brief Updates the object and should be called every frame.
	*/
	void Update();
	
	/*
	* @brief Clears internal data, should be called when
	* the object needs to be either reused or deleted.
	*/
	void Clear(void) {};
	
	/*
	* @brief Creates a new component of this class.
	* @return Pointer to the new component or NULL.
	*/
	Component* NewComponent(void) const;

	/**
	* @brief Sets the mesh of this object to the parameter's value.
	* @param mesh - pointer to an object Mesh class.
	*/
	const bool SetMesh(boost::any mesh);

	/**
	* @brief Updates the vertex shader of this instance.
	* @param vertexShader - string containing the name of a file with the shader.
	*/
	const bool SetVertexShader(boost::any vertexShader);

	/**
	* @brief Updates the fragment shader of this instance.
	* @param fragmentShader - string containing the name of a file with the shader.
	*/
	const bool SetFragmentShader(boost::any fragmentShader);

	/**
	* @brief Updates the 3D model of this instance.
	* @param fileName - file name of the 3d model.
	*/
	const bool SetMeshFile(boost::any fileName);

	/*
	* @brief Returns the name of the vertex shader file.
	* @return name of the file
	*/
	const std::string GetVertexShader(void) const;

	/*
	* @brief Returns the name of the fragment shader file.
	* @return name of the file.
	*/
	const std::string GetFragmentShader(void) const;
	
	/*
	* @brief Returns the name of the 3D model.
	* @return file name
	*/
	const std::string GetMeshFile(void) const;
	
	/*
	* @brief Returns a pointer to the 3d model of this object.
	* @return pointer to the 3D mesh.
	*/
	Mesh* GetMesh(void) const;
private:

	///file of the model	
	std::string m_fileName;

	///vertex shader file name
	std::string m_vS;

	///fragment shader file name
	std::string  m_fS;

	///pointer to the mesh 
	Mesh* m_mesh;

	///ID of the shader program
	unsigned int m_shaderProgram;

	/*
	* @brief Adds values to three vectors: variable name,  setter, getter.
	* Used in the Editor to modify components at run-time.
	*/
	void Reflection();

	/*
	* @brief Adds one instance of the class to the collection of derived components.
	*/
	void AddToList();

	///serialization
	friend class boost::serialization::access;
	template<class Archive>
	void save(Archive& ar, const unsigned int /*version*/) const {
		ar & SER_BASE_OBJECT_NVP(Component);
		ar & SER_NVP(m_fileName);
		ar & SER_NVP(m_vS);
		ar & SER_NVP(m_fS);
	}

	template<class Archive>
	void load(Archive& ar, const unsigned int /*version*/) {
		ar & SER_BASE_OBJECT_NVP(Component);
		ar & SER_NVP(m_fileName);
		ar & SER_NVP(m_vS);
		ar & SER_NVP(m_fS);
	}
	SER_SPLIT_MEMBER()
};
SER_CLASS_VERSION(MeshRenderer, 0);
SER_CLASS_EXPORT_KEY(MeshRenderer)
#endif MESHRENDERER_H