#ifndef TERRAINFAULTFORMATION_H
#define TERRAINFAULTFORMATION_H
#include "Terrain.h"

/*
* @brief Generates a 3d terrain using fault formation
* @author Olesia Kochergina
* @version 01
* @date 29/03/2017
*/
class TerrainFaultFormation : public Terrain {

public:

	/*
	* @brief Default constructor.
	*/
	TerrainFaultFormation();

	/*
	* @brief The destructor.
	*/
	~TerrainFaultFormation();

	/*
	* @brief Called just before the first update.
	*/
	void Start(void);

	/*
	* @brief Deletes the dynamically allocated heightmap array.
	*/
	void Clear(void);

	/*
	* @brief Creates a new component of this class.
	* @return Pointer to the new component or NULL.
	*/
	Component* NewComponent(void) const;

	/*
	* @brief Updates the terrain based on the amount of iterations.
	* @param iterations - number of iterations
	* @return true - updated
	*/
	bool SetNoIterations(boost::any iterations);

	/*
	* @brief Returns the current No of iterations.
	* @return number of iterations.
	*/
	int GetNoIterations();

private:

	///current number of iterations
	unsigned int m_iterations;

	/*
	* @brief Generates a new terrain.
	*/
	void Generate(void);

	/*
	* @brief Generates a 3D mesh.
	* @param p_vertexPoints - vertices of the mesh
	* @param p_texturePoints - texture coords of the mesh
	* @param p_normalPoints - normals of the mesh
	* @param p_buffer - reference to a structure which will hold the mesh.
	*/
	void GenerateMesh(std::vector<Vector3> p_vertexPoints, std::vector<Vector3> p_texturePoints, std::vector<Vector3> p_normalPoints, vBuffer& p_buffer);

	/*
	* @brief Adds values to three vectors: variable name,  setter, getter.
	* Used in the Editor to modify components at run-time.
	*/
	void Reflection();

	///serialization
	friend class boost::serialization::access;
	template<class Archive>
	void save(Archive& ar, const unsigned int /*version*/) const {
		ar & SER_BASE_OBJECT_NVP(Terrain);
		ar& SER_NVP(m_iterations);
	}

	template<class Archive>
	void load(Archive& ar, const unsigned int /*version*/) {
		ar & SER_BASE_OBJECT_NVP(Terrain);
		ar& SER_NVP(m_iterations);
	}
	SER_SPLIT_MEMBER()
};

SER_CLASS_VERSION(TerrainFaultFormation, 0);
SER_CLASS_EXPORT_KEY(TerrainFaultFormation)
#endif TERRAINFAULTFORMATION_H