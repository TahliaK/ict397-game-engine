#ifndef TERRAIN_H
#define TERRAIN_H
#include "../Struct.h"
#include "../Component.h"
#include "../Material_s.h"

/*
* @brief Base Terrain class used for rendering 3D landscapes.
* @author Olesia Kochergina
* @version 02
* @date 12/03/2017
*/
class Terrain : public Component {
public:

	/*
	* @brief Default c-tor.
	*/
	Terrain();

	/*
	* @brief Destructor.
	*/
	virtual ~Terrain();

	/*
	* @brief Initializes the object, called when creating the object.
	*/
	virtual void Initialize();

	/*
	* @brief Called just before the first update.
	*/
	virtual void Start();

	/*
	* @brief Updates the object and should be called every frame.
	*/
	virtual void Update();

	/*
	* @brief Clears internal data, should be called when
	* the object needs to be either reused or deleted.
	*/
	virtual void Clear();

	/*
	* @brief Creates a new component of this class, should be overriden by children.
	* @return Pointer to the new component or NULL.
	*/
	virtual Component* NewComponent() const = 0;

	/*
	* @brief Sets the smooth step and updates the terrain.
	* The smooth step is used to smooth height values based on the Moore
	* neighbourhood.
	* @param smoothStep - an unsigned integer representing the smooth step.
	* @return Returns true if the parameter's value is valid.
	*/
	bool SetSmoothStep(boost::any smoothStep);

	/*
	* @brief Sets the currently modified texture index.
	* If the index is equals to the amount of textures then it adds a new texture
	* @param textureIndex - the texture index as an unsigned integer.
	* @return true - if new texture or existing texture
	*/
	const bool SetTextureIndex(boost::any textureIndex);

	/*
	* @brief Updates the current texture's file name.
	* @param textureName - a string containing the file name.
	* @return true - if the texture is set.
	*/
	const bool SetTextureName(boost::any textureName);

	/*
	* @brief Updates the texture's minimum height.
	* @param minimumHeight - float variable with the minimum height.
	* @return true - if the height is set.
	*/
	const bool SetTextureMinHeight(boost::any minimumHeight);

	/*
	* @brief Updates the texture's maximum height.
	* @param minimumHeight - float variable with the maximum height.
	* @return true - if the height is set.
	*/
	const bool SetTextureMaxHeight(boost::any maximumHeight);

	/*
	* @brief Sets the terrain's detail map.
	* @param detailMapFile - the file name of the detail map texture.
	* @return true - the file is found and the texture is set.
	*/
	const bool SetDetailMap(boost::any detailMapFile);

	/*
	* @brief Sets the terrain's noise map.
	* @param noiseMapFile - the file name of the noise map texture.
	* @return true - the file is found and the texture is set.
	*/
	const bool SetNoiseMap(boost::any noiseMapFile);

	/*
	* @brief Returns the currently modified terrain texture.
	* @return the index of the texture.
	*/
	int GetTextureIndex(void) const;

	/*
	* @breif Returns the currently modified texture's file name.
	* @return the file name of the texture.
	*/
	std::string GetTextureName(void) const;

	/*
	* @brief Returns the minimum height of the currently modified terrain texture.
	* @return the starting position of the texture on the y-axis.
	*/
	float GetTextureMinHeight(void) const;

	/*
	* @brief Returns the minimum height of the currently modified terrain texture.
	* @return the ending position of the texture on the y-axis.
	*/
	float GetTextureMaxHeight(void) const;

	/*
	* @brief Returns the current smooth step.
	* @return smooth step.
	*/
	int GetSmoothStep(void) const;

	/*
	* @brief Returns the height of the terrain at a given position.
	* @param worldX - world position on the x-axis
	* @param worldZ - world position on the z-axis.
	* @return y-axis value.
	*/
	float GetHeight(float worldX, float worldZ);

	/*
	* @brief Returns the terrain's detail map.
	* @return the file name of the detail map.
	*/
	std::string GetDetailMap(void) const;

	/*
	* @brief Returns the terrain's noise map.
	* @return the file name of the noise map.
	*/
	std::string GetNoiseMap(void) const;

	/*const bool SetVertexShader(boost::any vertexShader);
	const bool SetFragmentShader(boost::any fragmentShader);
	std::string GetVertexShader(void) const;
	std::string GetFragmentShader(void) const;*/


	//Tahlia added in 15/5/2017
	const int getSize() {return m_width;}
protected:

	/*
	* @brief Generates a new terrain.
	*/
	virtual void Generate() = 0;

	/**
	* @brief Loops through all vertices and calls a function to change their heights.
	*/
	void Smooth();

	/**
	* @brief Smoothes the current terrain using Moore neighbourhood.
	* @param x - x-axis position on the 2D grid
	* @param y - y-axis position on the 2D grid
	* @param maxX - width of the grid
	* @param maxY - height of the grid
	* @param height - the current height of the terrain vertex.
	* @return updated height value
	*/
	float ChangeHeight(const int x, const int y, const int maxX, const int maxY, const float height) const;

	/*
	* @brief Adds values to three vectors: variable name,  setter, getter.
	* Used in the Editor to modify components at run-time.
	*/
	void Reflection();

	///shader program of this terrain
	unsigned int m_shaderProgram;

	///the amount of iterations when smoothing a terrain
	int m_smoothStep;

	///width of the terrain grid
	int m_width;

	///height of the terrain grid
	int m_height;

	///a structure which contains all info about the terrain mesh and its vertex buffer
	vBuffer m_data;

	///Contains every height value on the terrain; used in the GetHeight function
	vector<float> m_heights;

	/*std::string m_vS;
	std::string  m_fS;*/
	//Quadtree<Vector3> m_quadtree;

private:

	///detail map texture ID 
	unsigned int m_detailMap;

	///detail map file name
	std::string m_detailMapFile;

	///noise map texture ID
	unsigned int m_noiseMap;

	///noise map file name
	std::string m_noiseMapFile;

	///the currently modified texture
	int m_curTextureIndex;

	///list of all multitextures for this terrain
	std::vector<int> m_textures;

	///list of all multitexture names
	std::vector<std::string> m_nameTextures;

	///list of all minimum heights for each terrain texture
	std::vector<float> m_minTextures;

	///list of all maximum heights for each terrain texture
	std::vector<float> m_maxTextures;

	///serialization
	friend class boost::serialization::access;
	template<class Archive>
	void save(Archive& ar, const unsigned int /*version*/) const {
		ar & SER_BASE_OBJECT_NVP(Component);
		/*ar & heightmap;
		ar & m_box;

		ar & m_mat1;
		ar & m_data;*/
		ar & SER_NVP(m_smoothStep);
		//ar & SER_NVP(m_fS);
		//ar & SER_NVP(m_vS);
		ar & SER_NVP(m_nameTextures);
		ar & SER_NVP(m_minTextures);
		ar & SER_NVP(m_maxTextures);
		ar & SER_NVP(m_noiseMapFile);
		ar & SER_NVP(m_detailMapFile);
	}

	template<class Archive>
	void load(Archive& ar, const unsigned int /*version*/) {
		ar & SER_BASE_OBJECT_NVP(Component);
		ar & SER_NVP(m_smoothStep);
		//ar & SER_NVP(m_fS);
		//ar & SER_NVP(m_vS);
		/*ar & heightmap;
		ar & m_box;

		ar & m_mat1;
		ar & m_data;*/
		ar & SER_NVP(m_nameTextures);
		ar & SER_NVP(m_minTextures);
		ar & SER_NVP(m_maxTextures);
		ar & SER_NVP(m_noiseMapFile);
		ar & SER_NVP(m_detailMapFile);
	}
	SER_SPLIT_MEMBER()
};
SER_CLASS_VERSION(Terrain, 0);
SER_ABSTRACT(Terrain)
SER_CLASS_EXPORT_KEY(Terrain)

//Tahlia addition
class TerrainTracker {
public:
	static Terrain * CurrentTerrain() { return c_terrain;}
	static void SetCurrentTerrain(Terrain * t) { c_terrain = t;}
private:
	static Terrain * c_terrain;
};

#endif TERRAIN_H

/*void Compress(vector<Vertex3>& tempV,vector<Vertex3>& tempT, vector<Vertex3>& tempN);
void Update_Mesh(int iteration, vector<Vertex3> m_vArray, vector<Triangle3> trigV, vector<Ref> refs);
double calculate_error(int id_v1, int id_v2, Vector3 &p_result, vector<Vertex3> m_vArray, vector<Triangle3> trigV, vector<Ref> refs);
double Terrain::vertex_error(SymmetricMatrix q, double x, double y, double z);
void compact_mesh(vector<Vertex3> m_vArray, vector<Triangle3> trigV, vector<Ref> refs);
void update_triangles(int i0,Vertex3 &v,std::vector<int> &deleted,int &deleted_triangles, vector<Vertex3> m_vArray, vector<Triangle3> trigV, vector<Ref> refs);
bool flipped(Vector3 p, int i0, int i1, Vertex3 &v0, Vertex3 &v1,vector<int> &deleted, vector<Vertex3> m_vArray, vector<Triangle3> trigV, vector<Ref> refs);
*/