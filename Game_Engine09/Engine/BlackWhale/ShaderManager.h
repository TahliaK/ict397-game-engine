#ifndef  SHADER_MANAGER_H
#define SHADER_MANAGER_H
#include "ShaderProgram.h"
#include <vector>

/*
* @class ShaderManager
* @brief Manages shader programs
* @author Olesia Kochergina
* @date 12/04/2017
*/
class ShaderManager {

public:
	
	/*
	* @brief Activates a shader program by its ID.
	* @param ID - id of the shader program
	* @return pointer to the shader program
	*/
	static ShaderProgram* ActivateShaderProgram(unsigned int ID);

	/*
	* @brief Returns a shader program by its ID
	* @param ID - id of the program
	* @return pointer to the shader program
	*/
	static ShaderProgram* Get(unsigned int ID);

	/*
	* @brief Creates a new shader program
	* @param vertexShader - filename of a shader which defines a vertex shader
	* @param fragmentShader - filenames of a file with a fragment shader
	* @return ID of the shader program
	*/
	static unsigned int CreateShaderProgram(std::string vertexShader, std::string fragmentShader);

	/*
	* @brief Returns the currently active shader program
	* @return Active program
	*/
	static ShaderProgram* GetActive(void);

	/*
	* @brief Removes all shader programs. 
	* Should be called before quitting the engine.
	*/
	static void Clear();
private:

	///vector which stores all shader programs
	static std::vector<ShaderProgram> m_shaders;

	///index of the active shader
	static unsigned int m_activeShader;

	///amount of shader programs
	static unsigned int m_amount;
};
#endif // ! SHADER_MANAGER_H
