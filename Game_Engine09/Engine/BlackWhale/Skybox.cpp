#include "Skybox.h"
#include "Master.h"
#include "SceneManager.h"
#include "ShaderManager.h"
#include "ModelManager.h"
#include "MeshRenderer.h"
BOOST_CLASS_EXPORT_IMPLEMENT(Skybox)

Skybox::Skybox() : m_model() {	
	Initialize();
	Reflection();
	m_shaderProgram = ShaderManager::CreateShaderProgram("Resources/Shaders/geom3D.vert", "Resources/Shaders/geom3D.frag");
	m_model = ((OBJ_model*)ModelManager::AddMesh("Resources\\Rendering\\Skybox.obj",m_shaderProgram));
	m_model->SetDeferredGeomShaderID(2);
	ShaderManager::ActivateShaderProgram(m_shaderProgram);
	int i=0;
	for (std::map<std::string, Material>::iterator iterator = m_model->GetMaterialLibrary()->begin(); iterator != m_model->GetMaterialLibrary()->end(); ++iterator) {
		//m_model->GetMaterialLibrary()->at(iterator->first).path = m_fileNames[i];
		m_fileNames[i] = iterator->second.path;
		m_images[i] = iterator->second.texture;
		iterator->second.emission = m_color;
		if(i<5)
			++i;
	}
}

Skybox::Skybox(const Skybox& original) : m_model(0) {

}

Skybox::~Skybox() {

}

void Skybox::Initialize() {
	static bool l_inList = false;
	if (!l_inList) {
		l_inList = true;
		Skybox* l_pointer = new Skybox;
		m_derivedClasses.push_back(l_pointer);
	}
}

void Skybox::Start() {
	IFORSI(0, 6) {
		LoadImage(i, m_fileNames[i]);
	}
}

void Skybox::Update() {
	Master::gl.UniformMatrix4fv(ShaderManager::GetActive()->GetLocation("proj"), 1, GRAPHICS_FALSE, &SceneManagement::SceneManager::GetActive()->GetMainCamera()->GetProjectionMatrix().Get()[0]);
	Master::gl.UniformMatrix4fv(ShaderManager::GetActive()->GetLocation("view"), 1, GRAPHICS_FALSE, &SceneManagement::SceneManager::GetActive()->GetMainCamera()->GetViewMatrix().Get()[0]);
	Vector3 l_position = m_gameObject->GetTransform()->position + SceneManagement::SceneManager::GetActive()->GetMainCamera()->GetGameObject()->GetTransform()->position;
	Matrix4 l_modelMatrix = GraphicsEngine::BuildModelMatrix(SceneManagement::SceneManager::GetActive()->GetMainCamera()->GetModelMatrix(), l_position, m_gameObject->GetTransform()->rotation, m_gameObject->GetTransform()->scale);
	Master::gl.UniformMatrix4fv(ShaderManager::GetActive()->GetLocation("model"), 1, GRAPHICS_FALSE, l_modelMatrix);
	m_model->Draw();
}

void Skybox::Clear() {

}

Component* Skybox::NewComponent() const {
	Component* l_pointer = new Skybox;
	return l_pointer;
}

bool Skybox::LoadImage(int p_index, boost::any p_var) {
	try {
		std::string l_temp = boost::any_cast<string> (p_var);
		if (l_temp.compare("") == 0) {
			m_fileNames[p_index] = "";
			m_images[p_index] = Master::NULL_TEXTURE;
			
			int i=0;
			std::map<std::string, Material>::iterator iterator;
			for (iterator = m_model->GetMaterialLibrary()->begin(); iterator != m_model->GetMaterialLibrary()->end(); ++iterator) {
				iterator->second.path = m_fileNames[i];
				iterator->second.texture = m_images[i];
				if (i<5)
					++i;
			}
			return true;
		}
		int l_index = 0;
		if(p_index == 5 || p_index == 1)
			l_index = GraphicsEngine::LoadTexture(l_temp, false);
		else
			l_index = GraphicsEngine::LoadTexture(l_temp, true);

		if (l_index != 0) {
			m_fileNames[p_index] = l_temp;
			m_images[p_index] = l_index;
			int i=0;
			std::map<std::string, Material>::iterator iterator;
			for (iterator = m_model->GetMaterialLibrary()->begin(); iterator != m_model->GetMaterialLibrary()->end(); ++iterator) {
				iterator->second.path = m_fileNames[i];
				iterator->second.texture = m_images[i];
				if (i<5)
					++i;
			}
		}
		else {
			return false;
		}
	}
	catch (boost::bad_any_cast& /*bac*/) {
		return false;
	}
	return true;
}

bool Skybox::SetFront(boost::any p_var) {
	return LoadImage(2, p_var);
}

std::string Skybox::GetFront(void) const {
	return m_fileNames[2];
}

bool Skybox::SetBack(boost::any p_var) {
	return LoadImage(0, p_var);
}

std::string Skybox::GetBack(void) const {
	return m_fileNames[0];
}

bool Skybox::SetRight(boost::any p_var) {
	return LoadImage(4, p_var);
}

std::string Skybox::GetRight(void) const {
	return m_fileNames[4];
}

bool Skybox::SetLeft(boost::any p_var) {
	return LoadImage(3, p_var);
}

std::string Skybox::GetLeft(void) const {
	return m_fileNames[3];
}

bool Skybox::SetUp(boost::any p_var) {
	return LoadImage(5, p_var);
}

std::string Skybox::GetUp(void) const {
	return m_fileNames[5];
}

bool Skybox::SetDown(boost::any p_var) {
	return LoadImage(1, p_var);
}

std::string Skybox::GetDown(void) const {
	return m_fileNames[1];
}

RGBA Skybox::GetColor() const {
	return m_color;
}

bool Skybox::SetColor(boost::any var) {
	try {
		RGBA l_temp = boost::any_cast<RGBA> (var);
		IFORSI(0, 4) {
			if (l_temp.GetColor()[i] > 1 || l_temp.GetColor()[i] < 0)
				return false;
		}
		m_color = l_temp;
		int i =0;
		for (std::map<std::string, Material>::iterator iterator = m_model->GetMaterialLibrary()->begin(); iterator != m_model->GetMaterialLibrary()->end(); ++iterator) {
			iterator->second.emission = m_color;
			if (i<5)
				++i;
		}
	}
	catch (boost::bad_any_cast& /*bac*/) {
		return false;
	}
	return true;
}

void Skybox::Reflection() {
	m_Variables.push_back("Front Image");
	m_Setters.push_back(boost::bind(&Skybox::SetFront, this, _1));
	m_Getters.push_back(boost::bind(&Skybox::GetFront, this));

	m_Variables.push_back("Back Image");
	m_Setters.push_back(boost::bind(&Skybox::SetBack, this, _1));
	m_Getters.push_back(boost::bind(&Skybox::GetBack, this));

	m_Variables.push_back("Right Image");
	m_Setters.push_back(boost::bind(&Skybox::SetRight, this, _1));
	m_Getters.push_back(boost::bind(&Skybox::GetRight, this));

	m_Variables.push_back("Left Image");
	m_Setters.push_back(boost::bind(&Skybox::SetLeft, this, _1));
	m_Getters.push_back(boost::bind(&Skybox::GetLeft, this));

	m_Variables.push_back("Up Image");
	m_Setters.push_back(boost::bind(&Skybox::SetUp, this, _1));
	m_Getters.push_back(boost::bind(&Skybox::GetUp, this));

	m_Variables.push_back("Down Image");
	m_Setters.push_back(boost::bind(&Skybox::SetDown, this, _1));
	m_Getters.push_back(boost::bind(&Skybox::GetDown, this));

	m_Variables.push_back("Color");
	m_Setters.push_back(boost::bind(&Skybox::SetColor, this, _1));
	m_Getters.push_back(boost::bind(&Skybox::GetColor, this));
}