/**
* @class 
* @brief
* @author Olesia Kochergina
* @version 03
* @date 09/03/2017
*/

#ifndef GRAPHICSENGINE_H
#define GRAPHICSENGINE_H

#include <iostream>
#include <vector>
#include <map>

#include "Math_s.h"
#include "GBuffer.h"

#define BUFSIZE 512

class GraphicsEngine{

public:

	///Holds the amount of draw calls made per frame
	static unsigned int S_DRAW_CALLS;
	static void Initialize(void);
	static void Destroy(void);

	static void Start();

	/**
	* @brief Default constructor
	*/
	GraphicsEngine();

	/**
	* @brief Destructor to delete pointers.
	*/
	~GraphicsEngine();

	/**
	* @author Olesia Kochergina
	* @date 16/09/2016
	* @brief Allows to turn on/off fog
	* @param true - fog on; false - off
	* @param nearR - starting point
	* @param farR - ending point
	*/
	static void FogControl(bool isFoggy, float nearR, float farR);


	/**
	* @brief Adds a texture to the buffer defined by it's name. Supports jpg, png, tiff and bmp formats.
	* @param fileName - the texture's name and it's extension.
	* @return unsigned int texture - returns the texture itself.
	*/
	static const unsigned int LoadTexture(std::string fileName,bool flip);
	static const bool DeleteTexture(unsigned int texture);

	static const unsigned int LoadCubemap(const std::vector<std::string> files);

	static const bool LoadHeightmap(std::string fileName, int*& texture, int& width, int& height );

	static const void SetDeferredGeometry(void);
	static const void SetDeferredLight(void);
	static const void ResetDeferred(void);
	static const unsigned int GetGBufferColorTexture();

	static void EnableTexture(int ID, unsigned int texture, int UniformLocation);
	static void DisableTexture(int ID);

	static Matrix4 BuildModelMatrix(Matrix4 modelMatrix, Vector3 position, Vector3 rotation, Vector3 scale);
	/**
	* @brief adds all textures from a particular directory, have not been tested yet.
	* @param directory - the directory of textures, which need to be loaded.
	* @param ext - an array of legal file extensions e.g. jpg, png, bmp
	* @param Resize - the amount of textures.
	* @return the first address of an array of textures.
	*/
	const unsigned int* loadTextures(std::string directory,std::string ext[],int Resize);
	
	/**
	* @brief The class's contructor to initialize all variables and allow to load static or dynamic data.
	* @param type 
	*	0 - Display List (Static Data) 
	*	1 - VBO (Static Data)
	*	2 - IBO (Static Data)
	*	3 - VBO (Dynamic Data) 
	*	4 - IBO (Dynamic Data) 
	*/
	GraphicsEngine(int type);

	static void AddPolygon(float x, float y, float maxX, float maxY, vector<Vector3>& data, bool clockwise);
	
	/**
	* @brief Allows to apply one color type transformation just before rendering primitives.
	* Overrides all other TexEnv* functions. Sending -1 will automatically disable the function.
	* @param colorType - GL_MODULATE, GL_REPLACE, GL_TEXTURE etc or -1. 
	* @param colorValue - an array of 3 floats
	*/
	void overrideTexEnv(int colorType, float colorValue[]); 

	/**
	* @brief Sets the texture settings
	* @param isRGB - True if RGB, false otherwise
	*/
	static void SwitchToRGB(bool isRGB);

	/**
	* @brief Sets the colour settings
	*/
	static void ColorSettings();
	static std::vector<Vector3> GenerateNormals(const std::vector<Vector3>& tempV);
	static const void Reshape(void);
	static const void Clear(void);
private:
	static unsigned int m_deferredVAO;
	static unsigned int m_deferredVBO;
	static void RenderGBuffer(void);
	static GBuffer m_gbuffer;
	static const unsigned int LoadTexture(unsigned int textureType, unsigned int textureParam,float param,std::string fileName,bool flip);

	static map<std::string,unsigned int> m_textureCollection;
		
	/// GL_MODULATE, GL_TEXTURE, -1 OR GL_REPLACE, Used to apply a transformation just before rendering.
	int envColorSystem;

	///Used to apply a specific color filter to primitives before rendering them.
	float envColor[3];

	///Used for restoring the initial envColor
	float tempColor[3];

	/// The purpose of the class, shows whether it is dynamic or static.
	unsigned int state;

	///type of current world: 3D /2D
	unsigned int textureType;


	///A pointer to textures
	unsigned int* textures;

	///the amount of textures
	int textureCount;

	///Used for IBO to identify polygons
	int polygonIndex;

	///Holds an object for textures
	unsigned int TBO;
	
	///Type of an object
	int type;
	
	/// the current polygon index
	static int curIndex;

	/// holds indices of display lists
	int objectIndex[2];

	/// the index of a texture in the buffer
	int lastImage;
};
#endif GRAPHICSENGINE_H