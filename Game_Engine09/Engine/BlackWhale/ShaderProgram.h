#ifndef SHADERPROGRAM_H
#define SHADERPROGRAM_H
#include "Shader.h"
#include <iostream>
#include <map>

/*
* @class ShaderManager
* @brief Identifies a single shader program
* @author Olesia Kochergina
* @date 20/01/2017
*/
class ShaderProgram{
public:

	/*
	* @brief Default c-tor
	*/
	ShaderProgram();


	//ShaderProgram(const ShaderProgram& original);
	
	/*
	* @brief D-tor
	*/
	~ShaderProgram();

	/*
	* @brief Creates a new shader program placeholder.
	*/
	void Create();
	
	/**
	* @brief Attaches fragment and vertex shaders.
	*/
	void Attach();

	/*
	* @brief Links the shader program
	* @return true - shader program is successfully linked
	*/
	bool Link();

	/*
	* @brief Activates a shader program.
	* @return true - success
	*/
	bool Activate();
	
	/*
	* @brief Displays any errors that were identified when compiling the source
	*/
	void PrintInfoLog();

	/*
	* @brief Returns the id of the shader program
	* @return ID of the shader program
	*/
	int GetID();

	/*
	* @brief Sets the file name of the fragment shader
	* @param filename - name of the file that contains glsl source code
	*/
	void SetFragmentShader(std::string fileName);

	/*
	* @brief Sets the file name of the vertex shader
	* @param filename - name of the file that contains glsl source code
	*/
	void SetVertexShader(std::string fileName);

	/*
	* @brief Returns the file name of the vertex shader
	* @return name of the vertex shader file
	*/
	std::string GetVertexShader(void);

	/*
	* @brief Returns the file name of the fragment shader
	* @return name of the fragment shader file
	*/
	std::string GetFragmentShader(void);

	/*
	* @brief Destroys the shader program.
	*/
	void Clear();

	/*
	* @brief Adds a location of a uniform shader variable to a map with shader variables
	* @param shaderVariable - name of the variable
	*/
	void SetULocation(std::string shaderVariable);

	/*
	* @brief Returns the index of a shader variable by its name
	*/
	int GetLocation(std::string shaderVariable);

	/*
	* @brief Automatically identifies shader variables and adds them to the map with variables and their indices
	*/
	void SetLocations(void);

	/*
	* @brief Adds a location of a attribute shader variable to a map with shader variables
	* @param shaderVariable - name of the variable
	*/
	void SetALocation(std::string shaderVariable);
private:

	///shader program id
	unsigned int m_program;

	///true - linked
	int m_linked;

	///fragment shader
	Shader m_fs;

	///vertex shader
	Shader m_vs;

	///file name of the fragment shader
	std::string m_fsName;

	///file name of the vertex shader
	std::string m_vsName;

	///map with variables and their indices 
	std::map<std::string,int> m_object_key;
};
#endif