#ifndef TIME_H
#define TIME_H

#define NUM_FPS_SAMPLES 64
/*
* @class Time
* @brief Allows to manage time in the game engine
* @author Olesia Kochergina
* @date 03/02/2017
*/
class Time{
public:

	/**
	* @param frameStart true - beginning of the frame; false - ending 
	*/
	static void UpdateTime(bool frameStart);
	static void Start();

	/**
	* @brief The time in seconds it took to complete the last frame
	* @return dt
	*/
	static float GetDeltaTime();
	static float GetCurrent();

	/**
	* @brief Returns frames per second
	* @return fps
	*/
	static float GetFPS();
private:
	
	//time in milliseconds it took to complete the last frame
	static float m_deltaTime;
	
	//current time in milliseconds 
	static float m_beginFrame;
	
	//time at last frame
	static float m_endFrame;

	//time since the start
	static float m_startTime;

	//total amount of frames since the start
	static int m_frameCount;

	//frames per second
	static float m_fps;
	static float m_fpsSamples[NUM_FPS_SAMPLES];
	
};
#endif