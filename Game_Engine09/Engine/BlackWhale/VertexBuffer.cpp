#include "VertexBuffer.h"
#include "Master.h"
unsigned int VertexBuffer::s_bufferState = GRAPHICS_STATIC_DRAW;

bool VertexBuffer::CreateVBO(vBuffer* info, int shaderProgram) {
	//error checking
	if (info->data.vertex.size() == 0 || info->data.texture.size() == 0 || info->data.normal.size() == 0)
		return false;

	int l_vector_size = sizeof(Vector3);
	float l_buffer_size = (info->data.vertex.size() + info->data.texture.size() + info->data.normal.size()) * l_vector_size;
	Master::gl.GenVAO(1, &info->VAO);
	Master::gl.BindVAO(info->VAO);//bind the current buffer

	Master::gl.GenBuffer(1, &info->VBO);
	Master::gl.BindBuffer(GRAPHICS_ARRAY_BUFFER, info->VBO);//bind the current buffer
	Master::gl.BufferData(GRAPHICS_ARRAY_BUFFER, l_buffer_size, 0, s_bufferState);

	//define data in the buffer
	Master::gl.BufferSubData(GRAPHICS_ARRAY_BUFFER, 0, info->data.vertex.size() * l_vector_size, &info->data.vertex.at(0));
	Master::gl.BufferSubData(GRAPHICS_ARRAY_BUFFER, info->data.vertex.size() * l_vector_size, info->data.texture.size() * l_vector_size, &info->data.texture.at(0));
	Master::gl.BufferSubData(GRAPHICS_ARRAY_BUFFER, (info->data.vertex.size() + info->data.texture.size())*l_vector_size, info->data.normal.size() * l_vector_size, &info->data.normal.at(0));

	Master::gl.BindBuffer(GRAPHICS_ARRAY_BUFFER, info->VBO);
	int offset[] = { 0,info->data.vertex.size(), info->data.vertex.size() + info->data.texture.size() };
	for (unsigned i = 0; i < 3; i++) {
		Master::gl.EnableVertexAttribArray(info->attribLocation[i]);
		Master::gl.VertexAttribPointer(info->attribLocation[0], 3, GRAPHICS_FLOAT, GRAPHICS_FALSE, 0, BUFFER_OFFSET(offset[i]));
	}

	if (info->attribLocation[0] == -1 || info->attribLocation[1] == -1 || info->attribLocation[2] == -1) {
		cout << "INVALID ATTRIBUTES: " << info->attribLocation[0] << " " << info->attribLocation[1] << " " << info->attribLocation[2] << " IN SHADER PROGRAM: " << shaderProgram << endl;
	}
	return true;
}

void VertexBuffer::RenderVBO(const vBuffer* p_info, int p_fIndex, int p_lIndex) {
	GraphicsEngine::S_DRAW_CALLS++;

	if (Master::IS_FRAME_RENDER_MODE)
		Master::gl.PolygonMode(GRAPHICS_FRONT, GRAPHICS_LINE);
	else
		Master::gl.PolygonMode(GRAPHICS_FRONT, GRAPHICS_FILL);
	/*
	//DOES NOT WORK WITH MULTITEXTURED VBOs unless textures are sent to shaders
	int l_vector_size = sizeof(Vector3);
	int l_offset[] = {0, (p_lIndex-p_fIndex) * sizeof(Vector3), ((p_lIndex-p_fIndex) * 2) * sizeof(Vector3)};
	glBindVertexArray(p_info->VAO);
	glBindBuffer(GL_ARRAY_BUFFER, p_info->VBO);
	for(unsigned i=0;i<3;i++){
	glEnableVertexAttribArray(p_info->attribLocation[i]);
	glVertexAttribPointer(p_info->attribLocation[i],3,GL_FLOAT,GL_FALSE,0, BUFFER_OFFSET(l_offset[i]));
	}
	glDrawArrays(GL_TRIANGLES,p_fIndex, p_lIndex-p_fIndex);*/


	//stride is for interleaved arrays -> xyzrgbst xyzrgbst
	int l_vector_size = sizeof(Vector3);
	//p_fIndex - the index of the buffer to start from 
	//[0] - vertex information starting from the p_fIndex position 
	//[1] - texture coord information starting from p_fIndex + the Resize of the vertex component
	//[2] - normal vector information starting from p_fIndex + the Resize of the vertex and texture components
	//ONLY ONE BUFFER IS USED FOR RENDERING MODELS WITH MULTIPLE ATTRIBUTES AND MULTIPLE TEXTURES!
	int l_offset[] = { p_fIndex * l_vector_size, (p_info->data.vertex.size() + p_fIndex) * l_vector_size, (p_info->data.vertex.size() * 2 + p_fIndex)*l_vector_size };
	//int l_data_type[] = {GL_FLOAT,GL_SHORT,GL_INT_2_10_10_10_REV}; 
	Master::gl.BindVAO(p_info->VAO);
	Master::gl.BindBuffer(GRAPHICS_ARRAY_BUFFER, p_info->VBO);
	for (unsigned i = 0; i < 3; i++) {
		Master::gl.EnableVertexAttribArray(p_info->attribLocation[i]);
		Master::gl.VertexAttribPointer(p_info->attribLocation[i], 3, GRAPHICS_FLOAT, GRAPHICS_FALSE, 0, BUFFER_OFFSET(l_offset[i]));//Expensive operator -> glDrawArrays can be used instead
	}
	Master::gl.DrawArrays(GRAPHICS_TRIANGLES, 0, p_lIndex - p_fIndex);
}

void VertexBuffer::CreateVBO(unsigned int *VAO, unsigned int *VBO, vector<Vector3> dataV, vector<Vector3> dataT, vector<Vector3> dataN, int numberOfCoordinates, int shaderProgram) {
	int textureSize = sizeof(Vector3);

	float bufferSize = dataV.size() * sizeof(Vector3) + dataN.size() * sizeof(Vector3);

	bufferSize += dataT.size()*textureSize;

	Master::gl.GenVAO(1, VAO);
	Master::gl.BindVAO(*VAO);//bind the current buffer

	Master::gl.GenBuffer(1, VBO);
	Master::gl.BindBuffer(GRAPHICS_ARRAY_BUFFER, *VBO);//bind the current buffer
	Master::gl.BufferData(GRAPHICS_ARRAY_BUFFER, bufferSize, 0, s_bufferState);

	//define data in the buffer
	Master::gl.BufferSubData(GRAPHICS_ARRAY_BUFFER, 0, dataV.size() * sizeof(Vector3), &dataV.at(0));
	if (dataT.size() > 0) {
		Master::gl.BufferSubData(GRAPHICS_ARRAY_BUFFER, dataV.size() * sizeof(Vector3), dataT.size()*textureSize, &dataT.at(0));
	}
	else {
		cout << "VBO: No texture information provided" << endl;
	}
	if (dataN.size() > 0) {
		Master::gl.BufferSubData(GRAPHICS_ARRAY_BUFFER, dataV.size() * sizeof(Vector3) + dataT.size()*textureSize, dataN.size() * sizeof(Vector3), &dataN.at(0));
	}
	else {
		cout << "VBO: No normal information provided" << endl;
	}

	Master::gl.BindBuffer(GRAPHICS_ARRAY_BUFFER, *VBO);

	int pos = Master::gl.GetALoc(shaderProgram, "position");
	int tex = Master::gl.GetALoc(shaderProgram, "texCoord");
	int nor = Master::gl.GetALoc(shaderProgram, "normal");

	Master::gl.EnableVertexAttribArray(pos);
	Master::gl.VertexAttribPointer(pos, 3, GRAPHICS_FLOAT, GRAPHICS_FALSE, 0, 0);

	Master::gl.EnableVertexAttribArray(tex);
	Master::gl.VertexAttribPointer(tex, numberOfCoordinates, GRAPHICS_FLOAT, GRAPHICS_FALSE, 0, BUFFER_OFFSET(dataV.size()));

	if (dataN.size() > 0) {
		Master::gl.EnableVertexAttribArray(nor);
		Master::gl.VertexAttribPointer(nor, 3, GRAPHICS_FLOAT, GRAPHICS_FALSE, 0, BUFFER_OFFSET(dataV.size() + dataT.size()));
	}
	if (pos == -1 || tex == -1 || nor == -1) {
		cout << "INVALID ATTRIBUTES: " << pos << " " << tex << " " << nor << " IN SHADER PROGRAM: " << shaderProgram << endl;
	}
	//cout<<"OFFSET: "<<bufferSize<<" "<<dataV.size()<<" "<<dataT.size()<<" "<<dataN.size()<<endl;
	/*
	//get info from buffer
	int tempSize = dataT.size();
	vector<Vector3> temp(tempSize);
	glGetBufferSubData(GL_ARRAY_BUFFER,0,sizeof(Vector3)*tempSize,&temp.at(0));*/
}

void VertexBuffer::RenderVBO(unsigned int *VAO, unsigned int *VBO, int pos, int tex, int nor, float first, float last) {
	GraphicsEngine::S_DRAW_CALLS++;
	Master::gl.BindVAO(*VAO);
	Master::gl.BindBuffer(GRAPHICS_ARRAY_BUFFER, *VBO);
	Master::gl.EnableVertexAttribArray(pos);
	Master::gl.EnableVertexAttribArray(tex);
	Master::gl.EnableVertexAttribArray(nor);

	Master::gl.VertexAttribPointer(pos, 3, GRAPHICS_FLOAT, GRAPHICS_FALSE, 0, 0);
	int temp = (last - first) * sizeof(Vector3);
	Master::gl.VertexAttribPointer(tex, 3, GRAPHICS_FLOAT, GRAPHICS_FALSE, 0, BUFFER_OFFSET(temp));
	temp = ((last - first) * 2) * sizeof(Vector3);
	Master::gl.VertexAttribPointer(nor, 3, GRAPHICS_FLOAT, GRAPHICS_FALSE, 0, BUFFER_OFFSET(temp));

	Master::gl.DrawArrays(GRAPHICS_TRIANGLES, first, last);
}

void VertexBuffer::CreateIBO(iBuffer* info, int shaderProgram) {
	Master::gl.GenBuffer(1, &info->IBO);
	Master::gl.BindBuffer(GRAPHICS_ELEMENT_ARRAY_BUFFER, info->IBO);
	Master::gl.BufferData(GRAPHICS_ELEMENT_ARRAY_BUFFER, info->indices.size() * sizeof(unsigned int), &info->indices.at(0), s_bufferState);
	CreateVBO(&info->vertexBuffer, shaderProgram);
	/*glEnable(GL_PRIMITIVE_RESTART);
	glPrimitiveRestartIndex(info->indices.size());*/
	Master::gl.BindBuffer(GRAPHICS_ELEMENT_ARRAY_BUFFER, 0);
}

void VertexBuffer::CreateIBO(unsigned int *IBO, unsigned int *VBO, unsigned int *VAO, vector<unsigned int> indices, vector<Vector3> dataV, vector<Vector3> dataT, vector<Vector3> dataN, int shaderProgram) {
	CreateVBO(VAO, VBO, dataV, dataT, dataN, 3, shaderProgram);

	Master::gl.GenBuffer(1, IBO);
	Master::gl.BindBuffer(GRAPHICS_ELEMENT_ARRAY_BUFFER, *IBO);
	Master::gl.BufferData(GRAPHICS_ELEMENT_ARRAY_BUFFER, indices.size() * sizeof(unsigned int), &indices.at(0), s_bufferState);
	Master::gl.BindBuffer(GRAPHICS_ELEMENT_ARRAY_BUFFER, 0);
}

void VertexBuffer::RenderIBO(iBuffer* p_info, int p_fIndex, int p_lIndex) {
	GraphicsEngine::S_DRAW_CALLS++;
	//stride is for interleaved arrays -> xyzrgbst xyzrgbst
	int l_vector_size = sizeof(Vector3);
	//p_fIndex - the index of the buffer to start from 
	//[0] - vertex information starting from the p_fIndex position 
	//[1] - texture coord information starting from p_fIndex + the Resize of the vertex component
	//[2] - normal vector information starting from p_fIndex + the Resize of the vertex and texture components
	//ONLY ONE BUFFER IS USED FOR RENDERING MODELS WITH MULTIPLE ATTRIBUTES AND MULTIPLE TEXTURES!
	if (Master::IS_FRAME_RENDER_MODE)
		Master::gl.PolygonMode(GRAPHICS_FRONT, GRAPHICS_LINE);
	else
		Master::gl.PolygonMode(GRAPHICS_FRONT, GRAPHICS_FILL);

	Master::gl.BindVAO(p_info->vertexBuffer.VAO);
	Master::gl.BindBuffer(GRAPHICS_ARRAY_BUFFER, p_info->vertexBuffer.VBO);
	int l_offset[] = { p_fIndex * l_vector_size, (p_info->indices.size() + p_fIndex) * l_vector_size, (p_info->indices.size() * 2 + p_fIndex)*l_vector_size };
	for (unsigned i = 0; i < 3; i++) {
		Master::gl.EnableVertexAttribArray(p_info->vertexBuffer.attribLocation[i]);
		Master::gl.VertexAttribPointer(p_info->vertexBuffer.attribLocation[i], 3, GRAPHICS_FLOAT, GRAPHICS_FALSE, 0, BUFFER_OFFSET(l_offset[i]));
	}
	Master::gl.BindBuffer(GRAPHICS_ELEMENT_ARRAY_BUFFER, p_info->IBO);
	//Master::gl.DrawElements(GRAPHICS_TRIANGLES, (p_lIndex - p_fIndex), GRAPHICS_UNSIGNED_INT, BUFFER_OFFSET(p_fIndex));

	Master::gl.DrawElements(GRAPHICS_TRIANGLES, p_info->indices.size(), GRAPHICS_UNSIGNED_INT, BUFFER_OFFSET(p_fIndex));

}


void VertexBuffer::CreateIBO(unsigned int *IBO, unsigned int *VBO, unsigned int *VAO, vector<unsigned int> indices, vector<Vector3> data, char* name, int shaderProgram) {
	Master::gl.GenBuffer(1, IBO);
	Master::gl.BindBuffer(GRAPHICS_ELEMENT_ARRAY_BUFFER, *IBO);
	Master::gl.BufferData(GRAPHICS_ELEMENT_ARRAY_BUFFER, indices.size() * sizeof(int), &indices.at(0), s_bufferState);
	Master::gl.BindBuffer(GRAPHICS_ELEMENT_ARRAY_BUFFER, 0);

	Master::gl.GenBuffer(1, VBO);
	Master::gl.BindBuffer(GRAPHICS_ARRAY_BUFFER, *VBO);//bind the current buffer
	Master::gl.BufferData(GRAPHICS_ARRAY_BUFFER, data.size() * sizeof(Vector3), &data.at(0), s_bufferState);
	Master::gl.BindBuffer(GRAPHICS_ARRAY_BUFFER, 0);

	Master::gl.GenVAO(1, VAO);
	Master::gl.BindVAO(*VAO);//bind the current array

	Master::gl.BindBuffer(GRAPHICS_ARRAY_BUFFER, *VBO);//bind the current buffer

	int attrib = Master::gl.GetALoc(shaderProgram, name);

	Master::gl.EnableVertexAttribArray(attrib);
	Master::gl.VertexAttribPointer(attrib, 3, GRAPHICS_FLOAT, GRAPHICS_FALSE, sizeof(Vector3), BUFFER_OFFSET(0));

	//Master::gl.BindBuffer(GRAPHICS_ELEMENT_ARRAY_BUFFER, *IBO);

	if (attrib) {
		cout << "INVALID ATTRIBUTE: " << attrib << endl;
	}
}

void VertexBuffer::RenderIBO(unsigned int* VAO, unsigned int *VBO, unsigned int* IBO, vector<unsigned int> indices, int pos, int tex, int nor, int first, int last, int shaderProgram) {
	GraphicsEngine::S_DRAW_CALLS++;
	Master::gl.BindVAO(*VAO);
	Master::gl.BindBuffer(GRAPHICS_ARRAY_BUFFER, *VBO);

	Master::gl.EnableVertexAttribArray(pos);
	Master::gl.EnableVertexAttribArray(tex);
	Master::gl.EnableVertexAttribArray(nor);

	Master::gl.VertexAttribPointer(pos, 3, GRAPHICS_FLOAT, GRAPHICS_FALSE, 0, 0);
	int temp = (last - first) * sizeof(Vector3);
	Master::gl.VertexAttribPointer(tex, 3, GRAPHICS_FLOAT, GRAPHICS_FALSE, 0, BUFFER_OFFSET(temp));
	temp = ((last - first) * 2) * sizeof(Vector3);
	Master::gl.VertexAttribPointer(nor, 3, GRAPHICS_FLOAT, GRAPHICS_FALSE, 0, BUFFER_OFFSET(temp));

	Master::gl.BindBuffer(GRAPHICS_ELEMENT_ARRAY_BUFFER, *IBO);
	Master::gl.DrawElements(GRAPHICS_TRIANGLES, indices.size(), GRAPHICS_UNSIGNED_INT, BUFFER_OFFSET(first));
}


void VertexBuffer::DeprecatedCreateVBO(unsigned int *VBO, vector<Vector3> dataV, vector<Vector3> dataT, vector<Vector3> dataN, int numberOfCoordinates) {
	/*int textureSize = sizeof(Vector3);

	float bufferSize = dataV.size() * sizeof(Vector3) + dataN.size() * sizeof(Vector3);

	bufferSize += dataT.size()*textureSize;

	Master::gl.GenBuffer(1, VBO);
	Master::gl.BindBuffer(GRAPHICS_ARRAY_BUFFER, *VBO);//bind the current buffer
	Master::gl.BufferData(GRAPHICS_ARRAY_BUFFER, bufferSize, 0, s_bufferState);

	//define data in the buffer
	Master::gl.BufferSubData(GRAPHICS_ARRAY_BUFFER, 0, dataV.size() * sizeof(Vector3), &dataV.at(0));
	if (dataT.size()>0) {
		Master::gl.BufferSubData(GRAPHICS_ARRAY_BUFFER, dataV.size() * sizeof(Vector3), dataT.size()*textureSize, &dataT.at(0));
	}
	else {
		cout << "VBO: No texture information provided" << endl;
	}
	if (dataN.size()>0) {
		Master::gl.BufferSubData(GRAPHICS_ARRAY_BUFFER, dataV.size() * sizeof(Vector3) + dataT.size()*textureSize, dataN.size() * sizeof(Vector3), &dataN.at(0));
	}

	Master::gl.BindBuffer(GRAPHICS_ARRAY_BUFFER, *VBO);

	Master::gl.EnableClientState(GRAPHICS_VERTEX_ARRAY);
	Master::gl.VertexPointer(3, GRAPHICS_FLOAT, 0, BUFFER_OFFSET(0));
	Master::gl.EnableClientState(GRAPHICS_TEXTURE_COORD_ARRAY);
	Master::gl.TexCoordPointer(numberOfCoordinates, GRAPHICS_FLOAT, 0, BUFFER_OFFSET(dataV.size() * sizeof(Vector3)));

	if (dataN.size()>0) {
		Master::gl.EnableClientState(GRAPHICS_NORMAL_ARRAY);
		Master::gl.NormalPointer(GRAPHICS_FLOAT, 0, BUFFER_OFFSET(dataV.size() * sizeof(Vector3) + dataT.size() * sizeof(Vector3)));
	}*/
}

void VertexBuffer::DeprecatedRenderVBO(unsigned int *VBO, float Resize) {
	/*Master::gl.BindBuffer(GRAPHICS_ARRAY_BUFFER, *VBO);
	Master::gl.EnableClientState(GRAPHICS_VERTEX_ARRAY);
	Master::gl.VertexPointer(3, GRAPHICS_FLOAT, 0, BUFFER_OFFSET(0));
	Master::gl.EnableClientState(GRAPHICS_TEXTURE_COORD_ARRAY);
	int temp = Resize * sizeof(Vector3);
	Master::gl.TexCoordPointer(3, GRAPHICS_FLOAT, 0, BUFFER_OFFSET(temp));
	Master::gl.EnableClientState(GRAPHICS_NORMAL_ARRAY);
	temp = (Resize * 2) * sizeof(Vector3);
	Master::gl.NormalPointer(GRAPHICS_FLOAT, 0, BUFFER_OFFSET(temp));
	Master::gl.PolygonMode(GRAPHICS_FRONT, GRAPHICS_FILL);
	Master::gl.DrawArrays(GRAPHICS_TRIANGLES, 0, Resize);*/
}