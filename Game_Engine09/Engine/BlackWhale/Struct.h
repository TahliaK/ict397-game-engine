/**
* @class Header file for structs
* @brief Contains necessary structures which are used in the physics and rendering classes.
* @author Olesia Kochergina
* @version 02
* @date 09/03/2017
*/
#ifndef STRUCT_H
#define STRUCT_H
#include <math.h>
#include <iostream>
#include <vector>
#include "Math_s.h"
#include "GraphicsEngine.h"
#include "Interfaces\DirectMediaLayer.h"

///to avoid redefinition problems
class GameObject;
using namespace std;

struct SymmetricMatrix { 

public:

	// Constructor

	SymmetricMatrix(double c=0) {
		for(int i=0; i <10 ;i++)
			m[i] = c;  
	}

	SymmetricMatrix(	double m11, double m12, double m13, double m14, 
		double m22, double m23, double m24,
		double m33, double m34,
		double m44) {
			m[0] = m11;  m[1] = m12;  m[2] = m13;  m[3] = m14; 
			m[4] = m22;  m[5] = m23;  m[6] = m24; 
			m[7] = m33;  m[8] = m34;
			m[9] = m44;
	}

	// Make plane

	SymmetricMatrix(double a,double b,double c,double d)
	{
		m[0] = a*a;  m[1] = a*b;  m[2] = a*c;  m[3] = a*d; 
		m[4] = b*b;  m[5] = b*c;  m[6] = b*d; 
		m[7 ] =c*c; m[8 ] = c*d;
		m[9 ] = d*d;
	}

	double operator[](int c) const { return m[c]; }

	// Determinant

	double det(	int a11, int a12, int a13,
		int a21, int a22, int a23,
		int a31, int a32, int a33)
	{
		double det =  m[a11]*m[a22]*m[a33] + m[a13]*m[a21]*m[a32] + m[a12]*m[a23]*m[a31] 
		- m[a13]*m[a22]*m[a31] - m[a11]*m[a23]*m[a32]- m[a12]*m[a21]*m[a33]; 
		return det;
	}

	const SymmetricMatrix operator+(const SymmetricMatrix& n) const
	{ 
		return SymmetricMatrix( m[0]+n[0],   m[1]+n[1],   m[2]+n[2],   m[3]+n[3], 
			m[4]+n[4],   m[5]+n[5],   m[6]+n[6], 
			m[ 7]+n[ 7], m[ 8]+n[8 ],
			m[ 9]+n[9 ]);
	}

	SymmetricMatrix& operator+=(const SymmetricMatrix& n)
	{
		m[0]+=n[0];   m[1]+=n[1];   m[2]+=n[2];   m[3]+=n[3]; 
		m[4]+=n[4];   m[5]+=n[5];   m[6]+=n[6];   m[7]+=n[7]; 
		m[8]+=n[8];   m[9]+=n[9];
		return *this; 
	}

	double m[10];
};

struct MeshData{

	/// Vector of xyz-Vector3 coordinates
	vector<Vector3> vertex;

	/// Vector of xyz-texture coordinates
	vector<Vector3> texture;

	/// Vector of xyz-normal coordinates
	vector<Vector3> normal; 

	void operator = (const MeshData& original){
		vertex.assign(original.vertex.begin(),original.vertex.end());
		texture.assign(original.texture.begin(),original.texture.end());
		normal.assign(original.normal.begin(),original.normal.end());
	}

	void Clear() {
		texture.clear();
		vertex.clear();
		normal.clear();
	}

};

struct Vertex3{
	Vector3 p;

	//for terrain simplification
	SymmetricMatrix q;
	int border;
	int tstart,tcount;

	Vertex3 operator = (Vector3 v){
		Vertex3 temp;
		temp.p = v;
		p = v;
		return temp;
	}

	Vertex3 operator = (Vertex3 v){
		p = v.p;
		q = v.q;
		border = v.border;
		tstart = v.tstart;
		tcount = v.tcount;
		return v;
	}
};

struct Ref{
	int tid,tvertex;
};

struct Triangle3{
	int v[3];
	double error[4];
	bool deleted,dirty;
	Vector3 normal;

	Triangle3 (){}

	Triangle3 (int* v){
		for(int i=0;i<3;i++)
			this->v[i] = v[i];
	}

	Triangle3 (int v1,int v2, int v3){
		v[0] = v1;
		v[1] = v2;
		v[2] = v3;
	}

	Triangle3 operator = (Triangle3 t){
		for(int i=0;i<3;i++){
			v[i] = t.v[i];
			error[i] = t.error[i];
		}
		error[3] = t.error[3];
		deleted = t.deleted;
		dirty = t.dirty;
		normal = t.normal;
		return t;
	}
};

struct vBuffer{
	unsigned int VBO;
	unsigned int VAO;
	MeshData data;
	///position, texture coordinates, normal
	int attribLocation[3];

	vBuffer(){
		VBO = 0;
		VAO = 0;
		for(unsigned i=0;i<3;i++){
			attribLocation[i] = 100000;
		}
	}

	vBuffer(const vBuffer& original){
		VBO = original.VBO;
		VAO = original.VAO;
		for(unsigned i =0;i<3;i++){
			attribLocation[i] = original.attribLocation[i];
		}
		data = original.data;
	}

	~vBuffer(){	
		Clear();
	}

	void Clear(){
		data.Clear();
		if(VBO!=0){
			//Master::gl.DeleteBuffers(1, &VBO);
			VBO=0;
		}
		if(VAO!=0){
			//Master::gl.DeleteBuffers(1, &VAO);
			VAO=0;
		}
	}
};

struct iBuffer{
	unsigned int IBO;
	vector<unsigned int> indices;
	vBuffer vertexBuffer;
};


/*struct Multitexture{
int ID;
int ShaderID;
unsigned int texture;
std::string name;
Multitexture(){};
~Multitexture(){};
Multitexture(int ID,unsigned int texture, int ShaderID, std::string name){
this->ID = ID;
this->texture = texture;
this->ShaderID = ShaderID;
this->name = name;
}
void BindTexture(){
glUniform1i(glGetUniformLocation(ShaderID,name.c_str()),ID);
glActiveTexture(GL_TEXTURE0+ID);
glBindTexture(GL_TEXTURE_2D,texture);
}
void UnbindTexture(){
glActiveTexture(GL_TEXTURE0+ID);
glDisable(GL_TEXTURE_2D);
}
};*/

/// Contains all the operations required by axis-aligned bounding boxes
struct AABB{

public:
	
	int layer;

	
	Vector3 min;

	
	Vector3 max;

	/**
	* @brief A constructor to set min and max points.
	* @param min - the minimum point of the box
	* @param max - the maximum point of the box
	*/
	AABB(const Vector3 min,const Vector3 max){
		AABB::min = min;
		AABB::max = max;
		CalculateCenter();
		layer =0;
	}

	~AABB(){
	}

	/**
	* @brief Copies the parameter's value to this instance's values.
	* @param the original AABB box
	* @return a copy of this object
	*/
	AABB& operator = (const AABB &original){
		min = original.min;
		max = original.max;
		layer = original.layer;
		CalculateCenter();
		return *this;
	}

	bool operator == (const AABB &original) {
		return (min == original.min && max == original.max);
	}

	/**
	* @brief Returns true if this AABB contains an AABB defined
	* by the argument.
	* @return true - this box contains the argument's box; otherwise false.
	*/
	bool Contains(const AABB& box) const{
		return (min.x <= box.min.x && min.y <= box.min.y &&
			min.z <= box.min.z && max.x >= box.max.x && 
			max.y >= box.max.y && max.z >= box.max.z );
	}

	friend ostream & operator <<(ostream & os, const AABB& aabb) {
		os << aabb.min << "  " << aabb.max;
		return os;
	}
	

	bool Contains(const Vector3& point) const{
		return min.x <= point.x && max.x >= point.x
			&& min.y <= point.y && max.y >= point.y
			&& min.z <= point.z && max.z >= point.z;
	}

	bool Intersect(const AABB& box) const{
		if(max.x < box.min.x || min.x > box.max.x) 
			return false;
		if(max.y < box.min.y || min.y > box.max.y) 
			return false;
		if(max.z < box.min.z || min.z > box.max.z) 
			return false;
		return true;

	}

	static AABB FindMinimumAABB(vector<Vector3> point){
		AABB tempBox;
		tempBox.min = MINIMUM_INIT_INT;//reset 
		tempBox.max = MAXIMUM_INIT_INT;

		for(unsigned j=0;j<point.size();j++){
			for(unsigned i=0;i<3;i++){
				if(tempBox.min[i] > point[j][i])
					tempBox.min.Set(i,point[j][i]);
				if(tempBox.max[i] < point[j][i])
					tempBox.max.Set(i,point[j][i]);
			}
		}
		int counter=0;
		for(int i=0;i<3;i++){
			if(tempBox.min[i] > tempBox.max[i])
				counter++;
		}
		if(counter==3){
			Vector3 minimum = tempBox.max;
			tempBox.max = tempBox.min;
			tempBox.min = tempBox.max;
		}	
		return tempBox;
	}

	/**
	* @brief A constructor to set min and max points.
	* @param x1 - the minimum x-axis point of the box
	* @param y1 - the minimum y-axis point of the box
	* @param z1 - the minimum z-axis point of the box
	* @param x2 - the maximum x-axis point of the box
	* @param y2 - the maximum y-axis point of the box
	* @param z2 - the maximum z-axis point of the box
	*/
	AABB(float x1,float y1,float z1,float x2,float y2,float z2){
		AABB::min = Vector3(x1,y1,z1);
		AABB::max = Vector3(x2,y2,z2);
		CalculateCenter();
		layer =0;
	}

	/*
	* @brief A default constructor which sets the layer to 0.
	*/
	AABB(){
		layer = 0;
	}

	AABB(const AABB& box){
		this->min = box.min;
		this->max = box.max;
		this->layer = box.layer;
		CalculateCenter();
	}

	/*
	* @brief Sets the minimum 3D point of this box
	* @param min - minimum point
	*/
	void SetMinimum(const Vector3 min){
		this->min = min;
		CalculateCenter();
	}

	/*
	* @brief Sets the maximum 3D point of this box
	* @param max - maximum point
	*/
	void SetMaximum(const Vector3 max){
		this->max = max;
		CalculateCenter();
	}

	/*
	* @brief Sets the minimum and maximum points of the box.
	* @param min - minimum point
	* @param max - maximum point
	*/
	void SetBox(const Vector3 min, const Vector3 max){
		this->min = min;
		this->max = max;
		CalculateCenter();
	}

	/**
	* @brief Copies the parameter to this box.
	* box - a box of the AABB type
	*/
	void SetBox(const AABB& box){
		this->min = box.min;
		this->max = box.max;
		this->layer = box.layer;
		CalculateCenter();
	}

	/*
	* @brief Returns the minimum 3D point of the box if the parameter's value 
	* is; if its 1 then the maximum 3d point will be returned. Otherwise, it returns 
	* a zero vector.
	* @param index - 0 - min 3D vector; 1 - max 3D vector
	*/
	const Vector3 GetValue(int index){
		if(index==0)
			return min;
		else if(index==1)
			return max;
		else 
			return Vector3(0);
	}
	/*
	* @brief Returns the center of the bounding box.
	* @return 3D vector which determines the center of the box
	*/
	Vector3 Center() const {
		return center;
	}

private:
	Vector3 center;
	void CalculateCenter(){
		center = Vector3((min.x+max.x)/2,(min.y+max.y)/2,(min.z+max.z)/2);
	}
};

///BoxOBB is not implemented
struct BoxOBB{
	///center of the Box
	Vector3 center;
	///unit vector representing the local x-axis,unit vector representing the local y-axis,unit vector representing the local z-axis
	Vector3 uv[3];
	///half width of the Box(x),half height of the Box(y),half depth of the Box(z)
	Vector3 half;


	float angle;
	BoxOBB(Vector3 center, Vector3 extent){
		this->center = center;
		for(int i=0;i<3;i++){
			this->uv[i]=uv[i];
		}
		this->half = extent;
	}
	void AddAngle(float angle){
		this->angle = angle;
	}
	void ComputeUnitVector(){
		/*		Vector3 point1(center.x+half.x, center.y+half.y,center.z+half.z);
		Vector3 point2(center.x-half.x, center.y+half.y,center.z+half.z);
		Vector3 point3(center.x-half.x, center.y-half.y,center.z+half.z);
		Vector3 uZ = VectorMath::Normalized(VectorMath::SurfaceNormal(point1, point2, point3))

		Vector3 point4(center.x+half.x, center.y-half.y,center.z-half.z);
		Vector3 point5(center.x+half.x, center.y-half.y,center.z+half.z);
		Vector3 point6(center.x+half.x, center.y+half.y,center.z+half.z);
		Vector3 uX = VectorMath::Normalized(VectorMath::SurfaceNormal(point4, point5, point6));

		Vector3 point7(center.x+half.x, center.y+half.y,center.z-half.z);
		Vector3 point8(center.x-half.x, center.y+half.y,center.z-half.z);
		Vector3 point9(center.x-half.x, center.y+half.y,center.z+half.z);
		Vector3 uY = VectorMath::Normalized(VectorMath::SurfaceNormal(point7, point8, point9))*/
		/*		uv[0] = uX;
		uv[1] = uY;
		uv[2] = uZ;*/
	}

	void Rot(Vector3 rotationAngle){		
		Vector3 point;
		if(rotationAngle.x !=0){
			float cosT = cos(rotationAngle.x);
			float sinT = sin(rotationAngle.x);
			point.y = cosT * point.y - sinT * point.z;
			point.z = sinT * point.y + cosT * point.z;
		}
		if(rotationAngle.y !=0){
			float cosT = cos(rotationAngle.y);
			float sinT = sin(rotationAngle.y);
			point.z = cosT * point.z - sinT * point.x;
			point.x = sinT * point.z + cosT * point.x;
		}
		if(rotationAngle.z !=0){
			float cosT = cos(rotationAngle.z);
			float sinT = sin(rotationAngle.z);
			point.x = cosT * point.x - sinT * point.y;
			point.y = sinT * point.x + cosT * point.y;
		}
	}

	void Rotate(Vector3 degrees){
		Vector3 point;
		Vector3 rotationAngle(degrees);
		rotationAngle = rotationAngle * 3.14f / 180;//convert to radian value
//		float angle = rotationAngle.x;
		if(rotationAngle.x !=0){
			float cosT = cos(rotationAngle.x);
			float sinT = sin(rotationAngle.x);
			point.y = cosT * point.y - sinT * point.z;
			point.z = sinT * point.y + cosT * point.z;
		}
		if(rotationAngle.y !=0){
			float cosT = cos(rotationAngle.y);
			float sinT = sin(rotationAngle.y);
			point.z = cosT * point.z - sinT * point.x;
			point.x = sinT * point.z + cosT * point.x;
		}
		if(rotationAngle.z !=0){
			float cosT = cos(rotationAngle.z);
			float sinT = sin(rotationAngle.z);
			point.x = cosT * point.x - sinT * point.y;
			point.y = sinT * point.x + cosT * point.y;
		}
		/*float x2= point.x *point.x;
		float y2 = point.y * point.y;
		float z2 = point.z * point.z;*/
	}
};

///contains information required by rigid bodies; not fully implemented
struct RigidBody{
	///the gravity vector
	Vector3 Gravity;
	///the mass of the rigid body
	float mass;
	///rate of change over time or a measure of distance travelled in a measure of time; velocity +=acceleration * time_step
	Vector3 velocity;
	///A measure of velocity changed in a measure of time; acceleration = force/mass
	Vector3 acceleration; 
	/// position+=velocity *time_step
	Vector3 position;
	/// the rigid body's collider
	AABB collider;
	///the force applied to the rigid body
	Vector3 force;
	///a force that has a tendency to rotate an object; can be calculated by figuring out how far a force is applied from the center of rotation of an object
	Vector3 torgue;

	/*
	* @brief A constructor to assign the standard values to the rigid body
	* @param position - the position of the rb
	* @param velocity - the velocity of the rb
	* @param mass - the mass of the rb
	*/
	RigidBody(const Vector3 position, const Vector3 velocity, float mass){
		this->mass = mass;
		this->velocity = velocity;
		this->position = position;
		Gravity = Vector3(0,-9.8f,0);
	}

	/**
	* @brief Updates the velocity of the rb depending on the gravity and fps
	* @param the number of seconds per frame
	*/
	void BeforeCollision(float dt){
		velocity=velocity +Gravity*dt;
	}

	RigidBody(const RigidBody& rb){
		this->collider = rb.collider;
	}

	/**
	* @brief Reaction of the rb
	* @param the contact point
	*/
	void Impulse(Vector3 /*contact*/){
		for(int i=0;i<3;i++){
			if(position[i] <-1 && velocity[i]<0){
				velocity.Set(i,-velocity[i]);
			}
			if(position[i] >1 && velocity[i]>0){
				velocity.Set(i,-velocity[i]);
			}
		}
	}

	/**
	* @brief Derivative
	* @param the number of seconds per frame
	*/
	Vector3 Integration(float dt){
		position  =position + velocity *dt;
	}
	/**
	* @brief Verlet integration
	* @param dt - fps 
	* @return Updated velocity
	*/
	Vector3 Integration1(float dt){
		Vector3 lastAcceleration = acceleration;
		position  =position + velocity *dt + (lastAcceleration * dt * dt * 0.5);
		acceleration = force /mass;
		Vector3 averageAcceleration = (lastAcceleration +acceleration)/2;
		velocity = velocity + averageAcceleration * dt;
	}

	/**
	* @brief The default constructor.
	*/
	RigidBody(){}
};

///Allows to define a ray struct
struct Ray{

public:
	///the starting point of the ray
	Vector3 rayBegin;
	///the ending point of the ray
	Vector3 rayEnd;
	///the inverse of the ray
	Vector3 inverse;
	/// + -
	int sign[3];
	///parameter or ray distance
	float time;

	/*
	* @brief A constructor to Create a ray.
	* @param b - starting point
	* @param e - ending point
	* @param d - distance
	*/
	Ray(Vector3 b,Vector3 e, float d){
		Ray::rayBegin = b; 
		Ray::rayEnd=e; 

		inverse = Vector3(1/Direction().x,1/Direction().y,1/Direction().z);
		sign[0] = inverse.x<0;
		sign[1] = inverse.y<0;
		sign[2] = inverse.z<0;
		if(d>0)
			time = d;
		else
			time =rayDistance;
	};

	/**
	* @brief A constructor to Create a ray.
	* @param s - starting point
	* @param e - ending point
	*/
	Ray(Vector3 s,Vector3 e){
		time=rayDistance;
		Ray::rayBegin = s; 
		Ray::rayEnd=e; 
		inverse = Vector3(1/Direction().x,1/Direction().y,1/Direction().z);
		sign[0] = inverse.x<0;
		sign[1] = inverse.y<0;
		sign[2] = inverse.z<0;
	};

	/**
	* @brief Get a point at any distance specified by the parameter.
	* @param time - distance
	* @return 3D position
	*/
	Vector3 GetPoint(float time) const{
		if(time>0.0){
			return Vector3(rayBegin.x + Direction().x*time,rayBegin.y + Direction().y*time,rayBegin.z + Direction().z*time);
		}
		else 
			return Vector3(rayBegin.x + Direction().x*this->time,rayBegin.y + Direction().y*this->time,rayBegin.z + Direction().z*this->time);//parameteric equation of the line
	}

	/**
	* @brief The default constructor.
	*/
	Ray(){
		rayBegin = Vector3(0);
		rayEnd = Vector3(0,0,1);
		inverse = Vector3(1/Direction().x,1/Direction().y,1/Direction().z);
		time = rayDistance;
	}

	/**
	* @brief Direction vector of the line passing through RayBegin and RayEnd
	* @return Direction vector
	*/
	Vector3 Direction() const{
		return Vector3 (rayEnd.x-rayBegin.x, rayEnd.y-rayBegin.y, rayEnd.z-rayBegin.z);
	}
private:
	///the standard distance of the ray
	static const int rayDistance = 2;
};

#endif STRUCT_H