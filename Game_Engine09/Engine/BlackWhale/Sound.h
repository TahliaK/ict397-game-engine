#ifndef SOUND_H
#define SOUND_H
#include "Component.h"

#define SOUNDFILE_NOTFOUND -999
#define SOUNDLOAD_FAILED -1000
#define SOUNDLOOP_ENDLESS -1
#define SOUNDTYPE_SFX 1
#define SOUNDTYPE_MUSIC 2

class Sound : public Component {
public:
	Sound();
	virtual ~Sound();
	void Initialize();
	void Start();
	void Update();
	void Clear();
private:

};
//	///serialization
//	friend class boost::serialization::access;
//	template<class Archive>
//	void save(Archive& ar, const unsigned int /*version*/) const {
//		ar & SER_BASE_OBJECT_NVP(Component);
//	}
//	template<class Archive>
//	void load(Archive& ar, const unsigned int /*version*/) {
//		ar & SER_BASE_OBJECT_NVP(Component);
//	}
//	SER_SPLIT_MEMBER()
//};
//
//SER_CLASS_VERSION(Sound, 0);
////SER_ABSTRACT(Sound)
//SER_CLASS_EXPORT_KEY(Sound)
#endif SOUND_H