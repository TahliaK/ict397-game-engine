#ifndef SHADER_H
#define SHADER_H
#define ERROR_STATE -1
/*
* @class Shader 
* @brief Creates a shader that can be attached to a shader program
* @author Olesia Kochergina
* @date 12/01/2017
*/
class Shader{
public:
	/*
	* @brief Default c-tor
	*/
	Shader();

	/*
	* @brief D-tor
	*/
	~Shader();

	/*
	* @brief Creates a new empty shader.
	* @param shaderType - GL_FRAGMENT_SHADER or GL_VERTEX_SHADER or GL_GEOMETRY_SHADER
	* @return true if the shader type is valid and a new shader is created
	*/
	bool CreateShader(unsigned int shaderType);

	/*
	* @brief Attaches source code to the current shader
	* @param numOfStrings - amount of strings 
	* @param strings - pointer to pointers with strings
	* @param lenOfStrings - pointer to string lengths 
	*/
	void AttachSource(int numOfStrings, const char **strings, int *lenOfStrings);

	/*
	* @brief Compiles the current shader.
	*/
	void CompileShader();

	/*
	* @brief Returns the shader ID
	* @return shader ID
	*/
	unsigned int GetShader();

	/*
	* @brief Returns the status of the shader
	* @return true - the shader is valid and can be used
	*/
	bool IsCompiled();

	/*
	* @brief Displays shader errors if there any.
	*/
	void PrintInfoLog();
private:

	///id of the shader
	unsigned int m_shader;

	///status of the shader
	int m_compiled;
};
#endif