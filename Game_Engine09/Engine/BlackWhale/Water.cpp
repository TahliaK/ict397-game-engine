#include "Water.h"
#include "VertexBuffer.h"
#include "GraphicsEngine.h"
#include "Master.h"
#include "Time.h"
#include "SceneManager.h"
#include "ShaderManager.h"
BOOST_CLASS_EXPORT_IMPLEMENT(Water)

Water::Water() : Component() {
	Initialize();
	Reflection();
	static bool l_inList = false;
	if (!l_inList) {
		l_inList = true;
		Water* l_pointer = new Water;
		m_derivedClasses.push_back(l_pointer);
	}
}

Water::Water(const Water& original) : Component(original) {
	m_waveSpeed = original.m_waveSpeed;
	m_moveFactor = original.m_moveFactor;
	m_data = original.m_data;
	m_material = m_material;
	normalMap = normalMap;
}

Water::~Water() {
	Clear();
}
Component* Water::NewComponent() const {
	Component* l_pointer = new Water;
	return l_pointer;
}

void Water::Initialize() {
	m_name = "Water";
	m_waveSpeed = 0.00007f;
	m_moveFactor = 0;
	//1360 x 696
	//m_transform->scale = Vector3(340,0,174);
	//m_transform->position = Vector3(-170,0,-87);
	m_material.path = "Resources/Rendering/WaterDUDV.png";
	m_material.texture = GraphicsEngine::LoadTexture(m_material.path, false);
	normalMap = GraphicsEngine::LoadTexture("Resources/Rendering/WaterNormalMap.png", false);
	m_material.ambient = RGBA(0.084, 0.159, 0.309);
	if (m_material.texture == 0)
		cout << "INVALID WATER TEXTURE" << endl;
}

void Water::Clear() {
	m_data.Clear();
}

void Water::Start() {
	{
		m_data.attribLocation[0] = Master::gl.GetALoc(ShaderManager::GetActive()->GetID(), "position");
		m_data.attribLocation[1] = Master::gl.GetALoc(ShaderManager::GetActive()->GetID(), "texCoord");
		m_data.attribLocation[2] = Master::gl.GetALoc(ShaderManager::GetActive()->GetID(), "normal");

		/*m_data.attribLocation[0] = Master::Master::gl..GetALoc(Master::s_GeomPassShader.GetID(),"position");
		m_data.attribLocation[1] = Master::Master::gl..GetALoc(Master::s_GeomPassShader.GetID(),"texCoord");
		m_data.attribLocation[2] = Master::Master::gl..GetALoc(Master::s_GeomPassShader.GetID(),"normal");
	*/}
	float polygonV[] = { m_gameObject->GetTransform()->position.x,m_gameObject->GetTransform()->position.z,m_gameObject->GetTransform()->position.x + m_gameObject->GetTransform()->scale.x,m_gameObject->GetTransform()->position.z + m_gameObject->GetTransform()->scale.z };
	GraphicsEngine::AddPolygon(polygonV[0], polygonV[1], polygonV[2], polygonV[3], m_data.data.vertex, true);
	for (unsigned i = 0; i < m_data.data.vertex.size(); i++) {
		m_data.data.vertex.at(i).z = m_data.data.vertex.at(i).y;
		m_data.data.vertex.at(i).y = 1;
	}
	/*	float pos[4];
	pos[0] = (float)30/Master::screen[1][0];
	pos[1] = (float)200/Master::screen[1][1];
	pos[2] = (float)(1065)/Master::screen[1][0];
	pos[3] = (float)667/Master::screen[1][1];*/
	//float polygonT[] = {pos[0],pos[1],pos[2],pos[3]};
	float polygonT[] = { 0,0,1,1 };
	GraphicsEngine::AddPolygon(polygonT[0], polygonT[1], polygonT[2], polygonT[3], m_data.data.texture, true);
	m_data.data.normal = GraphicsEngine::GenerateNormals(m_data.data.vertex);
	{
		//VertexBuffer::CreateVBO(&m_data,Master::s_GeomPassShader.GetID());
		//VertexBuffer::CreateVBO(&m_data,ShaderManager::GetActive()->GetID());
		VertexBuffer::CreateVBO(&m_data, ShaderManager::GetActive()->GetID());
	}
}
void Water::Update() {
	if (m_material.texture == 0)
		m_material.texture = GraphicsEngine::LoadTexture(m_material.path, false);
	if (normalMap == 0)
		normalMap = GraphicsEngine::LoadTexture("Resources/Rendering/WaterNormalMap.png", false);
	//
	Master::gl.TexEnvi(GRAPHICS_TEXTURE_ENV, GRAPHICS_TEXTURE_ENV_MODE, GRAPHICS_MODULATE);

	//Master::gl.UniformMatrix3fv(Master::gl.GetUniformLocation(Master::s_3DShader.GetID(),"Material"),1,GRAPHICS_FALSE,&tMatrix[0]);
	m_moveFactor += m_waveSpeed * 2;//Time::GetCurrent();
	m_moveFactor = fmod(m_moveFactor, 1);
	Matrix4 l_modelMatrix(SceneManagement::SceneManager::GetActive()->GetMainCamera()->GetModelMatrix());
	l_modelMatrix = Matrix4::Scale(l_modelMatrix, Vector4(m_gameObject->GetTransform()->scale.x, m_gameObject->GetTransform()->scale.y, m_gameObject->GetTransform()->scale.z, 0));
	l_modelMatrix = Matrix4::Rotate(l_modelMatrix, Vector4(m_gameObject->GetTransform()->rotation.x, m_gameObject->GetTransform()->rotation.y, m_gameObject->GetTransform()->rotation.z, 0));
	l_modelMatrix = Matrix4::Translate(l_modelMatrix, Vector4(m_gameObject->GetTransform()->position.x, m_gameObject->GetTransform()->position.y, m_gameObject->GetTransform()->position.z, 0));

	{
		Master::gl.UniformMatrix4fv(ShaderManager::GetActive()->GetLocation("model"), 1, GRAPHICS_FALSE, l_modelMatrix);
		Master::gl.UniformMatrix4fv(ShaderManager::GetActive()->GetLocation("proj"), 1, GRAPHICS_FALSE, SceneManagement::SceneManager::GetActive()->GetMainCamera()->GetProjectionMatrix());
		Master::gl.UniformMatrix4fv(ShaderManager::GetActive()->GetLocation("view"), 1, GRAPHICS_FALSE, SceneManagement::SceneManager::GetActive()->GetMainCamera()->GetViewMatrix());
		//Master::gl.Uniform3fv(ShaderManager::GetActive()->GetLocation("cameraPos"), 1, SceneManagement::SceneManager::GetActive()->GetMainCamera()->GetGameObject()->GetTransform()->position);
		Master::gl.Uniform1i(ShaderManager::GetActive()->GetLocation("u_shader"), 3);
		GraphicsEngine::SwitchToRGB(true);
		/*Master::gl.ActiveTexture(GRAPHICS_TEXTURE0);
		Master::gl.BindTexture(GRAPHICS_TEXTURE_2D, m_material.texture);*/
		Master::gl.TexEnvi(GRAPHICS_TEXTURE_ENV, GRAPHICS_TEXTURE_ENV_MODE, GRAPHICS_MODULATE);
		Master::gl.UniformMatrix4fv(ShaderManager::GetActive()->GetLocation("Material"), 1, GRAPHICS_FALSE, m_material.GetVector());
		Master::gl.Uniform1f(ShaderManager::GetActive()->GetLocation("shininess"), m_moveFactor);
		//Master::gl.UniformMatrix4fv(ShaderManager::GetActive()->GetLocation("model"), 1, GRAPHICS_FALSE, l_modelMatrix);
		/*GraphicsEngine::EnableTexture(0,Master::s_reflectionFBO.GetColor(),ShaderManager::GetActive()->GetLocation("reflectionT"));
		GraphicsEngine::EnableTexture(1,Master::s_refractionFBO.GetColor(),ShaderManager::GetActive()->GetLocation("refractionT"));*/
		GraphicsEngine::EnableTexture(0, m_material.texture, ShaderManager::GetActive()->GetLocation("texture"));
		GraphicsEngine::EnableTexture(1, normalMap, ShaderManager::GetActive()->GetLocation("DetailMap"));
		GraphicsEngine::EnableTexture(2, GraphicsEngine::GetGBufferColorTexture(), ShaderManager::GetActive()->GetLocation("multitexture"));
		//Master::gl.Uniform1f(ShaderManager::GetActive()->GetLocation("moveFactor"),m_moveFactor);

		//Master::gl.UniformMatrix4fv(Master::s_WaterDRShader.GetLocation("model"), 1, GRAPHICS_FALSE, SceneManagement::SceneManager::GetActive()->GetMainCamera()->GetModelMatrix());
		//Master::gl.UniformMatrix4fv(Master::s_WaterDRShader.GetLocation("proj"), 1, GRAPHICS_FALSE, SceneManagement::SceneManager::GetActive()->GetMainCamera()->GetProjectionMatrix());
		//Master::gl.UniformMatrix4fv(Master::s_WaterDRShader.GetLocation("view"), 1, GRAPHICS_FALSE, SceneManagement::SceneManager::GetActive()->GetMainCamera()->GetViewMatrix());
		//Master::gl.Uniform3fv(Master::s_WaterDRShader.GetLocation("cameraPos"), 1, SceneManagement::SceneManager::GetActive()->GetMainCamera()->GetGameObject()->GetTransform()->position);

		////Master::gl.UniformMatrix4fv(Master::s_WaterDRShader.GetLocation("model"), 1, GRAPHICS_FALSE, l_modelMatrix);
		//GraphicsEngine::EnableTexture(0, Master::s_reflectionFBO.GetColor(), Master::s_WaterDRShader.GetLocation("reflectionT"));
		//GraphicsEngine::EnableTexture(1, Master::s_refractionFBO.GetColor(), Master::s_WaterDRShader.GetLocation("refractionT"));
		//GraphicsEngine::EnableTexture(2, m_material.texture, Master::s_WaterDRShader.GetLocation("dudvMap"));
		//GraphicsEngine::EnableTexture(3, normalMap, Master::s_WaterDRShader.GetLocation("normalMap"));
		//Master::gl.Uniform1f(Master::s_WaterDRShader.GetLocation("moveFactor"), m_moveFactor);
		/*BindMultitexture(0,Master::s_reflectionFBO.GetColor(),Master::s_GeomPassShader.GetLocation("reflectionT"));
		BindMultitexture(1,Master::s_refractionFBO.GetColor(),Master::s_GeomPassShader.GetLocation("refractionT"));
		BindMultitexture(2,m_material.texture,Master::s_GeomPassShader.GetLocation("dudvMap"));
		BindMultitexture(3,normalMap,Master::s_GeomPassShader.GetLocation("normalMap"));
		Master::gl.Uniform1f(Master::s_GeomPassShader.GetLocation("moveFactor"),move_factor);*/
	}
	VertexBuffer::RenderVBO(&m_data, 0, m_data.data.vertex.size());
	for (unsigned i = 0; i < 4; i++)
		GraphicsEngine::DisableTexture(i);
	Master::gl.ActiveTexture(GRAPHICS_TEXTURE0);
	Master::gl.BindTexture(GRAPHICS_TEXTURE_2D, Master::NULL_TEXTURE);
}

//bool Water::SetMoveFactor(boost::any p_var){
//	try {
//		m_moveFactor = boost::any_cast<float> (p_var);
//	}
//	catch (boost::bad_any_cast& /*bac*/) {
//		return false;
//	}
//	return true;
//}
//float Water::GetMoveFactor(void) {
//	return m_moveFactor;
//}
bool Water::SetWaveSpeed(boost::any p_var) {
	try {
		m_waveSpeed = boost::any_cast<float> (p_var);
	}
	catch (boost::bad_any_cast& /*bac*/) {
		return false;
	}
	return true;
}

float Water::GetWaveSpeed(void) const {
	return m_waveSpeed;
}

bool Water::SetColor(boost::any p_var) {
	try {
		RGBA l_temp = boost::any_cast<RGBA> (p_var);
		IFORSI(0, 4) {
			if (l_temp.GetColor()[i] > 1 || l_temp.GetColor()[i] < 0)
				return false;
		}
		m_material.ambient = l_temp;
	}
	catch (boost::bad_any_cast& /*bac*/) {
		return false;
	}
	return true;
}

RGBA Water::GetColor(void) const {
	return m_material.ambient;
}
void Water::Reflection() {
	m_Variables.push_back("Wave speed");
	m_Setters.push_back(boost::bind(&Water::SetWaveSpeed, this, _1));
	m_Getters.push_back(boost::bind(&Water::GetWaveSpeed, this));

	m_Variables.push_back("Color");
	m_Setters.push_back(boost::bind(&Water::SetColor, this, _1));
	m_Getters.push_back(boost::bind(&Water::GetColor, this));
}