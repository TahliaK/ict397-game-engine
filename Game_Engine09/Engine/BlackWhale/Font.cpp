#include "Font.h"
#include "Label.h"
#include "Master.h"
using namespace Text;

Font::Font() {
	m_texture = 0;
}

Font::Font(int p_texture, string p_fontFile){
	m_texture = p_texture;
	m_loader  = MeshGenerator(p_fontFile);
}

Font::~Font() {
//	Clear();
}

void Font::Clear() {
	if (m_texture != 0) {
		//GraphicsEngine::DeleteTexture(m_texture);
		m_texture = 0;
	}
}

void Font::LoadSDF(std::string name){
	//textureAtlas.LoadTexture(name);
}


void Font::LoadText(Label* text, MeshData& data){
	return m_loader.CreateMesh(text,data);
}

int Font::GetTexture() const{
	return m_texture;

}