#include "GraphicsEngine.h"

#include <stdio.h>
#include <math.h>
#include <iostream>
#include <fstream>
#include <sstream>

#include <Windows.h>
#include "Camera.h"

#include "Master.h"
#include "Interfaces\DirectMediaLayer.h"
using namespace std;
unsigned int GraphicsEngine::S_DRAW_CALLS=0;
int GraphicsEngine::curIndex;
std::map<std::string, unsigned int> GraphicsEngine::m_textureCollection;
GBuffer GraphicsEngine::m_gbuffer;
unsigned int GraphicsEngine::m_deferredVAO;
unsigned int GraphicsEngine::m_deferredVBO;

void GraphicsEngine::Initialize(void) {
	ThirdParty::DirectMediaLayer::Initialize();
	int d = 0;
	int* argc = &d;
	Master::gl.Init(argc);
	Master::gl.InitDisplayMode(DISPLAY_MODE_DEPTH | DISPLAY_MODE_DOUBLE | DISPLAY_MODE_RGBA);
	Master::gl.Enable(GRAPHICS_SCISSOR_TEST);//clipping 
	Master::gl.Enable(GRAPHICS_CULL_FACE);//removing back faces
	Master::gl.Enable(GRAPHICS_DEPTH_TEST);//hidden surface removal
}
void GraphicsEngine::Destroy(void) {
	Clear();
	ThirdParty::DirectMediaLayer::Destroy();
}

void GraphicsEngine::Start() {
	/*glEnable(GL_LINE_SMOOTH);
	glEnable(GL_POINT_SMOOTH);
	glEnable(GL_POLYGON_SMOOTH);
	glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);
	glHint(GL_LINE_SMOOTH_HINT,GL_NICEST);
	glHint(GL_POINT_SMOOTH_HINT,GL_NICEST);
	glHint(GL_POLYGON_SMOOTH_HINT,GL_NICEST);*/
}
GraphicsEngine::GraphicsEngine() {}

GraphicsEngine::GraphicsEngine(int type) {
	state = GRAPHICS_STATIC_DRAW;
	textureType = GRAPHICS_TEXTURE_2D;
	textureCount = 0;

	lastImage = 0;
	GraphicsEngine::curIndex = 0;
	objectIndex[0] = curIndex;
	TBO = NULL;
	polygonIndex = 0;
	for (int i = 0; i < 3; i++) {
		envColor[i] = 1;
		tempColor[i] = 1;
	}
	this->type = type;
	if (type == 2 || type == 4) {
		state = GRAPHICS_DYNAMIC_DRAW;
	}
	envColorSystem = -1;
}

GraphicsEngine::~GraphicsEngine() {
	//	glDeleteLists(10000+objectIndex[0],objectIndex[1]-objectIndex[0]);
}

void GraphicsEngine::SwitchToRGB(bool isRGB) {
	if (isRGB) {
		//returns an error 1280 INVALID_ENUM
		//	glTexEnvi(GL_TEXTURE_ENV,GL_TEXTURE_ENV_MODE,GL_TEXTURE);
		Master::gl.TexEnvi(GRAPHICS_TEXTURE_ENV, GRAPHICS_TEXTURE_ENV_MODE, GRAPHICS_COMBINE);
		Master::gl.Disable(GRAPHICS_ALPHA_TEST);
		Master::gl.Disable(GRAPHICS_BLEND);
	}
	else {
		Master::gl.Enable(GRAPHICS_BLEND);//optional
		Master::gl.BlendFunc(GRAPHICS_SRC_ALPHA, GRAPHICS_ONE_MINUS_SRC_ALPHA);//optional
		Master::gl.Enable(GRAPHICS_ALPHA_TEST);
		Master::gl.AlphaFunc(GRAPHICS_GREATER, 0.5);
		Master::gl.TexEnvi(GRAPHICS_TEXTURE_ENV, GRAPHICS_TEXTURE_ENV_MODE, GRAPHICS_REPLACE);
	}
}

void GraphicsEngine::ColorSettings() {
	/*glTexEnvi(GL_TEXTURE_ENV,GL_TEXTURE_ENV_MODE,GL_TEXTURE);
	glDisable(GL_ALPHA_TEST);
	glDisable(GL_TEXTURE_2D);
	glEnable( GL_BLEND );
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);*/
}

void GraphicsEngine::overrideTexEnv(int colorType, float colorValue[]) {
	if (colorValue != NULL) {
		for (int i = 0; i < 3; i++) {
			envColor[i] = colorValue[i];
			tempColor[i] = envColor[i];
		}
	}
	envColorSystem = colorType;
}

const unsigned int GraphicsEngine::GetGBufferColorTexture() {
	return m_gbuffer.GetDiffuseTexture();
}

const void GraphicsEngine::ResetDeferred(void) {
	Master::gl.ClearColor(Master::s_worldColor);
	RenderGBuffer();
	Master::gl.ReadBuffer(GRAPHICS_COLOR_ATTACHMENT4);
	Master::gl.CullFace(GRAPHICS_BACK);
	Master::gl.Disable(GRAPHICS_BLEND);
	Master::gl.DrawBuffer(GRAPHICS_COLOR_ATTACHMENT0);
	IFORSI(0, 3) {
		Master::gl.ActiveTexture(GRAPHICS_TEXTURE + i);
		Master::gl.Disable(GRAPHICS_TEXTURE_2D);
	}
	Master::gl.ActiveTexture(GRAPHICS_TEXTURE0);
	Master::gl.BindTexture(GRAPHICS_TEXTURE_2D, 0);
	Master::gl.BindFB(GRAPHICS_FRAMEBUFFER, 0);
	Master::gl.DepthMask(GRAPHICS_TRUE);
	Master::gl.Enable(GRAPHICS_DEPTH_TEST);
	Master::gl.Disable(GRAPHICS_BLEND);
}

const void GraphicsEngine::SetDeferredGeometry(void) {
	m_gbuffer.Bind();
	m_gbuffer.Write();
	Master::gl.Disable(GRAPHICS_SCISSOR_TEST);
	Master::gl.Enable(GRAPHICS_DEPTH_TEST);
	Master::gl.DepthMask(GRAPHICS_TRUE);
	Master::gl.CullFace(GRAPHICS_BACK);
	Master::gl.Disable(GRAPHICS_BLEND);
	Master::gl.DepthFunc(GRAPHICS_LEQUAL);
	Master::gl.Enable(GRAPHICS_DEPTH_TEST);
	Master::gl.Disable(GRAPHICS_TEXTURE_2D);
	Master::gl.ClearColor(Master::s_worldColor);
	Master::gl.Clear(GRAPHICS_COLOR_BUFFER_BIT | GRAPHICS_DEPTH_BUFFER_BIT);
	
}

const void GraphicsEngine::SetDeferredLight(void) {
	m_gbuffer.Unbind();
	Master::gl.DepthMask(GRAPHICS_FALSE);
	Master::gl.DepthFunc(GRAPHICS_LEQUAL);
	Master::gl.Disable(GRAPHICS_DEPTH_TEST);
	Master::gl.DepthMask(GRAPHICS_FALSE);
	Master::gl.BlendFunc(GRAPHICS_ONE, GRAPHICS_ONE);
	Master::gl.Disable(GRAPHICS_BLEND);
	Master::gl.Clear(GRAPHICS_COLOR_BUFFER_BIT | GRAPHICS_DEPTH_BUFFER_BIT);
	m_gbuffer.Read();
}

void GraphicsEngine::RenderGBuffer(void) {
	if (m_deferredVAO == 0)
	{
		float quadVertices[] = {
			// Positions        // Texture Coords
			-1.0f, 1.0f, 0.0f, 0.0f, 1.0f,
			-1.0f, -1.0f, 0.0f, 0.0f, 0.0f,
			1.0f, 1.0f, 0.0f, 1.0f, 1.0f,
			1.0f, -1.0f, 0.0f, 1.0f, 0.0f,
		};
		// Setup plane VAO
		Master::gl.GenVAO(1, &m_deferredVAO);
		Master::gl.GenBuffer(1, &m_deferredVBO);
		Master::gl.BindVAO(m_deferredVAO);
		Master::gl.BindBuffer(GRAPHICS_ARRAY_BUFFER, m_deferredVBO);
		Master::gl.BufferData(GRAPHICS_ARRAY_BUFFER, sizeof(quadVertices), &quadVertices, GRAPHICS_STATIC_DRAW);

		Master::gl.EnableVertexAttribArray(0);
		Master::gl.VertexAttribPointer(0, 3, GRAPHICS_FLOAT, GRAPHICS_FALSE, 5 * sizeof(float), (void*)0);
		Master::gl.EnableVertexAttribArray(1);
		Master::gl.VertexAttribPointer(1, 2, GRAPHICS_FLOAT, GRAPHICS_FALSE, 5 * sizeof(float), (void*)(3 * sizeof(float)));
	}
	Master::gl.BindVAO(m_deferredVAO);
	Master::gl.PolygonMode(GRAPHICS_FRONT, GRAPHICS_FILL);
	Master::gl.DrawArrays(GRAPHICS_TRIANGLE_STRIP, 0, 4);
	Master::gl.BindVAO(0);
}

const void GraphicsEngine::Reshape(void) {
	Clear();
	m_gbuffer.Create();
}

const void GraphicsEngine::Clear(void) {
	m_gbuffer.Clear();
}
const unsigned int* GraphicsEngine::loadTextures(string directory, string ext[], int Resize) {
	WIN32_FIND_DATA data;
	for (int i = 0; i < Resize; i++) {
		if (strcmp(ext[i].c_str(), "bmp") == 0 || strcmp(ext[i].c_str(), "jpg") == 0 || strcmp(ext[i].c_str(), "png") == 0) {
			HANDLE hFind = ::FindFirstFile((directory + "/*." + ext[i]).c_str(), &data);
			if (hFind != INVALID_HANDLE_VALUE) {
				do {
					if (!data.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY) {
						textures[textureCount] = LoadTexture(data.cFileName, true);
						textureCount++;
					}
				} while (::FindNextFile(hFind, &data));
				::FindClose(hFind);
			}
		}
	}
	return textures;
}

const unsigned int GraphicsEngine::LoadCubemap(const std::vector<std::string> p_files) {
	unsigned int texture = NULL;
	unsigned int textureType = GRAPHICS_TEXTURE_CUBE_MAP;
	bool flip = false;
	float l_param = GRAPHICS_CLAMP_TO_EDGE;
	Master::gl.GenTexture(1, &texture);
	Master::gl.ActiveTexture(GRAPHICS_TEXTURE0);
	Master::gl.BindTexture(textureType, texture);
	ThirdParty::DirectMediaLayer l_sdl;

	IFORSI(0, p_files.size()) {
		ThirdParty::Image l_image;
		l_image.invert = flip;

		if (!l_sdl.LoadImage2D(p_files.at(i), l_image))
			cout << "[ERROR] CANNOT LOAD IMAGE: " << p_files.at(i) << endl;
		else {
			unsigned int format = GRAPHICS_RGBA;
			if (l_image.bytesPerPixel * 8 == 24) {
				format = GRAPHICS_RGB;
			}
			else if (l_image.bytesPerPixel * 8 == 32) {
				format = GRAPHICS_RGBA;
			}
			else {
				cout << "[ERROR] CANNOT LOAD IMAGE INVALID PIXEL FORMAT: " << p_files.at(i) << endl;
			}
			Master::gl.TexImage2D(GRAPHICS_TEXTURE_CUBE_MAP_POSITIVE_X + i, 0, l_image.bytesPerPixel, l_image.width, l_image.height, 0, format, GRAPHICS_UNSIGNED_BYTE, l_image.pixels);

			l_sdl.Clear();
		}
	}


	Master::gl.TexParami(textureType, GRAPHICS_TEXTURE_WRAP_R, l_param);
	Master::gl.TexParamf(textureType, GRAPHICS_TEXTURE_WRAP_S, l_param);
	Master::gl.TexParamf(textureType, GRAPHICS_TEXTURE_WRAP_T, l_param);
	Master::gl.TexParami(textureType, GRAPHICS_TEXTURE_MIN_FILTER, GRAPHICS_LINEAR);
	Master::gl.TexParami(textureType, GRAPHICS_TEXTURE_MAG_FILTER, GRAPHICS_LINEAR);
	Master::gl.BindTexture(textureType, 0);
	return texture;
}

const bool GraphicsEngine::DeleteTexture(unsigned int p_texture) {
	for (std::map<std::string, unsigned int>::iterator it = m_textureCollection.begin(); it != m_textureCollection.end();) {
		if (it->second == p_texture) {
			m_textureCollection.erase(it);
			Master::gl.DeleteTextures(1, &p_texture);
			return true;
		}
		else
			++it;
	}
	return false;
}

Matrix4 GraphicsEngine::BuildModelMatrix(Matrix4 p_modelMatrix, Vector3 p_position, Vector3 p_rotation, Vector3 p_scale) {
	p_modelMatrix = Matrix4::Scale(p_modelMatrix, Vector4(p_scale.x, p_scale.y, p_scale.z, 0));
	p_modelMatrix = Matrix4::Rotate(p_modelMatrix, Vector4(p_rotation.x, p_rotation.y, p_rotation.z, 0));
	p_modelMatrix = Matrix4::Translate(p_modelMatrix, Vector4(p_position.x, p_position.y, p_position.z, 0));
	return p_modelMatrix;
}

void GraphicsEngine::EnableTexture(int p_ID, unsigned int p_texture, int p_loc) {
	Master::gl.Uniform1i(p_loc, p_ID);
	Master::gl.ActiveTexture(GRAPHICS_TEXTURE0 + p_ID);
	Master::gl.BindTexture(GRAPHICS_TEXTURE_2D, p_texture);
}

void GraphicsEngine::DisableTexture(int ID) {
	Master::gl.ActiveTexture(GRAPHICS_TEXTURE + ID);
	Master::gl.BindTexture(GRAPHICS_TEXTURE_2D, Master::NULL_TEXTURE);
	Master::gl.Disable(GRAPHICS_TEXTURE_2D);
}

const  unsigned int GraphicsEngine::LoadTexture(unsigned int textureType, unsigned int textureParam, float p_param, std::string p_fileName, bool flip) {
	unsigned int texture = 0;
	if (m_textureCollection.size() != 0 && m_textureCollection.find(p_fileName) != m_textureCollection.end() && m_textureCollection.at(p_fileName) != -1 && m_textureCollection.at(p_fileName) != 0) {
		return m_textureCollection.at(p_fileName);
	}
	else {
		ThirdParty::DirectMediaLayer l_sdl;
		ThirdParty::Image l_image;
		l_image.invert = flip;

		if (!l_sdl.LoadImage2D(p_fileName, l_image))
			cout << "[ERROR] CANNOT LOAD IMAGE: " << p_fileName << endl;
		else {
			unsigned int format = GRAPHICS_RGBA;
			if (l_image.bytesPerPixel * 8 == 24) {
				format = GRAPHICS_RGB;
			}
			else if (l_image.bytesPerPixel * 8 == 32) {
				format = GRAPHICS_RGBA;
			}
			else {
				cout << "[ERROR] CANNOT LOAD IMAGE INVALID PIXEL FORMAT: " << p_fileName << endl;
			}
			Master::gl.GenTexture(1, &texture);
			Master::gl.BindTexture(textureType, texture);
			//glTexStorage2D(GL_TEXTURE_2D,4,GL_RGBA8,surface->w,surface->h);
			//glGenerateMipmap(GL_TEXTURE_2D);
			Master::gl.TexParamf(textureType, GRAPHICS_TEXTURE_LOD_BIAS, -0.4);
			Master::gl.TexParami(textureType, GRAPHICS_GENERATE_MIPMAP, GRAPHICS_TRUE);//removed from GL 3.1 and above
			Master::gl.TexParamf(textureType, GRAPHICS_TEXTURE_WRAP_S, p_param);
			Master::gl.TexParamf(textureType, GRAPHICS_TEXTURE_WRAP_T, p_param);
			Master::gl.TexParami(textureType, GRAPHICS_TEXTURE_MAG_FILTER, GRAPHICS_LINEAR);
			//Master::gl.TexParami(textureType,GL_TEXTURE_MIN_FILTER,GL_LINEAR);
			Master::gl.TexParami(textureType, GRAPHICS_TEXTURE_MIN_FILTER, GRAPHICS_LINEAR_MIPMAP_LINEAR);

			Master::gl.TexImage2D(textureParam, 0, l_image.bytesPerPixel, l_image.width, l_image.height, 0, format, GRAPHICS_UNSIGNED_BYTE, l_image.pixels);


			//glTexImage2D(textureParam,0,(int)surface->format->BytesPerPixel, surface->w,surface->h,0,format, GL_UNSIGNED_BYTE,surface->pixels);
			if (GRAPHICS_EXT_texture_filter_anisotropic) {

				float amount = 4.0;
				if (amount > GRAPHICS_MAX_TEXTURE_MAX_ANISOTROPY_EXT)
					amount = GRAPHICS_MAX_TEXTURE_MAX_ANISOTROPY_EXT;
				Master::gl.TexParamf(GRAPHICS_TEXTURE_2D, GRAPHICS_TEXTURE_MAX_ANISOTROPY_EXT, amount);
			}
			else {
				//not supported
			}
			l_sdl.Clear();
		}
		if (texture != 0)
			m_textureCollection.insert(std::pair<std::string,unsigned int>(p_fileName, texture)).first->second;
	}
	return texture;
}

const unsigned int GraphicsEngine::LoadTexture(std::string p_fileName, bool p_flip) {
	return LoadTexture(GRAPHICS_TEXTURE_2D, GRAPHICS_TEXTURE_2D, GRAPHICS_REPEAT, p_fileName, p_flip);
}

const bool GraphicsEngine::LoadHeightmap(std::string fileName, int*& texture, int& width, int& height) {
	ThirdParty::DirectMediaLayer l_sdl;
	ThirdParty::Image l_image;
	if (!l_sdl.LoadImage2D(fileName, l_image)) {
		std::cout << "Cannot load the " << fileName.c_str() << " image for terrain." << std::endl;
		return false;
	}
	else {
		width = l_image.width;
		height = l_image.height;
		texture = new int[width * height];
		IFORSI(0, width) {
			JFORSI(0, height) {
				ThirdParty::RGB_pixel l_rgb;
				//get pixel's color
				l_sdl.GetRGB(i, j, l_rgb);
				//added  int 
				texture[width * i + j] = (int)((l_rgb.r + l_rgb.g + l_rgb.b) / 3.0f);
			}
		}
		l_sdl.Clear();
	}
	return true;
}


void GraphicsEngine::FogControl(bool isFoggy, float nearR, float farR) {
	/*if(isFoggy){
		Master::gl.Enable(GRAPHICS_FOG);
		glClearColor(0.7, 0.7, 0.7, 1);
		float fogColor[4] = {0.7, 0.7, 0.7, 1};
		glFogi (GL_FOG_MODE, GL_LINEAR);
		glFogfv (GL_FOG_COLOR, fogColor);
		glFogf (GL_FOG_DENSITY, 0.35);
		glHint (GL_FOG_HINT, GL_NICEST);
		glFogf (GL_FOG_START, nearR);//70
		glFogf (GL_FOG_END, farR);//20000
	}else{
		glDisable(GL_FOG);
		glClearColor(97.0/255.0, 140.0/255.0, 185.0/255.0, 1.0);
	}*/
}

void GraphicsEngine::AddPolygon(float minX, float minY, float maxX, float maxY, vector<Vector3>& data, bool clockwise) {

	float CW[] = { minX,maxY,maxX,maxY,maxX,minY,   minX,maxY,maxX,minY,minX,minY };
	float CCW[] = { maxX,minY,maxX,maxY,minX,maxY,   minX,minY,maxX,minY,minX,maxY };
	if (clockwise) {
		for (unsigned i = 0; i < 12; i += 2)
			data.push_back(Vector3(CW[i], CW[i + 1], 0));
	}
	else {
		for (unsigned i = 0; i < 12; i += 2)
			data.push_back(Vector3(CCW[i], CCW[i + 1], 0));
	}
}

vector<Vector3> GraphicsEngine::GenerateNormals(const vector<Vector3>& tempV) {
	vector<Vector3> tempN(tempV.size());
	for (unsigned int i = 0; i < tempV.size() - 2; i += 3)
	{
		Vector3 v0 = tempV[i];
		Vector3 v1 = tempV[i + 1];
		Vector3 v2 = tempV[i + 2];

		Vector3 normal = Vector3::SurfaceNormal(v1, v0, v2);

		tempN[i] = normal;
		tempN[i + 1] =  normal;
		tempN[i + 2] =  normal;
	}
	for (unsigned int i = 0; i < tempV.size(); i++)
	{
		tempN[i] = Vector3::Normalize(tempN[i]);
	}
	return tempN;
}