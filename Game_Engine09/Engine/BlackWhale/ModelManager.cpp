#include "ModelManager.h"

#include <fstream>
//#include <thread>
//#include <future>
#include "OBJ_loader.h"
std::map<std::string, Mesh*> ModelManager::s_collection;
//std::thread ModelManager::s_thread;
Mesh* ModelManager::AddMesh(std::string p_fileName, int p_shader) {
	if (s_collection.find(p_fileName) != s_collection.end()) {
		return s_collection.at(p_fileName);
	}
	else {
		/*if (s_thread.joinable())
			s_thread.join();*/
		//.OBJ file
		if (p_fileName.substr(p_fileName.size() - 3).compare("obj") == 0) {
			Mesh* l_model = new OBJ_model(p_shader);
			//auto future = std::async(LoadOBJFile, *((OBJ_model*)l_model), p_fileName);
			
			//s_thread = thread(LoadOBJFile(*((OBJ_model*)l_model), p_fileName));
			if (LoadOBJFile(*((OBJ_model*)l_model), p_fileName)) {
				s_collection.insert(std::pair<std::string, Mesh*>(p_fileName, l_model));
				return s_collection.at(p_fileName);
			}
		}
	}
	return NULL;
}

void ModelManager::Clear(void) {
	std::map<std::string, Mesh*>::iterator l_it = s_collection.begin();
	while (l_it != s_collection.end()) {
		delete l_it->second;
		l_it->second = NULL;
		l_it++;
	}
	s_collection.clear();
}

Mesh* ModelManager::GetMesh(std::string p_fileName) {
	if (s_collection.find(p_fileName) != s_collection.end()) {
		return s_collection.at(p_fileName);
	}
	return NULL;
}

bool ModelManager::LoadOBJFile(OBJ_model& model, string filename) {
	bool l_result = false;
	std::ifstream inFile;
	OBJ_loader OBJloader;

	OBJloader.SetCurrentModel(&model);
	inFile.open(filename);
	if (!inFile.bad() && !inFile.fail() && inFile.is_open()) {
		inFile >> OBJloader;
		l_result = true;
	}
	else {
		cout << "OBJ file does not exist: " << filename << endl;
		l_result = false;
	}
	inFile.close();
	return l_result;
}