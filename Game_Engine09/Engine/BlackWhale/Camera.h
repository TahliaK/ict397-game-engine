#ifndef CAMERA_H
#define CAMERA_H
#include "Math_s.h"
#include "GameObject.h"
#include "Transform.h"
#define PIdiv180 (PI/180.0)
#include "FrustumCulling.h"
/*
* @class Camera
* @brief Defines the generic behaviour of 2D/3D camera.
* @version 04
* @date 22/05/2017
* @author Olesia Kochergina
*/
class Camera : public Component {
public:

	/*
	* @brief Default c-tor.
	*/
	Camera();

	/*
	* @brief Copy c-tor.
	*/
	Camera(const Camera& original);

	/*
	* @brief Destructor.
	*/
	virtual ~Camera() {};

	/*
	* @brief Initializes the object, called when creating the object.
	*/
	void Initialize();

	/*
	* @brief Called just before the first update.
	*/
	void Start();

	/*
	* @brief Updates the object and should be called every frame.
	*/
	void Update();

	/*
	* @brief Clears internal data, should be called when
	* the object needs to be either reused or deleted.
	*/
	void Clear() {};

	/*
	* @brief Creates a new component of this class.
	* @return Pointer to the new component or NULL.
	*/
	Component* NewComponent(void) const;
	
	/*
	* @brief Vertically inverts the camera.
	*/
	void InvertPitch();

	/**
	* @brief
	* @param rotation - rotation.x is pitch; rotation.y is yaw; rotation.z is roll;
	*/
	void Rotate(const Vector3 rotation);

	/**
	* @brief
	* @param direction - direction.x is for moving forward or backward; direction.y is for moving upward or downward; direction.z is used for strafing right or left;
	*/
	void Move(const Vector3 direction);

	/*
	* @brief Returns the target of the camera based on its position and view.
	* @return target of the camera.
	*/
	Vector3 GetTarget(void) const;

	/*
	* @brief Returns the model matrix of the camera
	* @return Model matrix
	*/
	Matrix4& GetModelMatrix();

	/*
	* @brief Returns the view matrix of the camera
	* @return View matrix
	*/
	Matrix4& GetViewMatrix();

	/*
	* @brief Returns the projection matrix of the camera
	* @return Projection matrix
	*/
	Matrix4& GetProjectionMatrix();
	
	/**
	* @brief If the parameter is true then the up vector 
	* will not be updated with other transformation vectors.
	* @param lock - bool value
	* @return true - if set 
	*/
	bool SetLockUp(boost::any lock);
	
	/**
	* @brief Sets the camera mode, which can be either 2D or 3D
	* @param mode - if true then 3D
	* @return true - if set
	*/
	bool SetMode(boost::any mode);

	/**
	* @brief Returns the camera mode
	* @return true - 3D camera, else false
	*/
	bool GetMode(void) const;

	/**
	* @brief Returns wether the up vector is locked or not
	* @return true - up vector is locked
	*/
	bool GetLockUp(void) const;

	/*
	* @brief Checks whether the parameter's value is visible or not
	* @param point - 3d point
	* @return true - visible
	*/
	bool Contains(Vector3 point);

	/*
	* @brief Checks whether the parameter's value is visible or not
	* @param collider - pointer to a collider
	* @return true - visible
	*/
	bool Contains(ColliderAABB* collider);
	
	/*
	* @brief Builds an orthographic projection for 2D elements.
	* @param p_left - left side
	* @param p_right - right side
	* @param p_bottom - bottom side
	* @param p_top - top side
	* @param p_near - near plane
	* @param p_far - far plane
	* @return Returns the new matrix.
	*/
	static Matrix4 Ortho(float p_left, float p_right, float p_bottom, float p_top, float p_near, float p_far);

	/*
	* @brief Builds a view matrix.
	* @param pos - position of the camera.
	* @param view - view of the camera
	* @param up - the up vector of the camera.
	* @return the view matrix.
	*/
	static Matrix4 LookAt(Vector3 pos, Vector3 view, Vector3 up);

	/*
	* @brief Builds a perspective projection for 3D elements.
	* @param p_fov - field of view
	* @param p_aspect - width/height
	* @param p_near - near plane
	* @param p_far - far plane
	* @return the new projection matrix.
	*/
	static Matrix4 Perspective(float p_fov, float p_aspect, float p_near, float p_far);

	/*
	* @brief Resets the projection matrix.
	*/
	void ResetProjection(void);
	
private:
	
	/*
	* @brief Resets the view matrix.
	*/
	void ResetView(void);

	///frustum culling object for the current camera
	FrustumCulling m_frustum;

	/*
	* @brief Used by the game world editor
	*/
	void Reflection();

	///locks the up vector if true
	bool m_lockUpVector;

	///identifies the camera mode where true is the 3D camera
	bool m_is3Dmode;
	
	///identifies the right vector
	Vector3 right;

	///camera rotation 
	Vector3 view;

	///up vector 
	Vector3 up;

	///Contains all translations, rotations or scaling applied to vertices
	Matrix4 m_model;

	///Controls the way a scene is viewed similar to glLookAt(position,view,up)
	Matrix4 m_view;

	///Orthographic - wont modify the Resize of objects - useful for CAD and 2D games
	/// right,left,top,bottom represent positions of clipping planes
	///|	2/(right-left)			0				0			-(right+left)/(right-left)	|
	///|			0		2/(top-bottom)			0			-(top+bottom)/(top-bottom)	|
	///|			0				0			-2/(far-near)	-(far+near)/(far-near)		|
	///|			0				0				0						1				|
	///Perspective - projecs the world coordinates to the unit cube
	///specified by viewing angle(FOV), aspect, near and far
	///near and far respesent positions of near and far clipping planes
	/// top = near * tan(PI/180 * FOV/2); bottom = - top; right = top * aspect; left = -right
	///|	2*near/(right-left)				0				right+left/right-left				0			|
	///|			0				2*near/(top-bottom)		(top+bottom)/(top-bottom)			0			|
	///|			0						0				-(far+near)/(far-near)		-2*far*near/far-near|
	///|			0						0							-1						0			|
	Matrix4 m_projection;

	///boost serialization
	friend class boost::serialization::access;
	template<class Archive>
	void save(Archive& ar, const unsigned int /*version*/) const {
		ar & SER_BASE_OBJECT_NVP(Component);
		ar& SER_NVP(m_lockUpVector);
		ar& SER_NVP(view);
		ar& SER_NVP(right);
		ar& SER_NVP(up);
		ar& SER_NVP(m_model);
		ar& SER_NVP(m_view);
		ar& SER_NVP(m_projection);
	}

	template<class Archive>
	void load(Archive& ar, const unsigned int /*version*/) {
		ar & SER_BASE_OBJECT_NVP(Component);
		ar& SER_NVP(m_lockUpVector);
		ar& SER_NVP(view);
		ar& SER_NVP(right);
		ar& SER_NVP(up);
		ar& SER_NVP(m_model);
		ar& SER_NVP(m_view);
		ar& SER_NVP(m_projection);
	}

	SER_SPLIT_MEMBER()
};
SER_CLASS_VERSION(Camera, 0)
BOOST_CLASS_EXPORT_KEY(Camera)

#endif CAMERA_H