#ifndef  SKYBOX_H
#define SKYBOX_H
#include "Component.h"
#include "Math_s.h"
#include "OBJ_model.h"

/*
* @class Skybox
* @brief Contains two types of light: point and directional; used for displaying light sources in the game world.
* Note: the class should divided into four classes: base, directional, point, spot.
* @version 01
* @date 15/05/2017
* @author Olesia Kochergina
*/
class Skybox :public Component{

public:

	/*
	* @brief Default c-tor.
	*/
	Skybox();

	/*
	* @brief Copy c-tor.
	*/
	Skybox(const Skybox& original);

	/*
	* @brief Destructor.
	*/
	~Skybox();

	/*
	* @brief Initializes the object, called when creating the object.
	*/
	void Initialize();

	/*
	* @brief Called just before the first update.
	*/
	void Start();

	/*
	* @brief Updates the object and should be called every frame.
	*/
	void Update();

	/*
	* @brief Clears internal data, should be called when
	* the object needs to be either reused or deleted.
	*/
	void Clear();

	/*
	* @brief Creates a new component of this class.
	* @return Pointer to the new component or NULL.
	*/
	Component* NewComponent(void) const;

	/*
	* @brief Updates the front image of this skybox.
	* @param image - string file name.
	* @return true - if set.
	*/
	bool SetFront(boost::any image);
		
	/*
	* @brief Returns the front image's file name of the light source.
	* @return file name
	*/
	std::string GetFront(void) const;

	/*
	* @brief Updates the back image of this skybox.
	* @param image - string file name.
	* @return true - if set.
	*/
	bool SetBack(boost::any image);

	/*
	* @brief Returns the back image's file name of the light source.
	* @return file name
	*/
	std::string GetBack(void) const;

	/*
	* @brief Updates the Up image of this skybox.
	* @param image - string file name.
	* @return true - if set.
	*/
	bool SetUp(boost::any image);

	/*
	* @brief Returns the Up image's file name of the light source.
	* @return file name
	*/
	std::string GetUp(void) const;

	/*
	* @brief Updates the Down image of this skybox.
	* @param image - string file name.
	* @return true - if set.
	*/
	bool SetDown(boost::any image);

	/*
	* @brief Returns the Down image's file name of the light source.
	* @return file name
	*/
	std::string GetDown(void) const;

	/*
	* @brief Updates the Left image of this skybox.
	* @param image - string file name.
	* @return true - if set.
	*/
	bool SetLeft(boost::any image);

	/*
	* @brief Returns the Left image's file name of the light source.
	* @return file name
	*/
	std::string GetLeft(void) const;

	/*
	* @brief Updates the Left image of this skybox.
	* @param image - string file name.
	* @return true - if set.
	*/
	bool SetRight(boost::any image);

	/*
	* @brief Returns the Left image's file name of the light source.
	* @return file name
	*/
	std::string GetRight(void) const;

	/*
	* @brief Sets the color of the skybox.
	* @param color - RGBA variable.
	* @return true - if set.
	*/
	bool SetColor(boost::any color);

	/*
	* @brief Returns the color of the skybox.
	* @return color
	*/
	RGBA GetColor(void) const;
private:

	/*
	* @brief Loads an image defined by its file name and adds it to the array of texture IDs.
	* @param index - index of the image from 0 to 5 incl.
	* @param filename - string that contains the file name of the texture such as Resources/Rendering/image.jpg
	* @return true - if the operation is successful
	*/
	bool LoadImage(int index, boost::any filename);
		
	///array with all file names: down, up, front, left, back,right
	std::string m_fileNames[6];


	///obj model
	OBJ_model* m_model;

	///indices to images
	int m_images[6];

	/*
	* @brief Adds values to three vectors: variable name,  setter, getter.
	* Used in the Editor to modify components at run-time.
	*/
	void Reflection(void);

	///the colour of the skybox
	RGBA m_color;
	
	///id of the shader
	unsigned int m_shaderProgram;

	///name of the vertex shader
	std::string m_vS;
	
	///name of teh fragment shader
	std::string  m_fS;
	
	///serialization
	friend class boost::serialization::access;
	template<class Archive>
	void save(Archive& ar, const unsigned int /*version*/) const {
		ar & SER_BASE_OBJECT_NVP(Component);
		ar& SER_NVP(m_fileNames);
		ar& SER_NVP(m_color);
	}

	template<class Archive>
	void load(Archive& ar, const unsigned int /*version*/) {
		ar & SER_BASE_OBJECT_NVP(Component);
		ar& SER_NVP(m_fileNames);
		ar& SER_NVP(m_color);
	}

	SER_SPLIT_MEMBER()
};
SER_CLASS_VERSION(Skybox, 0);
SER_CLASS_EXPORT_KEY(Skybox)
#endif // ! SKYBOX_H
