#ifndef GBUFFER_H
#define GBUFFER_H
#include "FrameBuffer.h"
/*
* @brief Identifies a g-buffer for deferred shading.
* @author Olesia Kochergina
* @version 02
* @date 18/05/2017
*/
class GBuffer {
public:
	///types of color textures
	enum GBUFFER_TEXTURE { GB_POSITION, GB_NORMAL, GB_TEXCOORD, GB_NUM_TEXTURES };

	/*
	* @brief default c-tor.
	*/
	GBuffer(void);

	/*
	* @brief Destructor.
	*/
	~GBuffer(void);

	/*
	* @brief Creates a new g-buffer.
	* @return true - if the buffer is created without errors.
	*/
	bool Create(void);

	/*
	* @brief Writes data to the buffer.
	*/
	void Write(void) const;

	/*
	* @brief Reads data from the buffer.
	*/
	void Read(void) const;

	/*
	* @brief Binds the buffer.
	*/
	void Bind(void) const;

	/*
	* @brief Unbindes the buffer.
	*/
	void Unbind(void) const;

	/*
	* @brief Reads the specified texture.
	* @param type - GB_POSITION,GB_NORMAL,GB_TEXCOORD,GB_NUM_TEXTURES
	*/
	void SetReadTextureType(GBUFFER_TEXTURE type);

	/*
	* @brief Deletes the g-buffer.
	*/
	void Clear();

	/*
	* @brief Returns the ID of the diffuse texture.
	* @return ID
	*/
	unsigned int GetDiffuseTexture();

	/*
	* @brief Returns the ID of the position texture.
	* @return ID
	*/
	unsigned int GetPositionTexture();

	/*
	* @brief Returns the ID of the normal texture.
	* @return ID
	*/
	unsigned int GetNormalTexture();
private:

	///internal frame buffer
	FrameBuffer m_fbo;

	///array of g-buffer texture with normal, position and etc. data
	unsigned int m_textures[GB_NUM_TEXTURES];

	///ID of the depth texture
	unsigned int m_depth;
};
#endif GBUFFER_H