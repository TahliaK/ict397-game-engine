#include "OBJ_loader.h"
#include "GraphicsEngine.h"
#include <istream>
#include <fstream>
#include <iostream>
#include <sstream>
#include "Master.h"
using namespace std;

OBJ_loader::OBJ_loader() {
	/*minBB = AABB();
	minBB.min = Vector3(100000000);
	minBB.max = Vector3(-100000000);
	minBB.owner = NULL;*/
	m_currentModel = NULL;
}

OBJ_loader::~OBJ_loader()
{
	/*if(m_currentModel){
	m_currentModel->~OBJ_model();
	delete m_currentModel;
	}
	m_currentModel = NULL;*/
}

bool OBJ_loader::ParseMatLib(const std::string p_filename)
{
	stringstream l_command;
	float l_num;
	char l_delim1 = ' ';
	ifstream l_file(p_filename);
	if (l_file == NULL) {
		std::cout << "ERROR 404: l_file not found: " << p_filename << " in OBJ_loader" << ENDL;
		return false;
	}
	while (l_file.good()) {
		string l_line;
		getline(l_file, l_line);
		if (l_line.substr(0, 6) != "newmtl") {
			// read the first word of the l_line
			if (l_file.eof())
				break;
		}
		//if the l_line is empty or commented out then go to the next iteration
		if (l_line.substr(0, 1) == "#" || l_line == "")
		{
			continue;
		}
		else if (l_line.substr(0, 6) == "newmtl") {//new Material section
			l_command = stringstream(l_line.substr(7));
			Material l_newMat1;
			l_newMat1.texture = Master::NULL_TEXTURE;
			getline(l_command, l_line);
			l_newMat1.mtlname = l_line;
			bool l_isAdded = false;
			do {
				if (l_file.eof())
					break;
				l_line = "";
				getline(l_file, l_line);
				if (l_line == "" || l_line.substr(0, 6) == "newmtl") {
					break;
				}
				else if (l_line.substr(1, 2) == "Ka") {//ambient
					l_command = stringstream(l_line.substr(4));
					for (unsigned i = 0; i < 3; i++) {
						l_num = ReadNumber(l_command, l_delim1);
						l_newMat1.ambient.Set(i, l_num);
					}
				}
				else if (l_line.substr(1, 2) == "Kd") {//diffuse
					l_command = stringstream(l_line.substr(4));
					for (unsigned i = 0; i < 3; i++) {
						l_num = ReadNumber(l_command, l_delim1);
						l_newMat1.diffuse.Set(i, l_num);
					}
				}
				else if (l_line.substr(1, 2) == "Ks") {//specular
					l_command = stringstream(l_line.substr(4));
					for (unsigned i = 0; i < 3; i++) {
						l_num = ReadNumber(l_command, l_delim1);
						l_newMat1.specular.Set(i, l_num);
					}
				}
				else if (l_line.substr(1, 2) == "Ke") {//emission
					l_command = stringstream(l_line.substr(4));
					for (unsigned i = 0; i < 3; i++) {
						l_num = ReadNumber(l_command, l_delim1);
						l_newMat1.emission.Set(i, l_num);
					}
				}
				else if (l_line.substr(1, 5) == "illum") {//illumination model
					l_command = stringstream(l_line.substr(7));
					l_num = ReadNumber(l_command, l_delim1);
					l_newMat1.illum = (int)l_num;
				}
				else if (l_line.substr(1, 2) == "Ns") {//Shininess
					l_command = stringstream(l_line.substr(4));
					l_num = ReadNumber(l_command, l_delim1);
					l_newMat1.shininess = (int)l_num;
				}
				else if (l_line.substr(1, 6) == "map_Ka" || l_line.substr(1, 6) == "map_Kd") {//path to image
					l_command = stringstream(l_line.substr(8));
					getline(l_command, l_line);
					l_newMat1.path = l_line;
					l_newMat1.texture = GraphicsEngine::LoadTexture(Master::s_modelFolder + l_newMat1.path, true);
				}
				else if (l_line.substr(1, 2) == "Tf") {//Transmission filter
					l_command = stringstream(l_line.substr(4));
					l_num = ReadNumber(l_command, l_delim1);
					l_newMat1.transFilter = l_num;
				}
				else if (l_line.substr(1, 1) == "d") {///dissolve for the current material - 1 is fully opaque
					l_command = stringstream(l_line.substr(3));
					l_num = ReadNumber(l_command, l_delim1);
					l_newMat1.ambient.a = l_num;
					l_newMat1.specular.a = l_num;
					l_newMat1.diffuse.a = l_num;
					l_newMat1.emission.a = l_num;
				}
			} while (true);
			if (!l_isAdded) {
				Material l_newMat2;//to allow multiple Materials within one mtl
				l_newMat2.Copy(l_newMat1);
				m_currentModel->AddMat(l_newMat2);
			}
		}
	}
	return true;
}

/*AABB& OBJ_loader::GetBoundingBox(){
return minBB;
}*/

std::istream & operator >>(std::istream & p_input, OBJ_loader &p_self)
{
	std::string l_line;
	while (true) {
		getline(p_input, l_line);
		if (l_line.substr(0, 7) == "mtllib ")
			break;
	}
	std::string l_mtllib = Master::s_modelFolder + l_line.substr(7);
	p_self.ParseMatLib(l_mtllib);
	std::stringstream l_command;
	Vector3 l_vector(0);
	std::string l_part;
	int l_fMat = 0;
	int l_lMat = -1;
	bool l_lFace = false;
	matF l_mat;
	char l_delim1 = ' ';
	char l_delim2 = '/';
	while (!p_input.eof())
	{
		getline(p_input, l_line);
		if (l_line == "" || l_line.substr(0, 2) == "# " || l_line.substr(0, 2) == "g ")
		{
			continue;
		}
		else if (l_line.substr(0, 2) == "v ")
		{
			l_command = stringstream(l_line.substr(2));
			getline(l_command, l_part, l_delim1);
			for (unsigned i = 0; i < 3; i++)
				l_vector.Set(i, p_self.ReadNumber(l_command, l_delim1));
			p_self.GetCurrentModel()->AddVertexV(l_vector);
		}
		else if (l_line.substr(0, 2) == "vt")
		{
			l_command = stringstream(l_line.substr(2));
			getline(l_command, l_part, l_delim1);
			for (unsigned i = 0; i < 3; i++)
				l_vector.Set(i, p_self.ReadNumber(l_command, l_delim1));
			p_self.GetCurrentModel()->AddVertexT(l_vector);
		}
		else if (l_line.substr(0, 2) == "vn")
		{
			l_command = stringstream(l_line.substr(2));
			getline(l_command, l_part, l_delim1);
			for (unsigned i = 0; i < 3; i++)
				l_vector.Set(i, p_self.ReadNumber(l_command, l_delim1));
			p_self.GetCurrentModel()->AddVertexN(l_vector);
		}
		else if (l_line.substr(0, 2) == "f ") {
			l_command = stringstream(l_line.substr(2));
			l_line = l_line.substr(2);
			Vector3 vf, tf, nf;
			if (l_line.find(l_delim2) == string::npos) {//if only vertices
				for (unsigned i = 0; i < 3; i++) {
					float temp = p_self.ReadNumber(l_command, l_delim1);
					vf.Set(i, --temp);
				}
			}
			else {// vertices and textures and/or normals 
			   //if a string starting from the first slash + 1 contains another slash after the first space
			   //delim1 "  "; delim "\"
			   //l_line.substr(l_line.find(l_delim1))
				if (l_line.substr(l_line.find(l_delim2) + 1).find(l_delim2) > l_line.find(l_delim1)) {//only vertex and texture faces
					for (unsigned i = 0; i < 3; i++) {
						float temp = p_self.ReadNumber(l_command, l_delim2);
						vf.Set(i, --temp);
						temp = p_self.ReadNumber(l_command, l_delim1);
						tf.Set(i, --temp);
					}
				}
				else {
					//if a character after the next first l_delim2 is equal to l_delim2 then there is no texture data
					if (l_line.at(l_line.find(l_delim2) + 1) == l_delim2) {//only vertex and normal data

						for (unsigned i = 0; i < 3; i++) {
							float temp = p_self.ReadNumber(l_command, l_delim2);
							vf.Set(i, --temp);
							getline(l_command, l_part, l_delim2);//empty 
							temp = p_self.ReadNumber(l_command, l_delim1);
							nf.Set(i, --temp);
						}
					}
					else {//all three
						for (unsigned i = 0; i < 3; i++) {
							float temp = p_self.ReadNumber(l_command, l_delim2);
							vf.Set(i, --temp);
							temp = p_self.ReadNumber(l_command, l_delim2);
							tf.Set(i, --temp);
							temp = p_self.ReadNumber(l_command, l_delim1);
							nf.Set(i, --temp);

						}

					}
				}
			}
			p_self.GetCurrentModel()->AddVFace(vf);
			p_self.GetCurrentModel()->AddTFace(tf);
			p_self.GetCurrentModel()->AddNFace(nf);
			l_lMat++;
			l_lFace = true;
		}
		else if (l_line.substr(0, 2) == "us") {
			//add mtl code, get usemtl			
			l_command = stringstream(l_line.substr(7));
			getline(l_command, l_part);
			l_mat.mtlname = l_part;
		}
		else if (l_lFace) {
			matF l_copy;
			l_lFace = false;
			l_copy.mtlname = l_mat.mtlname;
			l_copy.fFace = l_fMat;
			l_copy.lFace = l_lMat;
			p_self.GetCurrentModel()->AssignVertices(&l_copy);
			p_self.GetCurrentModel()->AddMatKey(l_copy);
			l_fMat = l_lMat + 1;
		}
	}

	if (l_lFace) {//the last Material in a file
		matF l_copy;
		l_copy.mtlname = l_mat.mtlname;
		l_copy.fFace = l_fMat;
		l_copy.lFace = l_lMat;
		p_self.GetCurrentModel()->AssignVertices(&l_copy);
		p_self.GetCurrentModel()->AddMatKey(l_copy);
		l_fMat = l_lMat + 1;
	}
	p_self.GetCurrentModel()->GenerateVBO();
	return p_input;
}

float OBJ_loader::ReadNumber(std::stringstream& p_command, const char p_delim) const {
	float l_conversion;
	string l_part;
	getline(p_command, l_part, p_delim);
	istringstream stringInput(l_part);
	stringInput >> l_conversion;
	return l_conversion;
}

OBJ_model* OBJ_loader::GetCurrentModel()const {
	return m_currentModel;
}

void OBJ_loader::SetCurrentModel(OBJ_model* model) {
	m_currentModel = model;
}