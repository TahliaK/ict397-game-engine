/**
* Signed Distance Field impl
*/
#ifndef FONT_H
#define FONT_H
#include <iostream>
#include "Struct.h"
#include "MeshGenerator.h"

class Label;
namespace Text{
class Font{
public:
	Font();
	Font(int texture, std::string fontFile);
	~Font();
	void LoadSDF(std::string fileName);
	void LoadText( Label* text, MeshData& data);
	int GetTexture() const;
	void Clear();
private:
	unsigned int m_texture;
	MeshGenerator m_loader;
};
}
#endif FONT_H
