#ifndef EVENT_AI_STRUCT_H
#define EVENT_AI_STRUCT_H
#include "AIComponent.h"

/**@enum MessageAudience
*@brief Potential groups for messages to be sent to
*@author Tahlia Kowald
*@version 1.0
*@date 29/5/17
*/
typedef enum MessageAudience{
	Individual = 0, Enemies = 1, Allies = 2, Followers = 3, Neutral = 4, Players = 5
} MessageAudience;

inline MessageAudience operator |(MessageAudience a, MessageAudience b)
{
	return static_cast<MessageAudience>(static_cast<int>(a) | static_cast<int>(b));
}

/**@enum MessageType
*@brief Potential categories of messages
*@author Tahlia Kowald
*@version 1.0
*@date 29/5/17
*/
typedef enum MessageType {
	None = -1, Alert = 0, NoAlert = 1, Fight = 2, Flee = 3, SendingAttack = 4, RecievedAttack_Acknowledge = 6
} MessageType;

/**@struct Message
*@brief Messages with associated information (audience, reciever, type & damage)
*@author Tahlia Kowald
*@version 2.0
*@date 29/5/17
*/
typedef struct Message{
	Message() : m_audience(Individual),
	m_type(NoAlert), m_damageLevel(0){}

	MessageAudience m_audience;
	std::string m_recieverID; //if for specific GameObject or player
	MessageType m_type;
	int m_damageLevel;
} Message;

/**@struct MessageReciever
*@brief A messageReciever with relevant address, name and type info
*@author Tahlia Kowald
*@version 1.0
*@date 29/5/17
*/
typedef struct MessageReciever{
	MessageReciever() : m_item(nullptr), m_objectName(""),
		 m_audienceType(Individual) {}
	Component * m_item;
	std::string m_objectName;
	MessageAudience m_audienceType;
} MessageReciever;


#endif