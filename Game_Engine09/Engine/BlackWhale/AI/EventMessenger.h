//EventMessenger.h

#ifndef EVENT_MESSENGER_H
#define EVENT_MESSENGER_H
#include "..\GameObject.h"
#include <vector>
#include <string>
#include "EventAIStructs.h"

/**@class EventMessenger
*@brief Singleton inter-agent message manager class
*@author Tahlia Kowald
*@version 3.0
*@date 1/6/17
*/
class EventMessenger{
public:

	/**@brief Registers a gameObject with the class
	*@param aim the AI_Messengers class
	*@param name the messengerID to register
	*@param audience the object's audience status
	*@returns true if successful (ie. no naming conflict with others, not already registered)
	*/
	static bool registerGameObject(Component * aim, std::string name, MessageAudience audienceType);
	
	/**@brief deregisters a gameObject with the class
	*@param aim the AI_Messenger class to deregister
	*/
	static void deregisterGameObject(Component * aim);

	/**@brief Gets the messenger for a given script in the same gameObject
	*@param script - the Scripting component
	*@param Messenger - the messenger component
	*@returns false if not found, true if found
	*/
	static bool getMessengerForScript(Component * script, Component *& Messenger);
	
	/**@brief Gets the gameObject for a given objectID
	*@param go the gameObject to be loaded into
	*@param name the name of the gameObject
	*@returns false if not found, true if found
	*/
	static bool getGameObjectForThisID(GameObject *& go, std::string name);

	/**@brief Updates the game object registration (changes name)
	*@param aim - the AI_Messenger component
	*@param newname - the new name
	*@returns false if not found, true if found
	*/
	static bool updateGameObject(Component * aim, std::string newName);
	
	/**@brief Updates the game object registration (changes audience status)
	*@param aim - the AI_Messenger component
	*@param newname - the new name
	*@returns false if not found, true if found
	*@note AI_Messenger::SetAudience just calls this
	*/
	static bool updateGameObject(Component * aim, int newAudience);

	/**@brief Send Message
	*Takes a message from the user, and makes it available for retrieval
	*@param Msg the message to send
	*/
	static void sendMsg(Message & msg);
	
	/**@brief Recieves message
	*Removes the message from the pool and gives the data to the reference message
	*@param KeyFor the key of the object to recieve
	*@param m the message to recieve with
	*@returns true if successful false if key doesn't exist or item doesn't exist
	*/
	static bool recieveMsg(std::string KeyFor, Message & m);

	/**@brief Checks if there's a message for a particular component
	*@param aim the AI_Messenger to use
	*@returns true if there is a message, false if there isn't
	*/
	static bool isMsgFor(Component * aim);

	/**@brief Generates an ObjectName
	*ObjectName will be the size of the storage array.
	*@warning this doesn't guarantee that the name is not already taken
	*@returns the given name
	*/
	static std::string generateName() { return std::to_string(gameObjects.size());}

private:
	static std::vector<MessageReciever> gameObjects; //for tracking registered AI_Messengers
	static std::map<std::string, std::vector<Message>> messages; //for tracking messages

	static std::vector<MessageReciever>::iterator registeredObjectReference(Component * aim);
	static bool nameAlreadyExists(std::string name);

	static std::vector<MessageReciever> getRecipients(int flags);
	static std::vector<MessageReciever> getRecipients(MessageAudience audienceType);
};

#endif