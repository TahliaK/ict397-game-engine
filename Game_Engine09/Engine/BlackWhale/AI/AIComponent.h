//AIComponent.h
#ifndef AI_MESSENGER_H
#define AI_MESSENGER_H
#include "..\Component.h"
#include "EventMessenger.h"
#include "EventAIStructs.h"
#include "..\Interfaces\LuaScript.h"

/**@class AI_Messenger
*@brief Messenger component for inter-agent messaging
*@author Tahlia Kowald
*@version 3.1
*@date 2/6/17
*/
class AI_Messenger : public Component {
public:
	AI_Messenger(); //c-tor

	~AI_Messenger(); //d-tor
 
 //inherited
	void Initialize();
	void Start();
	void Update();
	void Clear();

	/**@brief sets the GameObject's MessengerID
	*@param newID = the GameObject's new string ID
	*@returns true if change successful (no conflict with other items)
	*/
	bool SetMessengerID(boost::any newID);
	
	/**@brief sets the GameObject's Enemy status
	*@param isEnemy = boolean state
	*@returns true if change successful
	*/
	bool SetIsEnemy(boost::any isEnemy);
	
	/**@brief sets the GameObject's Ally status
	*@param isEnemy = boolean state
	*@returns true if change successful
	*/
	bool SetIsAlly(boost::any isAlly);
	
	/**@brief sets the GameObject's Follower status
	*@param isEnemy = boolean state
	*@returns true if change successful
	*/
	bool SetIsFollower(boost::any isFollower);
	
	/**@brief sets the GameObject's Neutral status
	*@param isEnemy = boolean state
	*@returns true if change successful
	*/
	bool SetIsNeutral(boost::any isNeutral);
	
	/**@brief sets the GameObject's Player status
	*@param isEnemy = boolean state
	*@returns true if change successful
	*/
	bool SetIsPlayer(boost::any isPlayer);

	/**@brief Gets the GameObject's MessengerID
	*@returns the ID
	*/
	std::string GetMessengerID() {return messenger_ID;}
	
	/**@brief Gets the GameObject's Enemy status
	*@returns Enemy status
	*/
	bool GetIsEnemy() {return isEnemy;}
	
	/**@brief Gets the GameObject's Enemy status
	*@returns Enemy status
	*/
	bool GetIsAlly() {return isAlly;}
	
	/**@brief Gets the GameObject's Enemy status
	*@returns Enemy status
	*/
	bool GetIsFollower() {return isFollower;}
	
	/**@brief Gets the GameObject's Enemy status
	*@returns Enemy status
	*/
	bool GetIsNeutral() {return isNeutral;}
	
	/**@brief Gets the GameObject's Enemy status
	*@returns Enemy status
	*/
	bool GetIsPlayer() {return isPlayer;}

	/**@brief Sends a message from this component to the message manager
	*@param recieverID - the reciever's messengerID (not relevant if directed at group)
	*@param recievers - the group it is directed (only relevant if not Individual)
	*@param type - the type of the message
	*@param damageLevel - the damageLevel of the message
	*/
	void SendMessage(std::string recieverID, int recievers, int type, int damageLevel);

	//inherited
	Component * NewComponent(void) const;
	void Reflection();

private:
	std::string messenger_ID;
	bool isEnemy, isAlly, isFollower, isNeutral, isPlayer;
	static bool l_inList;

	void updateRecieverFlags();

};

/**@class AI_Script
*@brief Superclass Scripting component for AI_related scripts
*@author Tahlia Kowald
*@version 3.1
*@date 2/6/17
*/
class AI_Script : public Component
{
public:
	//inherited
	virtual void Initialize() = 0;
	virtual void Start(); //needs to be called by child classes in their Start methods
	virtual void Update() = 0;
	virtual void Clear() = 0;

	//for subclasses
	virtual bool SetFileName(boost::any fileName) = 0;
	virtual std::string GetFileName(void) = 0;
	virtual bool SetActive(boost::any active);
	virtual bool GetActive();
	
protected:
	//uses superclass so if we added other types of scripts we could use them
	Script * m_script;
	AI_Messenger * m_messages;
	bool isActive;
	static bool l_inList;
};


#endif