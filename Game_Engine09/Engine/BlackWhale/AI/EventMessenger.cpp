//EventMessenger.cpp
#include "EventMessenger.h"

std::vector<MessageReciever> EventMessenger::gameObjects = std::vector<MessageReciever>();
std::map<std::string, std::vector<Message>> EventMessenger::messages = 
	std::map<std::string, std::vector<Message>>();

bool operator==(const MessageReciever& lhs, const MessageReciever& rhs)
{
    bool isSame = true;
	if(lhs.m_item != rhs.m_item) {isSame = false;}
	if(lhs.m_audienceType != rhs.m_audienceType) {isSame = false;}
	if(lhs.m_objectName != rhs.m_objectName) {isSame = false;}
	return isSame;
}

bool EventMessenger::registerGameObject(Component * aim, std::string name, 
										MessageAudience audienceType)
{
	bool success = false;
	auto it = registeredObjectReference(aim);
	if(it == gameObjects.end())
	{
		//add to list
		if(!nameAlreadyExists(name))
		{
			MessageReciever msgR_temp;
			msgR_temp.m_item = (Component*)aim; 
			msgR_temp.m_objectName = name;
			msgR_temp.m_audienceType = audienceType;

			gameObjects.push_back(msgR_temp);
			messages.insert(std::pair<std::string, std::vector<Message>>
				(msgR_temp.m_objectName, std::vector<Message>(0)));
			success = true;
		}
	}

	return success;
}

//using code from
//http://stackoverflow.com/questions/39912/how-do-i-remove-an-item-from-a-stl-vector-with-a-certain-value
//to prevent reshuffling whole vector every time
void EventMessenger::deregisterGameObject(Component * aim)
{
	std::vector<MessageReciever>::iterator it = registeredObjectReference(aim);
	if(it != gameObjects.end())
	{
		using std::swap;
		messages.erase(it->m_objectName);
		swap(*it, gameObjects.back());
		gameObjects.pop_back();
	}
}

bool EventMessenger::getMessengerForScript(Component * script, Component *& Messenger)
{
	int i;
	GameObject * targetGo = script->GetGameObject();

	for(i = 0; i < gameObjects.size(); i++)
	{
		if(gameObjects[i].m_item->GetGameObject() == targetGo)
			break;
	}

	if(i < gameObjects.size())
	{
		Messenger = gameObjects[i].m_item;
		return true;
	}

	return false;

}

bool EventMessenger::getGameObjectForThisID(GameObject *& go, std::string name)
{
	for(int i = 0; i < gameObjects.size(); i++)
	{
		if(gameObjects[i].m_objectName == name)
		{
			go = gameObjects[i].m_item->GetGameObject();
			return true;
		}
	}

	go = nullptr;

	return false;
}

bool EventMessenger::updateGameObject(Component * aim, std::string newName)
{
	auto it = registeredObjectReference(aim);

	if(it != gameObjects.end())
	{
		auto temp = std::pair<std::string, std::vector<Message>>();
		for(int i = 0; i < messages.at(it->m_objectName).size(); i++)
		{
			temp.second.push_back(messages.at(it->m_objectName)[i]);
		}
		temp.first = newName;
		messages.erase(it->m_objectName);
		messages.insert(temp);
		it->m_objectName = newName;
		return true;
	}

	return false;
}

bool EventMessenger::updateGameObject(Component * aim, int newAudience)
{
	auto it = registeredObjectReference(aim);

	if(it != gameObjects.end())
	{
		it->m_audienceType = static_cast<MessageAudience>(newAudience);
		return true;
	}

	return false;
}

void EventMessenger::sendMsg(Message & msg)
{
	//for individual
	if(msg.m_audience == Individual)
	{
		try{
		messages.at(msg.m_recieverID).push_back(msg);
		}
		catch(std::out_of_range)
		{
			std::cout << "[EventMessenger ERROR] Sending message to non-existent or unregistered game object" << std::endl;
		}
	}
	else
	{
		std::vector<MessageReciever> temp = getRecipients(msg.m_audience);
		for(int i = 0; i < temp.size(); i++)
		{
			messages.at(temp[i].m_objectName).push_back(msg);
		}

	}
}

bool EventMessenger::recieveMsg(std::string keyFor, Message & m) 
{
	try{
		m.m_audience = messages.at(keyFor).at(0).m_audience;
		m.m_damageLevel = messages.at(keyFor).at(0).m_damageLevel;
		m.m_recieverID = messages.at(keyFor).at(0).m_recieverID;
		m.m_type = messages.at(keyFor).at(0).m_type;

		std::vector<Message> * temp = &messages.at(keyFor);

		std::swap(temp->begin(), temp->end());
		temp->pop_back();
		return true;
	}catch (std::out_of_range){} //message doesn't exist, return default

	return false;
}

bool EventMessenger::isMsgFor(Component * aim) { return false;}

/* Private methods */

std::vector<MessageReciever>::iterator EventMessenger::registeredObjectReference(Component * aim)
{
	std::vector<MessageReciever>::iterator it;

	for(it = gameObjects.begin(); it < gameObjects.end(); it++)
	{
		if(it->m_item == aim)
			break;
	}

	return it;
}

bool EventMessenger::nameAlreadyExists(std::string name)
{
	std::vector<MessageReciever>::iterator it;

	for(it = gameObjects.begin(); it < gameObjects.end(); it++)
	{
		if(it->m_objectName == name)
			break;
	}

	if(it == gameObjects.end())
		return false;
	else
		return true;

}

std::vector<MessageReciever> getRecipients(int flags)
{
	std::vector<MessageReciever> temp;
	if(flags - Players >= 0)
	{
		temp = getRecipients(Players);
		flags = flags - Players;
	}

	if(flags - Neutral >= 0)
	{
		auto tempVec = getRecipients(Neutral);
		for(int i = 0; i < tempVec.size(); i++)
		{
			if(std::find(temp.begin(), temp.end(), tempVec[i]) == temp.end())
			{
				temp.push_back(tempVec[i]);
			}
		}
		flags = flags - Neutral;
	}

	if(flags - Followers >= 0)
	{
		auto tempVec = getRecipients(Followers);
		for(int i = 0; i < tempVec.size(); i++)
		{
			if(std::find(temp.begin(), temp.end(), tempVec[i]) == temp.end())
			{
				temp.push_back(tempVec[i]);
			}
		}
		flags = flags - Followers;
	}

	if(flags - Allies >= 0)
	{
		auto tempVec = getRecipients(Allies);
		for(int i = 0; i < tempVec.size(); i++)
		{
			if(std::find(temp.begin(), temp.end(), tempVec[i]) == temp.end())
			{
				temp.push_back(tempVec[i]);
			}
		}
		flags = flags - Allies;
	}

	if(flags - Enemies >= 0)
	{
		auto tempVec = getRecipients(Enemies);
		for(int i = 0; i < tempVec.size(); i++)
		{
			if(std::find(temp.begin(), temp.end(), tempVec[i]) == temp.end())
			{
				temp.push_back(tempVec[i]);
			}
		}
		flags = flags - Enemies;
	}

	return temp;
}

std::vector<MessageReciever> EventMessenger::getRecipients(MessageAudience audienceType)
{
	std::vector <MessageReciever> temp = std::vector<MessageReciever>(0);
	for(int i = 0; i < gameObjects.size(); i++)
	{
		if(gameObjects[i].m_audienceType >= audienceType)
		{
			temp.push_back(gameObjects[i]);
		}
	}

	return temp;
}