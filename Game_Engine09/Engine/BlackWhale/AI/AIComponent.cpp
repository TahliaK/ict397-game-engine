//AI_Component .cpp
#include "AIComponent.h"
#include "..\Transform.h"
#include "..\Terrain\Terrain.h"
#include <math.h>

bool AI_Messenger::l_inList = false;

AI_Messenger::AI_Messenger()
{
	Reflection();
	Initialize();
}

AI_Messenger::~AI_Messenger()
{
	Clear();
}

void AI_Messenger::Initialize()
{
	messenger_ID = EventMessenger::generateName();
	isEnemy = false; isAlly = false; isFollower = false;
	isNeutral = false; isPlayer = false;
	m_name = "AI Messenger";

	if(!l_inList){
		l_inList = true;
		AI_Messenger * l_pointer = new AI_Messenger;
		m_derivedClasses.push_back(l_pointer);
	}
}

void AI_Messenger::Start()
{
	EventMessenger::registerGameObject(this, messenger_ID, Individual);
}

void AI_Messenger::Update()
{

}

void AI_Messenger::Clear()
{

}



bool AI_Messenger::SetMessengerID(boost::any newID)
{
	bool success = false;
	try{
		std::string temp = messenger_ID;
		messenger_ID = boost::any_cast<std::string>(newID);
		if(!EventMessenger::updateGameObject(this, messenger_ID))
		{
			messenger_ID = temp; //EventMessenger rejected the change, revert name-change
		}
		else
		{
			success = true;
		}
	}catch (boost::bad_any_cast /*bac*/){}

	return success;
}

bool AI_Messenger::SetIsEnemy(boost::any isEnemy)
{
	bool success = false;
	try
	{
		this->isEnemy = boost::any_cast<bool>(isEnemy);
		updateRecieverFlags();
		success = true;
	} catch(boost::bad_any_cast /*bac*/){}

	return success;
}

bool AI_Messenger::SetIsAlly(boost::any isAlly)
{
	bool success = false;
	try
	{
		this->isAlly = boost::any_cast<bool>(isAlly);
		updateRecieverFlags();
		success = true;
	} catch(boost::bad_any_cast /*bac*/){}

	return success;
}

bool AI_Messenger::SetIsFollower(boost::any isFollower)
{
	bool success = false;
	try
	{
		this->isFollower = boost::any_cast<bool>(isFollower);
		updateRecieverFlags();
		success = true;
	} catch(boost::bad_any_cast /*bac*/){}

	return success;
}

bool AI_Messenger::SetIsNeutral(boost::any isNeutral)
{
	bool success = false;
	try
	{
		this->isNeutral = boost::any_cast<bool>(isNeutral);
		updateRecieverFlags();
		success = true;
	} catch(boost::bad_any_cast /*bac*/){}

	return success;
}

bool AI_Messenger::SetIsPlayer(boost::any isPlayer)
{
	bool success = false;
	try
	{
		this->isPlayer = boost::any_cast<bool>(isPlayer);
		updateRecieverFlags();
		success = true;
	} catch(boost::bad_any_cast /*bac*/){}

	return success;
}

Component * AI_Messenger::NewComponent(void) const
{
	Component * l_pointer = new AI_Messenger;
	return l_pointer;
}

void AI_Messenger::Reflection()
{
	m_Variables.push_back("Object ID");
	m_Setters.push_back(boost::bind(&AI_Messenger::SetMessengerID, this, _1));
	m_Getters.push_back(boost::bind(&AI_Messenger::GetMessengerID, this));

	m_Variables.push_back("Is Enemy:");
	m_Setters.push_back(boost::bind(&AI_Messenger::SetIsEnemy, this, _1));
	m_Getters.push_back(boost::bind(&AI_Messenger::GetIsEnemy, this));

	m_Variables.push_back("Is Ally:");
	m_Setters.push_back(boost::bind(&AI_Messenger::SetIsAlly, this, _1));
	m_Getters.push_back(boost::bind(&AI_Messenger::GetIsAlly, this));

	m_Variables.push_back("Is Follower:");
	m_Setters.push_back(boost::bind(&AI_Messenger::SetIsFollower, this, _1));
	m_Getters.push_back(boost::bind(&AI_Messenger::GetIsFollower, this));

	m_Variables.push_back("Is Neutral NPC:");
	m_Setters.push_back(boost::bind(&AI_Messenger::SetIsNeutral, this, _1));
	m_Getters.push_back(boost::bind(&AI_Messenger::GetIsNeutral, this));

	m_Variables.push_back("Is Player:");
	m_Setters.push_back(boost::bind(&AI_Messenger::SetIsPlayer, this, _1));
	m_Getters.push_back(boost::bind(&AI_Messenger::GetIsPlayer, this));
}

void AI_Messenger::updateRecieverFlags()
{
	//talk to EventMessenger about updating flags
}

void AI_Messenger::SendMessage(std::string recieverID, int recievers,
							   int type, int damageLevel)
{
	Message msg;
	msg.m_recieverID = recieverID;
	msg.m_audience = static_cast<MessageAudience>(recievers); 
	msg.m_type = static_cast<MessageType>(type);
	msg.m_damageLevel = damageLevel;

	EventMessenger::sendMsg(msg);
}

bool AI_Script::l_inList = false;

void AI_Script::Start()
{
	Component * temp;
	if(!EventMessenger::getMessengerForScript(this, temp))
	{
		m_messages = nullptr;
		std::cout << "[AI_SCRIPT WARNING] No messenger associated with AI_Script" << std::endl;
	}
	else
	{
		m_messages = (AI_Messenger*)temp;
		//testing
		m_messages->SendMessage(std::string("item1"), Individual, Alert, 7);
	}
}

bool AI_Script::SetActive(boost::any active)
{
	try{
		isActive = boost::any_cast<bool>(active);
		return true;
	}catch(boost::bad_any_cast /*bac*/){}

	return false;
}

bool AI_Script::GetActive()
{
	return isActive;
}
