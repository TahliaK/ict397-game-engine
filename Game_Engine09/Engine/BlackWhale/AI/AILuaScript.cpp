#include "AILuaScript.h"
#include "..\Time.h"
#include "..\Terrain\Terrain.h"

bool AI_LuaScript::l_inList = false;

AI_LuaScript::AI_LuaScript()
{
	Reflection();
	Initialize();
}

AI_LuaScript::~AI_LuaScript()
{
	m_script->Clear();
}

void AI_LuaScript::Initialize()
{
	m_name = "AI_LuaScript";
	m_script = new LuaScript();

	((LuaScript *)m_script)->RegisterFunction(AI_LuaScript_AddOns::Lua_SendMsg, "SendMessage");
	((LuaScript *)m_script)->RegisterFunction(AI_LuaScript_AddOns::Lua_GetMsg, "GetMessage");
	((LuaScript *)m_script)->RegisterFunction(AI_LuaScript_AddOns::Lua_GetPositionOf, "GetPositionOf");
	((LuaScript *)m_script)->RegisterFunction(AI_LuaScript_AddOns::Lua_BasicMove, "AI_BasicMove");
	((LuaScript *)m_script)->RegisterFunction(AI_LuaScript_AddOns::Lua_Put, "AI_Put");

	if(!l_inList){
		l_inList = true;
		AI_LuaScript * l_pointer = new AI_LuaScript;
		m_derivedClasses.push_back(l_pointer);
	}	

	m_messages = nullptr;
}

void AI_LuaScript::Start()
{
	AI_Script::Start();
	m_script->Start();

}

void AI_LuaScript::Update()
{
	/*if(m_messages != nullptr)
	{
		Message temp;
		while(EventMessenger::recieveMsg(m_messages->GetMessengerID(), temp))
		{
			messageList.push_back(temp);
		}
	}*/
	m_script->Update();
}

void AI_LuaScript::Clear()
{
	m_script->Clear();
}

Component * AI_LuaScript::NewComponent() const
{
	Component * l_pointer = new AI_LuaScript;
	return l_pointer;
}

void AI_LuaScript::Reflection()
{
	m_Variables.push_back("Script filename:");
	m_Setters.push_back(boost::bind(&AI_Script::SetFileName, this, _1));
	m_Getters.push_back(boost::bind(&AI_Script::GetFileName, this));

	m_Variables.push_back("Is Active:");
	m_Setters.push_back(boost::bind(&AI_Script::SetActive, this, _1));
	m_Getters.push_back(boost::bind(&AI_Script::GetActive, this));
}

bool AI_LuaScript::SetFileName(boost::any fileName)
{
	return ((LuaScript *)m_script)->SetFileName(fileName);
}

std::string AI_LuaScript::GetFileName()
{
	return ((LuaScript *)m_script)->GetFileName();
}

/**************** AI_LuaScript_AddOns *******************/

int AI_LuaScript_AddOns::Lua_SendMsg(lua_State * State)
{
	bool success = false;

	double audience, damageLevel, message_type;
	std::string recieverID, senderID;

	//get variables from stack
	if(LuaScriptFunctions::hasEnoughParams(State, 5))
	{	
		int itemIndex = 1;
		if(lua_type(State, itemIndex) == LUA_TSTRING)
		{
			recieverID = lua_tostring(State, itemIndex);
			lua_pop(State, 1);
			senderID = lua_tostring(State, itemIndex);
			lua_pop(State, 1);

			int numberOfValidParams = 0;
			for(int i = -1; i > -4; i--)
			{
				if(lua_type(State, i) == LUA_TNUMBER)
				{
					numberOfValidParams++;
				}
			}

			if(numberOfValidParams == 3)
			{
				audience = lua_tonumber(State, itemIndex);
				lua_pop(State, itemIndex);
				message_type = lua_tonumber(State, itemIndex);
				lua_pop(State, itemIndex);
				damageLevel = lua_tonumber(State, itemIndex);
				lua_pop(State, itemIndex);
				success = true;
			}
		}
	}

	//LuaScriptFunctions::clearStack(State);

	//send message
	if(success)
	{
		GameObject * go;
		AI_Messenger * aim = nullptr;
		if(EventMessenger::getGameObjectForThisID(go, senderID))
		{
			std::vector<Component *> aimessengers = go->GetComponents(aim);
			//there should only be one so lets just take the first one

			try{
				((AI_Messenger *)aimessengers.at(0))->SendMessage(recieverID, 
					(int)audience, (int)message_type, (int)damageLevel);
				lua_pushboolean(State, 0);
			}catch (std::out_of_range)
			{
				lua_pushboolean(State, 1);
			}

			
		}
		else
		{
			lua_pushboolean(State, 1);
		}
	}
	else
	{
		lua_pushboolean(State, 1);
	}

	return 1;
}

int AI_LuaScript_AddOns::Lua_GetPositionOf(lua_State * state)
{
	std::string targetObject;
	GameObject * targetGo = nullptr;
	bool success = false;

	if(lua_type(state, 1) == LUA_TSTRING)
	{
		targetObject = lua_tostring(state, 1);
		lua_pop(state, 1);

		if(!EventMessenger::getGameObjectForThisID(targetGo, targetObject))
		{
			targetGo = nullptr;
		}
	}
	
	if(targetGo != nullptr)
	{
		LuaScriptFunctions::pushToStackBoolean(state, true);
		LuaScriptFunctions::pushToStackNumber(state, targetGo->GetTransform()->position.x);
		LuaScriptFunctions::pushToStackNumber(state, targetGo->GetTransform()->position.y);
		LuaScriptFunctions::pushToStackNumber(state, targetGo->GetTransform()->position.z);

	}
	else
	{
		LuaScriptFunctions::pushToStackBoolean(state, false);
		for(int i = 0; i < 3; i++)
		{
			LuaScriptFunctions::pushToStackNumber(state, 0);
		}
	}

	return 4;
}

int AI_LuaScript_AddOns::Lua_GetMsg(lua_State * state)
{
	Message temp;
	int damage, type;

	damage = 0; type = -1;

	if(LuaScriptFunctions::hasEnoughParams(state, 1))
	{
		std::string thisItemID;
		if(lua_type(state, 1) == LUA_TSTRING)
		{
			thisItemID = lua_tostring(state, 1);
			if(EventMessenger::recieveMsg(thisItemID, temp))
			{
				damage = temp.m_damageLevel;
				type = temp.m_type;
			}
		}
	}

	LuaScriptFunctions::clearStack(state);

	LuaScriptFunctions::pushToStackNumber(state, (double)type);
	LuaScriptFunctions::pushToStackNumber(state, (double)damage);

	return 2;
}

int AI_LuaScript_AddOns::Lua_Put(lua_State * state)
{
	GameObject * thisGo;
	bool success = false;

	if(LuaScriptFunctions::hasEnoughParams(state, 4))
	{
		std::string thisItemID;
		if(lua_type(state, 1) == LUA_TSTRING)
		{
			thisItemID = lua_tostring(state, 1);
			lua_pop(state, 1);
			if(EventMessenger::getGameObjectForThisID(thisGo, thisItemID))
			{
				int validInputs = 0;
				for(int i = 1; i < 4; i++)
				{
					if(lua_type(state, i) == LUA_TNUMBER)
					{
						validInputs++;
					}
				}

				if(validInputs == 3)
				{
					float newX = lua_tonumber(state, 1);
					lua_pop(state, 1);
					float newY = lua_tonumber(state, 1);
					lua_pop(state, 1);
					float newZ = lua_tonumber(state, 1);
					lua_pop(state, 1);

					LuaScriptFunctions::clearStack();

					thisGo->GetTransform()->position.x = newX;
					thisGo->GetTransform()->position.z = newZ;

					if(newY == 0)
					{
						newY = TerrainTracker::CurrentTerrain()->GetHeight(newX, newZ);
					}

					thisGo->GetTransform()->position.y = newY;
					success = true;
				}
			}
		}
	}

	LuaScriptFunctions::pushToStackBoolean(state, success);

	return 1;
}

int AI_LuaScript_AddOns::Lua_BasicMove(lua_State * state)
{
	std::string ID;
	Vector3 targetPosition;
	Vector3 currentPosition;
	Vector3 currentVelocity;
	double timeElapsed;
	double offset;
	bool success = false;

	timeElapsed = Time::GetDeltaTime();

	if(LuaScriptFunctions::hasEnoughParams(state, 6))
	{
		if(lua_type(state, 1) == LUA_TSTRING)
		{
			ID = lua_tostring(state, 1);
			lua_pop(state, 1);

			int valid = 0;
			for(int i = 1; i < 6; i++)
			{
				if(lua_type(state, i) == LUA_TNUMBER)
					valid++;
			}

			if(valid == 4)
			{
				targetPosition.x = lua_tonumber(state, 1);
				lua_pop(state, 1);
				targetPosition.y = lua_tonumber(state, 1);
				lua_pop(state, 1);
				targetPosition.z = lua_tonumber(state, 1);
				lua_pop(state, 1);
				currentVelocity.x = lua_tonumber(state, 1);
				lua_pop(state, 1);
				offset = lua_tonumber(state, 1);
				lua_pop(state, 1);

				GameObject * g = nullptr;
				EventMessenger::getGameObjectForThisID(g, ID);
				currentPosition = g->GetTransform()->position;
				currentVelocity.z = currentVelocity.x;
				currentVelocity.y = currentVelocity.x;

				success = BasicMove(currentPosition, targetPosition,
									currentVelocity, timeElapsed, offset);
			}

			LuaScriptFunctions::clearStack(state);
		}
	}

	LuaScriptFunctions::pushToStackBoolean(state, success);

	return 1;
}


bool AI_LuaScript_AddOns::BasicMove(Vector3 &currentPosition, Vector3 &targetPosition,
									Vector3 &currentVelocity, double timeElapsed, double offset)
{
	Vector3 toTarget = targetPosition - currentPosition;
	toTarget = Vector3::Normalize(toTarget);
	if(toTarget.x == 0 && toTarget.z == 0)
	{// nothing to do, already in spot
		return true;
	}
	else
	{
		currentVelocity = toTarget * Vector3::Length(currentVelocity);
		Vector3 displacement = currentVelocity * timeElapsed;
		Vector3 newPosition = currentPosition + displacement;

		Vector3 realTargetPosition = targetPosition - toTarget * offset;

		Vector3 toRealTarget = realTargetPosition - newPosition;
		toRealTarget = Vector3::Normalize(toRealTarget);
		if(toRealTarget.x == 0 && toRealTarget.z == 0)
		{
			currentPosition = realTargetPosition; //is in target spot
			return true;
		}
		else
		{
			float dp = Vector3::Dot(toRealTarget, toTarget);
			if(dp < 0.0f)
			{
				currentPosition = realTargetPosition;
				return true;
			}
			else
			{
				currentPosition = newPosition;
			}
		}
	}

	return false;
}