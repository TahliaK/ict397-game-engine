#ifndef AI_LUA_H
#define AI_LUA_H
#include "AIComponent.h"
#include "..\..\LuaScript.h"
#include "..\..\LuaScriptFunctions.h"
#include "EventAIStructs.h"
#include <vector>

class AI_LuaScript : public AI_Script
{
public:
	AI_LuaScript();
	~AI_LuaScript();

	void Initialize();
	void Start();
	void Update();
	void Clear();

	bool SetFileName(boost::any fileName);
	std::string GetFileName();

	Component * NewComponent() const;
private:
	void Reflection();
	static bool l_inList;

	std::vector<Message> messageList;
};

class AI_LuaScript_AddOns : public LuaScriptFunctions
{
public:
	//takes string recievedID & number audience, type & damageLevel
	static int Lua_SendMsg(lua_State * state);
	static int Lua_GetMsg(lua_State * state);
	static int Lua_GetPositionOf(lua_State * state);
	static int Lua_BasicMove(lua_State * state);
	static int Lua_Put(lua_State * state); 

private:
	static bool BasicMove(Vector3 &currentPosition, Vector3 &targetPosition,
									Vector3 &currentVelocity, double timeElapsed, double offset);
};

#endif