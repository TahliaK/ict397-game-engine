#ifndef LABEL_H
#define LABEL_H
#include "Font.h"
#include "Struct.h"
#include "Material_s.h"
#include "UserInterface.h"

/*
* @class Label
* @brief Used for displaying 2D labels within the game world.
* @version 02
* @date 18/04/2017
* @author Olesia Kochergina
*/
class Label : public UserInterface {

public:

	/*
	* @brief Default c-tor.
	*/
	Label();

	/*
	* @brief Destructor.
	*/
	~Label();

	/*
	* @brief Initializes the object, called when creating the object.
	*/
	void Initialize();

	/*
	* @brief Called just before the first update.
	*/
	void Start();

	/*
	* @brief Updates the object and should be called every frame.
	*/
	void Update();

	/*
	* @brief Clears internal data, should be called when
	* the object needs to be either reused or deleted.
	*/
	void Clear();

	/*
	* @brief Creates a new component of this class.
	* @return Pointer to the new component or NULL.
	*/
	Component* NewComponent(void) const;

	/*
	* @brief Sets the text of the label.
	* @param text - string variable containing the content of the label.
	* @return true - the label's text is updated.
	*/
	bool SetText(boost::any text);

	/*
	* @brief Sets the horizontal alignment of the text.
	* NOT IMPLEMENTED
	* @param state - 0,1,2 (left,middle, right)
	*/
	void SetHorizontalAlignment(int state);

	/*
	* @brief Sets the vertical alignment of the text.
	* NOT IMPLEMENTED
	* @param state - 0,1,2 (top,middle, bottom)
	*/
	void SetVerticalAlignment(int state);

	/*
	* @brief Sets the font's texture.
	* @param fontName - string variable, file name of a file where the texture is stored.
	* @return true - if the font's texture is changed.
	*/
	bool SetFontTextureFileName(boost::any fontName);

	/*
	* @brief Sets the font's description.
	* @param fontName - string variable, file name of a file where the description is stored.
	* @return true - if the font's description is changed.
	*/
	bool SetFontFileName(boost::any fontName);

	/*
	* @brief Sets the number of lines and updates the mesh.
	* @param amount - int variable, number of lines.
	* @return true - if the label is changed.
	*/
	bool SetNumOfLines(boost::any amount);

	/*
	* @brief Sets the font's size.
	* @param size - float variable, size of the font.
	* @return true - if the font is changed.
	*/
	bool SetFontSize(boost::any size);

	/*
	* @brief Returns the file name of the font.
	* @return string containing the font's definition.
	*/
	std::string GetFontFileName(void) const;

	/*
	* @brief Returns the text of the label.
	* @return text of the label.
	*/
	std::string GetText(void) const;

	/*
	* @brief Returns the size of the font.
	* @return size of the font
	*/
	float GetFontSize(void) const;

	/*
	* @brief Returns the maximum number of lines.
	* @return max number of lines.
	*/
	float GetMaxLineSize(void) const;

	/*
	* @brief Returns the current number of lines.
	* @return current number of lines.
	*/
	int GetNumOfLines(void) const;

	/*
	* @brief Returns the file name of the font's texture.
	* @return  string containing the font's texture.
	*/
	std::string GetFontTextureFileName(void) const;
private:

	/*
	* @brief Checks whether the mouse is over the object or not.
	*/
	void Interactivity(void);

	/*
	* @brief Creates a new mesh.
	*/
	void LoadFont(void);

	/*
	* @brief Adds values to three vectors: variable name,  setter, getter.
	* Used in the Editor to modify components at run-time.
	*/
	void Reflection();

	///text within the label
	std::string m_text;

	///file name of the font's description
	std::string m_fontName;

	///file name of the font's texture
	std::string m_fontTextureName;

	///size of the font
	float m_fontSize;

	///the max number of lines
	float m_lineMaxSize;

	///the current number of lines
	int m_numberOfLines;

	///the font itself.
	Text::Font m_font;

	///stores the mesh of the label
	vBuffer m_data;

	///Depends on the parent: 0 - right; 1 - center; 2 - left
	int m_hAlignment;
	///Depends on the parent: 0 - up; 1 - middle; 2 - down
	int m_vAlignment;

	///serialization
	friend class boost::serialization::access;
	template<class Archive>
	void save(Archive& ar, const unsigned int /*version*/) const {
		ar & SER_BASE_OBJECT_NVP(UserInterface);
		ar& SER_NVP(m_text);
		ar& SER_NVP(m_fontSize);
		ar& SER_NVP(m_lineMaxSize);
		ar& SER_NVP(m_fontName);
		ar& SER_NVP(m_fontTextureName);
	}

	template<class Archive>
	void load(Archive& ar, const unsigned int /*version*/) {
		ar & SER_BASE_OBJECT_NVP(UserInterface);
		ar& SER_NVP(m_text);
		ar& SER_NVP(m_fontSize);
		ar& SER_NVP(m_lineMaxSize);
		ar& SER_NVP(m_fontName);
		ar& SER_NVP(m_fontTextureName);
	}
	SER_SPLIT_MEMBER()
};

SER_CLASS_VERSION(Label, 0);
SER_CLASS_EXPORT_KEY(Label)
#endif LABEL_H