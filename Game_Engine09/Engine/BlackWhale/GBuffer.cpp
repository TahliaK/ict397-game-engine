#include "GBuffer.h"
#include "Master.h"
#include "Debug.h"

GBuffer::GBuffer() {
	m_depth = 0;
	IFORSI(0, GB_NUM_TEXTURES) {
		m_textures[i] = 0;
	}
}

GBuffer::~GBuffer() {
	Clear();
}

void GBuffer::Write() const {
	m_fbo.BindBuffer(GRAPHICS_DRAW_FRAMEBUFFER);
	unsigned int l_drawBuffers[] = { GRAPHICS_COLOR_ATTACHMENT0, GRAPHICS_COLOR_ATTACHMENT1, GRAPHICS_COLOR_ATTACHMENT2 };
	Master::gl.DrawBuffers(3, l_drawBuffers);
}

void GBuffer::Read() const {
	m_fbo.BindBuffer(GRAPHICS_READ_FRAMEBUFFER);
	IFORSI(0, GB_NUM_TEXTURES) {
		Master::gl.ActiveTexture(GRAPHICS_TEXTURE0 + i);
		Master::gl.BindTexture(GRAPHICS_TEXTURE_2D, m_textures[i]);
	}
}

void GBuffer::Bind() const {
	m_fbo.BindBuffer(GRAPHICS_FRAMEBUFFER);
}

void GBuffer::Unbind() const {
	m_fbo.UnbindBuffer(GRAPHICS_FRAMEBUFFER);
}

bool GBuffer::Create() {
	int screen[] = { Master::s_screen[0],Master::s_screen[1],((int)Master::s_screen[2]) / 8 * 8,((int)Master::s_screen[3]) / 8 * 8 };
	//int screen[] = { 0,0,744,440 };

	m_fbo.CreateBuffer(DEPTH_TEXTURE, screen);
	m_fbo.BindBuffer(GRAPHICS_FRAMEBUFFER);
	m_depth = m_fbo.GetDepth();
	unsigned int l_invalidColor = m_fbo.GetColor();
	// delete invalid texture created by the frame buffer in the beginning.
	if (l_invalidColor != 0) {
		Master::gl.DeleteTextures(1, &l_invalidColor);
		l_invalidColor = 0;
	}
	m_textures[0] = m_fbo.CreateColorTexture(0, GRAPHICS_RGB32F, GRAPHICS_RGB, GRAPHICS_FLOAT);
	m_textures[1] = m_fbo.CreateColorTexture(1, GRAPHICS_RGB32F, GRAPHICS_RGB, GRAPHICS_FLOAT);
	m_textures[2] = m_fbo.CreateColorTexture(2, GRAPHICS_RGBA, GRAPHICS_RGBA, GRAPHICS_UNSIGNED_BYTE);
	// final
	/*glBindTexture(GL_TEXTURE_2D, m_final);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, 744, 440, 0, GL_RGB, GL_FLOAT, NULL);
	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT4, GL_TEXTURE_2D, m_final, 0);
	*/
	unsigned int l_drawBuffers[] = { GRAPHICS_COLOR_ATTACHMENT0, GRAPHICS_COLOR_ATTACHMENT1, GRAPHICS_COLOR_ATTACHMENT2 };
	Master::gl.DrawBuffers(3, l_drawBuffers);

	if (!Debug::RunFramebuffer())
		return false;
	// restore default FBO
	Unbind();
	return true;
}

void GBuffer::SetReadTextureType(GBUFFER_TEXTURE type) {
	Master::gl.ReadBuffer(GRAPHICS_COLOR_ATTACHMENT0 + type);
}

void GBuffer::Clear() {
	m_fbo.Clear();
	m_depth = 0;
	IFORSI(0, GB_NUM_TEXTURES - 1) {
		if (m_textures[i] != 0) {
			Master::gl.DeleteTextures(1, &m_textures[i]);
			m_textures[i] = 0;
		}
	}
	//its deleted by the fbo
	m_textures[GB_NUM_TEXTURES - 1] = 0;
}

unsigned int GBuffer::GetDiffuseTexture() {
	return m_textures[GB_TEXCOORD];
}

unsigned int GBuffer::GetPositionTexture() {
	return m_textures[GB_POSITION];
}

unsigned int GBuffer::GetNormalTexture() {
	return m_textures[GB_NORMAL];
}