#include "Input_GL.h"
#include "../Master.h"
#include "../Engine.h"
#include <GL\glew.h>
#include <GL\freeglut.h>

void Input_GL::InitializeF() {
	//glutSetKeyRepeat(GLUT_KEY_REPEAT_OFF);
	glutIgnoreKeyRepeat(1);

	glutSpecialFunc(Input::SpecialFunction);
	glutSpecialUpFunc(Input::SpecialUpFunction);

	glutKeyboardFunc(Input::KeyboardFunction);
	glutKeyboardUpFunc(Input::KeyboardUpFunction);

	glutMouseFunc(Input::MouseFunction);
	//glutMotionFunc(ActiveMotionF);
	glutMotionFunc(Input::ActiveMotionFunction);
	glutPassiveMotionFunc(Input::PassiveMotionFunction);
	for (int i = 0; i < 127; i++) {
		input.insert(pair<int, bool>(i, false));
	}
	Update();
}

void Input_GL::SpecialF(int keyCode, int width, int height) {
	ProcessSpecial(keyCode, true);
}

void Input_GL::SpecialUpF(int keyCode, int width, int height) {
	ProcessSpecial(keyCode, false);
}

void Input_GL::KeyboardF(unsigned char keyCode, int width, int height) {
	ProcessNormal(keyCode, true);
}

void Input_GL::KeyboardUpF(unsigned char keyCode, int width, int height) {
	ProcessNormal(keyCode, false);
}

void Input_GL::MouseF(int button, int state, int x, int y) {
	mouse_position[0] = x;
	mouse_position[1] = y;
	bool temp = false;
	if (state == GLUT_DOWN)
		temp = true;

	if (button == GLUT_LEFT_BUTTON)
		input.at(LEFT_BUTTON) = temp;
	if (button == GLUT_MIDDLE_BUTTON)
		input.at(RIGHT_BUTTON) = temp;
	if (button == GLUT_RIGHT_BUTTON)
		input.at(RIGHT_BUTTON) = temp;
	//Engine::Update();
}

void Input_GL::PassiveMotionF(int x, int y) {
	prev_mouse_position = mouse_position;
	mouse_position[0] = (float)x;
	mouse_position[1] = (float)y;
	Engine::Update();
}

void Input_GL::ProcessNormalF(int keyCode, bool value) {
	switch (keyCode) {
	case 27:
		input.at(ESCAPE_KEY) = value;
		break;
	case 32:
		input.at(SPACE_KEY) = value;
		break;
	}

	for (int i = 65; i <= 90; i++) {
		if (keyCode == i)
			input.at((KeyCode)(i - 59)) = value;
	}
	for (int i = 97; i <= 122; i++) {
		if (keyCode == i)
			input.at((KeyCode)(i - 65)) = value;
	}
}

void Input_GL::ProcessSpecialF(int keyCode, bool value) {
	switch (keyCode) {
	case GLUT_KEY_F1:
		input.at(F1) = value;
		break;
	case GLUT_KEY_F2:
		input.at(F2) = value;
		break;
	case GLUT_KEY_F3:
		input.at(F3) = value;
		break;
	case GLUT_KEY_F4:
		input.at(F4) = value;
		break;
	case GLUT_KEY_F5:
		input.at(F5) = value;
		break;
	case GLUT_KEY_F6:
		input.at(F6) = value;
		break;
	case GLUT_KEY_F7:
		input.at(F7) = value;
		break;
	case GLUT_KEY_F8:
		input.at(F8) = value;
		break;
	case GLUT_KEY_F9:
		input.at(F9) = value;
		break;
	case GLUT_KEY_F10:
		input.at(F10) = value;
		break;
	case GLUT_KEY_F11:
		input.at(F11) = value;
		break;
	case GLUT_KEY_F12:
		input.at(F12) = value;
		break;
	case GLUT_KEY_LEFT:
		input.at(LEFT_KEY) = value;
		break;
	case GLUT_KEY_RIGHT:
		input.at(RIGHT_KEY) = value;
		break;
	case GLUT_KEY_UP:
		input.at(UP_KEY) = value;
		break;
	case GLUT_KEY_DOWN:
		input.at(DOWN_KEY) = value;
		break;
	case GLUT_KEY_PAGE_UP:
		input.at(PAGE_UP_KEY) = value;
		break;
	case GLUT_KEY_PAGE_DOWN:
		input.at(PAGE_DOWN_KEY) = value;
		break;
	case GLUT_KEY_HOME:
		input.at(HOME_KEY) = value;
		break;
	case GLUT_KEY_END:
		input.at(END_KEY) = value;
		break;
	case GLUT_KEY_SHIFT_L:
		input.at(SHIFT_LEFT_KEY) = value;
		break;
	case GLUT_KEY_SHIFT_R:
		input.at(SHIFT_RIGHT_KEY) = value;
		break;
	case GLUT_KEY_ALT_L:
		input.at(ALT_LEFT_KEY) = value;
		break;
	case GLUT_KEY_ALT_R:
		input.at(ALT_RIGHT_KEY) = value;
		break;
	case GLUT_KEY_INSERT:
		input.at(INSERT_KEY) = value;
		break;
	}
}