#ifndef  FRUSTUM_CULLING_H
#define FRUSTUM_CULLING_H
#include "Math_s.h"
#include "Physics\ColliderAABB.h"
/*
* @class Frustum Culling 
* @brief Allows to cull 3d objects that are not in the viewing frustum.
* @author Olesia Kochergina
* @date 11/05/2017
* 
*/
class FrustumCulling {
public:
	/*
	* @brief Default c-tor
	*/
	FrustumCulling();

	/*
	* @brief Destructor
	*/
	~FrustumCulling();

	/*
	* @brief Sets up a virtual perspective projection.
	* @param fov - field of view
	* @param aspect - width/height
	* @param near - near plane
	* @param far - far plane
	*/
	void Perspective(float fov, float aspect, float near, float far);

	/*
	* @brief Sets up a virtual look at matrix.
	* @param pos - position of the camera
	* @param view - camera target
	* @param up - up vector
	*/
	void LookAt(Vector3 pos, Vector3 view, Vector3 up);

	/*
	* @brief Checks whether the parameter's value is inside the current frustum or not.
	* The approach is called frustum radar
	* @param point - the point to be checked
	* @return true - inside, false - outside
	*/
	bool Contains(Vector3 point);

	/*
	* @brief Checks whether the parameter's value is inside the current frustum or not.
	* Geometric approach is used.
	* @param box - the axis-aligned bounding box.
	* @return true - inside, false - outside
	*/
	bool Contains(ColliderAABB* box);
private:

	///planes
	enum {
		TOP = 0, BOTTOM, LEFT,
		RIGHT, NEARP, FARP
	};

	///near plane
	float m_near;

	///far plane
	float m_far;

	///near plane width
	float m_nWidth;

	///near plane height
	float m_nHeight;

	///far plane width
	float m_fWidth;

	///far plane height
	float m_fHeight;

	///width/height
	float m_ratio;

	///field of view
	float m_fov;

	///tangent of PI/180 by half of the fov
	float m_tang;

	///position of the camera
	Vector3 m_camPos;

	///6 planes
	Plane3 m_plane[6];

	///near and far corners of the frustum
	Vector3 ntl, ntr, nbl, nbr, ftl, ftr, fbl, fbr;
	
	///camera referential
	Vector3 m_camRef[3];
};
#endif // ! FRUSTUM_CULLING_H
