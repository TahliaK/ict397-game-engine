#ifndef PARTICLE_SYSTEM_H
#define PARTICLE_SYSTEM_H
#include "Component.h"
#include "Material_s.h"
/*
* @class 
* @brief
* @author Olesia Kochergina
* @date 24/05/2017
*/
class ParticleSystem : public Component {
	///
	struct Particle {
		Vector3 position;
		Vector3 velocity;
		float life;

		Particle() : position(0), velocity(0), life(0) {
			
		}
	};

public:

	/*
	* @brief Default c-tor.
	*/
	ParticleSystem();

	/*
	* @brief Copy c-tor.
	*/
	ParticleSystem(const ParticleSystem& original);

	/*
	* @brief Destructor.
	*/
	~ParticleSystem();

	/*
	* @brief Initializes the object, called when creating the object.
	*/
	void Initialize();

	/*
	* @brief Called just before the first update.
	*/
	void Start();

	/*
	* @brief Updates the object and should be called every frame.
	*/
	void Update();

	/*
	* @brief Clears internal data, should be called when
	* the object needs to be either reused or deleted.
	*/
	void Clear();

	/*
	* @brief Creates a new component of this class.
	* @return Pointer to the new component or NULL.
	*/
	Component* NewComponent(void) const;

	/*
	* @brief Updates the front image of this skybox.
	* @param image - string file name.
	* @return true - if set.
	*/
	bool SetImage(boost::any image);

	/*
	* @brief Returns the front image's file name of the light source.
	* @return file name
	*/
	std::string GetImage(void) const;

	/*
	* @brief Updates the back image of this skybox.
	* @param image - string file name.
	* @return true - if set.
	*/
	bool SetLife(boost::any image);

	/*
	* @brief Returns the back image's file name of the light source.
	* @return file name
	*/
	std::string GetLife(void) const;

	/*
	* @brief Updates the Up image of this skybox.
	* @param image - string file name.
	* @return true - if set.
	*/
	bool SetMaxParticles(boost::any image);

	/*
	* @brief Returns the Up image's file name of the light source.
	* @return file name
	*/
	std::string GetMaxParticles(void) const;

	/*
	* @brief Updates the Down image of this skybox.
	* @param image - string file name.
	* @return true - if set.
	*/
	bool SetDown(boost::any image);

	/*
	* @brief Returns the Down image's file name of the light source.
	* @return file name
	*/
	std::string GetDown(void) const;

	

	/*
	* @brief Sets the color of the skybox.
	* @param color - RGBA variable.
	* @return true - if set.
	*/
	bool SetColor(boost::any color);

	/*
	* @brief Returns the color of the skybox.
	* @return color
	*/
	RGBA GetColor(void) const;
private:

	///vector of particles
	std::vector<Particle> m_particles;

	///max amount of particles
	int m_max;

	///no of frames a particle is alive
	int m_life;
};
#endif PARTICLE_SYSTEM_H
