#include "ShaderProgram.h"
#include <istream>
#include <fstream>
#include <string>
#include "Master.h"
#include "Debug.h"
#define SHADER_ERROR -10000;
using namespace std;
ShaderProgram::ShaderProgram(){
	m_linked = GRAPHICS_FALSE;
	m_program = 0;
	m_fsName = "";
	m_vsName = "";
}

//ShaderProgram::ShaderProgram(const ShaderProgram& p_or) {
//	m_linked = GRAPHICS_FALSE;
//	m_program = p_or.m_program;
//	m_fsName = p_or.m_fsName;
//	m_vsName = p_or.m_vsName;
//	m_fs = p_or.m_fs;
//	m_vs = p_or.m_vs;
//	
//	m_object_key = p_or.m_object_key;
//}

ShaderProgram::~ShaderProgram() {  }

void ShaderProgram::Clear(){
	m_object_key.clear();
	if(m_program!=0){
		Master::gl.DetachShader(m_program,m_fs.GetShader());
		Master::gl.DetachShader(m_program,m_vs.GetShader());
		Master::gl.DeleteProgram(m_program);
		m_program = 0;
	}
}
int ShaderProgram::GetID(){
	return m_program;
}

void ShaderProgram::SetFragmentShader(string temp){
	m_fs.CreateShader(GRAPHICS_FRAGMENT_SHADER);
	m_fsName = temp;
	std::string f1;
	std::ifstream inFile;
	inFile.open(m_fsName);

	if (!inFile.bad() && !inFile.fail() && inFile.is_open()) {
		do {
			getline(inFile, temp);
			f1.append(temp + "\n");
		} while (!inFile.eof());
	}
	else {
		std::cout << "Shader file does not exist: " << m_fsName << std::endl;
	}
	inFile.close();
	f1.append("\0");

	const char *t1 = f1.c_str();
	//add shaders source and compile
	m_fs.AttachSource(1, &t1, NULL);
	m_fs.CompileShader();
	m_fs.PrintInfoLog();
}

void ShaderProgram::SetVertexShader(string temp){
	m_vs.CreateShader(GRAPHICS_VERTEX_SHADER);
	m_vsName = temp;
	std::string f1;
	std::ifstream inFile;
	inFile.open(m_vsName);

	if (!inFile.bad() && !inFile.fail() && inFile.is_open()) {
		do {
			getline(inFile, temp);
			f1.append(temp + "\n");
		} while (!inFile.eof());
	}
	else {
		std::cout << "Shader file does not exist: " << m_fsName << std::endl;
	}
	inFile.close();
	f1.append("\0");

	const char *t1 = f1.c_str();
	//add shaders source and compile
	m_vs.AttachSource(1, &t1, NULL);
	m_vs.CompileShader();
	m_vs.PrintInfoLog();
}

void ShaderProgram::Create(){
	m_program = Master::gl.CreateProgram();
}

void ShaderProgram::Attach(){
	Master::gl.AttachShader(m_program,m_fs.GetShader());
	Master::gl.AttachShader(m_program,m_vs.GetShader());
}

bool ShaderProgram::Link(){
	std::string temp;
	//std::string f1;
	//std::string f2;

	//std::ifstream inFile;
	//inFile.open(m_fsName);

	//if(!inFile.bad() && !inFile.fail() && inFile.is_open()){
	//	do{
	//		getline(inFile,temp);
	//		f1.append(temp+"\n");
	//	}while(!inFile.eof());
	//}else{
	//	std::cout<<"Shader file does not exist: "<<m_fsName<<std::endl;
	//}
	//inFile.close();

	//inFile.open(m_vsName);
	//if(!inFile.bad() && !inFile.fail() && inFile.is_open()){
	//	do{
	//		getline(inFile,temp);
	//		f2.append(temp+"\n");
	//	}while(!inFile.eof());
	//}else{
	//	std::cout<<"Shader file does not exist: "<<m_vsName<<std::endl;
	//}
	//inFile.close();
	//f1.append("\0");
	//f2.append("\0");

	//const char *t1 = f1.c_str();
	//const char *t2 = f2.c_str();
	////add shaders source and compile
	//m_fs.AttachSource(1,&t1,NULL);
	//m_vs.AttachSource(1,&t2,NULL);

	//m_fs.CompileShader();
	//m_vs.CompileShader();

	//m_fs.PrintInfoLog();
	//m_vs.PrintInfoLog();
	if(m_fs.IsCompiled() && m_vs.IsCompiled()){
		//glBindFragDataLocation(m_program,0,"outputF");
		Master::gl.LinkProgram(m_program);
		Master::gl.GetProgramiv(m_program, GRAPHICS_LINK_STATUS,&m_linked);
		std::cout<<"ShaderProgram. Linked program: "<<m_program<<std::endl;
		PrintInfoLog();
	}
	if(m_linked == 1)
		return true;
	return false;
}

std::string ShaderProgram::GetVertexShader(void) {
	return m_vsName;
}

std::string ShaderProgram::GetFragmentShader(void) {
	return 	m_fsName;
}

bool ShaderProgram::Activate(){
	if(m_linked){
		Master::gl.UseProgram(m_program);
	}
	return m_linked;
}

void ShaderProgram::PrintInfoLog(){
	std::string log = Master::gl.GetProgramInfoLog(m_program);
	std::cout<<log<<std::endl;	
}

void ShaderProgram::SetLocations(void) {
	int l_amount = 0;
	const int l_maxLength = 40;
	Master::gl.GetProgramiv(m_program, GRAPHICS_ACTIVE_ATTRIBUTES, &l_amount);
	IFORSI(0, l_amount) {
		int l_length = 0;
		int l_size = 0;
		unsigned int l_type;
		char l_name[l_maxLength];
		Master::gl.GetActiveAttribute(m_program, i, l_maxLength, &l_length, &l_size, &l_type, l_name);
		SetALocation(std::string(l_name));
	}
	Master::gl.GetProgramiv(m_program, GRAPHICS_ACTIVE_UNIFORMS, &l_amount);
	IFORSI(0, l_amount) {
		int l_length = 0;
		int l_size = 0;
		unsigned int l_type;
		char l_name[l_maxLength];
		Master::gl.GetActiveUniform(m_program, i, l_maxLength, &l_length, &l_size, &l_type, l_name);
		std::string l_strName(l_name);
		
		if (l_strName.find('[') != l_strName.size())
			l_strName = l_strName.substr(0, l_strName.find('['));
		SetULocation(l_strName);
	}
}

void ShaderProgram::SetALocation(std::string p_var){
	m_object_key.insert(pair<std::string, int> (p_var, Master::gl.GetALoc(m_program,p_var)));
}

void ShaderProgram::SetULocation(std::string p_var) {	
	m_object_key.insert(pair<std::string, int>(p_var, Master::gl.GetULoc(m_program, p_var)));
}

int ShaderProgram::GetLocation(std::string p_var){
	if(m_object_key.find(p_var) != m_object_key.end())
		return m_object_key[p_var];
	else
		return SHADER_ERROR;
}