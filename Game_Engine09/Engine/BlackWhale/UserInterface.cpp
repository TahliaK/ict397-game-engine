#include "UserInterface.h"
#include "Input\Input.h"
#include "Master.h"
#include "Transform.h"
GameObject UserInterface::m_camera;
BOOST_CLASS_EXPORT_IMPLEMENT(UserInterface)

UserInterface::UserInterface() : Component() {
	Reflection();
	Initialize();
	if (m_camera.GetComponents().size() == 1) {
		SetUp();
	}
}

UserInterface::~UserInterface() {
	Clear();
}

void UserInterface::Initialize() {
	m_name = "UserInterface";
	m_interactable = true;
	if (m_gameObject) {
		m_gameObject->GetTransform()->scale = Vector3(75, 25, 0);
		m_gameObject->GetTransform()->rotation = Vector3(0, 0, 0);
		//z = 0 or 1
		m_gameObject->GetTransform()->position = Vector3(30, 2, 1);
	}
	m_order = 0;
	m_color = RGBA(1, 1, 1);
}

void UserInterface::SetUp() {
	m_camera.AddComponent(new Camera);
	((Camera*)m_camera.GetComponents().back())->SetMode(false);
	m_camera.Update();
}

RGBA UserInterface::GetColor(void) const {
	return m_color;
}

bool UserInterface::SetColor(boost::any var) {
	try {
		m_color = boost::any_cast<RGBA> (var);
	}
	catch (boost::bad_any_cast& /*bac*/) {
		return false;
	}
	return true;
}
int UserInterface::GetOrder(void) const {
	return m_order;
}
bool UserInterface::SetOrder(boost::any var) {
	try {
		m_order = boost::any_cast<int> (var);
	}
	catch (boost::bad_any_cast& /*bac*/) {
		return false;
	}
	return true;
}

void UserInterface::Update() {
	Interactivity();
}

bool UserInterface::IsMouseOver(void) const {
	return m_mouseOver;
}

bool UserInterface::SetInteractable(boost::any p_var) {
	try {
		m_interactable = boost::any_cast<bool> (p_var);
	}
	catch (boost::bad_any_cast& /*bac*/) {
		return false;
	}
	return true;
}
bool UserInterface::GetInteractable(void) const {
	return m_interactable;
}

void UserInterface::Reflection() {
	m_Variables.push_back("Interactable");
	m_Setters.push_back(boost::bind(&UserInterface::SetInteractable, this, _1));
	m_Getters.push_back(boost::bind(&UserInterface::GetInteractable, this));

	m_Variables.push_back("Color");
	m_Setters.push_back(boost::bind(&UserInterface::SetColor, this, _1));
	m_Getters.push_back(boost::bind(&UserInterface::GetColor, this));

	m_Variables.push_back("Order");
	m_Setters.push_back(boost::bind(&UserInterface::SetOrder, this, _1));
	m_Getters.push_back(boost::bind(&UserInterface::GetOrder, this));
}