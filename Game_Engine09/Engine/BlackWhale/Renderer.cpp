#include "Renderer.h"
#include "Master.h"

Renderer::Renderer(){
	m_enabled = true;
	m_visible = true;
	m_castShadows = true;
	m_receiveShadows = true;
	//bool l_inList = false;
	//if(!l_inList){
	//	l_inList = true;
	//	Renderer* l_pointer = new Renderer;
	//	m_derivedClasses.push_back(l_pointer);
	//}
	Reflection();
}

Renderer::~Renderer(){

}

const bool Renderer::SetIsEnabled(boost::any p_enabled){
	try{
		m_enabled = boost::any_cast<bool> (p_enabled);
	}catch(boost::bad_any_cast& /*bac*/){
		return false;
	}
	return true;	
}

const bool Renderer::SetIsVisible(boost::any p_visible){
	try{
		 m_visible = boost::any_cast<bool> (p_visible);
	}catch(boost::bad_any_cast& /*bac*/){
		return false;
	}
	return true;	
}

const bool Renderer::SetReceiveShadows(boost::any p_receive){
	try{
		m_receiveShadows = boost::any_cast<bool> (p_receive);
	}catch(boost::bad_any_cast& /*bac*/){
		return false;
	}
	return true;	
}

const bool Renderer::SetCastShadows(boost::any p_cast){
	try{
		m_castShadows = boost::any_cast<bool> (p_cast);
	}catch(boost::bad_any_cast& /*bac*/){
		return false;
	}
	return true;
}

const bool Renderer::SetMaterials(boost::any p_materials){
	try{
		m_materials = boost::any_cast<std::vector<Material>> (p_materials);
	}catch(boost::bad_any_cast& /*bac*/){
		return false;
	}
	return true;
}

const bool Renderer::AddMaterial(boost::any p_material){
	try{
		m_materials.push_back(boost::any_cast<Material> (p_material));
	}catch(boost::bad_any_cast& /*bac*/){
		return false;
	}
	return true;
}

const bool Renderer::GetIsEnabled(void) const{
	return m_enabled;
}

const bool Renderer::GetIsVisible(void) const{
	return m_visible;
}

const bool Renderer::GetReceiveShadows(void) const{
	return m_receiveShadows;
}

const bool Renderer::GetCastShadows(void) const{
	return m_castShadows;
}

const std::vector<Material> Renderer::GetMaterials(void) const{
	return m_materials;
}

void Renderer::Reflection(void){
	m_Variables.push_back(PRINTNAME(m_enabled));
	m_Variables.push_back(PRINTNAME(m_visible));
	m_Variables.push_back(PRINTNAME(m_receiveShadows));
	m_Variables.push_back(PRINTNAME(m_castShadows));
	m_Variables.push_back(PRINTNAME(m_materials));

	m_Setters.push_back(boost::bind(&Renderer::SetIsEnabled,this,_1));
	m_Setters.push_back(boost::bind(&Renderer::SetIsVisible,this,_1));
	m_Setters.push_back(boost::bind(&Renderer::SetReceiveShadows,this,_1));
	m_Setters.push_back(boost::bind(&Renderer::SetCastShadows,this,_1));
	m_Setters.push_back(boost::bind(&Renderer::SetMaterials,this,_1));

	m_Getters.push_back(boost::bind(&Renderer::GetIsEnabled,this));
	m_Getters.push_back(boost::bind(&Renderer::GetIsVisible,this));
	m_Getters.push_back(boost::bind(&Renderer::GetReceiveShadows,this));
	m_Getters.push_back(boost::bind(&Renderer::GetCastShadows,this));
	m_Getters.push_back(boost::bind(&Renderer::GetMaterials,this));
}