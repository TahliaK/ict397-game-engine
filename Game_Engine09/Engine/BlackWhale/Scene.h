#ifndef SCENE_H
#define SCENE_H
#include "Camera.h"
#include "Light.h"
#include "GameObject.h"

namespace SceneManagement {

	/*
	* @brief Represents a single scene (level).
	* @author Olesia Kochergina
	* @date 19/04/2017
	* @version 02
	*/
	class Scene : public InObject {
	public:

		/*
		* @brief Default c-tor.
		*/
		Scene();

		/*
		* @brief Destructor.
		*/
		~Scene();

		/*
		* @brief Initializes the object, called when creating the object.
		*/
		void Initialize();

		/*
		* @brief Called just before the first update.
		*/
		void Start();

		/*
		* @brief Updates the object and should be called every frame.
		*/
		void Update();

		/*
		* @brief Clears internal data, should be called when
		* the object needs to be either reused or deleted.
		*/
		void Clear();

		/*
		* @brief NOT IMPLEMENTED
		*/
		void Reshape();

		/*
		* @brief Returns a pointer to the main camera within the scene.
		* @return the main camera
		*/
		Camera* GetMainCamera(void);

		/*
		* @brief Returns game objects without parents.
		* @return modifiable vector of root game objects.
		*/
		std::vector<GameObject*>& GetRootGameObjects(void);

		/*
		* @brief Removes an object from the scene by its index.
		* @param index - index of the game object
		*/
		void DeleteGameObject(unsigned int index);

		/*
		* @brief Adds a new component to a game object.
		* @param gameobject - the game object
		* @param component - component that needs to be added
		* @return true - component is added; false - scene has no camera or the component is invalid
		*/
		bool AddComponent(GameObject* gameobject, Component* component);

		/*
		* @brief Returns the scene's ID
		* @return ID
		*/
		int GetID(void)const;

		/*
		* @brief Updates the scene's ID
		* @param index - scene ID
		*/
		void SetID(const int index);
	private:

		/*
		* @brief Returns the main light.
		* @return the light
		*/
		Light* GetMainLight();

		/*
		* @brief Updates the scene using deferred shading for 3D graphics.
		*/
		void Deferred();

		/*
		* @brief REMOVE FROM HERE
		*/
		void Input();

		///stores camera index where [0] - go index and [1] - component index
		Vector2 m_cameraIndex;

		///stores camera index where [0] - go index and [1] - component index
		Vector2 m_lightIndex;

		///collection of all go within the scene
		std::vector<GameObject*> m_gameObjects;

		///scene ID
		int m_index;

		///true - if the scene is altered
		bool m_modified;

		///true - if from a file
		bool m_loaded;

		///path of the scene
		std::string m_path;

		///serialization
		friend class boost::serialization::access;
		template<class Archive>
		void save(Archive& ar, const unsigned int /*version*/) const {
			ar & SER_BASE_OBJECT_NVP(InObject);
			ar& SER_NVP(m_gameObjects);
		}

		template<class Archive>
		void load(Archive& ar, const unsigned int /*version*/) {
			ar & SER_BASE_OBJECT_NVP(InObject);
			ar& SER_NVP(m_gameObjects);
		}

		SER_SPLIT_MEMBER()
	};
};
SER_CLASS_VERSION(SceneManagement::Scene, 0);
BOOST_CLASS_EXPORT_KEY(SceneManagement::Scene)
#endif SCENE_H