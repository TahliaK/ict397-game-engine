#include "Label.h"
#include "Master.h"
#include "VertexBuffer.h"
#include "SceneManager.h"
#include "Input\Input.h"
using namespace std;
using namespace Text;
BOOST_CLASS_EXPORT_IMPLEMENT(Label)

Label::Label() :UserInterface() {
	Reflection();
	Initialize();

	static bool l_inList = false;
	if (!l_inList) {
		l_inList = true;
		Label* l_pointer = new Label;
		m_derivedClasses.push_back(l_pointer);
	}
}
void Label::Clear() {
	m_data.Clear();
	m_font.Clear();
}
Label::~Label() {
	Clear();
}

void Label::Initialize() {
	m_name = "Label";
	if (m_gameObject) {
		m_gameObject->GetTransform()->scale = Vector3(75, 25, 0);
		m_gameObject->GetTransform()->rotation = Vector3(0, 0, 0);
		m_gameObject->GetTransform()->position = Vector3(100, 100, 1);
	}
	m_hAlignment = 1;
	m_vAlignment = 0;
	m_text = "Enter Text";
	m_fontSize = 6;
	m_lineMaxSize = 10;
	m_fontTextureName = Master::s_fontFolder + "verdana.png";
	m_fontName = Master::s_fontFolder + "verdana.fnt";
}

void Label::Start() {
	m_font = Font(GraphicsEngine::LoadTexture(m_fontTextureName, false), m_fontName);
	LoadFont();
	//to update the object after the 3D scene
	if (m_gameObject)
		m_gameObject->SetLayer(1);
}

float Label::GetFontSize() const {
	return m_fontSize;
}
float Label::GetMaxLineSize() const {
	return m_lineMaxSize;
}
int Label::GetNumOfLines()const {
	return m_numberOfLines;
}

bool Label::SetNumOfLines(boost::any var) {
	try {
		m_numberOfLines = boost::any_cast<int> (var);
		LoadFont();
	}
	catch (boost::bad_any_cast& /*bac*/) {
		return false;
	}
	return true;
}

bool Label::SetFontSize(boost::any var) {
	try {
		m_fontSize = boost::any_cast<float> (var);
		LoadFont();
	}
	catch (boost::bad_any_cast& /*bac*/) {
		return false;
	}
	return true;
}

Component* Label::NewComponent() const {
	Component* l_pointer = new Label;
	return l_pointer;
}

void Label::LoadFont() {
	Vector3 pos = m_gameObject->GetTransform()->position;
	m_gameObject->GetTransform()->position = Vector3(0, 0, 1);
	m_gameObject->GetTransform()->scale.x = m_text.size()*m_fontSize;
	m_gameObject->GetTransform()->scale.y = m_fontSize;
	m_data.Clear();
	//load the text
	m_font.LoadText(this, m_data.data);
	if (m_data.data.vertex.size() > 0 && m_data.data.texture.size() > 0 && m_data.data.normal.size() > 0) { //if there is no error
		m_data.attribLocation[0] = Master::gl.GetALoc(Master::s_TextShader.GetID(), "position");
		m_data.attribLocation[1] = Master::gl.GetALoc(Master::s_TextShader.GetID(), "texCoord");
		m_data.attribLocation[2] = Master::gl.GetALoc(Master::s_TextShader.GetID(), "normal");
		VertexBuffer::CreateVBO(&m_data, Master::s_TextShader.GetID());
	}
	else
		cout << "Label.cpp: TEXT LOADING ERROR" << endl;
	m_gameObject->GetTransform()->position = pos;
}

void Label::SetHorizontalAlignment(int state) {
	if (state >= 0 && state <= 2)
		m_hAlignment = state;
}

void Label::SetVerticalAlignment(int state) {
	if (state >= 0 && state <= 2)
		m_vAlignment = state;
}

void Label::Update() {
	if (!m_gameObject || !m_gameObject->GetVisible())
		return;
	UserInterface::Update();
	Master::s_TextShader.Activate();
	Matrix4 l_modelMatrix(((Camera*)m_camera.GetComponents().at(1))->GetModelMatrix());
	//l_modelMatrix = Matrix4::Scale(l_modelMatrix, Vector4(m_gameObject->GetTransform()->scale.x / 50, m_gameObject->GetTransform()->scale.y / 25, m_gameObject->GetTransform()->scale.z, 0));
	l_modelMatrix = Matrix4::Rotate(l_modelMatrix, Vector4(0, 0, m_gameObject->GetTransform()->rotation.z, 0));
	l_modelMatrix = Matrix4::Translate(l_modelMatrix, Vector4(m_gameObject->GetTransform()->position.x / 50, (m_gameObject->GetTransform()->position.y) / (50) - 2, 0, 0));

	Master::gl.UniformMatrix4fv(Master::s_TextShader.GetLocation("model"), 1, GRAPHICS_FALSE, l_modelMatrix);

	Master::gl.BindTexture(GRAPHICS_TEXTURE_2D, (unsigned int)m_font.GetTexture());
	GraphicsEngine::SwitchToRGB(false);
	Master::gl.Uniform4fv(Master::s_TextShader.GetLocation("color"), 1, m_color.GetColor());
	Master::gl.Uniform1i(Master::s_TextShader.GetLocation("isSDF"), true);
	Master::gl.PolygonMode(GRAPHICS_FRONT, GRAPHICS_FILL);
	VertexBuffer::RenderVBO(&m_data, 0, m_data.data.vertex.size());
};

void Label::Interactivity() {
	float l_sWidth = Master::s_screen[2] - Master::s_screen[0];
	float l_sHeight = Master::s_screen[3] - Master::s_screen[1];
	float minX = (m_gameObject->GetTransform()->position.x / 100 * l_sWidth);
	float maxX = (m_gameObject->GetTransform()->scale.x) + minX;
	float maxY = (100 - m_gameObject->GetTransform()->position.y) / 100 * l_sHeight;
	float minY = maxY - m_gameObject->GetTransform()->scale.y;
	if (minX < Input::GetMousePosition()[0] && maxX > Input::GetMousePosition()[0] &&
		minY < Input::GetMousePosition()[1] && maxY > Input::GetMousePosition()[1]) {
		m_mouseOver = true;
	}
	else {
		m_mouseOver = false;
	}
}
bool Label::SetText(boost::any var) {
	try {
		m_text = boost::any_cast<std::string> (var);
		m_gameObject->GetTransform()->scale.x = m_text.size() * 8;
		m_gameObject->GetTransform()->scale.y = 13;
		LoadFont();
	}
	catch (boost::bad_any_cast& /*bac*/) {
		return false;
	}
	return true;
}
std::string Label::GetText(void) const {
	return m_text;
}

bool Label::SetFontFileName(boost::any var) {
	try {
		std::string l_temp = boost::any_cast<std::string> (var);
		if (m_fontName.compare(l_temp) == 0)
			return true;
		else
			m_fontName = l_temp;
		m_font = Font(GraphicsEngine::LoadTexture(Master::s_fontFolder + m_fontTextureName, false), Master::s_fontFolder + m_fontName);
		LoadFont();
	}
	catch (boost::bad_any_cast& /*bac*/) {
		return false;
	}
	return true;
}
std::string Label::GetFontFileName(void) const {
	return m_fontName;
}

bool Label::SetFontTextureFileName(boost::any var) {
	try {
		std::string l_temp = boost::any_cast<std::string> (var);
		if (m_fontTextureName.compare(l_temp) == 0)
			return true;
		else
			m_fontTextureName = l_temp;
		m_font.Clear();
		m_font = Font(GraphicsEngine::LoadTexture(Master::s_fontFolder + m_fontTextureName, false), Master::s_fontFolder + m_fontName);
		LoadFont();
	}
	catch (boost::bad_any_cast& /*bac*/) {
		return false;
	}
	return true;
}
std::string Label::GetFontTextureFileName(void)const {
	return m_fontTextureName;
}

void  Label::Reflection() {
	m_Variables.push_back("Text");
	m_Setters.push_back(boost::bind(&Label::SetText, this, _1));
	m_Getters.push_back(boost::bind(&Label::GetText, this));

	m_Variables.push_back("Font size");
	m_Setters.push_back(boost::bind(&Label::SetFontSize, this, _1));
	m_Getters.push_back(boost::bind(&Label::GetFontSize, this));

	m_Variables.push_back("Font file");
	m_Setters.push_back(boost::bind(&Label::SetFontFileName, this, _1));
	m_Getters.push_back(boost::bind(&Label::GetFontFileName, this));

	m_Variables.push_back("Texture");
	m_Setters.push_back(boost::bind(&Label::SetFontTextureFileName, this, _1));
	m_Getters.push_back(boost::bind(&Label::GetFontTextureFileName, this));
}