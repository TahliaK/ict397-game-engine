#include "Character.h"
using namespace Text;
Character::Character(int id){
	this->id = id;
}

Character::Character(int id, float xTextureCoord, float yTextureCoord, float xTexSize, float yTexSize,float xOffset, float yOffset, float sizeX, float sizeY, float xAdvance){
	this->id = id;
	this->x = xTextureCoord;
	this->y = yTextureCoord;
	this->xOffset = xOffset;
	this->yOffset = yOffset;
	this->xSize = sizeX;
	this->ySize = sizeY;
	this->width = xTexSize + xTextureCoord;
	this->height = yTexSize + yTextureCoord;
	this->xAdvance = xAdvance;
}

Character::Character(){

}

std::istream & operator >>(std::istream & input,Character & C ){
	char temp;
	input >> temp;
	C.id = (int)temp;
	return input;
}

std::ostream & operator <<(std::ostream & output, const Character & C){
	output << (char)C.id;
	return output;
}