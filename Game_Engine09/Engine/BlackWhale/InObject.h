#ifndef INOBJECT_H
#define INOBJECT_H
#include <string>
#include <vector>
#include "Interfaces\Serialization.h"
/*
* @class InObject
* @brief Base class for all game objects and components.
* @author Olesia Kochergina
* @date 03/02/2017
*/
class InObject {
public:
	/*
	* @brief Default c-tor.
	*/
	InObject();

	/*
	* @brief Copy c-tor.
	* @param object - the original object to be copied from.
	*/
	InObject(const InObject& object);

	/*
	* @brief Destructor that must be overriden.
	*/
	virtual ~InObject() {};

	/*
	* @brief Initializes the object, called when creating the object.
	*/
	virtual void Initialize() {};

	/*
	* @brief Called just before the first update.
	*/
	virtual void Start() {};

	/*
	* @brief Updates the object and should be called every frame.
	*/
	virtual void Update() {};

	/*
	* @brief Clears internal data, should be called when
	* the object needs to be either reused or deleted.
	*/
	virtual void Clear() {};

	/*
	* @brief Compares two objects.
	* @param object - the object to be compared with.
	* @return true - objects point to the same address.
	*/
	const virtual bool operator == (const InObject* object) const;

	/*
	* @brief Removes static data from the class.
	*/
	static void ClearStatic(void);

	/**
	* @brief Sets the object's name.
	* @param A string containing the new name of the object which should not
	* exceed 50 characters and can only have letters and/or numbers and/or whitespaces.
	* @return True - the new name is set; otherwise, returns false.
	*/
	const bool SetName(const std::string name);

	/**
	* @brief Returns the object's name.
	* @return Name of the object
	*/
	const std::string GetName(void) const;

	/**
	* @brief Returns the object's ID.
	* @return ID of the object
	*/
	const int GetID(void) const;
protected:

	///the object's name
	std::string  m_name;

	///the object's ID
	int m_id;
private:

	///a collection of all objects
	static std::vector<int> s_objects;

	///static variable used for adding unique IDs to objects
	static int s_id;

	///serialization
	friend class boost::serialization::access;
	template<class Archive>
	void save(Archive& ar, const unsigned int /*version*/) const {
		ar& SER_NVP(m_name);
		ar& SER_NVP(m_id);
	}

	template<class Archive>
	void load(Archive& ar, const unsigned int /*version*/) {
		ar& SER_NVP(m_name);
		ar& SER_NVP(m_id);
	}

	SER_SPLIT_MEMBER()
};

SER_CLASS_VERSION(InObject, 0)
SER_ABSTRACT(InObject)
SER_CLASS_EXPORT_KEY(InObject)

#endif INOBJECT_H
