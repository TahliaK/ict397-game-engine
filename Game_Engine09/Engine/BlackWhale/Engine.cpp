#include "Engine.h"
#include "Sound.h"
#include "Time.h"
#include "GraphicsEngine.h"
#include "Master.h"
#include "SceneManager.h"
#include "ShaderProgram.h"
#include "Label.h"
#include "Renderer.h"
#include "MeshRenderer.h"
#include "Terrain\TerrainHeightmap.h"
#include "Terrain\TerrainFaultFormation.h"
#include "ModelManager.h"
#include "Input\Input.h"
#include "Interfaces\LuaScript.h"
#include "Skybox.h"
#include "Water.h"
#include "AI\AIComponent.h"
#include "AI\AILuaScript.h"
#include "..\SDL_Sound.h"

#undef main
GameObject Engine::s_camera;
GameObject Engine::s_light;

void Engine::Initialize() {
	s_light.AddComponent(new Light);
	s_camera.AddComponent(new Camera);
	MeshRenderer mesh;
	Camera cam;
	Image d;
	Label g;
	TerrainHeightmap th;
	TerrainFaultFormation tf;
	Skybox box;
	LuaScript SD;
	Water water;
	AI_Messenger aim;
	AI_LuaScript als;
	SDL_Sound sdl_s;
	GraphicsEngine::Initialize();
}

void Engine::Start() {
	GraphicsEngine::Start();
	//Sound::Start();
	Time::Start();

	//OpenGL 4.5 and above only
	//glClipControl(GL_UPPER_LEFT,GL_NEGATIVE_ONE_TO_ONE);


	//Master::gl.Enable(GL_STENCIL_TEST);
	//glEnable(GL_FRAMEBUFFER_SRGB);//applying gamma correction


	//Has to be 3.3 or above
	//cout<<"GLSL Version: "<<glGetString(GL_SHADING_LANGUAGE_VERSION)<<ENDL;
	//cout<<"OpenGL Version: "<<glGetString(GL_VERSION)<<ENDL;

	Master::Start();

	SceneManagement::SceneManager::SetActive(SceneManagement::SceneManager::LoadScene("Level0"));
	if (!SceneManagement::SceneManager::GetActive())
		SceneManagement::SceneManager::CreateScene("Level0");
	SceneManagement::SceneManager::GetActive()->Start();
	Input::Initialize();
	UserInterface::SetUp();
}

void Engine::Clear() {
	SceneManagement::SceneManager::Clear();
	Component::ClearDerivedClasses();
	Input::Clear();
	InObject::ClearStatic();
	ModelManager::Clear();
	GraphicsEngine::Destroy();
}
void Engine::Update() {
	Master::gl.Clear(GRAPHICS_COLOR_BUFFER_BIT | GRAPHICS_DEPTH_BUFFER_BIT);
	Master::gl.ClearColor(1, 1, 0, 1);
	Time::UpdateTime(true);
	////	glGetError();//ignore errors from the previous frame
	//	Master::gl.Uniform1f(Master::gl.GetULoc(1,"fps"),Time::GetCurrent());
	//	
	//	Master::gl.Enable(GRAPHICS_CLIP_PLANE0);
	//
	//	Master::s_shadowFBO.BindBuffer(GRAPHICS_FRAMEBUFFER);
	//	Master::gl.Clear(GRAPHICS_DEPTH_BUFFER_BIT);
	//	//glUniform4fv(glGetUniformLocation(Master::s_3DShader.GetID(),"plane"),1,&planeRefraction[0]);
	//	UpdateActiveScene(true);
	//	Master::s_shadowFBO.UnbindBuffer(GRAPHICS_FRAMEBUFFER);
	//
	//	PrepareWaterReflection();
	//	PrepareWaterRefraction();
	//
	//	//glDisable(GL_CLIP_DISTANCE0);
	//	float planeStandard[] = {0,-1,0,-100000};
	//	Master::gl.Clear(GRAPHICS_COLOR_BUFFER_BIT | GRAPHICS_DEPTH_BUFFER_BIT);
	//	if(FORWARD_SHADING)
	//		Master::gl.Uniform4fv(Master::s_3DShader.GetLocation("plane"),1,&planeStandard[0]);
	//	else
	//		Master::gl.Uniform4fv(Master::s_GeomPassShader.GetLocation("plane"),1,&planeStandard[0]);
	SceneManagement::SceneManager::GetActive()->Update();
	Time::UpdateTime(false);
}

void Engine::ReshapeFunction(int w, int h) {
	if (h == 0)
		h = 1;
	Master::s_screen[2] = w;
	Master::s_screen[3] = h;
	float ratio = 1.0f * w / h;
	
	SceneManagement::SceneManager::GetActive()->GetMainCamera()->GetProjectionMatrix().Identity();
	Master::gl.Viewport(0, 0, w, h);
	SceneManagement::SceneManager::GetActive()->GetMainCamera()->GetProjectionMatrix() = Camera::Perspective(45, ratio, 0.01, 2500);
	SceneManagement::SceneManager::GetActive()->GetMainCamera()->ResetProjection();
	Master::Reshape();
	//SceneManagement::SceneManager::GetActive()->Reshape();
	GraphicsEngine::Reshape();
}

Camera* Engine::GetMainCamera() {
	return ((Camera*&)s_camera.GetComponents().at(1));
}

Light* Engine::GetMainLight() {
	return ((Light*)s_light.GetComponents().at(1));
}

void Engine::PrepareWaterRefraction() {
	float planeRefraction[] = { 0,-1,0,1 };
	Master::s_refractionFBO.BindBuffer(GRAPHICS_FRAMEBUFFER);
	Master::gl.ClearColor(1, 0, 0, 1);
	Master::gl.Clear(GRAPHICS_COLOR_BUFFER_BIT | GRAPHICS_DEPTH_BUFFER_BIT);

	Master::gl.Uniform4fv(Master::s_GeomPassShader.GetLocation("plane"), 1, &planeRefraction[0]);
	//UpdateActiveScene(true);
	Master::s_refractionFBO.UnbindBuffer(GRAPHICS_FRAMEBUFFER);
}
void Engine::PrepareWaterReflection() {
	//1 is the water level
	float distance = 2 * (SceneManagement::SceneManager::GetActive()->GetMainCamera()->GetGameObject()->GetTransform()->position.y - 1);
	float planeReflection[] = { 0,1,0,-1 };
	SceneManagement::SceneManager::GetActive()->GetMainCamera()->Move(Vector3(0, -distance, 0));
	SceneManagement::SceneManager::GetActive()->GetMainCamera()->InvertPitch();
	Master::s_reflectionFBO.BindBuffer(GRAPHICS_FRAMEBUFFER);
	Master::gl.ClearColor(1, 0, 0, 1);
	Master::gl.Clear(GRAPHICS_COLOR_BUFFER_BIT | GRAPHICS_DEPTH_BUFFER_BIT);
	Master::gl.Uniform4fv(Master::s_GeomPassShader.GetLocation("plane"), 1, &planeReflection[0]);
	//UpdateActiveScene(true);
	SceneManagement::SceneManager::GetActive()->GetMainCamera()->InvertPitch();
	SceneManagement::SceneManager::GetActive()->GetMainCamera()->Move(Vector3(0, distance, 0));
	Master::s_reflectionFBO.UnbindBuffer(GRAPHICS_FRAMEBUFFER);
}
