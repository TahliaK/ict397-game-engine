#ifndef COMPONENT_H
#define COMPONENT_H

#include "GameObject.h"
#include "InObject.h"

/*
* @class Component
* @brief Base class for all components - objects that serve as properties of game objects.
* Note that a component should belong to one and only game
* object and game objects can have one to many components.
* @author Olesia Kochergina
* @version 03
* @date 17/03/2017
*/
class Component : public InObject {
public:

	/*
	* @brief Default c-tor.
	*/
	Component();

	/*
	* @brief Copy c-tor.
	* @param component - the original component to be copied from.
	*/
	Component(const Component& component);

	/*
	* @brief Destructor c-tor.
	*/
	virtual ~Component();

	/*
	* @brief Initializes the object, called when creating the object.
	*/
	virtual void Initialize() {};

	/*
	* @brief Called just before the first update.
	*/
	virtual void Start() {};

	/*
	* @brief Updates the object and should be called every frame.
	*/
	virtual void Update() {};

	/*
	* @brief Clears internal data, should be called when
	* the object needs to be either reused or deleted.
	*/
	virtual void Clear() {};

	/*
	* @brief Returns the compoenent's game object.
	* @return the component's gameObject.
	*/
	GameObject* GetGameObject(void) const;

	/*
	* @brief Returns true if one game object can have more than one instances of this class.
	* @return true - possible, false otherwise.
	*/
	bool IsMultipleOccurrence(void) const;

	/*
	* @brief Sets the component's game object.
	*/
	void SetGameObject(GameObject* gameObject);

	/*
	* @brief Creates a new component of this class, should be overriden by children.
	* @return Pointer to the new component or NULL.
	*/
	virtual Component* NewComponent(void) const;

	/*
	* @brief Returns a vector of all functions that are used as setters in reflection.
	* @return vector of setter functions.
	*/
	const std::vector<boost::function<bool(boost::any)>> GetSetters(void) const;

	/*
	* @brief Returns a vector of all functions that are used as getters in reflection.
	* @return vector of getter functions.
	*/
	const std::vector<boost::function<boost::any(void)>> GetGetters(void) const;

	/*
	* @brief Returns a vector of all variable names that the class uses in reflection.
	* @return vector of strings
	*/
	const std::vector<std::string> GetVariables(void) const;

	/*
	* @brief Returns a vector of all classes that inherit the Component class' behaviour.
	* Note that the vector contains only one instance of each class and some classes might
	* not appear if no instances are created.
	*/
	static std::vector<Component*> GetDerivedClasses();

	/*
	* @brief Removes all derived classes of the component class that have been
	* created during the execution. A particular class may only occur once and these
	* objects that are removed are normally never used.
	*
	*/
	static void ClearDerivedClasses();
protected:

	///identifies whether a gameobject can have more than one instance of this Component.
	bool m_multiple;

	///simulation of reflection - used for dynamic creation of components
	///stores set methods
	std::vector<boost::function<bool(boost::any)>> m_Setters;

	///stores get methods
	std::vector<boost::function<boost::any(void)>> m_Getters;

	///stores names of particular variables
	std::vector<std::string> m_Variables;

	///The owner of this component
	GameObject* m_gameObject;

	///vector of all derived classes where only one instance of each class exists
	static std::vector<Component*> m_derivedClasses;
private:

	///serialization
	friend class boost::serialization::access;
	template<class Archive>
	void save(Archive& ar, const unsigned int /*version*/) const {
		ar & SER_BASE_OBJECT_NVP(InObject);
		ar& SER_NVP(m_gameObject);
		ar& SER_NVP(m_multiple);
	}
	template<class Archive>
	void load(Archive& ar, const unsigned int /*version*/) {
		ar & SER_BASE_OBJECT_NVP(InObject);
		ar& SER_NVP(m_gameObject);
		ar& SER_NVP(m_multiple);
	}
	SER_SPLIT_MEMBER()
};

SER_CLASS_VERSION(Component, 0);
SER_ABSTRACT(Component)
SER_CLASS_EXPORT_KEY(Component)
#endif COMPONENT_H
