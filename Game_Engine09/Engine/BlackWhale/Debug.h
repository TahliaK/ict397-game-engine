#ifndef DEBUG_H
#define DEBUG_H
/*
* @class DEBUG
* @brief Contains functions to debug the game engine
* @author Olesia Kochergina
* @date 03/03/2017
*/
class Debug{
public:
	/*
	* @brief Prints OpenGL errors.
	*/
	static void RunGeneral();

	/*
	* @brief Prints OpenGL frame buffer errors.
	* @return true - valid setup; else invalid
	*/
	static bool RunFramebuffer();
};
#endif DEBUG_H