#ifndef MESH_H
#define MESH_H
#include "Physics\ColliderAABB.h"
/**
* @class Mesh
* @brief Base class for all 3D models.
* @author Olesia Kochergina
* @version 01
* @date 10/03/2017
* last update: 10/05/2017 - min bounding box
*/
class Mesh{
public:

	/*
	* @brief Constructor.
	* @param shaderProgram - shader program for the mesh.
	*/
	Mesh(unsigned int shaderProgram) {
		m_shaderProgram = shaderProgram; 
		m_bounds = new ColliderAABB(Vector3(2000000), Vector3(-2000000));
	};

	/*
	* @brief Displays the mesh on the screen.
	*/
	virtual const void Draw() = 0;

	/*
	* @brief D-tor to deallocate the memory.
	*/
	~Mesh() {
		if(m_bounds)
			delete m_bounds;
	}

	/*
	* @brief Returns the minimum axis aligned bounding box.
	* @return Minimum AABB
	*/
	ColliderAABB* GetMinimumCollider(void) const;
protected:

	///minimum bounding box
	ColliderAABB* m_bounds;

	///the mesh's shader program
	unsigned int m_shaderProgram;
private:
	
	//colors, normals, uv,positions
};
#endif MESH_H