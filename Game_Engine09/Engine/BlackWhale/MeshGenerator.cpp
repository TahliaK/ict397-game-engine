#include "MeshGenerator.h"
#include "Label.h"
#include "Master.h"
#include "GraphicsEngine.h"
#include "Transform.h"
using namespace Text;
float MeshGenerator::m_lineHeight = 0.01f;
float MeshGenerator::m_spaceASCII = 32;

MeshGenerator::MeshGenerator(){}

MeshGenerator::~MeshGenerator() {

}

void MeshGenerator::Clear() {

}

MeshGenerator::MeshGenerator(string p_fileName){
	metaData = FNT_loader(p_fileName);
}

void MeshGenerator::CreateMesh(Label* text, MeshData& data){
	vector<Line> lines = CreateStructure(text);
	data = CreateQuadVertices(text,lines);
}

vector<Text::Line> MeshGenerator::CreateStructure(Label* text){
	vector<Line> lines;
	Line currentLine = Line(metaData.GetSpaceWidth(),text->GetFontSize(),text->GetMaxLineSize());
	Word currentWord = Word(text->GetFontSize());
	for(unsigned i = 0; i< text->GetText().size();i++){
		int ascii = (int) text->GetText().at(i);
		if(ascii == m_spaceASCII){
			if(!currentLine.AddWord(currentWord)){
				lines.push_back(currentLine);
				currentLine = Line(metaData.GetSpaceWidth(),text->GetFontSize(),text->GetMaxLineSize());
				currentLine.AddWord(currentWord);
			}
			currentWord = Word(text->GetFontSize());
			continue;
		}
		Character character = metaData.GetCharacter(ascii);
		currentWord.AddCharacter(character);
	}

	if(!currentLine.AddWord(currentWord)){
		lines.push_back(currentLine);
		currentLine = Line(metaData.GetSpaceWidth(),text->GetFontSize(), text->GetMaxLineSize());
		currentLine.AddWord(currentWord);
	}
	lines.push_back(currentLine);
	return lines;
}

MeshData MeshGenerator::CreateQuadVertices(Label* text, vector<Text::Line> lines){
	text->SetNumOfLines(lines.size());
	float curserX = (text->GetGameObject()->GetTransform()->position.x/((float)Master::s_screen[2]/2)) -1;
	float curserY = (((text->GetGameObject()->GetTransform()->position.y/((float)Master::s_screen[3]/2)) - 1) * -1);
	MeshData temp;

	for(unsigned i = 0; i < lines.size(); i++){
		for(unsigned j = 0; j < lines.at(i).GetLine().size(); j++){
			for(unsigned k = 0; k < lines.at(i).GetLine().at(j).GetWord().size(); k++){
				//if its a newline character
				if ((char)lines.at(i).GetLine().at(j).GetWord().at(k).id == '\\' && (char)lines.at(i).GetLine().at(j).GetWord().at(k + 1).id == 'n') {
					curserY -= m_lineHeight * text->GetFontSize();
					curserX = (text->GetGameObject()->GetTransform()->position.x / ((float)Master::s_screen[2] / 2)) - 1;;
					++k;
					continue;
				}
				AddVertV(curserX,curserY, lines.at(i).GetLine().at(j).GetWord().at(k),text->GetFontSize(),temp.vertex);
				AddVertT(lines.at(i).GetLine().at(j).GetWord().at(k).x, lines.at(i).GetLine().at(j).GetWord().at(k).y, lines.at(i).GetLine().at(j).GetWord().at(k).width, lines.at(i).GetLine().at(j).GetWord().at(k).height, temp.texture);
				curserX += lines.at(i).GetLine().at(j).GetWord().at(k).xAdvance * text->GetFontSize();
			}
			curserX += metaData.GetSpaceWidth() * text->GetFontSize();
		}
		curserX = 0;
		curserY -= m_lineHeight * text->GetFontSize();
	}
	temp.normal = vector<Vector3>(temp.vertex.size());
	return temp;
}

void MeshGenerator::AddVertV(float curserX, float curserY, const Character& character, float fontSize, vector<Vector3>& vert){
	float x = (curserX) + (character.xOffset * fontSize);
	float y = (curserY) + (character.yOffset );
	float maxX = x + (character.xSize * fontSize);
	float maxY = y + (character.ySize * fontSize);
	//float properX = (2 * x) - 1;
	//float properY = (-2 * y) + 1;
	//float properMaxX = (2 * maxX) - 1;
	//float properMaxY = (-2 * maxY) + 1;
	//float temp = properY;
	//properY = properMaxY;
	//properMaxY = temp;
	////cout<<"Proper X AND Y: "<<	properX<< " "<<properY<<" "<<properMaxX<<" "<<properMaxY<<endl;
	////GraphicsEngine::AddPolygon(properX,properY,properMaxX,properMaxY,vert,false);
	GraphicsEngine::AddPolygon(x,y,maxX,maxY,vert,false);
}

void MeshGenerator::AddVertT(float x, float y, float maxX, float maxY, vector<Vector3>& vert){
	GraphicsEngine::AddPolygon(x,maxY,maxX,y,vert,false);//Vertically flipped by inversing maxY and y
}