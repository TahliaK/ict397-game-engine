/**
* @class Quadtree
* @brief Divide an object into four even squares which are then
* divided into four more evenly sized quads. Each quad is linked to
* its four children.
*/
#ifndef QUADTREE_H
#define QUADTREE_H
#include "Struct.h"
#include "Master.h"
#include "Physics\LinkedList.h"
#include "Physics\ColliderAABB.h"
using namespace DataStructures;
namespace DataStructures {
	template <class T> class Quadtree {
	public:
		Quadtree();
		Quadtree(ColliderAABB range);
		Quadtree(Quadtree<T>& original);
		~Quadtree();

		bool Insert(const T& data, Node<T>*& node, bool force = false);

		/**
		* @brief Create four children that divide this quad into four quads of equal area
		*
		*/
		void Subdivide();
		vector<ColliderAABB*> QueryRange(const ColliderAABB& range);
		void SetAABB(const ColliderAABB& m_boundary);
		const ColliderAABB& GetAABB();
		void Quadtree<T>::DrawAABB(const ColliderAABB& box);
		bool Delete(Node<T>*& node);
		void Clear();
	private:
		void SetParent(Quadtree* tree);

		///max size of colliders within one quadtree
		const static int CAPACITY = 3;

		///north west, north east, south west, south east
		Quadtree* m_children[4];

		///The parent node of the quad tree
		Quadtree* m_parent;

		///the current boundary
		ColliderAABB m_boundary;

		///
		bool m_hasChildren;

		///list of colliders
		LinkedList<T> m_points;
		///current amount of points
		int m_size;
	};
};

template <class T> Quadtree<T>::Quadtree() {
	//cout << "default ctor" << endl;
	for (int i = 0; i < 4; i++) {
		m_children[i] = NULL;
	}
	m_hasChildren = false;
	m_parent = NULL;
	m_size = 0;
}

template <class T> Quadtree<T>::Quadtree(ColliderAABB range) {
	//cout << "aabb ctor" << endl;
	for (int i = 0; i < 4; i++) {
		m_children[i] = NULL;
	}
	m_parent = NULL;
	m_size = 0;
	m_boundary = range;

	m_hasChildren = false;
}

template <class T> Quadtree<T>::Quadtree(Quadtree<T>& original) {
	//cout << "copy ctor" << endl;
}

template <class T> Quadtree<T>::~Quadtree() {
	Clear();
}

template <class T> void Quadtree<T>::Clear() {
	if (m_hasChildren) {
		for (int i = 0; i < 4; i++) {
			if (m_children[i]) {
				m_size = 0;
				m_points.Clear();
				m_children[i]->Clear();
				delete m_children[i];
				m_children[i] = NULL;
			}
		}
	}
	m_hasChildren = false;
}

template <class T> void Quadtree<T>::SetAABB(const ColliderAABB& m_boundary) {
	this->m_boundary = m_boundary;
}

template <class T> const ColliderAABB& Quadtree<T>::GetAABB() {
	return m_boundary;
}

template <class T> void Quadtree<T>::Subdivide() {
	ColliderAABB m_boundary = this->m_boundary;
	ColliderAABB ranges[] = {
		ColliderAABB(Vector3(m_boundary.Center().x, m_boundary.GetMin().y, m_boundary.Center().z), Vector3(m_boundary.GetMax().x,m_boundary.GetMax().y,m_boundary.GetMax().z)),
		ColliderAABB(Vector3(m_boundary.GetMin().x, m_boundary.GetMin().y, m_boundary.Center().z), Vector3(m_boundary.Center().x,m_boundary.GetMax().y,m_boundary.GetMax().z)),
		ColliderAABB(Vector3(m_boundary.Center().x, m_boundary.GetMin().y, m_boundary.GetMin().z), Vector3(m_boundary.GetMax().x,m_boundary.GetMax().y,m_boundary.Center().z)),
		ColliderAABB(Vector3(m_boundary.GetMin().x, m_boundary.GetMin().y, m_boundary.GetMin().z), Vector3(m_boundary.Center().x,m_boundary.GetMax().y,m_boundary.Center().z)) };
	m_hasChildren = true;
	for (int i = 0; i < 4; i++) {
		m_children[i] = new Quadtree(ranges[i]);
		m_children[i]->SetParent(this);
	}
}

template <class T> void Quadtree<T>::DrawAABB(const ColliderAABB& box) {

	/*	float p1[] = { box.min[0],box.max[1],box.min[2] };
		float p2[] = { box.max[0],box.min[1],box.min[2] };
		float p3[] = { box.max[0],box.max[1],box.max[2] };
		float p4[] = { box.min[0],box.min[1],box.max[2] };

		float p5[] = { box.min[0],box.max[1],box.max[2] };
		float p6[] = { box.min[0],box.min[1],box.min[2] };
		float p7[] = { box.max[0],box.min[1],box.max[2] };
		float p8[] = { box.max[0],box.max[1],box.min[2] };

		float *point1[] = { p1,p2,p3,p4 };
		float *point2[] = { p5,p6,p7,p8 };
		glDisable(GL_TEXTURE_2D);
		if (VAO == 0)
		{
			//	float quadVertices[] = {
			//		// Positions        // Texture Coords
			//		-1.0f, 1.0f, 0.0f, 0.0f, 1.0f,
			//		-1.0f, -1.0f, 0.0f, 0.0f, 0.0f,
			//		1.0f, 1.0f, 0.0f, 1.0f, 1.0f,
			//		1.0f, -1.0f, 0.0f, 1.0f, 0.0f,
			//	};
			float  quad[24];
			for (int i = 0, j = 0; j < 4, i < 24; i += 6, j++) {
				for (int k = 0; k < 6; k += 2) {
					quad[i + k] = point1[j][k / 2];
					quad[i + k + 1] = point2[j][k / 2];
				}
				//for (int j = 0; j<4; j++) {
				//for (int k = 0; k<3; k++)
				//cout << point1[i][k] << " " << point2[j][k] << endl;
				//glBegin(GL_LINES);
				//glColor3f(1, 0, 0);
				//glVertex3fv(point1[i]);
				//glColor3f(0, 1, 0);
				//glVertex3fv(point2[j]);
				//glEnd();
				//}
			}
			// Setup plane VAO
			Master::gl.GenVAO(1, &VAO);
			Master::gl.GenBuffer(1, &VBO);
			Master::gl.BindVAO(VAO);
			Master::gl.BindBuffer(GRAPHICS_ARRAY_BUFFER, VBO);
			Master::gl.BufferData(GRAPHICS_ARRAY_BUFFER, sizeof(quad), &quad, GRAPHICS_STATIC_DRAW);

			Master::gl.EnableVertexAttribArray(0);
			Master::gl.VertexAttribPointer(0, 3, GRAPHICS_FLOAT, GRAPHICS_FALSE, 24 * sizeof(float), (void*)0);
		}
		Master::gl.BindVAO(VAO);
		Master::gl.DrawArrays(GRAPHICS_LINES, 0, 8);
		Master::gl.BindVAO(0);
		glColor4f(1, 1, 1, 1);*/
}

template <class T> vector<ColliderAABB*> Quadtree<T>::QueryRange(const ColliderAABB& range) {
	vector<T> temp;
	if (!m_boundary.Intersect(&range)) {
		return temp;
	}
	//check this quad level
	for (int i = 0; i < m_size; i++) {
		if (range.Intersect(m_points.GetAllData()[i]) || range.Contains(m_points.GetAllData()[i])) {
			if (typeid(T) == typeid(ColliderAABB*)) {
				if (((ColliderAABB*)m_points.GetAllData()[i])->GetMin() == ((ColliderAABB*)m_points.GetAllData()[i])->GetMax())
					continue;
			}
			temp.push_back(m_points.GetAllData()[i]);
		}
	}

	//if has children
	if (m_children[0]) {
		for (int i = 0; i < 4; i++) {
			vector<T> childVector = m_children[i]->QueryRange(range);
			temp.insert(temp.end(), childVector.begin(), childVector.end());
		}
	}
	return temp;
}

template <class T> void Quadtree<T>::SetParent(Quadtree* tree) {
	m_parent = tree;
}
template <class T> bool Quadtree<T>::Delete(Node<T>*& p_node) {
	if (m_points.Delete(p_node)) {
		m_size--;
		return true;
	}
	else {
		if (m_hasChildren) {
			for (int i = 0; i < 4; i++) {
				if (m_children[i]) {
					if (m_children[i]->Delete(p_node))
						return true;
				}
			}
		}
	}
	return false;
}

template <class T> bool Quadtree<T>::Insert(const T& data, Node<T>*& p_node, bool force) {
	if (!m_boundary.Contains(data) && !force) {
		return false;
	}
	if (m_hasChildren) {
		for (int j = 0; j < 4; j++)
		{
			if (m_children[j]->Insert(data, p_node,force)) {
				return true;
			}
		}
		//if does not belong to any children then multiple nodes 25/05/2017
		//forces an insert
		//added to one child it might cause errors because it won't detect collisions in other trees.
		for (int j = 0; j < 4; j++)
		{
			if (m_children[j]->Insert(data, p_node,true)) {
				return true;
			}
		}
	}
	else {
		if (m_size < CAPACITY) {
			p_node = m_points.Insert(data);
			m_size++;
			return true;
		}
		else {
			if (m_children[0] == NULL) {
				Subdivide();
			}
			for (int j = 0; j < m_points.GetAllData().size(); j++) {
				for (int i = 0; i < m_size; i++) {
					if (m_children[i]) {
						if (m_children[i]->Insert(m_points.GetAllData().at(i), p_node)) {
							return true;
						}
					}
				}
			}

		}
	}
	return false;
}

#endif QUADTREE_H