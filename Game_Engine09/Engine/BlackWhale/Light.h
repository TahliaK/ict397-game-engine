#ifndef LIGHT_H
#define LIGHT_H
#include "Component.h"
#include "Struct.h"
#include "Image.h"
#include "Material_s.h"

/*
* @class Light
* @brief Contains two types of light: point and directional; used for displaying light sources in the game world.
* Note: the class should divided into four classes: base, directional, point, spot.
* @version 02
* @date 18/04/2017
* @author Olesia Kochergina
*/
class Light : public Component {

public:

	/*
	* @brief Default c-tor.
	*/
	Light();

	/*
	* @brief Copy c-tor.
	*/
	Light(const Light& original);

	/*
	* @brief Destructor.
	*/
	~Light() {};

	/*
	* @brief Initializes the object, called when creating the object.
	*/
	void Initialize();

	/*
	* @brief Called just before the first update.
	*/
	void Start();

	/*
	* @brief Updates the object and should be called every frame.
	*/
	void Update();

	/*
	* @brief Clears internal data, should be called when
	* the object needs to be either reused or deleted.
	*/
	void Clear();

	/*
	* @brief Creates a new component of this class.
	* @return Pointer to the new component or NULL.
	*/
	Component* NewComponent(void) const;

	/*
	* @brief Sets the radius of the light source.
	* @param radius - float variable.
	* @return true - if set.
	*/
	bool SetRadius(boost::any radius);

	/*
	* @brief Sets the type of the light source.
	* @param type - int variable, where 0 - point, 1 - directional.
	* @return true - if set.
	*/
	bool SetLightType(boost::any type);

	/*
	* @brief Sets the color of the light source.
	* @param color - RGBA variable.
	* @return true - if set.
	*/
	bool SetColor(boost::any color);

	/*
	* @brief Returns the radius of the light source.
	* @return radius
	*/
	float GetRadius(void) const;

	/*
	* @brief Returns the radius of the light source.
	* @return type - 0 is for point, 1 is for directional
	*/
	int GetLightType(void) const;

	/*
	* @brief Returns the color of the light source.
	* @return color
	*/
	RGBA GetColor(void) const;

	/*
	* @brief Sends all light sources within a scene to a light shader, should be called every frame.
	*/
	static void UpdateLight();
private:

	/*
	* @brief Adds values to three vectors: variable name,  setter, getter.
	* Used in the Editor to modify components at run-time.
	*/
	void Reflection(void);

	///the ambient light color
	RGBA m_ambientColor;

	///radius of the light source
	float m_radius;

	///type of the light: 0 - point, 1 - directional 
	int m_type;

	///stores positions of all lights within one scene.
	static std::vector<Vector3> positions;

	///stores colors of all lights within one scene.
	static std::vector<Vector3> colors;

	///stores types of all lights within one scene.
	static std::vector<int> lightType;

	///stores radiuses of all lights within one scene.
	static std::vector<float> radius;

	///stores an image of a light source for debugging
	static Image m_light_picture;

	///id of the shader
	static unsigned int m_shaderProgram;

	///name of the vertex shader
	static std::string m_vS;

	///name of teh fragment shader
	static std::string  m_fS;


	///serialization
	friend class boost::serialization::access;
	template<class Archive>
	void save(Archive& ar, const unsigned int /*version*/) const {
		ar & SER_BASE_OBJECT_NVP(Component);
		//ar& m_light_picture;
		ar& SER_NVP(m_ambientColor);
		ar& SER_NVP(m_type);
		ar& SER_NVP(m_radius);
		/*ar & SER_NVP(m_fS);
		ar & SER_NVP(m_vS);*/
		//ar& m_direction;
		//ar& cutOff;
	}

	template<class Archive>
	void load(Archive& ar, const unsigned int /*version*/) {
		ar & SER_BASE_OBJECT_NVP(Component);
		//ar& boost::serialization::base_object<GameObject>(*this);
		//ar& m_light_picture;
		ar& SER_NVP(m_ambientColor);
		ar& SER_NVP(m_type);
		ar& SER_NVP(m_radius);
		/*ar & SER_NVP(m_fS);
		ar & SER_NVP(m_vS);*/
		//ar& m_direction;
		//ar& cutOff;
	}

	SER_SPLIT_MEMBER()
};
SER_CLASS_VERSION(Light, 0);
SER_CLASS_EXPORT_KEY(Light)
#endif LIGHT_H
