#ifndef MESHGENERATOR_H
#define MESHGENERATOR_H
#include "TextProcessing.h"
#include "Struct.h"
#include "FNT_loader.h"
#include "Character.h"
#include <iostream>
using namespace  std;
class Label;
struct Line;
namespace Text{
class MeshGenerator{
public:
	static float m_lineHeight;
	static float m_spaceASCII;

	MeshGenerator();
	MeshGenerator(string fileName);
	~MeshGenerator();
	void CreateMesh(Label* text, MeshData& data);
	void Clear();
private:
	FNT_loader metaData;
	vector<Line> CreateStructure(Label* text);

	MeshData CreateQuadVertices(Label* text, vector<Line> lines);

	void AddVertV(float curserX, float curserY, const Character& character, float fontSize, vector<Vector3>& vert);

	void AddVertT(float x, float y, float maxX, float maxY, vector<Vector3>& vert);
};
}
#endif MESHGENERATOR_H