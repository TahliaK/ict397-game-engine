#ifndef LUASCRIPT_H
#define LUASCRIPT_H
#include "Script.h"
extern "C" {
#include <lua.h>
#include <lualib.h>
#include <luaconf.h>
#include <lauxlib.h>
}

/**@class LuaScript
*@brief a lua-specific script class
*@author Tahlia Kowald
*@version 4.0
*@date 1/6/17
*/
class LuaScript: public Script{
public:
	LuaScript();
	/**@brief sets the script filename
	*@param fileName = the script's filename
	*@return true if filename is valid
	*@note this doesn't validate the file, just the string
	*/
	bool SetFileName(boost::any fileName);
	
	/**@brief gets the script's filename
	*@returns the filename
	*/
	std::string GetFileName(void);
	
	//these are inherited
	void Initialize();
	void Start();
	void Update();
	void  Clear();
	Component* NewComponent(void) const;

	/**@brief Registers a C function with the given script
	*@param f the C function
	*@param funcName the name of the function in Lua
	*@note this only works before Start() has been called for the file,
	* otherwise it has undefined behaviour
	*/
	void RegisterFunction(lua_CFunction f, std::string funcName);

private:

	static int Multiply(int p_1, int p_2);

	static int cpp_Multiply(lua_State* p_state);
	void RegisterCppFuncs();

	static bool s_inList;
	void  Reflection();
	lua_State* m_state;
	std::string m_fileName;
	std::map<std::string, lua_CFunction> toRegister;

	//Tahlia additions 14/5/17
	bool updatingScript;
	/* This runs a predefined function from the lua script that is called functionName
	It is confirmed safe if the function does not exist or if the function runs and terminates
	in an error. */
	int runBasicFunction(lua_State * p_state, std::string functionName);
};
#endif LUASCRIPT_H