/**
* @class SDL header file
* @brief 
* @author Olesia Kochergina
* @version 01
* @date 09/03/2017
*/

#ifndef DIRECTMEDIALAYER_H
#define DIRECTMEDIALAYER_H
#include <SDL.h>
#include <string>

namespace ThirdParty {
	struct RGB_pixel {
		float r, g, b;
		RGB_pixel() {
			r = g = b = 1;
		}
	};

	struct Image {
	public:
		bool invert;
		unsigned int bytesPerPixel;
		unsigned int width;
		unsigned int height;
		void* pixels;

		Image() {
			invert = false;
			//RGB
			bytesPerPixel = 3;
			width = 0;
			height = 0;
			pixels = NULL;
		}
		~Image() {}
	};

	class DirectMediaLayer {
	public:
		static void Initialize();
		static void Destroy();
		DirectMediaLayer();
		~DirectMediaLayer();

		/*
		* @brief Loads an image specified by the file name.
		* @param fileName - the directory + the name of the image
		* @param image - reference to the image. The m_invert value needs to be set before calling the function 
		* @return true - if the image is valid else false
		*/
		const bool LoadImage2D(std::string fileName, Image& image);
		const void Clear();
		const bool GetRGB(int x,int y, RGB_pixel& rgb);
	private:
		bool InvertSurface();
		bool InvertImage(int pitch, int height, void* pixels);

		Uint32 GetPixel(int x, int y);

		///SDL surface which is used to load and unload 2D images
		SDL_Surface* m_curSurface;
	};
};

#endif DIRECTMEDIALAYER_H