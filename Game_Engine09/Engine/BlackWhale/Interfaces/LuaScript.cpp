#include "LuaScript.h"
#include "..\..\LuaScriptFunctions.h"
#define FUNCTIONCALL_ERROR_HANDLED -1
#define FUNCTIONCALL_ERROR_UNHANDLED -2
#define FUNCTIONCALL_NOFUNCTION -3

bool LuaScript::s_inList = false;

LuaScript::LuaScript() : toRegister() {
	Reflection();
	Initialize();
}

void LuaScript::Initialize()
{
	m_multiple = true;
	m_name = "Lua Script";
	m_fileName = "Lua/";
	m_state = nullptr;
	updatingScript = true;

	if (!s_inList) {
		s_inList = true;
		LuaScript* l_pointer = new LuaScript;
		m_derivedClasses.push_back(l_pointer);
	}
}

Component*  LuaScript::NewComponent() const {
	Component* l_pointer = new LuaScript;
	return l_pointer;
}

bool LuaScript::SetFileName(boost::any var) {
	try {
		m_fileName = boost::any_cast<string> (var);
		Clear();
		Start();
		updatingScript = true; //prevents disabling of updating while typing in script name
	}
	catch (boost::bad_any_cast& /*bac*/) {
		return false;
	}
	return true;
}

std::string LuaScript::GetFileName() {
	return m_fileName;
}

void LuaScript::Start() {
	m_state = luaL_newstate();
	if (!m_state) 
	{
		std::cout << "[ERROR]: " << lua_tostring(m_state, -1) << endl;;
		m_state = nullptr;
	}
	else
	{
		luaL_openlibs(m_state);

		RegisterCppFuncs();

		if (m_fileName.compare("") != 0) {
			if (luaL_dofile(m_state, m_fileName.c_str()))				cout << "[ERROR]: " << lua_tostring(m_state, -1) << endl;
		}

		runBasicFunction(m_state, "start");
	}
}

int LuaScript::Multiply(int p_1, int p_2) {
	return p_1*p_2;
}

int LuaScript::cpp_Multiply(lua_State* p_state) {
	int l_paramAmount = lua_gettop(p_state);
	if (l_paramAmount < 2 || !lua_isnumber(p_state, -1) || !lua_isnumber(p_state, -2)) {
		cout << "[ERROR] invalid parameters" << endl;
		return 0;
	}
	int result = Multiply((int)lua_tonumber(p_state, -1), (int)lua_tonumber(p_state, -2));
	lua_pushnumber(p_state, result);
	return 1;
}

void LuaScript::Update(){ //confirmed - runs every frame
	if(updatingScript)
	{
		lua_settop(m_state, 0);
		int functionCall = runBasicFunction(m_state, "update");

		//if unhandled error or function doesn't exist, ceases to call update
		if(functionCall == FUNCTIONCALL_ERROR_UNHANDLED || 
			functionCall == FUNCTIONCALL_NOFUNCTION)
		{
			updatingScript = false;
		}
	}
}

void LuaScript::Clear() {
	if(m_state != nullptr)
	{
		runBasicFunction(m_state, "clear");
		lua_close(m_state);
		m_state = nullptr;
	}
}

void LuaScript::Reflection() {
	m_Variables.push_back("Script file");
	m_Setters.push_back(boost::bind(&LuaScript::SetFileName, this, _1));
	m_Getters.push_back(boost::bind(&LuaScript::GetFileName, this));
}

void LuaScript::RegisterFunction(lua_CFunction f, std::string funcName)
{
	toRegister.insert(std::pair<std::string, lua_CFunction>(funcName, f));
}

void LuaScript::RegisterCppFuncs()
{
	lua_register(m_state, "cpp_Multiply", cpp_Multiply);
	//lua_register(m_state, "GameObject_Create", LuaScriptFunctions::make3DObject);
	lua_register(m_state, "GameObject_Load3DModel", LuaScriptFunctions::load3DModel);
	lua_register(m_state, "GameObject_SetCollisions", LuaScriptFunctions::set3DCollisions);
	lua_register(m_state, "GameObject_SetFeature", LuaScriptFunctions::setObjectFeature);
	lua_register(m_state, "GameObject_Move", LuaScriptFunctions::moveObject);
	lua_register(m_state, "GameObject_Destroy", LuaScriptFunctions::destroy3Dobject);

	lua_register(m_state, "GameObjects_AreColliding", LuaScriptFunctions::areObjectsColliding);
	
	//camera controls
	//I've left out the camera creation & editing because I'm not sure whether should be in right now
	lua_register(m_state, "Camera_Move", LuaScriptFunctions::moveCamera);
	lua_register(m_state, "Camera_MovementWillCollide_GameObject", LuaScriptFunctions::movementWillCollide);
	lua_register(m_state, "Camera_MovementWillCollide_Scene", LuaScriptFunctions::movementWillCollideScene);

	//user input controls
	lua_register(m_state, "UserInput_MouseMovementVector", LuaScriptFunctions::mouseChangeVec);
	lua_register(m_state, "UserInput_MouseLocation", LuaScriptFunctions::mouseLocation);
	lua_register(m_state, "UserInput_KeyPressed", LuaScriptFunctions::keyPressed);

	//scene controls
	//I left new scene alone, figure it wasn't actually a scripting feature
	lua_register(m_state, "Scene_Load", LuaScriptFunctions::loadScene);
	lua_register(m_state, "Scene_SwapTo", LuaScriptFunctions::swapSceneTo);

	//Custom loader
	for(auto const &key : toRegister)
	{
		lua_register(m_state, key.first.c_str(), key.second);
	}
}

int LuaScript::runBasicFunction(lua_State * p_state, std::string functionName){
	lua_getglobal(p_state, functionName.c_str());
	int success = 0;
	if(lua_isfunction(p_state, -1))
	{
		int error = lua_pcall(m_state, 0, 0, 0);
		if(error != 0)
		{
			cout << "[ERROR]: " << m_fileName << " Function call:";
			switch(error)
			{
			case LUA_ERRRUN:
				cout << " Runtime error." << endl;
				success = FUNCTIONCALL_ERROR_HANDLED;
				cerr << lua_tostring(p_state, -1) << endl; //print error info
				break;
			case LUA_ERRMEM:
				cout << " Memory allocation error." << endl;
				success = FUNCTIONCALL_ERROR_UNHANDLED;
				cerr << lua_tostring(p_state, -1) << endl;
				break;
			case LUA_ERRERR:
				cout << " Runtime error + error handler error." << endl;
				success = FUNCTIONCALL_ERROR_UNHANDLED;
				cerr << lua_tostring(p_state, -1) << endl;
				break;
			}
		}
	}
	else 
	{
		cout << "[SCRIPT] " << m_fileName << " no " << functionName << " function found." << endl;
		success = FUNCTIONCALL_NOFUNCTION;
	}

	return success;
}