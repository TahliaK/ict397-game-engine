/*
* @author Olesia Kochergina
* @version 01
* @date 17/03/2017
*/
#ifndef COLLIDER_H
#define COLLIDER_H
#include "../Component.h"
#include "../Math_s.h"
#include "../Transform.h"

/*
* @class Collider
* @brief Base class for all colliders
* @version 01
* @date 03/04/2017
* @author Olesia Kochergina
*/
class Collider: public Component {
public:

	/*
	* @brief Default c-tor.
	*/
	Collider();

	/*
	* @brief Copy c-tor.
	*/
	Collider(const Collider& original);

	/*
	* @brief Destructor.
	*/
	~Collider();
	
	/**
	* @brief The method is called when this object is being created.
	*/
	virtual void Initialize();

	/**
	* @brief The method is called when the script
	* is enabled just before the Update method.
	*/
	virtual void Start();

	/**
	* @brief The method is called every frame.
	*/
	virtual void Update();

	/**
	* @brief The method is called before destroying the script.
	*/
	virtual void Clear();

	/*
	* @brief Creates a new component of this class.
	* @return Pointer to the new component or NULL.
	*/
	virtual Component* NewComponent(void) const = 0;

	/*
	* @brief Sets the minimum bounding box.
	* @param pointer to the min collider.
	*/
	virtual void SetMinAABB(Collider* box);

	/*
	* @brief Returns the minimum collider.
	* @return pointer to the min collider.
	*/
	virtual const Collider* GetMinAABB();
	
	/*
	* @brief Assigns the parameter's value to this collider.
	* NOT IMPLEMENTED
	* @param original - the info to be assigned to this collider.
	* @return pointer to this collider
	*/
	virtual Collider* operator = (const Collider* original);

	/*
	* @brief Compares this collider with the parameter's value.
	* @return true - colliders point to the same memory location.
	*/
	virtual bool operator == (const Collider* original);

	//static void SetTreeAABB(AABB gridAABB);
	
	/*
	* @brief Returns whether this collider is colliding with something at the moment or not.
	* @return true - colliding
	*/
	bool IsColliding(void) const;

	/*
	* @brief Resolves collisions by moving the game object of this component.
	*/
	void Response(void);
protected:

	///for collision resolution
	Vector3 m_movement;

	///previous transform
	Transform m_pTransform;
	
	///true - colliding with some object
	bool m_colliding;
	
	///ColliderAABB which stores the minimum bounding box
	//Collider* m_minBox;
private:
	
	/*
	* @brief Adds values to three vectors: variable name,  setter, getter.
	* Used in the Editor to modify components at run-time.
	*/
	void Reflection();

	///serialization
	friend class boost::serialization::access;
	template<class Archive>
	void save(Archive& ar, const unsigned int /*version*/) const {
		ar & SER_BASE_OBJECT_NVP(Component);
	}
	template<class Archive>
	void load(Archive& ar, const unsigned int /*version*/) {
		ar & SER_BASE_OBJECT_NVP(Component);
	}
	SER_SPLIT_MEMBER()
};

SER_CLASS_VERSION(Collider, 0);
SER_ABSTRACT(Collider)
SER_CLASS_EXPORT_KEY(Collider)
#endif COLLIDER_H