#ifndef PHYSICSENGINE_H
#define PHYSICSENGINE_H
#include "Collider.h"
class PhysicsEngine {
	static bool Overlap(const Collider* fCollider, const Collider* sCollider);
	static bool Overlap(const std::vector<Collider*> colliders, const Collider* mainCollider);
	static bool Overlap(const Collider* collider, const Vector3 point);
	static void ScreenPointTo3D(const Vector2 point2, Vector3& point3);

};
#endif PHYSICSENGINE_H