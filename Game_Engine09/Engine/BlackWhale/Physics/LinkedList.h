/*
* @brief
* @author Olesia Kochergina
* @date 13/03/2017
*
*/
#ifndef LINKEDLIST_H
#define LINKEDLIST_H

namespace DataStructures {
	/// <summary>
	/// The class is used in the linked list class to implement the linked
	/// list data type.
	/// </summary>
	/// <typeparam name="T">Type of each node.</typeparam>

	template <class T> class Node {
	public:
		/// Data to be stored in the node.
		T m_data;

		/// Reference to the next node.
		Node<T>* m_next;

		/*
		* @brief Constructor to initialize a node.
		* @param data - The node's value.
		* @param next - Reference to the next node.
		*/
		Node<T>(const T& p_data, Node<T>* p_next) {
			m_data = p_data;
			m_next = p_next;
		}

		/*
		* @brief The destructor
		*/
		~Node() {}

		/*
		* @brief Default ctor which sets the data members to null.
		*/
		Node<T>() {
			m_next = NULL;
			//m_data = NULL;
		}
	};
	template <class T> class LinkedList{
	private:
		/// The root node of the linked list.
		Node<T>* m_root;
	public:

		/*
		* @brief Default ctor.
		*/
		LinkedList<T>() {
			m_root = new Node<T>;
		}

		~LinkedList<T>() {
			if(m_root)
				delete m_root;
		}

		/*
		* @brief Inserts a new value into the linked list.
		* @param p_data - The value of type T.
		*/
		Node<T>*& Insert(const T& p_data) {
			Node<T>* l_node = new Node<T>(p_data, m_root->m_next);
			m_root->m_next = l_node;
			return m_root->m_next;
		}

		/*
		* @brief Deletes the first item from the linked list.
		* @return The value of the deleted item.
		*/
		T Delete() {
			Node<T>* l_node = m_root->m_next;
			m_root->m_next = l_node->m_next;
			T l_data = l_node->m_data;
			delete l_node;
			return l_data;
		}

		void Clear() {
			Node<T>* l_node = m_root->m_next;
			while (l_node)
			{
				m_root->m_next = l_node->m_next;
				delete l_node;
			}
		}

		/*
		* @brief Deletes the first occurence of the value specified by the parameter.
		* @param p_data - The value of an item to be deleted.
		* @return Returns true if the value is found and deleted.
		*/
		bool Delete(const T& p_data) {
			Node<T>* l_node = m_root->m_next;
			Node<T>* l_prev = m_root;
			while (l_node)
			{
				if (l_node->m_data == (p_data))
				{
					l_prev->m_next = l_node->m_next;
					delete l_node;
					return true;
				}
				l_prev = l_node;
				l_node = l_node->m_next;
			}
			return false;
		}

		/*
		* @brief Deletes the first occurence of the node specified by the parameter.
		* @param p_node - The item to be deleted.
		* @return Returns true if the item is found and deleted.
		*/
		bool Delete(Node<T>*& p_node) {
			Node<T>* l_node = m_root->m_next;
			Node<T>* l_prev = m_root;
			while (l_node){
				if (l_node == p_node)
				{
					l_prev->m_next = l_node->m_next;
					delete l_node;
					return true;
				}
				l_prev = l_node;
				l_node = l_node->m_next;
			}
			return false;
		}

		/*
		* @brief Returns a vector of all data stored in the linked list.
		* @return The vector with the data.
		*/
		std::vector<T> GetAllData() {
			std::vector<T> l_data;
			Node<T>* l_node = m_root->m_next;
			while (l_node)
			{
				l_data.push_back(l_node->m_data);
				l_node = l_node->m_next;
			}
			return l_data;
		}

		/*
		* @brief Prints out all items in the linked list.
		*/
		void Display() {
			Node<T>* l_node = m_root->m_next;
			while (l_node)
			{
				cout << (l_node->m_data + " ") << end;
				l_node = l_node->m_next;
			}
			cout << endl;
		}

		/*
		* @brief Finds an item specified by its value.
		* @param p_data - The value of the item to be found.
		* @return Returns true if the item is in the linked list.
		*/
		bool Search(const T& p_data) {
			Node<T>* l_node = m_root->m_next;
			while (l_node)
			{
				if (l_node->m_data == (p_data))
				{
					return true;
				}
				l_node = l_node->m_next;
			}
			return false;
		}
	};
};
#endif LINKEDLIST_H