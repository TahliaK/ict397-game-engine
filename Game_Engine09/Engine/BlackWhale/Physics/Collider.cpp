#include "Collider.h"
#include "ColliderAABB.h"
BOOST_CLASS_EXPORT_IMPLEMENT(Collider)
Collider::Collider():Component() {
	//Reflection();
	Initialize();
}

Collider::Collider(const Collider& original): Component(original){
	//Reflection();
	Initialize();
	m_pTransform = original.m_pTransform;
}

void Collider::Initialize() {
	m_name = "Collider";
	m_colliding = false;
//	m_minBox = NULL;
}

void Collider::Clear() {
	/*if (m_minBox) {
		delete m_minBox;
		m_minBox = NULL;
	}*/
}

Collider::~Collider() {
	Clear();
}

void Collider::Start() {

}

void Collider::Update() {
	/*if (!m_gameObject->GetStatic()) {
		Vector3 l_change = m_gameObject->GetTransform()->position - m_pTransform.position;
		if (l_change.x != 0 || l_change.y != 0 || l_change.z != 0) {
			if (m_LLPtr) {
				m_tree.Delete(m_LLPtr);
				m_LLPtr = NULL;
			}

			Vector3 curMin = ((ColliderAABB*)m_minBox)->GetMin();
			Vector3 curMax = ((ColliderAABB*)m_minBox)->GetMax();
			((ColliderAABB*)m_minBox)->SetMin(curMin + l_change);
			((ColliderAABB*)m_minBox)->SetMax(curMax + l_change);
			AABB l_box = AABB(((ColliderAABB*)m_minBox)->GetMin(), ((ColliderAABB*)m_minBox)->GetMax());
			m_tree.Insert(l_box, m_LLPtr);
		}
	}
	else {

	}
	m_pTransform = *m_gameObject->GetTransform();*/
}

void Collider::SetMinAABB(Collider* p_box) {
	//m_minBox = p_box;
}

bool Collider::IsColliding(void) const {
	return m_colliding;
}

void Collider::Response() {
	if (m_gameObject && !m_gameObject->GetStatic()) {
		if(m_colliding)
			m_gameObject->GetTransform()->position = m_pTransform.position + m_movement;
	}
}
const Collider* Collider::GetMinAABB() {
	return NULL;// m_minBox;
}

Collider* Collider::operator = (const Collider* original) { 
	return this; 
};


bool Collider::operator == (const Collider* p_original) { 
	return (this == p_original);
}

//
//void Collider::SetTreeAABB(AABB p_gridAABB) {
//	((ColliderAABB*)m_grid)->SetMin(p_gridAABB.min);
//	((ColliderAABB*)m_grid)->SetMax(p_gridAABB.max);
//}

void Collider::Reflection() {}