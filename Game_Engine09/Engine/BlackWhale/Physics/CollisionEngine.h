#ifndef COLLISIONENGINE_H
/*
* @author Olesia Kochergina
* @version 01
* @date 17/03/2017
*/
#define COLLISIONENGINE_H
#include "../Quadtree.h"
#include "Collider.h"
#include "ColliderAABB.h"
class CollisionEngine {
public:
	static void AddCollider(ColliderAABB* minCollider);
	static void RemoveCollider(ColliderAABB* minCollider);
	static std::vector<GameObject*> Intersect(ColliderAABB* collider);
	static Vector3 OverlapPoint(ColliderAABB* fCollider, ColliderAABB* sCollider);
	/*
	* @brief for boxes that 
	*/
	static bool OverlapPoint(ColliderAABB* fCollider, ColliderAABB* sCollider, Vector3& movement);
private:
	///passing by value is 3 times slower because of the ColliderAABB 
	///copy constructor comparing to passing by pointers. References cannot 
	///be used in std::vectors
	static DataStructures::Quadtree<ColliderAABB*> m_tree;
	static ColliderAABB m_gridCollider;
};
#endif COLLISIONENGINE_H