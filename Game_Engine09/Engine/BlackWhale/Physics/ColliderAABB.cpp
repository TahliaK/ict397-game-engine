#include "ColliderAABB.h"
#include "CollisionEngine.h"
#include "../MeshRenderer.h"
#define MAX_VECTOR 2000000
#define MIN_VECTOR -2000000
BOOST_CLASS_EXPORT_IMPLEMENT(ColliderAABB)
bool ColliderAABB::s_inList = false;

ColliderAABB::ColliderAABB(): Collider() {
	Reflection();
	Initialize();
	//m_tree.Insert(AABB(((ColliderAABB*)m_minBox)->GetMin(), ((ColliderAABB*)m_minBox)->GetMax()),m_LLPtr);
	if (!s_inList) {
		s_inList = true;
		ColliderAABB* l_pointer = new ColliderAABB;
		m_derivedClasses.push_back(l_pointer);
	}
}

void ColliderAABB::Start() {
	if (m_max == Vector3(MIN_VECTOR) && m_min == Vector3(MAX_VECTOR) && m_gameObject) {
		MeshRenderer l_temp;
		if ((m_gameObject->GetComponent(&l_temp)) && (((MeshRenderer*)m_gameObject->GetComponent(&l_temp))->GetMesh()) && (((MeshRenderer*)m_gameObject->GetComponent(&l_temp))->GetMesh()->GetMinimumCollider())) {
			m_min = ((MeshRenderer*)m_gameObject->GetComponent(&l_temp))->GetMesh()->GetMinimumCollider()->GetMinBox();
			m_max = ((MeshRenderer*)m_gameObject->GetComponent(&l_temp))->GetMesh()->GetMinimumCollider()->GetMaxBox();
		}
	}
	UpdateCollider();
	CollisionEngine::AddCollider(this);
}

void ColliderAABB::Initialize(void) {
	m_LLPtr = NULL;
	//	m_minBox = new ColliderAABB;
	m_name = "AABB Collider";
	m_stuck = false;
	m_min = Vector3(MAX_VECTOR);
	m_max = Vector3(MIN_VECTOR);
}

Component* ColliderAABB::NewComponent() const {
	Component* l_pointer = new ColliderAABB;
	//assumed that the component will definitely have a GO
	CollisionEngine::AddCollider((ColliderAABB*)l_pointer);
	return l_pointer;
}

ColliderAABB::ColliderAABB(const ColliderAABB& original): Collider(original) {
	Reflection();
	Initialize();
	m_min = original.m_min;
	m_max = original.m_max;
	m_sMax = original.m_sMax;
	m_sMin = original.m_sMin;
	m_LLPtr = original.m_LLPtr;
	UpdateCollider();
	if (m_gameObject) {
		CollisionEngine::AddCollider(this);
	}
}

ColliderAABB::ColliderAABB(const Vector3 min, const Vector3 max): Collider() {
	Reflection();
	Initialize();
	m_min = min;
	m_max = max;
	//CalculateCenter();
	UpdateCollider();
	SetMinAABB(this);
	if(this->m_gameObject)
		CollisionEngine::AddCollider(this);
	if (!s_inList) {
		s_inList = true;
		ColliderAABB* l_pointer = new ColliderAABB;
		m_derivedClasses.push_back(l_pointer);
	}
}

ColliderAABB::~ColliderAABB() {
	CollisionEngine::RemoveCollider(this);
	if (m_LLPtr) {
		delete m_LLPtr;
		m_LLPtr = NULL;
	}
}

DataStructures::Node<ColliderAABB*>*& ColliderAABB::GetLinkedListPointer(void){
	return m_LLPtr;
}

ColliderAABB* ColliderAABB::operator = (const ColliderAABB& original) {
	m_min = original.m_min;
	m_max = original.m_max;
	m_gameObject = original.m_gameObject;
	UpdateCollider();
	//CalculateCenter();
	return this;
}

bool ColliderAABB::operator == (const ColliderAABB& original) {
	return (m_min == original.m_min && m_max == original.m_max);
}

bool ColliderAABB::Contains(const ColliderAABB* box) const{
	return (m_sMin.x <= box->m_sMin.x && m_sMin.y <= box->m_sMin.y &&
		m_sMin.z <= box->m_sMin.z && m_sMax.x >= box->m_sMax.x &&
		m_sMax.y >= box->m_sMax.y && m_sMax.z >= box->m_sMax.z);
}

bool ColliderAABB::Intersect(const ColliderAABB* box) const{
	//if point to the same GO
	if (m_gameObject && box->m_gameObject && m_gameObject == box->m_gameObject) {
		return false;
	}
	IFORSI(0, 3) {
		if (GetMax()[i] < box->GetMin()[i] || GetMin()[i] > box->GetMax()[i])
			return false;
	}
	return true;
}

void ColliderAABB::UpdateCollider(void) {
	m_sMin = m_min;
	m_sMax = m_max;
	if (m_gameObject) {
		IFORSI(0, 3) {
			m_sMax.Set(i, m_sMax[i] * m_gameObject->GetTransform()->scale[i]);
			m_sMin.Set(i, m_sMin[i] * m_gameObject->GetTransform()->scale[i]);
		}
		m_sMin = m_sMin + m_gameObject->GetTransform()->position;
		m_sMax = m_sMax + m_gameObject->GetTransform()->position;
	}
}

void ColliderAABB::Update() {
	if (!m_gameObject->GetVisible())
		return;
	Vector3 l_pChange = m_gameObject->GetTransform()->position - m_pTransform.position;
	Vector3 l_sChange = m_gameObject->GetTransform()->scale - m_pTransform.scale;
	bool l_result = false;
	IFORSI(0, 3) {
		if (l_pChange[i] != 0 || l_sChange[i] != 0) {
			l_result = true;
			break;
		}
	}
	if (l_result) {
		UpdateCollider();
		CollisionEngine::AddCollider(this);
	}
	//only for dynamic objects 
	if (!m_gameObject->GetStatic()) {		
		////should be replaced with a proper function to response to collisions		
		m_sMin = m_sMin+ l_pChange;
		m_sMax = m_sMax + l_pChange;
		vector<GameObject*> t = CollisionEngine::Intersect(this);
		
		if (t.size() != 0) {
			m_colliding = true;
			IFORSI(0, t.size()) {
				Vector3 l_move;
				if (CollisionEngine::OverlapPoint(this, (ColliderAABB*)t.at(i)->GetComponent(this), l_move)) {
					m_movement = m_movement + l_move;
				}
				//m_gameObject->GetTransform()->position = m_pTransform.position + l_move;
			}
			
			//m_gameObject->GetTransform()->position = m_pTransform.position - l_change*5;
			//cout << "Main "<<m_gameObject->GetName() << endl;
			//m_gameObject->GetTransform()->position = m_pTransform.position;
		}
		else {
			m_colliding = false;
			m_movement = Vector3(0);
		}
			/*Camera* l_camera = (Camera*)m_gameObject->GetComponent(&Camera());
			if (l_camera) {
				Vector3 l_move(l_change*(-1));
				l_move.y = 0;
				l_camera->Move(l_move);
				cout << l_move << endl;
			}
			m_stuck = true;
		}
		else if(t.size() ==0){
			m_stuck = false;
		}
		else {
			
		}*/
		m_sMin = m_sMin - l_pChange;
		m_sMax = m_sMax - l_pChange;
	}
	else {	
		//static GOs
	}
	m_pTransform = *m_gameObject->GetTransform();
}

const Vector3 ColliderAABB::Center() const {
	Vector3 center = center = Vector3((m_sMin.x + m_sMax.x) / 2, (m_sMin.y + m_sMax.y) / 2, (m_sMin.z + m_sMax.z) / 2);
	return center;
}

Vector3 ColliderAABB::GetVertex(int index) const {
	//FAR
	//5_____7
	//|		|
	//|_____|
	//1		6
	//
	// NEAR
	//2_____4
	//|		|
	//|_____|
	//0		3
	Vector3 l_result;

	switch (index) {
	case 0://left,bottom,near
		l_result = GetMin();
		break;
	case 1://left,bottom,far
		l_result = GetMin();
		l_result.z = GetMax().z;
		break;
	case 2://left,up,near
		l_result = GetMin();
		l_result.y = GetMax().y;
		break;
	case 3://right,bottom,near
		l_result = GetMin();
		l_result.x = GetMax().x;
		break;
	case 4://right,up,near
		l_result = GetMax();
		l_result.z = GetMin().z;
		break;
	case 5://left,up,far
		l_result = GetMax();
		l_result.x = GetMin().x;
		break;
	case 6://right,bottom,far
		l_result = GetMax();
		l_result.y = GetMin().y;
		break;
	case 7://right,up,far
		l_result = GetMax();
		break;
	default:
		break;
	}
	return l_result;
}

bool ColliderAABB::SetMin(boost::any var) {
	try {
		m_min = boost::any_cast<Vector3> (var);
		m_sMin = m_min;
		if (m_gameObject) {
			IFORSI(0, 3) {
				m_sMin.Set(i, m_sMin[i] * m_gameObject->GetTransform()->scale[i]);
			}
			m_sMin = m_sMin + m_gameObject->GetTransform()->position;
		}
		CollisionEngine::AddCollider(this);
	}
	catch (boost::bad_any_cast& /*bac*/) {
		return false;
	}
	return true;
}

bool ColliderAABB::SetMax(boost::any var) {
	try {
		m_max = boost::any_cast<Vector3> (var);
		m_sMax = m_max;
		if (m_gameObject) {
			IFORSI(0, 3) {
				m_sMax.Set(i, m_sMax[i] * m_gameObject->GetTransform()->scale[i]);
			}
			m_sMax = m_sMax + m_gameObject->GetTransform()->position;
		}
		CollisionEngine::AddCollider(this);
	}
	catch (boost::bad_any_cast& /*bac*/) {
		return false;
	}
	return true;
}

const Vector3 ColliderAABB::GetMin(void)const {
	return m_sMin;
}
const Vector3 ColliderAABB::GetMax(void)const {
	return m_sMax;
}

const Vector3 ColliderAABB::GetMinBox(void) const {
	return m_min;
}
const Vector3 ColliderAABB::GetMaxBox(void) const {
	return m_max;
}

ColliderAABB* ColliderAABB::FindMinAABB(const std::vector<Vector3>& p_points) {
	ColliderAABB* tempBox = new ColliderAABB;
	tempBox->m_min = MINIMUM_INIT_INT;//reset 
	tempBox->m_max = MAXIMUM_INIT_INT;

	for (unsigned j = 0; j<p_points.size(); j++) {
		for (unsigned i = 0; i<3; i++) {
			if (tempBox->m_min[i] > p_points[j][i])
				tempBox->m_min.Set(i, p_points[j][i]);
			if (tempBox->m_max[i] < p_points[j][i])
				tempBox->m_max.Set(i, p_points[j][i]);
		}
	}
	int counter = 0;
	for (int i = 0; i<3; i++) {
		if (tempBox->m_min[i] > tempBox->m_max[i])
			counter++;
	}
	if (counter == 3) {
		Vector3 minimum = tempBox->m_max;
		tempBox->m_max = tempBox->m_min;
		tempBox->m_min = tempBox->m_max;
	}
	return tempBox;
}

void ColliderAABB::Reflection() {
	m_Variables.push_back("Minimum");
	m_Setters.push_back(boost::bind(&ColliderAABB::SetMin, this, _1));
	m_Getters.push_back(boost::bind(&ColliderAABB::GetMinBox, this));

	m_Variables.push_back("Maximum");
	m_Setters.push_back(boost::bind(&ColliderAABB::SetMax, this, _1));
	m_Getters.push_back(boost::bind(&ColliderAABB::GetMaxBox, this));
}