#include "CollisionEngine.h"
ColliderAABB CollisionEngine::m_gridCollider(Vector3(-500000, -5000, -500000), Vector3(500000, 5000, 500000));
DataStructures::Quadtree<ColliderAABB*> CollisionEngine::m_tree(CollisionEngine::m_gridCollider);

void CollisionEngine::AddCollider(ColliderAABB* p_min) {
	RemoveCollider(p_min);
	m_tree.Insert(p_min, p_min->GetLinkedListPointer());
}

void CollisionEngine::RemoveCollider(ColliderAABB* p_min) {
	if (p_min->GetLinkedListPointer()) {
		m_tree.Delete(p_min->GetLinkedListPointer());
		p_min->GetLinkedListPointer() = NULL;
	}
}

std::vector<GameObject*> CollisionEngine::Intersect(ColliderAABB* p_collider) {
	vector<ColliderAABB*> l_range = m_tree.QueryRange(*p_collider);
	vector<GameObject*> l_go;
	for (int i = 0; i < l_range.size(); i++) {
		//if attached to different game objects
		if (l_range.at(i)->GetGameObject() && l_range.at(i)->GetGameObject() != p_collider->GetGameObject() && l_range.at(i)->GetGameObject()->GetVisible()) {
			//check for collision
			if (p_collider->Intersect(l_range.at(i)))
				l_go.push_back(l_range.at(i)->GetGameObject());
		}
	}
	return l_go;
}

Vector3 CollisionEngine::OverlapPoint(ColliderAABB* p_fCollider, ColliderAABB* p_sCollider) {
	Vector3 l_overlap(0);
	for (int i = 0; i < 3; i++) {
		if (p_fCollider->Center()[i] < p_sCollider->Center()[i])
			l_overlap.Set(i, p_fCollider->GetMax()[i] - p_sCollider->GetMin()[i]);
		else
			l_overlap.Set(i, p_sCollider->GetMax()[i] - p_fCollider->GetMin()[i]);
	}
	return l_overlap;
}

bool CollisionEngine::OverlapPoint(ColliderAABB* p_fCollider, ColliderAABB* p_sCollider, Vector3& p_movement) {
	float front, back, left, right, top, bottom;
	right = p_sCollider->GetMax().x - p_fCollider->GetMin().x;
	left = p_sCollider->GetMin().x - p_fCollider->GetMax().x;
	top = p_sCollider->GetMin().y - p_fCollider->GetMax().y;
	bottom = p_sCollider->GetMax().y - p_fCollider->GetMin().y;
	front = p_sCollider->GetMin().z - p_fCollider->GetMax().z;
	back = p_sCollider->GetMax().z - p_fCollider->GetMin().z;
	if (left > 0 || right < 0 || top > 0 || bottom < 0 || front > 0 || back < 0)
		return false;
	//find offset of sides 
	p_movement.x = abs(left) < right ? left : right;
	p_movement.y = abs(top) < bottom ? top : bottom;
	p_movement.z = abs(front) < back ? front : back;

	//only use the smallest offset
	if (abs(p_movement.x) != abs(p_movement.z)) {
		if (abs(p_movement.x) < abs(p_movement.z))
			p_movement.z = 0;
		else
			p_movement.x = 0;
	}
	p_movement.y = 0;
	return true;
}