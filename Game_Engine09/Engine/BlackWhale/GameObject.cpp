#include "GameObject.h"
#include "Transform.h"
#include "SceneManager.h"
BOOST_CLASS_EXPORT_IMPLEMENT(GameObject)

GameObject::GameObject() : InObject() {
	m_transform = NULL;
	m_static = false;
	m_visible = true;
	AddComponent(new Transform);
	m_transform = (Transform*)m_components.back();
	m_name = "GameObject";
	if (SceneManagement::SceneManager::GetActive())
		m_scene = SceneManagement::SceneManager::GetActive()->GetID();
	else
		m_scene = -1;
	m_layer = 0;
}

GameObject::GameObject(const GameObject& original) {
	m_transform = NULL;
	m_static = original.m_static;
	m_visible = original.m_visible;
	m_layer = original.m_layer;
	m_scene = original.m_scene;
	m_components.clear();
	IFORSI(0, original.m_components.size()) {
		Component* l_temp = original.m_components.at(i);
		m_components.push_back(l_temp);
	}
}
GameObject* GameObject::NewObject() const {
	GameObject* l_pointer = new GameObject;
	return l_pointer;
}
void GameObject::Update() {
	IFORSI(0, m_components.size()) {
		m_components.at(i)->Update();
		JFORSI(0, m_transform->GetChildrenCount()) {
			if (m_transform->GetChild(j)->GetGameObject())
				m_transform->GetChild(j)->GetGameObject()->Update();
		}
	}
}

void GameObject::Start() {
	IFORSI(0, m_components.size()) {
		m_components.at(i)->SetGameObject(this);
		m_components.at(i)->Start();
	}
}

GameObject::~GameObject() {
	IFORSI(0, m_components.size()) {
		if (m_components.at(i)) {
			delete m_components.at(i);
			m_components.at(i) = NULL;
		}
	}
	m_components.clear();
	if (m_transform) {
		//delete m_transform;
		m_transform = NULL;
	}
}

bool GameObject::AddComponent(Component* component) {
	if (component) {
		//to ensure that only one instance of a particular component is attached to the gameobject if the component's m_multiple is false
		if (!component->IsMultipleOccurrence()) {
			IFORSI(0, m_components.size()) {
				if (typeid(*m_components.at(i)) == typeid(*component))
					return false;
			}
		}
		component->SetGameObject(this);
		component->Start();
		m_components.push_back(component);
		return true;
	}
	return false;
}

Transform* GameObject::GetTransform(void) const {
	return m_transform;
}
void GameObject::SetVisible(const bool p_visible) {
	m_visible = p_visible;
}
void GameObject::SetStatic(const bool p_static) {
	m_static = p_static;
}

void GameObject::SetLayer(const int p_layer) {
	m_layer = p_layer;
}

const bool GameObject::GetVisible(void) const {
	return m_visible;
}

const int GameObject::GetLayer(void) const {
	return m_layer;
}

const bool GameObject::GetStatic(void) const {
	return m_static;
}

Component* GameObject::GetComponent(Component* p_type) const {
	IFORSI(0, m_components.size()) {
		if (typeid(*m_components.at(i)) == typeid(*p_type)) {
			return m_components.at(i);
		}
	}
	return NULL;
}

std::vector<Component*> GameObject::GetComponents(Component* p_type) const {
	std::vector<Component*> l_return;
	IFORSI(0, m_components.size()) {
		if (typeid(m_components.at(i)) == typeid(p_type)) {
			l_return.push_back(m_components.at(i));
		}
	}
	return l_return;
}

std::vector<Component*> GameObject::GetComponents() const {
	return m_components;
}
