#include "FrustumCulling.h"
#define ANG2RAD 3.14159265358979323846/180.0
FrustumCulling::FrustumCulling() {

}

FrustumCulling::~FrustumCulling() {

}

void FrustumCulling::Perspective(float p_fov, float p_aspect, float p_near, float p_far) {
	m_fov = p_fov;
	m_ratio = p_aspect;
	m_near = p_near;
	m_far = p_far;

	m_tang = (float)tan(ANG2RAD * p_fov * 0.5);
	m_nHeight = p_near * m_tang;
	m_nWidth = m_nHeight * m_ratio;

	m_fHeight = p_far * m_tang;
	m_fWidth = m_fHeight * m_ratio;
}

void FrustumCulling::LookAt(Vector3 p_pos, Vector3 p_view, Vector3 p_up) {
	m_camPos = p_pos;
	m_camRef[2] = Vector3::Normalize(p_view - p_pos);
	m_camRef[0] = Vector3::Normalize(Vector3::Cross(m_camRef[2], p_up));
	m_camRef[1] = Vector3::Cross(m_camRef[0], m_camRef[2]);

	Vector3 dir, nc, fc;
	nc = p_pos - m_camRef[2] * m_near;
	//added negative one to make the algorithm work
	fc = p_pos - m_camRef[2] * m_far *-1;

	// compute the 4 corners of the frustum on the near plane
	ntl = nc + m_camRef[1] * m_nHeight - m_camRef[0] * m_nWidth;
	ntr = nc + m_camRef[1] * m_nHeight + m_camRef[0] * m_nWidth;
	nbl = nc - m_camRef[1] * m_nHeight - m_camRef[0] * m_nWidth;
	nbr = nc - m_camRef[1] * m_nHeight + m_camRef[0] * m_nWidth;

	// compute the 4 corners of the frustum on the far plane
	ftl = fc + m_camRef[1] * m_fHeight - m_camRef[0] * m_fWidth;
	ftr = fc + m_camRef[1] * m_fHeight + m_camRef[0] * m_fWidth;
	fbl = fc - m_camRef[1] * m_fHeight - m_camRef[0] * m_fWidth;
	fbr = fc - m_camRef[1] * m_fHeight + m_camRef[0] * m_fWidth;

	// compute the six planes
	// the function set3Points assumes that the points
	// are given in counter clockwise order
	m_plane[TOP].SetPoints(ntr, ntl, ftl);
	m_plane[BOTTOM].SetPoints(nbl, nbr, fbr);
	m_plane[LEFT].SetPoints(ntl, nbl, fbl);
	m_plane[RIGHT].SetPoints(nbr, ntr, fbr);
	m_plane[NEARP].SetPoints(ntl, ntr, nbr);
	m_plane[FARP].SetPoints(ftr, ftl, fbl);

	/*
	pl[NEARP].setNormalAndPoint(-Z,nc);
	pl[FARP].setNormalAndPoint(Z,fc);

	Vec3 aux,normal;

	aux = (nc + Y*nh) - p;
	aux.normalize();
	normal = aux * X;
	pl[TOP].setNormalAndPoint(normal,nc+Y*nh);

	aux = (nc - Y*nh) - p;
	aux.normalize();
	normal = X * aux;
	pl[BOTTOM].setNormalAndPoint(normal,nc-Y*nh);

	aux = (nc - X*nw) - p;
	aux.normalize();
	normal = aux * Y;
	pl[LEFT].setNormalAndPoint(normal,nc-X*nw);

	aux = (nc + X*nw) - p;
	aux.normalize();
	normal = Y * aux;
	pl[RIGHT].setNormalAndPoint(normal,nc+X*nw);
	*/
}

bool FrustumCulling::Contains(Vector3 p_point) {
	Vector3 l_vector = p_point - m_camPos;
	Vector3 l_check;

	//test Z axis
	l_check.z = Vector3::Dot(l_vector, m_camRef[2] );
	if (l_check.z > m_far || l_check.z < m_near)
		return false;
	//test Y axis
	l_check.y = Vector3::Dot(l_vector, m_camRef[1]);
	float l_aux = l_check.z * m_tang;
	if (l_check.y > l_aux || l_check.y < -l_aux)
		return false;
	//test m_camRef[0] axis
	l_check.x = Vector3::Dot(l_vector, m_camRef[0]);
	l_aux *= m_ratio;
	if (l_check.x > l_aux || l_check.x < -l_aux)
		return false;
	return true;
}

bool FrustumCulling::Contains(ColliderAABB* p_box) {
	//inside
	bool result = true;
	int out, in;

	// for each plane do ...
	for (int i = 0; i < 6; i++) {

		// reset counters for corners in and out
		out = 0; in = 0;
		// for each corner of the box do ...
		// get out of the cycle as soon as a box as corners
		// both inside and out of the frustum
		for (int k = 0; k < 8 && (in == 0 || out == 0); k++) {
			// is the corner outside or inside
			if (m_plane[i].Distance(p_box->GetVertex(k)) < 0) {
				out++;
			}
			else {
				in++;
			}
		}
		//if all corners are out
		if (!in) {
			return false;
		}
		// if some corners are out and others are in
		else if (out)//intersect
			result = true;
	}
	return(result);
}