#ifndef SDLSOUND_H
#define SDLSOUND_H
#include "BlackWhale\Sound.h"
#include "SDL.h"
#include "SDL_Mixer\SDL_mixer.h"
#include <vector>

/**@struct SDL_SFXItem
*@brief contains an SDL SFX item's information
*@param sfx_ptr a pointer to the loaded sound's location
*@param channel the channel it is being played on
*@param filename the associated filename
*@param angle the angle away from the listener location
*@param distance the distance away from the listener location
*@param numUsers number of components using this sound
*/
typedef struct SDL_SFXItem{
	SDL_SFXItem() : sfx_ptr(nullptr), channel(-1), angle(-1), distance(-1),
	numUsers(0){}
	Mix_Chunk * sfx_ptr;
	int channel;
	std::string filename;
	Sint16 angle; //angle of position of the stereo sound, default -1 (ignore)
	Uint8 distance; //distance of position of the stereo sound, default -1 (ignore)
	int numUsers;
} SDL_SFXItem;

/**@struct SDL_MusItem
*@brief contains the SDL music item
*@param mus_ptr the pointer to the loaded sound
*@param volume the volume of the music track
*@param filename the associated filename
*@param numUsers number of components using this sound
*/
typedef struct SDL_MusItem{
	SDL_MusItem() : mus_ptr(nullptr),
		volume(MIX_MAX_VOLUME), filename("nofile"),
		numUsers(0){}
	Mix_Music * mus_ptr;
	int volume; //range 0-128, default 128
	std::string filename;
	int numUsers;
} SDL_MusItem;

/**@class SDL_Sound_SFXInfoManager
*@brief Singleton class which manages SDL sounds
*/
class SDL_Sound_SFXInfoManager{
public:
	/**@brief initialises SDL and SDL_Mixer, must be called before playing
	*/
	static void initialiseSound();

	/**@brief closes SDL and SDL_Mixer
	*/
	static void closeSound();

	/**@brief loads a sound into memory
	*@param filename the filename of the file to load
	*@param soundType determines whether to load as Music of SFX
	*/
	static int loadSound(std::string filename, int soundType);
	

	/**@brief unloads a sound from memory
	*@param filename the filename of the file to unload
	*@param soundtype the type of file (music or sfx)
	*/
	static void unloadSound(std::string filename, int soundType);

	/**@brief plays a sound that is already loaded
	*@param soundID - ID number of loaded sound
	*@param times - number of times to play it, -1 = endless loop, 0 = once, 1 = twice
	*@param fadeIn - the amount of time to fade-in the track
	*/
	static int playSound(int soundID, int times, float fadeIn);

	/**@brief stops a sound that is already playing
	*@param soundID - ID number of loaded sound
	*@param fadeOut - the amount of time to fadeOut the track
	*/
	static void stopSound(int soundID, float fadeOut);

	/**@brief callback function for SDL_Mixer's SDL_Finish
	*@param channel - channel that is calling the function
	*/
	static void channel_finished(int channel); //to be called by SDL_Finish

	/**@brief callback function used by the callback function
	*/
	static void music_finished();

private:
	static int numberOfInits;
	static std::vector<SDL_SFXItem> sfxList;
	static std::vector<SDL_MusItem> musicList;

	static bool isValidID(int soundID);
	static bool isMusic(int soundID);
	static bool isLoaded(std::string filename, int soundType, int &index);
	static int IDToIndex(int soundID);
	static int IndexToID(int soundIndex, int soundType);

	static int firstFreeIndex(int soundType);
};

/**@class SDL_Sound
*@brief Sound component using SDL
*@see Component
*/
class SDL_Sound : public Sound {
public:
	SDL_Sound();
	~SDL_Sound();

	/**@brief Initializes the sound class
	*/
	void Initialize();

	/**@brief called on starting up a Sound component
	*/
	void Start();

	/**@brief called on update
	*Does nothing in this class, required for Component compatibility
	*/
	void Update();

	/**@brief called on closing the class*/
	void Clear();

	/**@brief Sets the music filename
	*@param file the filename to use
	*@returns true if successful, false if invalid
	*/
	bool SetFile(boost::any file);

	/**@brief Sets whether or not this is music
	*@param isMusic boolean whether or not it is music
	*@returns true if successful, false if invalid
	*/
	bool SetIsMusic(boost::any isMusic);

	/**@brief Sets whether the music is playing or not
	*Acts as a play/stop button
	*@param isPlaying boolean isPlaying value
	*/
	bool SetPlaying(boost::any isPlaying);

	/**@brief Sets an infinite repeat for this component
	*Doesn't take effect if it's already playing until next play
	*@param isRepeating boolean isRepeating value
	*/
	bool SetRepeat(boost::any isRepeating);

	/**@brief Sets the number of loops for this component
	*1 loop = plays twice, 0 loops = plays once, -1 = infinite
	*@param loopNum the number of times to loop the component
	*/
	bool SetLoopNum(boost::any loopNum);

	/**@brief Sets the fadeIn time of the component
	*@param fadeIn the fadein time in seconds
	*/
	bool SetFadeInTime(boost::any fadeIn);

	/**@brief Sets the fadeOut time of the component
	*@param fadeOut the fadeOut time in seconds
	*/
	bool SetFadeOutTime(boost::any fadeIn);


	std::string GetFile();
	bool GetIsMusic();
	bool GetPlaying();
	bool GetRepeat();
	int GetLoopNum();
	int GetFadeInTime();
	int GetFadeOutTime();

	//int LoadSound(std::string filename, int soundType);
	//bool PlaySound(int soundID, int times, bool fadeIn); //-1 = endless loop

	Component* NewComponent() const;
	void Reflection();

private:
	std::string m_filename;
	int m_soundID;
	int m_soundType;
	bool m_isPlaying;
	bool m_repeat;
	int m_loopNumber;
	int m_fadeInTime;
	int m_fadeOutTime;
	bool isStereoToCamera;//Not implemented

	void updateIsPlaying(bool isPlaying);
};
//	///serialization
//	friend class boost::serialization::access;
//	template<class Archive>
//	void save(Archive& ar, const unsigned int /*version*/) const {
//		ar & SER_BASE_OBJECT_NVP(Sound);
//		/*ar& SER_NVP(m_filename);
//		ar& SER_NVP(m_isPlaying);
//		ar& SER_NVP(m_repeat);*/
//	}
//	template<class Archive>
//	void load(Archive& ar, const unsigned int /*version*/) {
//		ar & SER_BASE_OBJECT_NVP(Sound);
//	}
//	SER_SPLIT_MEMBER()
//};
//
//SER_CLASS_VERSION(SDL_Sound, 0);
//SER_CLASS_EXPORT_KEY(SDL_Sound)
#endif SDLSOUND_H