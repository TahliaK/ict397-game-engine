#include "ScriptManager.h"
#include <lua.h>
#include <lualib.h>
#include <luabind\lua_include.hpp>
#include "luaconf.h"
#include "LuaScriptFunctions.h"



ScriptManager::ScriptManager() : memoryItems()
{
}


ScriptManager::~ScriptManager()
{
	for (int i = 0; i < memoryItems.size; i++)
	{
		try {
			if (memoryItems[i] != nullptr)
			{
				delete memoryItems[i];
				memoryItems[i] = nullptr;
			}
		}
		catch (std::exception) {} //if undelete-able, move on
	}
}


//LuaScriptManager

LuaScriptManager::LuaScriptManager()
{
	state = luaL_newstate();
	luaL_openlibs(state);
	registerEngineFunctions();
	registerLuaFunctions();
}

LuaScriptManager::~LuaScriptManager()
{
	//how to run ~ScriptManager() here??
}

bool LuaScriptManager::loadScript(std::string filename)
{
	
}

bool LuaScriptManager::runScript() {}

bool LuaScriptManager::loadRunScript(std::string filename) {}

bool LuaScriptManager::updateScripts() { update(); }

bool LuaScriptManager::initialiseScripts() { initialise(); }

bool LuaScriptManager::closeScripts() { close(); }

void LuaScriptManager::registerEngineFunctions()
{
	lua_pushcfunction(state, LuaScriptFunctions::make3DObject);
	lua_setglobal(state, "c_Make3DObject");

	lua_pushcfunction(state, LuaScriptFunctions::load3DModel);
	lua_setglobal(state, "c_Load3DModel");

	lua_pushcfunction(state, LuaScriptFunctions::set3DCollisions);
	lua_setglobal(state, "c_Set3DCollisions");

	lua_pushcfunction(state, LuaScriptFunctions::setObjectFeature);
	lua_setglobal(state, "c_SetObjectFeature");

	lua_pushcfunction(state, LuaScriptFunctions::moveObject);
	lua_setglobal(state, "c_MoveObject");

	lua_pushcfunction(state, LuaScriptFunctions::destroy3Dobject);
	lua_setglobal(state, "c_DestroyObject");

	lua_pushcfunction(state, LuaScriptFunctions::areObjectsColliding);
	lua_setglobal(state, "c_AreObjectsColliding");

	lua_pushcfunction(state, LuaScriptFunctions::createPerspectiveCamera);
	lua_setglobal(state, "c_CreatePerspectiveCamera");

	lua_pushcfunction(state, LuaScriptFunctions::editCameraPerspective);
	lua_setglobal(state, "c_EditPerspectiveCamera");

	lua_pushcfunction(state, LuaScriptFunctions::createOrthoCamera);
	lua_setglobal(state, "c_CreateOrthoCamera");

	lua_pushcfunction(state, LuaScriptFunctions::editCameraOrtho);
	lua_setglobal(state, "c_EditOrthoCamera");

	lua_pushcfunction(state, LuaScriptFunctions::moveCamera);
	lua_setglobal(state, "c_MoveCamera");

	lua_pushcfunction(state, LuaScriptFunctions::movementWillCollide);
	lua_setglobal(state, "c_MovementWillCollide");

	lua_pushcfunction(state, LuaScriptFunctions::movementWillCollideScene);
	lua_setglobal(state, "c_MovementWillCollideScene");

	lua_pushcfunction(state, LuaScriptFunctions::mouseChangeVec);
	lua_setglobal(state, "c_MouseChangeVec");

	lua_pushcfunction(state, LuaScriptFunctions::mouseLocation);
	lua_setglobal(state, "c_MouseLocation");

	lua_pushcfunction(state, LuaScriptFunctions::keyPressed);
	lua_setglobal(state, "c_KeyPressed");

	lua_pushcfunction(state, LuaScriptFunctions::timeDif);
	lua_setglobal(state, "c_TimeDif");

	lua_pushcfunction(state, LuaScriptFunctions::newScene);
	lua_setglobal(state, "c_NewScene");

	lua_pushcfunction(state, LuaScriptFunctions::loadScene);
	lua_setglobal(state, "c_LoadScene");

	lua_pushcfunction(state, LuaScriptFunctions::swapSceneTo);
	lua_setglobal(state, "c_SwapSceneTo");
}

//really need to write a reference Lua file for this one
void LuaScriptManager::registerLuaFunctions()
{

}