#ifndef LUASCRIPTFUNC_H
#define LUASCRIPTFUNC_H
#include "ScriptFunctions.h"
#include <typeinfo>
extern "C" {
#include <lua.h>
#include <lualib.h>
#include <luaconf.h>
#include <lauxlib.h>
}

/**@class LuaScriptFunctions
*@brief Contains Lua wrappers for all standard ScriptFunctions
*/
class LuaScriptFunctions :
	public ScriptFunctions
{
public:

	//These are all just wrappers to register ScriptFunctions with a Lua_state
	static int make3DObject(lua_State * L);
	static int load3DModel(lua_State *L); //Incomplete implementation
	static int set3DCollisions(lua_State *L);
	static int setObjectFeature(lua_State *L);
	static int moveObject(lua_State *L);
	static int destroy3Dobject(lua_State *L);

	static int areObjectsColliding(lua_State *L);
	static int createPerspectiveCamera(lua_State *L);
	static int editCameraPerspective(lua_State *L);
	static int createOrthoCamera(lua_State *L);
	static int editCameraOrtho(lua_State *L);

	static int moveCamera(lua_State *L);
	static int movementWillCollide(lua_State *L);
	static int movementWillCollideScene(lua_State *L);

	static int mouseChangeVec(lua_State *L);
	static int mouseLocation(lua_State *L);
	static int keyPressed(lua_State *L);

	static int timeDif(lua_State *L);

	static int newScene(lua_State * L);
	static int loadScene(lua_State * L);
	static int swapSceneTo(lua_State *L);

protected:
	/**@brief Pops an item of type T from the stack
	* Pops from stack regardless of if type is right, but puts error on stack if type is wrong
	*@param L the lua_state being used
	*@param item the item to store the information in
	*@returns false if error put on stack, true if successful
	*/
	template <typename T> static bool popFromStack(lua_State *L, T * item);

	/**@brief Pushes an item of Type T onto the stack
	* Returns false if there is no item (ie. you give it a dereferenced null ptr)
	*@param L the lua_state being used
	*@param item the item being pushed onto the stack
	*/
	template <typename T> static bool pushToStackPointer(lua_State *L, T & item);
	
	/**@brief Pushes a string onto the stack
	*Tests enough room on the stack, performs the push, and returns
	*@param L the current lua_state
	*@param item the item to push to stack
	*@returns true if success, false if failed
	*/
	static bool pushToStackString(lua_State * L, const char * item);
	
	/**@brief Pushes a number onto the stack
	*Tests enough room on the stack, performs the push, and returns
	*@param L the current lua_state
	*@param number the item to push to stack
	*@returns true if success, false if failed
	*/
	static bool pushToStackNumber(lua_State * L, double number);
	
	/**@brief Pushes a boolean onto the stack
	*Tests enough room on the stack, performs the push, and returns
	*@param L the current lua_state
	*@param boolean the item to push to stack
	*@returns true if success, false if failed
	*/
	static bool pushToStackBoolean(lua_State * L, bool boolean);

	/**@brief Checks if the stack has at least X number of items on it
	* Adds an error msg to the stack if there's insufficient number of params
	*@param L the lua_state in use
	*@param numberToHave the number of parameters needed
	*@return true if there are enough, false if not enough
	*/
	static bool hasEnoughParams(lua_State * L, int numberToHave); //includes sending error msg on stack
	
	/**@brief Clears the stack of the given Lua object
	*/
	static void clearStack(lua_State *L);

private:
	//encapsulated so that nobody has to deal with boost::any throwing errors
	static boost::any popFromStack(lua_State *L);
};

template <typename T>
inline static bool LuaScriptFunctions::popFromStack(lua_State *L, T * item) //for easier stack popping
{
	if (hasEnoughParams(L, 1))
	{
		try
		{
			item = boost::any_cast<T *>(LuaScriptFunctions::popFromStack(L));
			return true;
		}
		catch (boost::bad_any_cast)
		{
			pushToStackString(L, "Error: Incorrect param format");
			return false;
		}
	}
	else //hasEnoughParams already created errmsg
	{
		return false;
	}
}

template <typename T>
inline bool LuaScriptFunctions::pushToStackPointer(lua_State *L, T & item)
{
	if(lua_checkstack(L, sizeof(item)))
	{
		lua_pushlightuserdata(L, (void *)&item);
		return true;
	}
	return false;
}

inline bool LuaScriptFunctions::pushToStackString(lua_State * L, const char * item)
{
	if(lua_checkstack(L, 1))
	{
		lua_pushstring(L, item);
		return true;
	}
	return false;
}

inline bool LuaScriptFunctions::pushToStackNumber(lua_State * L, double number)
{
	if(lua_checkstack(L, 1))
	{
		lua_pushnumber(L, number);
		return true;
	}
	return false;
}

inline bool LuaScriptFunctions::pushToStackBoolean(lua_State * L, bool boolean)
{
	if(lua_checkstack(L, 1))
	{
		lua_pushboolean(L, boolean);
		return true;
	}
	return false;
}

inline bool LuaScriptFunctions::hasEnoughParams(lua_State *L, int numberToHave)
{
	if(lua_gettop(L) >= numberToHave)
	{
		return true;
	}

	return false;
}

inline void LuaScriptFunctions::clearStack(lua_State * L)
{
	lua_settop(L, 0);
}


#endif