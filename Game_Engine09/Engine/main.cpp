#include "BlackWhale\Engine.h"
#include "BlackWhale\Editor\EditorManager.h"
#define WIN32_LEAN_AND_MEAN

/*
* @brief Entry point.
*/
int main(int argc, char *argv[]){
	Editor::EditorManager l_editor;
	l_editor.Initialize("Game Engine", "Resources\\EDITOR_MODE.txt");
	Engine::Initialize();
	l_editor.Start();
	Engine::Clear();

	return l_editor.GetExitCode();
}
