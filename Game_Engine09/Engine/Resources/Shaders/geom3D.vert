#version 330 core

uniform mat4 proj;
uniform	mat4 model;
uniform mat4 view;

uniform vec4 plane;

in vec3 position;
in vec3 normal;
in vec3 texCoord;

out vec3 p;
out vec3 n;
out vec2 t;
out vec4 clipSpace;
void main(){
	gl_ClipDistance[0] = dot(vec4(position,1),plane);
	
	vec4 worldPos = model * vec4(position,1);
	p = worldPos.xyz;
	clipSpace = proj  * view * worldPos;
	gl_Position = clipSpace;
	t = texCoord.st;

	//n =  (model*vec4(normal,0.0)).xyz;
	mat3 nMatrix = transpose(inverse(mat3(model)));
	n = nMatrix * normal;
}