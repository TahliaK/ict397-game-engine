#version 330 core

uniform mat4 proj;
uniform	mat4 model;
uniform mat4 view;


in vec3 position;
in vec2 texCoord;
//uniform vec3 lightPos;
//vec3 out lightDir;

out vec2 m_coord;

void main(){
	gl_Position = vec4(position,1);
	m_coord = texCoord;
}