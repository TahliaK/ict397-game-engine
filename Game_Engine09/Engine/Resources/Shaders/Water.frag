#version 330 core

const float waveStrength = 0.06;//0.02
const float shineDamper = 30;//greater -> less shiny
const float reflectivity = 0.6;

uniform sampler2D reflectionT;
uniform sampler2D refractionT;
uniform sampler2D dudvMap;
uniform sampler2D normalMap;

uniform vec3 lightAmbient;
uniform float moveFactor;

in vec3 fromLightVector;
in vec4 clipSpace;
in vec2 uv;
in vec3 toCameraVector;


out vec4 outputF;

void main(void){
	vec2 ndc = (clipSpace.xy/clipSpace.w)/2 + 0.5;
	vec2 refractTexCoord = vec2(ndc.x,ndc.y);
	vec2 reflectTexCoord = vec2(ndc.x,-ndc.y);

	vec2 distortedTexCoord = texture(dudvMap, vec2(uv.x+moveFactor,uv.y)).rg  * 0.1;
	distortedTexCoord = uv + vec2(distortedTexCoord.x,distortedTexCoord.y + moveFactor);
	vec2 totalDistortion = (texture(dudvMap, distortedTexCoord).rg * 2.0 - 1.0) * waveStrength;
	
	refractTexCoord += totalDistortion;
	refractTexCoord = clamp(refractTexCoord, 0.001,0.999);

	reflectTexCoord += totalDistortion;
	reflectTexCoord.x = clamp(reflectTexCoord.x, 0.001,0.999);
	reflectTexCoord.y = clamp(reflectTexCoord.y, -0.999,-0.001);

	vec4 reflectColor = texture(reflectionT, reflectTexCoord);
	vec4 refractColor = texture(refractionT, refractTexCoord);

	vec3 viewVector = normalize(toCameraVector);
	float refractiveFactor = dot(viewVector, vec3(0,1,0));
	refractiveFactor = pow(refractiveFactor, 0.5);

	vec4 normalMapColor = texture(normalMap, distortedTexCoord);
	vec3 normal = vec3(normalMapColor.r * 2 - 1, normalMapColor.b, normalMapColor.g * 2 - 1);
	normal = normalize(normal);

	vec3 reflectedLight = reflect(normalize(fromLightVector), normal);
	float specular = max(dot(reflectedLight,viewVector),0);
	specular = pow(specular, shineDamper);
	vec3 specularHighlights = lightAmbient * specular * reflectivity;

	outputF = mix(reflectColor, refractColor,refractiveFactor);
	//outputF = mix(outputF, vec4(0.184,0.309,0.309,1),.2) + vec4(specularHighlights,0);
	//outputF = mix(outputF, vec4(176/255,196/255,222/255,1),.2) + vec4(specularHighlights,0);
	outputF = mix(outputF, vec4(119/255,136/255,153/255,1),.2) + vec4(specularHighlights,0);
}