//By Olesia Kochergina
//Modification: 08/05/2017, discards a fragment when the fragment is black and complletely transparent, need to change it to the sky color
#version 330 core

const int LIGHT = 32;

uniform sampler2D u_tPosition,u_tNormal,u_tColor;

uniform vec3 u_position[LIGHT];
uniform vec3 u_color[LIGHT];
uniform float u_radius[LIGHT];
uniform int u_type[LIGHT];
uniform int u_amount;
uniform vec3 u_cameraPosition;

in vec2 m_coord;

layout (location = 0) out vec4 outputF;

void main(){   
	vec3 Diffuse = texture(u_tColor, m_coord).rgb;
	if(Diffuse.r == 0 && Diffuse.g == 0 && Diffuse.b == 0 && texture(u_tColor, m_coord).a == 0)
		discard;
	vec3 lighting;
	vec3 Normal = (texture(u_tNormal, m_coord).rgb); 
	if(Normal.r == -2.0 && Normal.g == -2.0 && Normal.b == -2.0){
		lighting = Diffuse;// * 0.1;
		//lighting = vec3(0,0,0);		
	}else{
		vec3 FragPos = texture(u_tPosition, m_coord).rgb;		   
		float Specular = texture(u_tColor, m_coord).a;	
		// lighting calculations
		lighting = Diffuse * 0.1;  // hard-coded ambient component, set to 0 to make the scene black without lights
		//vec3 viewDir  = normalize(u_cameraPosition - FragPos);
		int maximum = u_amount;
		if(u_amount>LIGHT){
			maximum = LIGHT;
		}
		bool dirLight = false;
		for(int i=0;i<maximum;i++){
			if(u_type[i] == 1){
				dirLight = true;
			}
		}
		for(int i = 0; i < maximum; ++i)
		{
			if(u_type[i] == 0){//if point light
				// Calculate distance between light source and current fragment
				float distance = length(u_position[i] - FragPos);
				if(distance < u_radius[i]){
					// Diffuse
					vec3 lightDir = normalize(u_position[i] - FragPos);
					//vec3 diffuse = max(dot(normalize(u_position[i]),Normal), 0.0) * Diffuse * u_color[i];
					vec3 diffuse = max(dot(lightDir,Normal), 0.0) * Diffuse * u_color[i];
            
					// Specular 
					float spec = pow(max(dot(Normal, lightDir), 0.0), 16.0);
					vec3 specular = u_color[i] * Specular *spec;

					// Attenuation
					float attenuation = 1.0 - (distance/u_radius[i]);
					lighting += diffuse*attenuation + specular*attenuation;
				}else{//to make fragments darker when the light is far away
					//if(dirLight == false)
					//	lighting *=(u_radius[i]/distance);
				}
			}else if(u_type[i] == 1){//directional light
				vec3 lightDir = normalize(u_position[i] - FragPos);
				float NdotL = max(dot(Normal, lightDir), 0.0);
				vec3 diffuse = NdotL * Diffuse * u_color[i];
				lighting +=  diffuse*2;
			}else if(u_type[i] == 2){//spot light

			}
		}      
	}
	outputF = vec4(lighting, 1.0);
}