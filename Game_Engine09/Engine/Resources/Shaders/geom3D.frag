#version 330 core

uniform sampler2D texture;

uniform int u_shader=1;

uniform mat4 Material;//ambient, diffuse, specular, emission
uniform float shininess;

const float specular_intensity = 0.5;

//terrain
uniform int u_amountTextures;
const int m_maxTextures = 10;
uniform sampler2D DetailMap;
uniform sampler2D multitexture[m_maxTextures];
uniform float minH[m_maxTextures];
uniform float maxH[m_maxTextures];
uniform vec3 u_cameraPosition;
in vec3 p;
in vec3 n;
in vec2 t;
in vec4 clipSpace;

//layout (location = 0) out vec4 outputF;
layout (location = 0) out vec3 gPosition;
layout (location = 1) out vec3 gNormal;
layout (location = 2) out vec4 gColor;

vec3 diffuseModel(vec3 pos,vec3 norm,vec3 diff);
vec4 blend(vec4, float, vec4, float);

vec4 SetMaterial();
vec4 Model();
vec4 Terrain(int tiling);
vec4 Water();
void main()
{    
    gPosition = p;
	gNormal = normalize(n);
	vec4 color;
	if(u_shader==0.0 || u_shader == 2.0)
		color = Model() * SetMaterial();
	else if(u_shader == 1.0){
		/*float distance = length(u_cameraPosition-p);
		if(distance < 1)
		distance = 1;*/
		//if(length(u_cameraPosition-p)<200)
			color = Terrain(20) * texture2D(texture, t*4) * texture2D(DetailMap,t*300);//multitexture, noise map, detail map
		//else
		//	color = Terrain(20) * texture2D(texture, t*4)* texture2D(DetailMap,t*400/length(u_cameraPosition-p));
	}else if(u_shader == 3.0){
		color = Water();
	}
	if(color.a < 0.1)
		discard;//optimization
	if (u_shader == 2.0){
		gNormal = vec3(-2,-2,-2);//indicator not to add light for skybox
		color = color * Material[3];//skybox color
	}
	gColor = color;
}

vec4 SetMaterial(){
	vec4 ambient = Material[0];
	vec4 diffuse =  Material[1];
	vec4 specular = Material[2] * specular_intensity;
	return clamp(ambient + diffuse + specular, 0, 1);
}

vec4 Model(){
	//vec3 color = vec3(Material[1].xyz);
	vec4 clr = texture2D(texture, t);
	return clr;
}

vec4 Water(){
	float waveStrength = 0.06;
	vec2 uv = vec2(p.x, p.z) * 0.05;
	vec2 distortedTexCoord = texture(texture, vec2(uv.x+shininess,uv.y)).rg  * 0.1;
	distortedTexCoord = uv + vec2(distortedTexCoord.x,distortedTexCoord.y + shininess);
	vec2 totalDistortion = (texture(texture, distortedTexCoord).rg * 2.0 - 1.0) * waveStrength;
	
	vec2 ndc = (clipSpace.xy/clipSpace.w)/2 + 0.5;
	vec2 reflectTexCoord = vec2(ndc.x,-ndc.y);
	//vec2 reflectTexCoord = vec2(p.x,-p.y);
	reflectTexCoord += totalDistortion;
	reflectTexCoord.x = clamp(reflectTexCoord.x, 0.001,0.999);
	reflectTexCoord.y = clamp(reflectTexCoord.y, -0.999,-0.001);

	vec4 reflectColor = texture(multitexture[0], reflectTexCoord);
	
	vec4 tempOutputF = reflectColor;
	//return mix(tempOutputF, vec4(0.46,0.53,0.6,1),0.6);
	
	//ambient color 
	return mix(tempOutputF, Material[0],0.5);
}

vec4 Terrain(int tiling){
	int l_max = u_amountTextures;
	if(l_max > m_maxTextures)
		l_max = m_maxTextures;
	int i=0;
	vec4 l_result;
	float l_value[m_maxTextures];
	float l_total=0;
	for(i=0;i<l_max;i++){
		l_value[i] = 1/(abs(p.y-(minH[i]+maxH[i]/2)));
		l_total+=l_value[i];
		
	}
	for(i=0;i<l_max;i++){
		l_result+=(texture2D(multitexture[i-1],t*tiling)*(l_value[i]/l_total));
	}
	return (l_result);
}

vec4 blend(vec4 texture1, float a1, vec4 texture2, float a2)
{
    return texture1 * a1 + texture2 * a2;
}