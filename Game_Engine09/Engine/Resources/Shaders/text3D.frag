#version 330 core
 
uniform vec4 color;
uniform sampler2D texture;

in vec2 uv;
 
const float width = 0.5;
const float edge = 0.1;

out vec4 outputF;

void main()
{
	//float distance = 1.0 - texture2D(texture, uv).a;
	//float alpha = 1.0 - smoothstep(width, width + edge, distance);
	//outputF = vec4(color.xyz,alpha);
	outputF = texture2D(texture,uv)* color;
}