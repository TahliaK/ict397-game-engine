-- AI_WandererScript.lua
-- To control AI wanderers in a game
thisItemsName = "AI_Item1"

startPositionX = 0
startPositionY = 0
startPositionZ = 0

nextStopX = 0
nextStopY = 0
nextStopZ = 0

offset = 0
currentVelocity = 1
lowerLimitXZ = 0
upperLimitXZ = 500

function start()
	startPositionX = math.random(lowerLimitXZ, upperLimitXZ)
	startPositionZ = math.random(lowerLimitXZ, upperLimitXZ)
	
	--place gameobject in start position
	AI_Put(thisItemsName, startPositionX, 0, startPositionZ)
end

function update()
	-- update position with game's position
	startPositionX, startPositionY, startPositionZ = GetPositionOf(thisItemsName)
	
	-- if in position, choose next position
	if startPositionX == nextStopX
	then if startPositionZ == nextStopZ
		then nextPosition()
		end
	end
	
	-- wander
	BasicMove(thisItemsName, nextStopX, nextStopY, nextStopZ, currentVelocity, offset)
end

function nextPosition()
	nextStopX = math.random(lowerLimitXZ, upperLimitXZ);
	nextStopZ = math.random(lowerLimitXZ, upperLimitXZ);
end