#ifndef LUASCRIPT_H
#define LUASCRIPT_H
#include "Script.h"
extern "C" {
#include <lua.h>
#include <lualib.h>
#include <luaconf.h>
#include <lauxlib.h>
}

/**@class LuaScript
*@brief a Lua-specific Script class
*/
class LuaScript: public Script{
public:
	LuaScript();
	/**@brief Sets the filename of the associated script
	*@param fileName the filename
	*@returns true if successful (not invalid, too long, etc.)
	*@note This doesn't validate the file's existance and type
	*/
	bool SetFileName(boost::any fileName);
	
	/**@brief Gets the associated filename
	*@returns the string filename
	*/
	std::string GetFileName(void);
	void Start();
	void Update(); //inherited from Script.h
	void  Clear();
	
	/**@brief creates a new component pointer of this item type
	*@returns the pointer
	*@note see Component - this is inherited behaviour
	*/
	Component* NewComponent(void) const;
private:

	static int Multiply(int p_1, int p_2);

	static int cpp_Multiply(lua_State* p_state);
	static bool s_inList;
	void  Reflection();
	lua_State* m_state;
	std::string m_fileName;

	//Tahlia additions 14/5/17
	bool updatingScript;
	/* This runs a predefined function from the lua script that is called functionName
	It is confirmed safe if the function does not exist or if the function runs and terminates
	in an error. */
	int runBasicFunction(lua_State * p_state, std::string functionName);
};
#endif LUASCRIPT_H