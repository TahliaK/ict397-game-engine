#pragma once
#include <Windows.h>
typedef void(*PROCPTR)(WPARAM, LPARAM);

class WindowClass
{
public:
	WindowClass(HINSTANCE hInst, LPCSTR caption);
	void Show(int nCmdShow);
	void Update(void);
	static LRESULT CALLBACK WndProc(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam);
	WPARAM MessageLoop(void);
	HWND getHandle(void);
	int registerProcedure(WPARAM event, PROCPTR procedure);
	HINSTANCE getInstance(void);
	int getFreeID(void);
	long getWidth(void);
	long getHeight(void);
private:
	WNDCLASS wc;
	HWND hWnd;
	HINSTANCE hInst;
	LPSTR caption;
	int nextID;
};

