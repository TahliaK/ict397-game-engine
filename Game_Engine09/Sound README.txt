SDL_Sound Component

Sample rate: 22050
Bit depth: 16bit

Incorrect sample rate = playback sounds very weird.
Incorrect bit depth - load will fail.

Most sound files are 24+bits, SDL_Mixer is old so it only supports up to 16bit.
Convert your 24-bit sfx to 16bit with audacity or r8brain.