#version 330 core

uniform mat4 projection;
uniform mat4 model;
uniform mat4 view;

in vec3 position;

out vec3 texCoords;

void main(){
	mat4 view = mat4(mat3(view));
	vec4 pos = projection * view * vec4(position, 1.0);
	gl_Position =pos.xyww;
	texCoords = position;
}