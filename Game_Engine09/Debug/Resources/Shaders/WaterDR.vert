#version 330 core

in vec3 position;
in vec3 texCoord;
in vec3 normal;

out vec4 clipSpace;
out vec2 uv;
out vec3 toCameraVector;
out vec3 fromLightVector;

uniform mat4 proj;
uniform mat4 model;
uniform mat4 view;
uniform vec3 cameraPos;
uniform vec4 lightPosition;

const float tiling = .05;

out vec3 p;
out vec3 n;
out vec2 t;

void main(void){
	clipSpace = proj * view * model * vec4(position.x, position.y, position.z, 1);
	vec4 temp = proj * view * model * vec4(position.x, position.y, position.z, 1);
	gl_Position = temp;
	
	//uv = vec2(position.x/2 + 0.5, position.y/2 + 0.5) *tiling;
	uv = vec2(position.x, position.z) * tiling;
	toCameraVector = cameraPos - position;
	fromLightVector = position - lightPosition.xyz;


	vec4 worldPos = model* vec4(position,1);
	p = worldPos.xyz;
	t = texCoord.st;
	mat3 nMatrix = transpose(inverse(mat3(model)));
	n = nMatrix * normal;
}