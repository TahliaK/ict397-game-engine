#version 330 core

uniform mat4 model;

in vec3 position;
in vec3 texCoord;
in vec3 normal;

out vec2 uv;

void main(void){
	gl_Position = model * vec4(position.xy,0,1);
	uv = vec2(texCoord.st);
}


