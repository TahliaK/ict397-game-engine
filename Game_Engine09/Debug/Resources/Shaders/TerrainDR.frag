#version 330 core

uniform sampler2D detailMap;

uniform mat4 Material;//ambient, diffuse, specular, emission
uniform float shininess;
uniform float mTextureArraySize;
const int maxArraySize  = 10;
uniform sampler2D t0;
uniform sampler2D t1;
uniform sampler2D t2;
uniform sampler2D t3;
uniform sampler2D t4;
//uniform vec2 height[1];
uniform float minH1;
/*uniform float minH2;
uniform float minH3;
uniform float minH4;*/
//uniform float maxH0=10;
/*uniform float maxH1;
uniform float maxH2;
uniform float maxH3;
uniform float maxH4;*/
//uniform float minH[];
//uniform float maxH[];

const float specular_intensity = 0.5;

in vec3 p;
in vec3 n;
in vec2 t;

layout (location = 0) out vec3 gPosition;
layout (location = 1) out vec3 gNormal;
layout (location = 2) out vec4 gColor;


vec3 diffuseModel(vec3 pos,vec3 norm,vec3 diff);


vec4 ADSLightModel();
vec4 MultiTexturing();
vec3 blend(vec4, float, vec4, float);
void main()
{    
    gPosition = p;
	gNormal = normalize(n);
	vec3 color = vec3(Material[1].xyz);
	vec4 clr = MultiTexturing();
	if(clr.a < 0.1)
		discard;//optimization
	gColor = clr * ADSLightModel();
}

vec4 MultiTexturing(){	
	{
		return texture2D(t1,t);
	}//else
		return texture2D(t1,t);
	/*if(minH0 < p.y && maxH0 > p.y){
		return texture2D(t0,t);
	}else
		return texture2D(t1,t);*/
	
	return texture2D(t1,t);
}

vec3 blend(vec4 texture1, float a1, vec4 texture2, float a2)
{
    return texture1.rgb * a1 + texture2.rgb * a2;
}

vec4 ADSLightModel(){
	vec4 ambient = Material[0];
	vec4 diffuse =  Material[1];
	vec4 specular = Material[2] * specular_intensity;
	return clamp(ambient + diffuse + specular, 0, 1);
}