#version 330 core
uniform sampler2D texture;
uniform sampler2D noise;
uniform mat4 Material;
//uniform sampler2D shadow;

in vec2 uv;
in float attenuation;
in vec4 color;

out vec4 outputF;

void main(){
	//outputF = texture2D(texture,uv) * textureColor;

	//outputF = vec4(color * vec3(noise3(1)),1);
	outputF = texture2D(texture,uv) * color;
	//outputF =  color;
	//outputF = vec4(color,1);
}