#version 330 core

in vec3 position;
in vec3 texCoord;

out vec4 clipSpace;
out vec2 uv;
out vec3 toCameraVector;
out vec3 fromLightVector;

uniform mat4 proj;
uniform mat4 model;
uniform mat4 view;
uniform vec3 cameraPos;
uniform vec3 lightPosition;

const float tiling = .05;

void main(void){
	vec4 worldPos = model * vec4(position,1);
	clipSpace = proj * view * worldPos;
	gl_Position = clipSpace;
	uv = vec2(position.x, position.z) * tiling;

	//uv = vec2(position.x/2 + 0.5, position.y/2 + 0.5) *tiling;
	toCameraVector = cameraPos - worldPos.xyz;
	fromLightVector = position - lightPosition;
}