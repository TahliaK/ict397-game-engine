#version 330 core


const int MAX_LIGHTS = 1;

uniform vec4 lightPosition[MAX_LIGHTS];
uniform mat4 proj;
uniform	mat4 model;
uniform mat4 view;
uniform vec3 cameraPos;
//uniform float fps;
uniform mat4 Light;//a,d,s,e

const float shininess = 32;
const float specular_intensity = 0.5;

in vec3 position;
in vec3 normal;
in vec3 texCoord;

out vec2 uv;
out float attenuation;
out vec4 color;


void DeterminePosition();
void SendToFragmentShader();
vec4 ADSLightModel();


void main(){
	DeterminePosition();
	SendToFragmentShader();
}

void DeterminePosition(){

//	v.z = sin(1.0 * v.z + fps * 0.1);
	//v.z +=fps*5;
	gl_Position = proj * view * model*vec4(position,1);
}

void SendToFragmentShader(){
	if(position.y>5.5)
		color = vec4(position.y/15,position.y/15,position.y/15,1);
	else if(position.y > 3)
		color = vec4(position.y/10,position.y/7,position.y/15,1);
	else if(position.y > 0.5)
		color = vec4(position.y/5, position.y/5,position.y/10,1);
	else
		color = vec4(position.y/3,position.y/1,position.y/2,1);

	uv = texCoord.st;
	color *= ADSLightModel();
	//attenuation = exp(-9 * distance(gl_Position, light_position) * distance(gl_Position, light_position));
}


///Gouruad lighting model
vec4 ADSLightModel(){
	vec4 light_position = lightPosition[0];
	vec4 l_lightDir = normalize(light_position - (view * model* vec4(position,1)));//model only for position
	vec4 l_viewDir = vec4(normalize(vec3(vec4(cameraPos,1) - (view * model* vec4(position,1))).xyz ),1);//view direction
	mat3 l_normal_matrix = mat3(transpose(inverse(model)));//when non-uniform operations are applied to objects which change their normals, should be calculated on the CPU 
	vec4 l_normal = vec4(normalize(l_normal_matrix * normal),1);
	vec4 l_reflectDir = reflect(-l_lightDir, l_normal);//reflection direction
	

	vec4 ambient = Light[0];
	vec4 diffuse = max(0, dot(l_normal,l_lightDir)) * Light[1];
	vec4 specular = vec4(0,0,0,0);
	if(dot(l_lightDir,l_viewDir) > 0){
		specular = pow( max(0, dot(l_viewDir,l_reflectDir) ), shininess) * Light[2] * specular_intensity;
	}

	return clamp(ambient + diffuse + specular, 0, 1);
}