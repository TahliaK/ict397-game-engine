#include "ScriptFunctions.h"
#include "BlackWhale/Transform.h"
#include "BlackWhale/ModelManager.h"
#include "BlackWhale/MeshRenderer.h"
#include "BlackWhale/Physics/CollisionEngine.h"
#include "BlackWhale/SceneManager.h"
#include "BlackWhale/Time.h"


ScriptFunctions::ScriptFunctions(void)
{
}


ScriptFunctions::~ScriptFunctions(void)
{
}


GameObject * ScriptFunctions::make3DObject(std::string name, bool isVisible, bool isStatic, Vector3 pos, Vector3 rot, Vector3 scale)
{
	GameObject * go = new GameObject();
	go->SetVisible(isVisible);
	go->SetStatic(isStatic);
	
	Transform * t = go->GetTransform();
	t->position = pos; t->rotation = rot; t->scale = scale;

	return go;
}

bool ScriptFunctions::load3DModel(GameObject * go, std::string filename)
{
	bool success = true;
	try
	{
		go->AddComponent(new MeshRenderer(0, filename)); //check with Olesia - is shader int important?? What does it mean??
	}
	catch(std::exception e)
	{
		success = false;
	}
	
	return success;
}

bool ScriptFunctions::set3DCollisions(GameObject * go, bool hasCollisions, Vector3 collisionMin, Vector3 collisionMax)
{
	bool success = true;
	try{
		if(hasCollisions) //this allows you to add multiple colliders to the one object
		{
			ColliderAABB * col = new ColliderAABB(collisionMin, collisionMax);
			col->SetGameObject(go);
			CollisionEngine::AddCollider(col);
		}
		else
		{
			//don't really know how to remove a collider here
		}
	}
	catch(std::exception e)
	{
		success = false;
	}

	return success;
}

bool ScriptFunctions::setObjectFeature(GameObject * go, objectFeatures f, bool state)
{
	bool success = true;
	Vector3 n(0);
	switch(f)
	{
	case VISIBLE:
		go->SetVisible(state);
		break;
	case STATIC:
		go->SetStatic(state);
		break;
	case COLLISION:
		set3DCollisions(go, false, n, n);
		break;
	default:
		success = false;
	}	

	return success;
}

bool ScriptFunctions::moveObject(GameObject * go, Vector3 direction, Vector3 rotation) //error checking incomplete, always returns true
{
	bool success = true;
	Transform * t = go->GetTransform();
	t->position = t->position + direction;
	t->rotation = t->rotation + rotation;
	
	return success;
}

bool ScriptFunctions::destroy3DObject(GameObject * go)
{
	if(go != NULL)
	{
		delete go;
		return true;
	}
	else
	{
		return false;
	}
}

/// UI SCRIPTING FUNCTIONS HERE


///OBJECT MANAGEMENT
bool ScriptFunctions::areObjectsColliding(GameObject * g1, GameObject * g2)
{
	//Can't do unless I store which gameobject is with which

	return false;
} //incomplete

//Camera creation and editing functions

Camera * ScriptFunctions::createPerspectiveCamera(float p_fov, float p_aspect, float p_near, float p_far, Vector3 pos, Vector3 at, Vector3 up)
{
	Camera * cam = new Camera();
	editCameraPerspective(*cam, p_fov, p_aspect, p_near, p_far, pos, at, up);

	return cam;
}

bool ScriptFunctions::editCameraPerspective(Camera & c, float p_fov, float p_aspect, float p_near, float p_far, Vector3 pos, Vector3 at, Vector3 up)
{
	c.GetProjectionMatrix() = Camera::Perspective(p_fov, p_aspect, p_near, p_far);
	c.GetViewMatrix() = Camera::LookAt(pos, at, up);

	return true; 
} //error checking incomplete //error checking incomplete

Camera * ScriptFunctions::createOrthoCamera(float p_left, float p_right, float p_bottom, float p_top, float p_near, float p_far, Vector3 pos, Vector3 at, Vector3 up)
{
	Camera* c = new Camera();
	editCameraOrtho(*c, p_left, p_right, p_bottom, p_top, p_near, p_far, pos, at, up);

	return c;
}

bool ScriptFunctions::editCameraOrtho(Camera &c, float p_left, float p_right, float p_bottom, float p_top, float p_near, float p_far, Vector3 pos, Vector3 at, Vector3 up)
{
	c.GetProjectionMatrix() = Camera::Ortho(p_left, p_right, p_bottom, p_top, p_near, p_far);
	c.GetViewMatrix() = Camera::LookAt(pos, at, up);

	return true; 
} //error checking incomplete

//Script functions for camera movement
void ScriptFunctions::moveCamera(Camera * c, Vector3 direction, Vector3 rotation)
{
	c->Rotate(rotation);
	c->Move(direction);
}

bool movementWillCollide(Camera * c, Vector3 direction, Vector3 rotation, GameObject & go){ return false; } //incomplete
bool movementWillCollide(Camera * c, Vector3 direction, Vector3 rotation, SceneManagement::Scene s){ return false; } //incomplete

//script functions for input registration
void ScriptFunctions::mouseChangeVec(float &x, float &y)
{
	Vector2 current = Input::GetMousePosition();
	Vector2 previous = Input::GetPrevMousePosition();

	x = current.x - previous.x;
	y = current.y - previous.y;
}
void ScriptFunctions::mouseLocation(float &x, float &y)
{
	Vector2 v = Input::GetMousePosition();
	x = v.x; y = v.y;
}
bool ScriptFunctions::keyPressed(enum KeyCode k)
{
	bool pressed = false;
	if(Input::GetKeyDown(k))
	{
		pressed = true;
	}
	return pressed;
}


//Time monitoring controls
float ScriptFunctions::timeDif()
{
	return Time::GetDeltaTime();
}

//Scene management scripting
int ScriptFunctions::newScene(std::string sceneName)
{
	return SceneManagement::SceneManager::CreateScene(sceneName);
}

int ScriptFunctions::loadScene(std::string sceneName)
{
	return SceneManagement::SceneManager::LoadScene(sceneName);
}

bool ScriptFunctions::swapSceneTo(int sceneIndex)
{
	if(sceneIndex < SceneManagement::SceneManager::GetTotalAmount() 
		&& sceneIndex >= 0)
	{
		SceneManagement::SceneManager::SetActive(sceneIndex);
		return true;
	}

	return false;

}