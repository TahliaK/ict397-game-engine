#version 330 core

uniform sampler2D texture;
uniform mat4 Material;//ambient, diffuse, specular, emission
uniform mat4 Light;//a,d,s,e
uniform float shininess;

uniform vec3 lightDirection;
uniform float lightCutOff;

const float specular_intensity = 0.5;

//uniform sampler2D shadow;

in vec2 uv;
in float atten;

in ToFrag{
	vec3 normal;
	vec4 viewDir;

	vec4 reflectDir;
	vec4 lightDir;
}FragIn;

out vec4 outputF;

vec4 ADSLightModel();

void main(){
	vec4 textureColor = ADSLightModel();
	textureColor.xyz *= atten;
	float theta = dot(vec4(lightDirection,0), FragIn.lightDir);
	if(theta > lightCutOff){
		outputF = texture2D(texture,uv) * textureColor;
	}else{
		outputF = texture2D(texture,uv) * textureColor;
	}
}

///Phong lighting model
vec4 ADSLightModel(){
	vec4 ambient = Material[0] * Light[0];
	vec4 diffuse = max(0, dot(vec4(normalize(FragIn.normal),1),FragIn.lightDir)) * Material[1] * Light[1];
	vec4 specular = vec4(0,0,0,0);
	if(dot(FragIn.lightDir,FragIn.viewDir) > 0){
		specular = pow( max(0, dot(FragIn.viewDir,FragIn.reflectDir)), shininess) * Material[2] * Light[2] * specular_intensity;
	}

	return clamp(ambient + diffuse + specular, 0, 1);
}