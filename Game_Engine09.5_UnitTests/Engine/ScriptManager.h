#ifndef SCRIPTMANAGER_H
#define SCRIPTMANAGER_H
#include <vector>
#include "lua.h"
#include "LuaScriptFunctions.h"

/**@class ScriptManager
*@brief a Singleton class to manage scripts. Abstract.
*/
class ScriptManager
{
public:
	ScriptManager(); //c-tor
	//ScriptManager(const ScriptManager& sm); //copy c-tor
	virtual ~ScriptManager(); //d-tor

	virtual bool loadScript(std::string filename) = 0;
	virtual bool runScript() = 0;
	virtual bool loadRunScript(std::string filename) = 0;

	virtual bool updateScripts() = 0;
	virtual bool initialiseScripts() = 0;
	virtual bool closeScripts() = 0;

protected:
	/* This isn't strictly necessary but it gives the opportunity for
	dynamically allocated memory to be stored backed-up in this class
	so it can be automatically released in case the script stuffs up
	memory management somehow.*/
	std::vector<void *> memoryItems;

};

/**@class LuaScriptManager
*@brief A singleton class to manage Lua Scripts
*/
class LuaScriptManager : public ScriptManager
{
public:
	LuaScriptManager();
	~LuaScriptManager();

	virtual bool loadScript(std::string filename);
	virtual bool runScript();
	virtual bool loadRunScript(std::string filename);

	virtual bool updateScripts();
	virtual bool initialiseScripts();
	virtual bool closeScripts();

private:
	lua_State * state;

	void registerEngineFunctions();
	void registerLuaFunctions();

	//static because Lua can't overwrite things that aren't static
	static void update() {}; //to be overwritten by a script
	static void initialise() {}; // to be overwritten by a script
	static void close() {}; //to be overwritten by a script


};

#endif