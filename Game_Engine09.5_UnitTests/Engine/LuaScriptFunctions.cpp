#include "LuaScriptFunctions.h"
#include <boost/any.hpp>
#include "BlackWhale/MeshRenderer.h"
#include "BlackWhale/ModelManager.h"

//Keywords to use in Lua Scripts
#define SCRIPT_VISIBLE "OBJECT_VISIBLE"
#define SCRIPT_STATIC "OBJECT_STATIC"
#define SCRIPT_COLLISION "OBJECT_COLLISION"
//keycodes
#define SCRIPT_KEY_BACK "BACKSPACE_KEY"
#define SCRIPT_KEY_DELETE "DELETE_KEY"
#define SCRIPT_KEY_TAB "TAB_KEY"
#define SCRIPT_KEY_ESC "ESC_KEY"
#define SCRIPT_KEY_SPACE "SPACE_KEY"
#define SCRIPT_KEY_UP "DIRECTION_UP_KEY"
#define SCRIPT_KEY_LEFT "DIRECTION_LEFT_KEY"
#define SCRIPT_KEY_DOWN "DIRECTION_DOWN_KEY"
#define SCRIPT_KEY_RIGHT "DIRECTION_RIGHT_KEY"
#define SCRIPT_KEY_PGDOWN "PAGE_DOWN_KEY"
#define SCRIPT_KEY_PGUP "PAGE_UP_KEY"
#define SCRIPT_KEY_INSERT "INSERT_KEY"
#define SCRIPT_ALT_KEY_LFT "ALT_KEY_LEFT"
#define SCRIPT_ALT_KEY_RT "ALT_KEY_RIGHT"
#define SCRIPT_ALT_KEY	"ALT_KEY"
#define SCRIPT_SHIFT_KEY_LFT "SHIFT_KEY_LEFT"
#define	SCRIPT_SHIFT_KEY_RT "SHIFT_KEY_RIGHT"
#define SCRIPT_SHIFT_KEY "SHIFT_KEY"
#define SCRIPT_HOME_KEY	"HOME_KEY"
#define SCRIPT_END_KEY "END_KEY"
#define SCRIPT_LEFT_MOUSE "L_MOUSECLICK"
#define SCRIPT_RIGHT_MOUSE "R_MOUSECLICK"
#define SCRIPT_MIDDLE_MOUSE "MID_MOUSECLICK"


int LuaScriptFunctions::make3DObject(lua_State * L)
{
	//get user input from stack
	//item 1: name, item 2: is visible, item 3: is static, items 4-6: pos, items 7-9: rot, items 10-12 scale

	int paramNum = lua_gettop(L);
	int numReturns = 0;
	bool failed = false;

	if (paramNum < 12)
	{
		lua_pushstring(L, "Error: Not enough params"); 
		numReturns++; 
		failed = true;
	}
	else
	{
		GameObject go;
		Vector3 pos, rot, scale;
		const char * name;
		bool visible = false, goStatic = false;
		double xpos = 0, ypos = 0, zpos = 0, xrot = 0, yrot = 0, zrot = 0, xscale = 0, yscale = 0, zscale = 0;

		for (int i = 0; i < 12; i++)
		{
			if(failed)
				break;

			switch (i)
			{
			case 0: //name
				failed = !popFromStack(L, name);
				go.SetName(std::string(name));
				break;
			case 1: //is Vsisible
				failed = !popFromStack(L, &visible);
				go.SetVisible(visible);
				break;
			case 2: //is static
				failed = !popFromStack(L, &goStatic);
				go.SetStatic(goStatic);
				break;
			case 3: //position
				failed = !popFromStack(L, &xpos); //xpos used as double needed, while Vector3 may use float
				pos.x = xpos;
				break;
			case 4:
				failed = !popFromStack(L, &ypos);
				pos.y = ypos;
				break;
			case 5:
				failed = !popFromStack(L, &zpos);
				pos.z = zpos;
				go.GetTransform()->position = pos;
				break;
			case 6: //rotation
				failed = !popFromStack(L, &xrot);
				rot.x = xrot;
				break;
			case 7:
				failed = !popFromStack(L, &yrot);
				rot.y = yrot;
				break;
			case 8:
				failed = !popFromStack(L, &zrot);
				rot.z = zrot;
				go.GetTransform()->rotation = rot;
				break;
			case 9: //scale
				failed = !popFromStack(L, &xscale);
				scale.x = xscale;
				break;
			case 10:
				failed = !popFromStack(L, &yscale);
				scale.y = yscale;
				break;
			case 11:
				failed = !popFromStack(L, &zscale);
				scale.z = zscale;
				go.GetTransform()->scale = scale;
				break;
			} //endswitch

			if(failed){ numReturns++;}
			if(failed) break;

		} //endfor

		if(!failed)
		{
			pushToStack(L, go); //push game object onto the stack
			numReturns++;
		}
	}//endelse

	return numReturns; //return number of items pushed onto stack

}

int LuaScriptFunctions::load3DModel(lua_State * L)
{
	int numReturns = 0;
	int paramNum = lua_gettop(L);

	if(paramNum < 2)
	{
		pushToStack(L, "Error: Not enoguh params");
		numReturns++;
	}
	else
	{
		try //get user input
		{
			GameObject * go = boost::any_cast<GameObject *>(popFromStack(L));
			const char * filename = boost::any_cast<const char *>(popFromStack(L));

			//Create the MeshRenderer with the model associated with go
			MeshRenderer meshRen;
			if(!meshRen.SetMeshFile(std::string(filename)))
			{
				pushToStack(L, "Error: Obj. Filename may be invalid.");
				numReturns++;
			}
			else
			{
				meshRen.SetGameObject(go);
			}

			//Register the MeshRenderer as required (???)

		}
		catch(boost::bad_any_cast)
		{
			pushToStack(L, "Error: Incorrect param format");
			numReturns++;
		}
	}
	return numReturns;
}

int LuaScriptFunctions::set3DCollisions(lua_State *L)
{
	int numReturns = 0, numLoops = 0;
	bool failed = false;
	GameObject * go;
	bool hasCollisions;
	double cMinX = 0, cMinY = 0, cMinZ = 0, cMaxX = 0, cMaxY = 0, cMaxZ = 0;

	if(!hasEnoughParams(L, 8))
	{
		return 1;
	}

	do
	{
		switch(numLoops)
		{
		case 0:
			failed = !popFromStack(L, go);
			break;
		case 1:
			failed = !popFromStack(L, &hasCollisions);
			break;
		case 2:
			failed = !popFromStack(L, &cMinX);
			break;
		case 3:
			failed = !popFromStack(L, &cMinY);
			break;
		case 4:
			failed = !popFromStack(L, &cMinZ);
			break;
		case 5:
			failed = !popFromStack(L, &cMaxX);
			break;
		case 6:
			failed = !popFromStack(L, &cMaxY);
			break;
		case 7:
			failed = !popFromStack(L, &cMaxZ);
			break;
		case 8:
			if(!ScriptFunctions::set3DCollisions(go, hasCollisions, Vector3(cMinX, cMinY, cMinZ), Vector3(cMaxX, cMaxY, cMaxZ)))
			{
				pushToStack(L, "Error: Collision set failure.");
				numReturns++;
			}
		}

		numLoops++;

	}while(!failed && numLoops < 9);
	
	failed ? numReturns++ : pushToStack(L, failed);

	return numReturns;
}

int LuaScriptFunctions::setObjectFeature(lua_State *L)
{
	int numReturns = 0;
	GameObject * go;
	char * feature;
	bool state;

	bool failed = !popFromStack(L, go);
	if(!failed){ failed = !popFromStack(L, feature); }
	if(!failed){ failed = !popFromStack(L, &state); }

	if(failed){numReturns++;}
	else
	{
		std::string stringinput(feature);

		if(stringinput == SCRIPT_VISIBLE)
		{
			if (!ScriptFunctions::setObjectFeature(go, VISIBLE, state))
			{
				failed = true;
			}
		}
		else if(stringinput == SCRIPT_COLLISION)
		{
			if (!ScriptFunctions::setObjectFeature(go, COLLISION, state))
			{
				failed = true;
			}
		}
		else if(stringinput == SCRIPT_STATIC)
		{
			if (!ScriptFunctions::setObjectFeature(go, STATIC, state))
			{
				failed = true;
			}
		}
		else
		{
			const char * error = "Error: Object feature not recognised.";
			pushToStack(L, error);
			numReturns++;
		}

		if (failed)
		{
			const char * error = "Error: Feature change failure.";
			pushToStack(L, error);
			numReturns++;
		}
	}

	return numReturns;
}

int LuaScriptFunctions::moveObject(lua_State *L)
{
	GameObject * go;
	double dir[3] = {0, 0, 0}, rot[3] = {0, 0, 0};
	bool failed = false;
	int numReturns = 0, loopNum = 0;

	do{
		switch(loopNum)
		{
		case 0:
			failed = !popFromStack(L, go);
			break;
		case 1:
		case 2:
		case 3:
			failed = !popFromStack(L, &dir[loopNum-1]);
			break;
		case 4:
		case 5:
		case 6:
			failed = !popFromStack(L, &rot[loopNum-5]);
			break;
		case 7:
			if(!ScriptFunctions::moveObject(go, Vector3(dir[0], dir[1], dir[2]), Vector3(rot[0], rot[1], rot[2])))
			{
				pushToStack(L, "Error: Object movement failed.");
				numReturns++;
			}
		}
	}while(!failed && loopNum < 8);

	if(failed){numReturns++;}

	return numReturns;
}

int LuaScriptFunctions::destroy3Dobject(lua_State *L)
{
	GameObject * go;
	bool failed = false;
	int numReturns = 0;

	failed = !popFromStack(L, go);
	if(failed){numReturns++;}
	else
	{
		if(go != nullptr)
		{
			delete go;
			go = nullptr;
		}
		else
		{
			pushToStack(L, "Error: GameObject has already been deleted.");
			numReturns++;
		}
	}

	return numReturns;
}

// Object Collision function

int LuaScriptFunctions::areObjectsColliding(lua_State * L)
{
	GameObject * go1, * go2;
	bool failed = false;
	int numReturns = 0;

	failed = !popFromStack(L, go1);
	if(!failed) { failed = !popFromStack(L, go2); }

	if(!failed) //both objects retrieved
	{
		if(ScriptFunctions::areObjectsColliding(go1, go2))
		{
			bool success = true;
			if(!pushToStack<bool>(L, success))
			{
				numReturns++;
			}
		}
	}
	else
	{
		numReturns++; //bcos popFromStack puts an error msg on stack if it fails
	}

	return numReturns;

}

// Camera functions

int LuaScriptFunctions::createPerspectiveCamera(lua_State *L)
{
	bool failed = false; int numReturns = 0;
	//10 items needed - fov, aspect, near, far, position, atvec, upvec
	double fov = 0, aspect = 0, near = 0, far = 0;
	double pos[3] = {0, 0, 0}, at[3] = {0, 0, 0}, up[3] = {0, 0, 0};

	if(hasEnoughParams(L, 13))
	{
		int numLoops = 0;
		do
		{
			switch(numLoops)
			{
			case 0:
				failed = !popFromStack(L, &fov);
				break;
			case 1:
				failed = !popFromStack(L, &aspect);
				break;
			case 2:
				failed = !popFromStack(L, &near);
				break;
			case 3:
				failed = !popFromStack(L, &far);
				break;
			case 4:
			case 5:
			case 6:
				failed = !popFromStack(L, &pos[numLoops-4]);
				break;
			case 7:
			case 8:
			case 9:
				failed = !popFromStack(L, &at[numLoops-7]);
				break;
			case 10:
			case 11:
			case 12:
				failed = !popFromStack(L, &up[numLoops-10]);
			default: //all params have been retrieved by here
				Camera * c = ScriptFunctions::createPerspectiveCamera(fov, aspect, near, far,
					Vector3(pos[0], pos[1], pos[2]), Vector3(at[0], at[1], at[2]),
					Vector3(up[0], up[1], up[2]));
				if (!pushToStack(L, c))
				{
					pushToStack(L, "Error: Stack failure");
				}
				numReturns++;
			}

			numLoops++;
		} while(!failed && numLoops < 10);
	}
	else
	{
		numReturns++;
	}

	return numReturns;
}

int LuaScriptFunctions::editCameraPerspective(lua_State *L)
{
	bool failed = false;
	Camera * camptr, * newCam;
	int numReturns = 0;

	failed = !popFromStack(L, camptr);

	numReturns += createPerspectiveCamera(L);

	failed = !popFromStack(L, newCam);
	numReturns--;
	if(failed)
	{
		pushToStack(L, "Error: Invalid camera parameters.");
		numReturns++;
	}
	else
	{
		*camptr = *newCam; //first object (dereferenced) = second object (dereferenced)
		//no return - using pointers, so it should be fine ... ??
	}

	return numReturns;
}

int LuaScriptFunctions::createOrthoCamera(lua_State *L)
{
	//all numbers so lets go with this for camera parameters:
	bool failed = false; int numReturns = 0;
	double camValues[6] = {0, 0, 0, 0, 0, 0};
	double pos[3] = {0, 0, 0}, at[3] = {0, 0, 0}, up[3] = {0, 0, 0};

	if(hasEnoughParams(L, 15))
	{
		for(int i = 0; i < 6; i++)
		{
			if(failed)
				break;

			failed = !popFromStack(L, &camValues[i]);
			if(failed){ numReturns++;}
		}

		int state = 0;
		for(int i = 0; i < 3; i++)
		{
			if(failed)
				break;

			switch(state)
			{
			case 0:
				failed = !popFromStack(L, &pos[i]);
				if(i == 2)
				{
					i = -1; state++;
				}
				break;
			case 1:
				failed = !popFromStack(L, &at[i]);
				if(i == 2)
				{
					i = -1; state++;
				}
				break;
			case 2:
				failed = !popFromStack(L, &up[i]);
				break;
			}

			if(failed){ numReturns++; }
		}

		if(numReturns == 0) //if no errors so far
		{
			Camera * c = ScriptFunctions::createOrthoCamera(camValues[0], camValues[1], camValues[2],
				camValues[3], camValues[4], camValues[5], Vector3(pos[0], pos[1], pos[2]),
				Vector3(at[0], at[1], at[2]), Vector3(up[0], up[1], up[2]));

			pushToStack(L, c);
			numReturns++;
		}
	}
	else
	{
		numReturns++;
	}
	return numReturns;
}

int LuaScriptFunctions::editCameraOrtho(lua_State *L)
{
	bool failed = false;
	Camera * camptr, * newCam;
	int numReturns = 0;

	failed = !popFromStack(L, camptr);

	numReturns += createOrthoCamera(L);

	failed = !popFromStack(L, newCam);
	numReturns--;
	if(failed)
	{
		pushToStack(L, "Error: Invalid camera parameters.");
		numReturns++;
	}
	else
	{
		*camptr = *newCam; //first object (dereferenced) = second object (dereferenced)
		//no return - using pointers, so it should be fine ... ??
	}

	return numReturns;
}

//Camera movement functions

int LuaScriptFunctions::moveCamera(lua_State *L){ return 0; } //not done
int LuaScriptFunctions::movementWillCollide(lua_State *L) { return 0; } //not done
int LuaScriptFunctions::movementWillCollideScene(lua_State *L) { return 0;} //not done

// Input functions

int LuaScriptFunctions::mouseChangeVec(lua_State *L)
{
	float x, y;
	ScriptFunctions::mouseChangeVec(x, y);

	pushToStack(L, x);
	pushToStack(L, y);

	return 2; //hardcoded because it will literally always only ever be 2
}

int LuaScriptFunctions::mouseLocation(lua_State *L)
{
	float x, y;
	ScriptFunctions::mouseLocation(x, y);

	pushToStack(L, x);
	pushToStack(L, y);

	return 2; //again, hardcoded bcos lazy
}

int LuaScriptFunctions::keyPressed(lua_State *L)
{
	bool failed = false; int numReturns = 0;
	if(hasEnoughParams(L, 1))
	{
		char * keyCode;
		failed = !popFromStack(L, keyCode);
		if(failed)
		{
			numReturns++;
		}
		else
		{
			std::string s(keyCode);
			enum KeyCode k;
			//check standard letters
			if(s == "q") k = q_KEY; else if(s == "Q") k = Q_KEY;
			else if(s == "w") k = w_KEY; else if(s == "W") k = W_KEY;
			else if(s == "e") k = e_KEY; else if (s == "E") k = E_KEY;
			else if(s == "r") k = r_KEY; else if (s == "R") k = R_KEY;
			else if(s == "t") k = t_KEY; else if (s == "T") k = T_KEY;
			else if(s == "y") k = y_KEY; else if (s == "Y") k = Y_KEY;
			else if(s == "u") k = u_KEY; else if (s == "U") k = U_KEY;
			else if(s == "i") k = i_KEY; else if (s == "I") k = I_KEY;
			else if(s == "o") k = o_KEY; else if (s == "O") k = O_KEY;
			else if(s == "p") k = p_KEY; else if (s == "P") k = P_KEY;
			else if(s == "a") k = a_KEY; else if (s == "A") k = A_KEY;
			else if(s == "s") k = s_KEY; else if (s == "S") k = S_KEY;
			else if(s == "d") k = d_KEY; else if (s == "D") k = D_KEY;
			else if(s == "f") k = f_KEY; else if (s == "F") k = F_KEY;
			else if(s == "g") k = g_KEY; else if (s == "G") k = G_KEY;
			else if(s == "h") k = h_KEY; else if (s == "H") k = H_KEY;
			else if(s == "j") k = j_KEY; else if (s == "J") k = J_KEY;
			else if(s == "k") k = k_KEY; else if (s == "K") k = K_KEY;
			else if(s == "l") k = l_KEY; else if (s == "L") k = L_KEY;
			else if(s == "z") k = z_KEY; else if (s == "Z") k = Z_KEY;
			else if(s == "x") k = x_KEY; else if (s == "X") k = X_KEY;
			else if(s == "c") k = c_KEY; else if (s == "C") k = C_KEY;
			else if(s == "v") k = v_KEY; else if (s == "V") k = V_KEY;
			else if(s == "b") k = b_KEY; else if (s == "B") k = B_KEY;
			else if(s == "n") k = n_KEY; else if (s == "N") k = N_KEY;
			else if(s == "m") k = m_KEY; else if (s == "M") k = M_KEY;
			//check non-standard letters
			else if(s == SCRIPT_KEY_BACK) k = BACKSPACE_KEY;
			else if(s == SCRIPT_KEY_DELETE) k = DELETE_KEY;
			else if(s == SCRIPT_KEY_TAB) k = TAB_KEY;
			else if(s == SCRIPT_KEY_ESC) k = ESCAPE_KEY;
			else if(s == SCRIPT_KEY_SPACE) k = SPACE_KEY;
			else if(s == SCRIPT_KEY_UP) k = UP_KEY;
			else if(s == SCRIPT_KEY_LEFT) k = LEFT_KEY;
			else if(s == SCRIPT_KEY_DOWN) k = DOWN_KEY;
			else if(s == SCRIPT_KEY_RIGHT) k = RIGHT_KEY;
			else if(s == SCRIPT_KEY_PGDOWN) k = PAGE_DOWN_KEY;
			else if(s == SCRIPT_KEY_PGUP) k = PAGE_UP_KEY;
			else if(s == SCRIPT_KEY_INSERT) k = INSERT_KEY;
			else if(s == SCRIPT_ALT_KEY_LFT) k = ALT_LEFT_KEY;
			else if(s == SCRIPT_ALT_KEY_RT) k = ALT_RIGHT_KEY;
			else if(s == SCRIPT_ALT_KEY) k = ALT_LEFT_KEY; //need to handle properly
			else if(s == SCRIPT_SHIFT_KEY_LFT) k = SHIFT_LEFT_KEY;
			else if(s == SCRIPT_SHIFT_KEY_RT) k = SHIFT_RIGHT_KEY;
			else if(s == SCRIPT_SHIFT_KEY) k = SHIFT_LEFT_KEY; //need to handle properly
			else if(s == SCRIPT_HOME_KEY) k = HOME_KEY;
			else if(s == SCRIPT_END_KEY) k = END_KEY;
			else if(s == SCRIPT_LEFT_MOUSE) k = LEFT_BUTTON;
			else if(s == SCRIPT_RIGHT_MOUSE) k = RIGHT_BUTTON;
			else if(s == SCRIPT_MIDDLE_MOUSE) k = MIDDLE_BUTTON;
			//if unrecognised
			else k = NONE;


			switch(k)
			{
			case NONE:
				pushToStack(L, "Error: Keyword unrecognised.");
				numReturns++;
				break;
			default:
				bool b = ScriptFunctions::keyPressed(k);
				pushToStack(L, b);
				numReturns++;
			}
		}

	}
	return numReturns;
}

int LuaScriptFunctions::timeDif(lua_State * L)
{
	int numReturns = 0;
	float f = ScriptFunctions::timeDif();
	if (!pushToStack(L, f))
	{
		//will almost certainly never be triggered
		const char * error = "Error: Some kind of crazy pointer failure in the Lua stack";
		pushToStack(L, error);
	}

	numReturns++;
	return numReturns;
}


int LuaScriptFunctions::newScene(lua_State * L)
{
	int numReturns = 0, sceneID = -1;
	const char * sceneName;
	if (!popFromStack(L, sceneName))
	{
		numReturns++;
	}
	else
	{
		sceneID = ScriptFunctions::newScene(std::string(sceneName));
		pushToStack(L, sceneID);
		numReturns++;
	}

	return numReturns;
}

int LuaScriptFunctions::loadScene(lua_State * L)
{
	int numReturns = 0, sceneID = -1;
	const char * sceneName;

	if (!popFromStack(L, sceneName))
	{
		numReturns++;
	}
	else
	{
		sceneID = ScriptFunctions::loadScene(std::string(sceneName));
		pushToStack(L, sceneID);
		numReturns++;
	}

	return numReturns;
}

int LuaScriptFunctions::swapSceneTo(lua_State * L)
{
	int numReturns = 0, sceneID = -1;

	if (!popFromStack(L, &sceneID))
	{
		numReturns++;
	}
	else
	{
		bool b = ScriptFunctions::swapSceneTo(sceneID);
		if (!b)
		{
			const char * error = "Error: Scene swap unsuccessful."; 
			pushToStack(L, error);
			numReturns++;
		}
	}

	return numReturns;
}

boost::any LuaScriptFunctions::popFromStack(lua_State *L)
{
	int itemIndex = 1;
	int i = lua_type(L, itemIndex);
	boost::any returnval;
	switch (i)
	{
	case LUA_TSTRING:
		returnval = lua_tostring(L, itemIndex);
		break;
	case LUA_TBOOLEAN:
		returnval = lua_toboolean(L, itemIndex);
		break;
	case LUA_TNUMBER:
		returnval = lua_tonumber(L, itemIndex);
		break;
	case LUA_TFUNCTION:
		returnval = lua_tocfunction(L, itemIndex);
		break;
	case LUA_TLIGHTUSERDATA:
		returnval = lua_touserdata(L, itemIndex);
		break;
	default:
		returnval = nullptr;
	}

	lua_pop(L, itemIndex);

	return returnval;
}