#include "Debug.h"
#include "Master.h"
#include <iostream>

using namespace std;

void Debug::RunGeneral(){
	//update to output to a log file later on
	int error=0;
	if((error = Master::gl.GetError())!=0){
		switch(error){
		case GRAPHICS_INVALID_ENUM://ignored and no side effect
			cout<<"INVALID ENUM"<<endl;
			break;
		case GRAPHICS_INVALID_VALUE://ignored and no side effect
			cout<<"INVALID VALUE"<<endl;//: value is out of range
			break;
		case GRAPHICS_INVALID_OPERATION://ignored and no side effect
			cout<<"INVALID OPERATION"<<endl;//: operation is not allowed
			break;
		case GRAPHICS_INVALID_FRAMEBUFFER_OPERATION://ignored and no side effect
			cout<<"INVALID FRAMEBUFFER OPERATION"<<endl;//: buffer object is not complete
			break;
		case GRAPHICS_OUT_OF_MEMORY://GRAPHICS state is undefined 
			cout<<"OUT OF MEMORY"<<endl;
			break;
		case GRAPHICS_STACK_UNDERFLOW://internal stack to underflow
			cout<<"STACK UNDERFLOW"<<endl;
			break;
		case GRAPHICS_STACK_OVERFLOW://internal stack to overflows
			cout<<"STACK OVERLFLOW"<<endl;
			break;
		default://other error
			cout<<"UNDEFINED ERROR"<<endl;
			break;
		}
	}
}

bool Debug::RunFramebuffer(){
	int status = Master::gl.CheckFBstatus(GRAPHICS_FRAMEBUFFER);
	switch(status){
	case GRAPHICS_FRAMEBUFFER_INCOMPLETE_ATTACHMENT:
		cout<<"GL_FRAMEBUFFER_INCOMPLETE_ATTACHMENT"<<endl;
		break;
	case GRAPHICS_FRAMEBUFFER_INCOMPLETE_DIMENSIONS_EXT:
		cout<<"GL_FRAMEBUFFER_INCOMPLETE_DIMENSIONS"<<endl;
		break;
	case GRAPHICS_FRAMEBUFFER_INCOMPLETE_MISSING_ATTACHMENT:
		cout<<"GL_FRAMEBUFFER_INCOMPLETE_MISSING_ATTACHMENT"<<endl;
		break;
	case GRAPHICS_FRAMEBUFFER_INCOMPLETE_DRAW_BUFFER_EXT:
		cout<<"GL_FRAMEBUFFER_INCOMPLETE_DRAW_BUFFER_EXT"<<endl;
		break;
	case GRAPHICS_FRAMEBUFFER_INCOMPLETE_READ_BUFFER_EXT:
		cout<<"GL_FRAMEBUFFER_INCOMPLETE_READ_BUFFER_EXT"<<endl;
		break;
	case GRAPHICS_FRAMEBUFFER_INCOMPLETE_FORMATS_EXT:
		cout<<"GL_FRAMEBUFFER_INCOMPLETE_FORMATS_EXT"<<endl;
		break;
	case GRAPHICS_FRAMEBUFFER_UNSUPPORTED:
		cout<<"GL_FRAMEBUFFER_UNSUPPORTED"<<endl;
		break;
	case GRAPHICS_FRAMEBUFFER_COMPLETE:
		//cout<<"GL_FRAMEBUFFER_COMPLETE"<<endl;
		return true;
		break;
	default://other error
		cout<<"FRAMEBUFFER UNDEFINED ERROR"<<endl;
		break;
	}
	return false;
}