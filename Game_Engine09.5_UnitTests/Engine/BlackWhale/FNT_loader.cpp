#include "FNT_loader.h"
#include "MeshGenerator.h"
#include <utility>
#include "Master.h"
using namespace std;
using namespace Text;

int FNT_loader::DESIRED_PADDING = 3;
char FNT_loader::DELIM = ' ';
char FNT_loader::NUMBER_SEPARATOR = ',';

FNT_loader::FNT_loader() {
}

FNT_loader::~FNT_loader() {
	Clear();
}
void FNT_loader::Clear() {
	m_collection.clear();
	values.clear();
}
FNT_loader::FNT_loader(std::string p_fileName) {
	int l_imageWidth = 0;
	m_aspect = (float)Master::s_screen[2] / Master::s_screen[3];
	ifstream myfile(p_fileName);
	string segment;
	string line;
	int lineNumber = 0;
	int charAmount = 0;
	//NEED TO ADD ERROR CHECKING
	while (myfile.good() && getline(myfile, line)) {
		stringstream line_buf(line);
		values.clear();
		while (getline(line_buf, segment, DELIM)) {
			stringstream segment_buf(segment);
			vector<string> pairs;
			string waste;
			while (getline(segment_buf, waste, '=')) {
				pairs.push_back(waste);
			}
			if (pairs.size() == 2) {
				values.insert(make_pair(pairs.at(0), pairs.at(1)));
			}
		}
		if (lineNumber == 0)
			LoadPadddingData();
		else if (lineNumber == 1) {
			LoadLineSizes();
			l_imageWidth = GetValue("scaleW");
		}
		else if (lineNumber == 2) {
			//texture name
		}
		else if (lineNumber == 3) {
			charAmount = GetValue("count");
		}
		else if (lineNumber >= 4 && lineNumber < 4 + charAmount) {
			LoadCharacterData(l_imageWidth);
		}
		else
			break;
		lineNumber++;
	}
	myfile.close();
}

Character FNT_loader::GetCharacter(int ASCII) {
	return m_collection.at(ASCII);
}

void FNT_loader::LoadPadddingData() {
	for (unsigned i = 0; i < 4; i++)
		padding[i] = GetValues("padding").at(i);
	m_paddingWidth = padding[PAD::LEFT] + padding[PAD::RIGHT];
	m_paddingHeight = padding[PAD::TOP] + padding[PAD::BOTTOM];
}

void FNT_loader::LoadLineSizes() {
	int lineHeightPixels = GetValue("lineHeight") - m_paddingHeight;
	m_vSize = MeshGenerator::m_lineHeight / (float)lineHeightPixels;
	m_hSize = m_vSize / m_aspect;
}

float FNT_loader::GetSpaceWidth() const {
	return m_spaceWidth;
}

void FNT_loader::LoadCharacter(int p_imageSize, Character& character) {
	int l_id = GetValue("id");
	if (l_id == MeshGenerator::m_spaceASCII) {
		m_spaceWidth = (GetValue("xadvance") - m_paddingWidth) * m_hSize;
		character = Character(-1);
		return;
	}
	float xTex = ((float)GetValue("x") + (padding[PAD::LEFT] - DESIRED_PADDING)) / p_imageSize;
	float yTex = ((float)GetValue("y") + (padding[PAD::TOP] - DESIRED_PADDING)) / p_imageSize;
	int width = GetValue("width") - (m_paddingWidth - (2 * DESIRED_PADDING));
	int height = GetValue("height") - ((m_paddingHeight)-(2 * DESIRED_PADDING));
	float quadWidth = width * m_hSize;
	float quadHeight = height * m_vSize;
	float xTexSize = (float)width / p_imageSize;
	float yTexSize = (float)height / p_imageSize;
	float xOff = (GetValue("xoffset") + padding[PAD::LEFT] - DESIRED_PADDING) * m_hSize;
	float yOff = (GetValue("yoffset") + (padding[PAD::TOP] - DESIRED_PADDING)) * m_vSize;
	float xAdvance = (GetValue("xadvance") - m_paddingWidth) * m_hSize;
	character = Character(l_id, xTex, yTex, xTexSize, yTexSize, xOff, yOff, quadWidth, quadHeight, xAdvance);
}

vector<int> FNT_loader::GetValues(string var) const {
	vector<int> val;
	stringstream buf(values.at(var));
	vector<string> pairs;
	string waste;
	while (getline(buf, waste, NUMBER_SEPARATOR)) {
		pairs.push_back(waste);
	}
	for (unsigned i = 0; i < pairs.size(); i++) {
		istringstream ss(pairs.at(i));
		int temp;
		ss >> temp;
		val.push_back(temp);
	}
	return val;
}


void FNT_loader::LoadCharacterData(int p_imageWidth) {
	Character c;
	LoadCharacter(p_imageWidth, c);
	if (c.id != -1)
		m_collection.insert(make_pair(c.id, c));
}

int FNT_loader::GetValue(string p_var) const {
	int val;
	string temp = values.at(p_var);
	istringstream ss(temp);
	ss >> val;
	return val;
}