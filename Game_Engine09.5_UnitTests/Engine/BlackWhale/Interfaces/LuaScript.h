#ifndef LUASCRIPT_H
#define LUASCRIPT_H
#include "Script.h"
#include <lua.h>
class LuaScript: public Script{
public:
	LuaScript();
	bool SetFileName(boost::any fileName);
	std::string GetFileName(void);
	void Start();
	void  Clear();
	Component* NewComponent(void) const;
private:
	static bool s_inList;
	void  Reflection();
	lua_State* m_state;
	std::string m_fileName;
};
#endif LUASCRIPT_H