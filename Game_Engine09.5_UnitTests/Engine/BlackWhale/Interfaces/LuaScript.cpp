#include "LuaScript.h"

extern "C" {
#include <lua.h>
#include <lualib.h>
#include <luaconf.h>
#include <lauxlib.h>
}

bool LuaScript::s_inList = false;

LuaScript::LuaScript() {
	Reflection();
	m_multiple = true;
	m_name = "Lua Script";
	if (!s_inList) {
		s_inList = true;
		LuaScript* l_pointer = new LuaScript;
		m_derivedClasses.push_back(l_pointer);
	}
}
Component*  LuaScript::NewComponent() const {
	Component* l_pointer = new LuaScript;
	return l_pointer;
}

bool LuaScript::SetFileName(boost::any var) {
	try {
		m_fileName = boost::any_cast<string> (var);
		//Start()
	}
	catch (boost::bad_any_cast& /*bac*/) {
		return false;
	}
	return true;
}

std::string LuaScript::GetFileName() {
	return m_fileName;
}

void LuaScript::Start() {
	//m_state = luaL_newstate();
	if (!m_state) {
		//std::cout << "[ERROR]: " << lua_tostring(m_state, -1) << endl;
	}
	luaL_openlibs(m_state);

	//lua_register(m_state, "cpp_Multiply", cpp_Multiply);

	if (m_fileName.compare("") != 0) {
		//if (luaL_dofile(m_state, m_fileName.c_str()))		//	cout << "[ERROR]: " << lua_tostring(m_state, -1) << endl;
	}
}

void LuaScript::Clear() {
	//lua_close(m_state);
}

void LuaScript::Reflection() {
	m_Variables.push_back("Script file");
	m_Setters.push_back(boost::bind(&LuaScript::SetFileName, this, _1));
	m_Getters.push_back(boost::bind(&LuaScript::GetFileName, this));
}