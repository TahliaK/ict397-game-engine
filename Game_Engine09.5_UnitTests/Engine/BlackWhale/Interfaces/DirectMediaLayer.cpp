#include "DirectMediaLayer.h"
#include <SDL_image.h>
#include <SDL_video.h>
using namespace ThirdParty;

DirectMediaLayer::DirectMediaLayer() {
	m_curSurface = NULL;
}

DirectMediaLayer::~DirectMediaLayer() {
	Clear();
}

void DirectMediaLayer::Initialize() {
	if (SDL_Init(SDL_INIT_EVERYTHING)<0) {
		//"[ERROR] Could Not Initialize SDL: %s", SDL_GetError());
	}
}
void DirectMediaLayer::Destroy() {
	SDL_Quit();
}

const bool DirectMediaLayer::LoadImage2D(std::string p_fileName, ThirdParty::Image& p_image) {
	m_curSurface = IMG_Load((char*)p_fileName.c_str());
	if (!m_curSurface)
		return false;
	if (p_image.invert) {
		/*if (!InvertSurface())
			return false;*/
		InvertSurface();
	}
	p_image.bytesPerPixel = (int)m_curSurface->format->BytesPerPixel;
	p_image.height = m_curSurface->h;
	p_image.width = m_curSurface->w;
	p_image.pixels = m_curSurface->pixels;


	return true;
}

const void DirectMediaLayer::Clear() {
	if (m_curSurface) {
		SDL_FreeSurface(m_curSurface);
		m_curSurface = NULL;
	}
}

bool DirectMediaLayer::InvertSurface() {
	return InvertImage(m_curSurface->pitch, m_curSurface->h, m_curSurface->pixels);
}

bool DirectMediaLayer::InvertImage(int p_pitch, int p_height, void* p_pixels)
{
	int l_index;
	void* l_tempRow;
	int l_halfHeight;

	l_tempRow = (void *)malloc(p_pitch);
	if (NULL == l_tempRow)
	{
		SDL_SetError("Not enough memory for image inversion");
		return false;
	}
	//if p_height is odd, don't need to swap middle row 
	l_halfHeight = (int)(p_height * .5);
	for (l_index = 0; l_index < l_halfHeight; l_index++) {
		//uses string.h 
		memcpy((Uint8 *)l_tempRow,
			(Uint8 *)(p_pixels)+
			p_pitch * l_index,
			p_pitch);

		memcpy(
			(Uint8 *)(p_pixels)+
			p_pitch * l_index,
			(Uint8 *)(p_pixels)+
			p_pitch * (p_height - l_index - 1),
			p_pitch);
		memcpy(
			(Uint8 *)(p_pixels)+
			p_pitch * (p_height - l_index - 1),
			l_tempRow,
			p_pitch);
	}
	free(l_tempRow);
	return true;
}

const bool DirectMediaLayer::GetRGB(int p_x, int p_y, RGB_pixel& p_rgb) {
	if (!m_curSurface)
		return false;
	Uint8 l_r, l_g, l_b;
	SDL_GetRGB(GetPixel( p_x, p_y), m_curSurface->format, &l_r, &l_g, &l_b);
	p_rgb.r = (float)l_r;
	p_rgb.g = (float)l_g;
	p_rgb.b = (float)l_b;
	return true;
}


Uint32 DirectMediaLayer::GetPixel(int x, int y)
{
	int bpp = m_curSurface->format->BytesPerPixel;
	Uint8 *p = (Uint8 *)m_curSurface->pixels + y * m_curSurface->pitch + x * bpp;

	switch (bpp) {
	case 1:
		return *p;
		break;

	case 2:
		return *(Uint16 *)p;
		break;

	case 3:
		if (SDL_BYTEORDER == SDL_BIG_ENDIAN)
			return p[0] << 16 | p[1] << 8 | p[2];
		else
			return p[0] | p[1] << 8 | p[2] << 16;
		break;

	case 4:
		return *(Uint32 *)p;
		break;

	default:
		return 0;
	}
}