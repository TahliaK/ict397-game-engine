#ifndef SCRIPT_H
#define SCRIPT_H
#include "../Component.h"
class Script : public Component {
public:
	/**
	* @brief The method is called when this object is being created.
	*/
	virtual void Initialize() { m_name = "Script"; }

	/**
	* @brief The method is called when the script
	* is enabled just before the Update method.
	*/
	virtual void Start() {}

	/**
	* @brief The method is called every frame.
	*/
	virtual void Update() {}

	/**
	* @brief The method is called before destroying the script.
	*/
	virtual void Clear() {}

	virtual void Reflection() {}
private:
	bool m_enabled;
};
#endif SCRIPT_H