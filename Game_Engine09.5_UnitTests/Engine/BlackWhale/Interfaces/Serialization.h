/*
* @brief
* @author Olesia Kochergina
* @version 02
* @date 15/03/2017
*/

#ifndef SERIALIZATION_H
#define SERIALIZATION_H

#include <boost\archive\detail\register_archive.hpp>
#include <boost\archive\text_oarchive.hpp>
#include <boost\archive\text_iarchive.hpp>
#include <boost\archive\xml_iarchive.hpp>
#include <boost\archive\xml_oarchive.hpp>

#include <boost\serialization\vector.hpp>
#include <boost\serialization\export.hpp>
#include <boost\serialization\base_object.hpp>
#include <boost\serialization\assume_abstract.hpp>
#include <boost\serialization\utility.hpp>
#include <boost\serialization\map.hpp>
#include <boost\serialization\vector.hpp>
#include <boost\serialization\string.hpp>
#include <boost\serialization\array.hpp>

//to have some sort of reflection for GUI
#include <boost\function.hpp>
#include <boost\bind.hpp>
#include <boost\any.hpp>

/// 0 - TXT;  1 -  XML; 2 - BINARY[NOT SUPPORTED]
#define OUTPUT_FORMAT 1
#define SER_CLASS_VERSION(T,N) BOOST_CLASS_VERSION(T,N)
#define SER_ABSTRACT(T) BOOST_SERIALIZATION_ASSUME_ABSTRACT(T)
#define SER_SPLIT_MEMBER() BOOST_SERIALIZATION_SPLIT_MEMBER()
#define SER_BASE_OBJECT_NVP(T) BOOST_SERIALIZATION_BASE_OBJECT_NVP(T)
#define SER_NVP(N) BOOST_SERIALIZATION_NVP(N)
#define SER_CLASS_EXPORT_KEY(T) BOOST_CLASS_EXPORT_KEY(T)
/*namespace ThirdParty {
	template<T>: class Serialization {
	public:
		static void AddClassVersion();

	};
};*/
#endif SERIALIZATION_H