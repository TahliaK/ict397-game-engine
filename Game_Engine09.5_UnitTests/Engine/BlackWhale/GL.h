/*
* @author Olesia Kochergina
* @version 03
* @date 10/03/2017
*/

#ifndef GL_H
#define GL_H

#define  STROKE_ROMAN               ((void *)0x0000)
#define  STROKE_MONO_ROMAN          ((void *)0x0001)
#define  BITMAP_9_BY_15             ((void *)0x0002)
#define  BITMAP_8_BY_13             ((void *)0x0003)
#define  BITMAP_TIMES_ROMAN_10      ((void *)0x0004)
#define  BITMAP_TIMES_ROMAN_24      ((void *)0x0005)
#define  BITMAP_HELVETICA_10        ((void *)0x0006)
#define  BITMAP_HELVETICA_12        ((void *)0x0007)
#define  BITMAP_HELVETICA_18        ((void *)0x0008)

#define TEXTURE_2D 0x0DE1



#include "Struct.h"

#include "Material_s.h"
class OpenGL;
class GL {

public:
	virtual void Enable(unsigned int state) = 0;
	virtual void Disable(unsigned int state) = 0;
	virtual void Color(RGBA color) = 0;
	virtual void ClearColor(RGBA p_color) = 0;
	virtual void RasterPos(Vector3 position) = 0;
	virtual void BitmapString(void *font, std::string, Vector3 position = 0) = 0;
	virtual void ActiveTexture(unsigned int ID) = 0;
	virtual void BindTexture(unsigned int target, unsigned int texture) = 0;

	virtual void MatrixMode(unsigned int mode) = 0;
	virtual void LoadIdentity() = 0;
	virtual void Viewport(int x, int y, int w, int h) = 0;
	virtual void Ortho(float left, float right, float top, float bottom, float p_near, float p_far) = 0;
	virtual void Perspective(float FOV, float aspect, float p_near, float p_far) = 0;
	virtual void ClearColor(float red, float green, float blue, float alpha) = 0;
	virtual void Clear(unsigned int mask) = 0;

	virtual void PushMatrix() = 0;
	virtual void PopMatrix() = 0;
	virtual void SwapBuffers() = 0;
	virtual void PostRedisplay() = 0;

	virtual void Init(int *argc = NULL, char **argv = NULL) = 0;
	virtual void InitDisplayMode(unsigned int mask) = 0;
	virtual void SetWindowRect(int x, int y, int w, int h) = 0;
	virtual void InitializeWindow(std::string window_name) = 0;
	virtual void EnterMainLoop() = 0;

	virtual unsigned int GetError() = 0;

	virtual std::string GetProgramInfoLog(unsigned int programID) = 0;

	virtual unsigned int CreateProgram() = 0;
	virtual void UseProgram(unsigned int programID) = 0;
	virtual void LinkProgram(unsigned int programID) = 0;
	virtual void DeleteProgram(unsigned int programID) = 0;
	virtual void GetProgramiv(unsigned int programID, unsigned int pname, int* param) = 0;

	virtual void AttachShader(unsigned int programID, unsigned int shaderID) = 0;
	virtual void DetachShader(unsigned int programID, unsigned int shaderID) = 0;
	virtual void DeleteShader(unsigned int shaderID) = 0;
	virtual unsigned int CreateShader(unsigned int shaderType) = 0;
	virtual void ShaderSource(unsigned int shaderID, int numOfStrings, const char** strings, int* lengthOfStrings) = 0;
	virtual void CompileShader(unsigned int shaderID) = 0;
	virtual void GetShaderiv(unsigned int shaderID, unsigned int pname, int* param) = 0;
	virtual std::string GetShaderInfoLog(unsigned int shaderID) = 0;

	virtual void DeleteBuffers(int amount, unsigned int* buffer) = 0;
	virtual void DeleteTextures(int amount, unsigned int* texture) = 0;

	virtual void GenFB(int amount, unsigned int* buffer) = 0;
	virtual void GenTexture(int amount, unsigned int* buffer) = 0;
	virtual void BindRB(unsigned int target, unsigned int buffer) = 0;
	virtual void GenRB(int amount, unsigned int* buffer) = 0;
	virtual void RBstorage(unsigned int target, unsigned int internalformat, unsigned int w, unsigned int h) = 0;
	virtual void FBRB(unsigned int target, unsigned int attachment, unsigned int original, unsigned int buffer) = 0;
	virtual void DeleteFB(int amount, unsigned int* buffer) = 0;
	virtual void DeleteRB(int amount, unsigned int* buffer) = 0;
	virtual void BindFB(unsigned int target, unsigned int buffer) = 0;
	virtual void FBtexture(unsigned int target, unsigned int attachment, unsigned int texture, int level) = 0;
	virtual void FbTexture2D(unsigned int fb, unsigned int attachment, unsigned int textarget, unsigned int texture, int level) = 0;
	virtual void DrawBuffers(int amount, const unsigned int* buffer) = 0;
	virtual void ReadBuffer(unsigned int attachment) = 0;
	virtual unsigned int CheckFBstatus(unsigned int target) = 0;
	virtual void TexImage2D(unsigned int target, int level, int internalformat, int width, int height, int border, unsigned int format, unsigned int type, const void *pixels) = 0;
	virtual void TexParami(unsigned int target, unsigned int pname, int param) = 0;
	virtual void TexParamf(unsigned int target, unsigned int pname, float param) = 0;
	virtual void TexEnvi(unsigned int target, unsigned int pname, int param) = 0;
	virtual void PolygonMode(unsigned int face, unsigned int mode) = 0;

	virtual void BindBuffer(unsigned int target, unsigned int buffer) = 0;
	virtual void GenVAO(int amount, unsigned int* buffer) = 0;
	virtual void BindVAO(unsigned int buffer) = 0;
	virtual void GenBuffer(int amount, unsigned int* buffer) = 0;
	virtual void BufferSubData(unsigned int target, int offset, int size, const void* data) = 0;
	virtual void BufferData(unsigned int target, int size, const void* data, unsigned int usage) = 0;
	virtual void EnableVertexAttribArray(unsigned int index) = 0;
	virtual void VertexAttribPointer(unsigned int index, int size, unsigned int type, unsigned char normalized, int stride, const void* pointer) = 0;
	virtual void DrawArrays(unsigned int mode, int first, int count) = 0;
	virtual void DrawElements(unsigned int mode, int count, int type, void* indices) = 0;
	
	virtual void BlendFunc(unsigned int sfactor, unsigned int dfactor) = 0;
	virtual void AlphaFunc(unsigned int func, float ref) = 0;


	virtual int GetALoc(unsigned int programID, std::string attribName) = 0;
	virtual int GetULoc(unsigned int programID, std::string uniformName) = 0;
	virtual void Uniform1f(int location, float value) = 0;
	virtual void Uniform1i(int location, int value) = 0;
	virtual void Uniform1iv(int location, int size, const int* value) = 0;
	virtual void Uniform1fv(int location, int size, const float* value)=0;
	virtual void Uniform2fv(int location, int size, const float* value) = 0;
	virtual void Uniform4fv(int location, int size, const Vector4& value) = 0;	
	virtual void Uniform4fv(int location, int size, const float* value) = 0;
	virtual void Uniform3fv(int location, int size, const Vector3& value) = 0;
	virtual void Uniform3fv(int location, int size, const float* value) = 0;
	virtual void UniformMatrix4fv(int location, int count, unsigned char transpose, const float* value) = 0;
	virtual void UniformMatrix4fv(int location, int count, unsigned char transpose, const Matrix4& value)=0;
	virtual void UniformMatrix3fv(int location, int count, unsigned char transpose, const float* value) = 0;
	
	virtual void DepthMask(unsigned char flag) = 0;
	virtual void DepthFunc(unsigned int func) = 0;
	virtual void CullFace(unsigned int mode) = 0;
	virtual void DrawBuffer(unsigned int mode)=0;
	virtual void GetActiveUniform(unsigned int programID, unsigned int location, const int bufSize, int* length, int* size, unsigned int* type, char* name)=0;

	virtual void GetActiveAttribute(unsigned int programID, unsigned int location, const int bufSize, int* length, int* size, unsigned int* type, char* name) = 0;
	//virtual void Frustum() =0;
};
#endif GL_H