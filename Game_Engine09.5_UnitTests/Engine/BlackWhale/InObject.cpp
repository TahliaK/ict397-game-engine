#include "InObject.h"
#include "Master.h"
BOOST_CLASS_EXPORT_IMPLEMENT(InObject)
int InObject::s_id = 1;
std::vector<int> InObject::s_objects;

InObject::InObject(){
	m_id = s_id;
	m_name = std::to_string(m_id);
	s_objects.push_back(s_id++);
}

InObject::InObject(const InObject& p_object){
	//m_id = s_id;
	//s_objects.push_back(s_id++);
	m_name = p_object.m_name;
}

const std::string InObject::GetName() const{
	return m_name;
}

void InObject::ClearStatic(void) {
	s_objects.clear();
}

const bool InObject::SetName(const std::string p_name){
	if(p_name.size() > 50)
		return false;
	else{
		IFORSI(0,p_name.size()){
			if((int)p_name.at(i) > 122  ||
				((int)p_name.at(i) < 65 && (int)p_name.at(i) > 57) ||
				((int)p_name.at(i) > 90 && (int)p_name.at(i) < 97) ||
				((int) p_name.at(i) < 48 && (int)p_name.at(i) != 32))
					return false;
		}
	}
	m_name = p_name;
	return true;
}

const int InObject::GetID() const{
	return m_id;
}

const bool InObject::operator == (const InObject* p_object) const{
	return (p_object == this);
}
