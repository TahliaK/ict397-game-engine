#ifndef IMAGE_H
#define IMAGE_H
#include "Struct.h"
#include "Material_s.h"
#include "UserInterface.h"

/*
* @class Image
* @brief Used for displaying 2D images within the game world.
* @version 02
* @date 18/04/2017
* @author Olesia Kochergina
*/
class Image : public UserInterface {

public:

	/*
	* @brief Default c-tor.
	*/
	Image();

	/*
	* @brief Destructor.
	*/
	~Image();

	/*
	* @brief Initializes the object, called when creating the object.
	*/
	void Initialize();

	/*
	* @brief Called just before the first update.
	*/
	void Start();

	/*
	* @brief Updates the object and should be called every frame.
	*/
	void Update();

	/*
	* @brief Clears internal data, should be called when
	* the object needs to be either reused or deleted.
	*/
	void Clear();

	/*
	* @brief Creates a new component of this class.
	* @return Pointer to the new component or NULL.
	*/
	Component* NewComponent(void) const;

	/*
	* @brief Sets the object's texture path and loads the new texture.
	* @param path - file name of the texture.
	* @return true - the texture is successfully updated.
	*/
	bool SetImage(boost::any path);

	/*
	* @brief Returns the file name of the image.
	* @return file name of the image.
	*/
	std::string GetImage(void) const;
private:

	///mesh data
	vBuffer m_data;

	///texture and its path
	Material m_material;

	/*
	* @brief Adds values to three vectors: variable name,  setter, getter.
	* Used in the Editor to modify components at run-time.
	*/
	void Reflection(void);

	/*
	* @brief Checks whether the mouse is over the object or not.
	*/
	void Interactivity(void);

	///serialization
	friend class boost::serialization::access;
	template<class Archive>
	void save(Archive& ar, const unsigned int /*version*/) const {
		ar & SER_BASE_OBJECT_NVP(UserInterface);
		ar& SER_NVP(m_material);
	}

	template<class Archive>
	void load(Archive& ar, const unsigned int /*version*/) {
		ar & SER_BASE_OBJECT_NVP(UserInterface);
		ar& SER_NVP(m_material);
	}
	SER_SPLIT_MEMBER()
};
SER_CLASS_VERSION(Image, 0);
SER_CLASS_EXPORT_KEY(Image)
#endif IMAGE_H