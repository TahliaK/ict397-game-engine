#ifndef TEXTPROCESSING_H
#define TEXTPROCESSING_H

#include <fstream>
#include <string>
#include <sstream>
#include <vector>
#include "Struct.h"
#include "Character.h"

using namespace std;
namespace  Text{
/*
* @brief One work in a text
*/
struct Word{
	float width;
	float fontSize;

	/**
	* @brief Default constructor,
	* which sets the font Resize to 10 and the word's width to 0.
	*/
	Word(){
		width = 0;
		fontSize = 10;
	}

	Word(float fontSize){
		this->fontSize = fontSize;
		width = 0;
	}

	~Word() {
		word.clear();
	}

	/**
	* @brief Adds a character to the end of the current word.
	*/
	void AddCharacter(Character p_char){
		word.push_back(p_char);
		width += p_char.xAdvance * fontSize;
	}

	/**
	* @brief Returns the whole word.
	*/
	const vector<Character> GetWord(void)const{
		return word;
	}

friend	std::istream & operator >>(std::istream & input,Word & W ){
	string word;
	input >> word;
	IFORSI(0,W.GetWord().size())
		W.AddCharacter(Character((int)word.at(i)));
	return input;
}

friend std::ostream & operator <<(std::ostream & output, const Word & W){
	IFORSI(0,W.GetWord().size())
		output << W.GetWord().at(i);
	output << " ";
	return output;
}

private:
	///holds the current word's characters
	vector<Character> word;
};

struct Line{
	float maxLength;
	float spaceSize;

	Line(){
		maxLength = 10000;
		spaceSize = 2;
		currentLength = 0;
	}

	Line(float spaceWidth, float fontSize, float maxLength){
		spaceSize = spaceWidth * fontSize;
		this->maxLength = maxLength;
		currentLength = 0;
	}
	~Line() {
		line.clear();
	}

	bool AddWord(Word word){
		if(currentLength + word.width <= maxLength){
			line.push_back(word);
			currentLength += word.width;
			return true;
		}
		cout<<"[ERROR] CANNOT INSERT A WORD"<<endl;
		return false;
	}

	const vector<Word> GetLine(void) const {
		return line;
	}

	friend std::istream & operator >>(std::istream & input,Line & /*L */){
	string line;
	input >> line;
	for(unsigned i = 0; i < line.size(); i++){
	//	if(line.at(i) == " ")
	//		L.AddWord(Word
	}
	return input;
}
friend  std::ostream & operator <<(std::ostream & output, const Line & L){
	for(unsigned i = 0; i <L.GetLine().size(); i++)
		output << L.GetLine().at(i);
	output << endl;
	return output;
}

private:
	vector<Word> line;
	float currentLength;
};
}
#endif TEXTPROCESSING_H