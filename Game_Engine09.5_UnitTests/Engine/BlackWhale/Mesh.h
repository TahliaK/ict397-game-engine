#ifndef MESH_H
#define MESH_H
#include "InObject.h"
//#include "Math_s.h"

/**
* @class Mesh
* @brief Base class for all 3D models.
* @author Olesia Kochergina
* @version 01
* @date 10/03/2017
*/
class Mesh: public InObject{
public:

	/*
	* @brief Constructor.
	* @param shaderProgram - shader program for the mesh.
	*/
	Mesh(unsigned int shaderProgram) {
		m_shaderProgram = shaderProgram; 
	};

	/*
	* @brief Displays the mesh on the screen.
	*/
	virtual const void Draw() = 0;
protected:

	///the mesh's shader program
	unsigned int m_shaderProgram;
private:
	//AABB bounds;
	//colors, normals, uv,positions
};
#endif MESH_H