#include "Light.h"
#include <iostream>
#include "Master.h"
#include "SceneManager.h"
BOOST_CLASS_EXPORT_IMPLEMENT(Light)
using namespace std;

std::vector<Vector3> Light::positions;
std::vector<Vector3> Light::colors;
std::vector<int> Light::lightType;
std::vector<float> Light::radius;
Image Light::m_light_picture;

void Light::Start() {
	if (DEBUG_MODE) {
		m_light_picture.SetImage("Resources/GUI/LightIcon.png");
	}
}

Light::Light() : Component() {
	Reflection();
	Initialize();
	static bool l_inList = false;
	if (!l_inList) {
		l_inList = true;
		Light* l_pointer = new Light;
		m_derivedClasses.push_back(l_pointer);
	}
}

Component* Light::NewComponent()const {
	Component* l_pointer = new Light;
	return l_pointer;
}

Light::Light(const Light& original) : Component(original) {
	m_name = "Light";
	m_ambientColor = original.m_ambientColor;
	m_type = original.m_type;
}

void Light::Initialize() {
	m_type = 0;
	m_multiple = false;
	m_name = "Light";
	m_ambientColor = RGBA(0.8, 0.8, 0.5);
	//"Resources/GUI/LightIcon.png";
	m_radius = 32;
}
void Light::Clear() {
	m_light_picture.Clear();
}



void Light::Update() {
	if (m_gameObject->GetVisible()) {
		{
			//1, 0.14, 0.07
			//1,0.045,1.8,1000
			// (-linear +  std::sqrtf(linear * linear - 4 * quadratic * (constant - (256.0 / 5.0) * 1)))  / (2 * quadratic); 
			positions.push_back(m_gameObject->GetTransform()->position);

			colors.push_back(Vector3(m_ambientColor.r, m_ambientColor.g, m_ambientColor.b));
			lightType.push_back(m_type);
			radius.push_back(m_radius);
		}
	}
}

void Light::UpdateLight() {
	Master::gl.Uniform1i(Master::s_LightPassShader.GetLocation("u_tPosition"), 0);
	Master::gl.Uniform1i(Master::s_LightPassShader.GetLocation("u_tNormal"), 1);
	Master::gl.Uniform1i(Master::s_LightPassShader.GetLocation("u_tColor"), 2);

	Master::gl.UniformMatrix4fv(Master::s_LightPassShader.GetLocation("view"), 1, GRAPHICS_FALSE, SceneManagement::SceneManager::GetActive()->GetMainCamera()->GetViewMatrix());
	Master::gl.UniformMatrix4fv(Master::s_LightPassShader.GetLocation("model"), 1, GRAPHICS_FALSE, SceneManagement::SceneManager::GetActive()->GetMainCamera()->GetModelMatrix());
	Master::gl.UniformMatrix4fv(Master::s_LightPassShader.GetLocation("proj"), 1, GRAPHICS_FALSE, SceneManagement::SceneManager::GetActive()->GetMainCamera()->GetProjectionMatrix());
	Master::gl.Uniform1i(Master::s_LightPassShader.GetLocation("u_amount"), positions.size());
	// Also send light relevant uniforms
	IFORSI(0, positions.size()) {
		Master::gl.Uniform3fv(Master::s_LightPassShader.GetLocation("u_position"), positions.size(), positions[0]);
		Master::gl.Uniform3fv(Master::s_LightPassShader.GetLocation("u_color"), positions.size(), colors[0]);
		// Update attenuation parameters and calculate radius
		Master::gl.Uniform1iv(Master::s_LightPassShader.GetLocation("u_type"), positions.size(), &lightType[0]);
		Master::gl.Uniform1fv(Master::s_LightPassShader.GetLocation("u_radius"), positions.size(), &radius[0]);
	}
	Master::gl.Uniform3fv(Master::s_LightPassShader.GetLocation("u_cameraPosition"), 1, SceneManagement::SceneManager::GetActive()->GetMainCamera()->GetGameObject()->GetTransform()->position);
	positions.clear();
	colors.clear();
	lightType.clear();
	radius.clear();
}

bool Light::SetRadius(boost::any var) {
	try {
		m_radius = boost::any_cast<float> (var);
	}
	catch (boost::bad_any_cast& /*bac*/) {
		return false;
	}
	return true;
}

bool Light::SetLightType(boost::any var) {
	try {
		m_type = boost::any_cast<int> (var);
	}
	catch (boost::bad_any_cast& /*bac*/) {
		return false;
	}
	return true;
}


bool Light::SetColor(boost::any var) {
	try {
		RGBA l_temp = boost::any_cast<RGBA> (var);
		IFORSI(0, 4) {
			if (l_temp.GetColor()[i] > 1 || l_temp.GetColor()[i] < 0)
				return false;
		}
		m_ambientColor = l_temp;
	}
	catch (boost::bad_any_cast& /*bac*/) {
		return false;
	}
	return true;
}


float Light::GetRadius() const {
	return m_radius;
}
int Light::GetLightType()const {
	return m_type;
}

RGBA Light::GetColor()const {
	return m_ambientColor;
}

void Light::Reflection() {
	m_Variables.push_back("Light Type");
	m_Setters.push_back(boost::bind(&Light::SetLightType, this, _1));
	m_Getters.push_back(boost::bind(&Light::GetLightType, this));

	/*m_Variables.push_back("Quadratic");
	m_Setters.push_back(boost::bind(&Light::SetQuadratic, this, _1));
	m_Getters.push_back(boost::bind(&Light::GetQuadratic, this));*/

	m_Variables.push_back("Radius");
	m_Setters.push_back(boost::bind(&Light::SetRadius, this, _1));
	m_Getters.push_back(boost::bind(&Light::GetRadius, this));

	m_Variables.push_back("Color");
	m_Setters.push_back(boost::bind(&Light::SetColor, this, _1));
	m_Getters.push_back(boost::bind(&Light::GetColor, this));
}
//
//
//#include "Light.h"
//#include <iostream>
//#include "Master.h"
//#include "SceneManager.h"
//#include "ShaderManager.h"
//std::string Light::m_vS = "Resources/Shaders/light.vert";
//std::string Light::m_fS = "Resources/Shaders/light.frag";
//unsigned int Light::m_shaderProgram = 0;
//
//BOOST_CLASS_EXPORT_IMPLEMENT(Light)
//using namespace std;
//int Light::LightNumber = 0;
//
//std::vector<Vector3> Light::positions;
//std::vector<Vector3> Light::colors;
//std::vector<float> Light::linear;
//std::vector<float> Light::quadratic;
//std::vector<float> Light::radius;
//
//
//
//
//void Light::Start() {
//	if (DEBUG_MODE) {
//		m_light_picture.SetImage(m_material.path);
//		//m_light_picture.GetTransform()->position = m_transform->position;
//		//m_light_picture.GetTransform()->position = m_gameObject->GetTransform()->position;
//		//m_light_picture.GetTransform()->position.y *= -m_gameObject->GetTransform()->position.y;
//		//m_light_picture.GetTransform()->scale = Vector3(1000);
//		//m_light_picture.Start();
//	}
//	if (m_shaderProgram == 0) {
//		m_shaderProgram = ShaderManager::CreateShaderProgram(m_vS, m_fS);
//	}
//}
//
//Light::Light() : Component() {
//	Reflection();
//	Initialize();
//	static bool l_inList = false;
//	if (!l_inList) {
//		l_inList = true;
//		Light* l_pointer = new Light;
//		m_derivedClasses.push_back(l_pointer);
//	}
//}
//Component* Light::NewComponent() {
//	Component* l_pointer = new Light;
//	return l_pointer;
//}
//
//Light::Light(const Light& original) : Component(original) {
//	m_name = "Light";
//	m_material.ambient = original.m_material.ambient;
//	m_material.ambient = RGBA(1, 1, 1);
//	m_material.diffuse = original.m_material.diffuse;
//
//	m_material.specular = original.m_material.specular;
//	m_material.emission = original.m_material.emission;
//	m_material.path = original.m_material.path;
//
//	m_attenuation = m_attenuation;
//	cutOff = 0;
//}
//
//void Light::Initialize() {
//	m_multiple = false;
//	m_name = "Light";
//	m_material.ambient = RGBA(0.5, 0.5, 0.3);
//	//m_material.ambient = RGBA(0.5, 0.5, 1);
//	//	m_material.ambient = RGBA(1,1,1,1);
//	m_material.diffuse = RGBA(0.8, 0.8, 0.3);
//
//	m_material.specular = RGBA(0.5, 0.5, 0.5);
//	m_material.emission = RGBA(0, 0, 0, 0);
//	m_material.path = "Resources/GUI/LightIcon.png";
//	//32 - 1,0.14,0.07
//	//50 - 1,0.09,0.032
//	//65 - 1,0.07,0.017
//	//100 - 1,0.045,0.0075
//
//	m_attenuation = Vector3(1, 0.045, 0.0075);
//	m_radius = 32;
//	cutOff = 0;
//}
//void Light::Clear() {
//	m_light_picture.Clear();
//}
//
//Material* Light::GetMaterial() {
//	return &m_material;
//}
//
//
//Light::~Light() {}
//
//void Light::Update() {
//	if (m_gameObject->GetVisible()) {
//		if (FORWARD_SHADING) {
//			if (DEBUG_MODE) {
//				Master::gl.Disable(GRAPHICS_CULL_FACE);
//				//m_light_picture.Update();
//				Master::gl.Enable(GRAPHICS_CULL_FACE);
//			}
//		}
//		else {
//			//1, 0.14, 0.07
//			//1,0.045,1.8,1000
//			// (-linear +  std::sqrtf(linear * linear - 4 * quadratic * (constant - (256.0 / 5.0) * 1)))  / (2 * quadratic); 
//			positions.push_back(m_gameObject->GetTransform()->position);
//
//			colors.push_back(Vector3(m_material.ambient.r, m_material.ambient.g, m_material.ambient.b));
//			linear.push_back(m_attenuation[1]);
//			quadratic.push_back(m_attenuation[2]);
//			radius.push_back(m_radius);
//		}
//	}
//}
//
//void Light::UpdateLight() {
//	if (m_shaderProgram == 0 || !ShaderManager::ActivateShaderProgram(m_shaderProgram))
//		return;
//	else if (m_shaderProgram == ShaderManager::GetActive()->GetID()) {
//
//		Master::gl.UseProgram(m_shaderProgram);
//		cout << "sa: " << m_shaderProgram << endl;
//		Master::gl.UniformMatrix4fv(ShaderManager::GetActive()->GetLocation("model"), 1, GRAPHICS_FALSE, &SceneManagement::SceneManager::GetActive()->GetMainCamera()->GetModelMatrix().Get()[0]);
//		Master::gl.UniformMatrix4fv(ShaderManager::GetActive()->GetLocation("proj"), 1, GRAPHICS_FALSE, &SceneManagement::SceneManager::GetActive()->GetMainCamera()->GetProjectionMatrix().Get()[0]);
//		Master::gl.UniformMatrix4fv(ShaderManager::GetActive()->GetLocation("view"), 1, GRAPHICS_FALSE, &SceneManagement::SceneManager::GetActive()->GetMainCamera()->GetViewMatrix().Get()[0]);
//		Master::gl.Uniform1i(ShaderManager::GetActive()->GetLocation("u_tPosition"), 0);
//		Master::gl.Uniform1i(ShaderManager::GetActive()->GetLocation("u_tNormal"), 1);
//		Master::gl.Uniform1i(ShaderManager::GetActive()->GetLocation("u_tColor"), 2);
//		Master::gl.UniformMatrix4fv(ShaderManager::GetActive()->GetLocation("model"), 1, GRAPHICS_FALSE, SceneManagement::SceneManager::GetActive()->GetMainCamera()->GetModelMatrix());
//		Master::gl.Uniform3fv(ShaderManager::GetActive()->GetLocation("u_cameraPosition"), 1, SceneManagement::SceneManager::GetActive()->GetMainCamera()->GetPosition());
//		Master::gl.Uniform1i(ShaderManager::GetActive()->GetLocation("u_amount"), positions.size());
//
//	}
//	// Also send light relevant uniforms
//	IFORSI(0, positions.size()) {
//		Master::gl.Uniform3fv(ShaderManager::GetActive()->GetLocation("u_position"), positions.size(), positions[0]);
//		Master::gl.Uniform3fv(ShaderManager::GetActive()->GetLocation("u_color"), positions.size(), colors[0]);
//		// Update attenuation parameters and calculate radius
//		Master::gl.Uniform1fv(ShaderManager::GetActive()->GetLocation("u_linear"), positions.size(), &linear[0]);
//		Master::gl.Uniform1fv(ShaderManager::GetActive()->GetLocation("u_quadratic"), positions.size(), &quadratic[0]);
//		Master::gl.Uniform1fv(ShaderManager::GetActive()->GetLocation("u_radius"), positions.size(), &radius[0]);
//	}
//	positions.clear();
//	colors.clear();
//	linear.clear();
//	quadratic.clear();
//	radius.clear();
//}
//
//Vector3* Light::GetAttenuation() {
//	return &m_attenuation;
//}
//
//bool Light::SetRadius(boost::any var) {
//	try {
//		m_radius = boost::any_cast<float> (var);
//	}
//	catch (boost::bad_any_cast& /*bac*/) {
//		return false;
//	}
//	return true;
//}
//
//bool Light::SetLinear(boost::any var) {
//	try {
//		m_attenuation.y = boost::any_cast<float> (var);
//	}
//	catch (boost::bad_any_cast& /*bac*/) {
//		return false;
//	}
//	return true;
//}
//
//bool Light::SetQuadratic(boost::any var) {
//	try {
//		m_attenuation.z = boost::any_cast<float> (var);
//	}
//	catch (boost::bad_any_cast& /*bac*/) {
//		return false;
//	}
//	return true;
//}
//
//bool Light::SetColor(boost::any var) {
//	try {
//		RGBA l_temp = boost::any_cast<RGBA> (var);
//		IFORSI(0, 4) {
//			if (l_temp.GetColor()[i] > 1 || l_temp.GetColor()[i] < 0)
//				return false;
//		}
//		m_material.ambient = l_temp;
//	}
//	catch (boost::bad_any_cast& /*bac*/) {
//		return false;
//	}
//	return true;
//}
//
//
//float Light::GetRadius() {
//	return m_radius;
//}
//float Light::GetLinear() {
//	return m_attenuation[1];
//}
//float Light::GetQuadratic() {
//	return m_attenuation[2];
//}
//RGBA Light::GetColor() {
//	return m_material.ambient;
//}
//
//void Light::Reflection() {
//	m_Variables.push_back("Linear");
//	m_Setters.push_back(boost::bind(&Light::SetLinear, this, _1));
//	m_Getters.push_back(boost::bind(&Light::GetLinear, this));
//
//	m_Variables.push_back("Quadratic");
//	m_Setters.push_back(boost::bind(&Light::SetQuadratic, this, _1));
//	m_Getters.push_back(boost::bind(&Light::GetQuadratic, this));
//
//	m_Variables.push_back("Radius");
//	m_Setters.push_back(boost::bind(&Light::SetRadius, this, _1));
//	m_Getters.push_back(boost::bind(&Light::GetRadius, this));
//
//	m_Variables.push_back("Color");
//	m_Setters.push_back(boost::bind(&Light::SetColor, this, _1));
//	m_Getters.push_back(boost::bind(&Light::GetColor, this));
//}