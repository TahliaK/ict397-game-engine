#ifndef CONSOLE_C_H
#define CONSOLE_C_H
#include "Controller.h"

namespace Editor {

	/*
	* @class Console_c
	* @brief NOT IMPLEMENTED
	* @author Olesia Kochergina
	* @date 15/02/2017
	*/
	class Console_c : public Controller {
	public:

		/*
		* @brief Default c-tor.
		*/
		Console_c();

		/*
		* @brief Destructor.
		*/
		~Console_c();

		/*
		* @brief Called by WM_CREATE message received from the message loop.
		* @param info - contains information about the window being created.
		* @return Returns 0 to indicate that the message is processed, if -1 then the window is destroyed.
		*/
		int Create(CREATESTRUCT* info);

		/*
		* @brief Called by WM_DESTROY message received from the message loop.
		* @return Returns 0 to indicate that the message is processed.
		*/
		int Destroy();

		/*
		* @brief Called by WM_NOTIFY message received from the message loop.
		* @param id - ID of the control sending the message.
		* @param lParam - pointer to NMHDR which contains the notification code and other information.
		* @return Can be ignored.
		*/
		int Notify(int id, LPARAM lParam);

		/*
		* @brief Called by WM_COMMAND message received from the message loop.
		* @param wParam - ID of the control
		* @param msg -
		* @param hwnd - handle to the window
		* @return Returns 0 to indicate that the message is processed.
		*/
		int Command(WPARAM wParam, LPARAM msg, HWND hwnd);
	};
}
#endif CONSOLE_C_H