/*
* @brief Windows will invoke the callback functions when an event is triggered.
* The functions help to direct messages to specific windows and dialogs. 
* @author Olesia Kochergina
* @date 01/02/2017
*/
#include <windows.h>
#ifndef PROCEDURE_H
#define PROCEDURE_H

namespace Editor {

	/*
	* @brief Window procedure callback which is responsible for handling all messages.
	* @param hwnd - handle to the window.
	* @param msg - the message.
	* @param wParam - depends on the message.
	* @param lParam - depends on the message.
	* @return returns whether the message has been processed or not.
	*/
	LRESULT CALLBACK windowProcedure(HWND hwnd, UINT msg, WPARAM wParam, LPARAM lParam);

	/*
	* @brief Dialog procedure callback.
	* @param hwnd - handle to the window.
	* @param msg - the message.
	* @param wParam - depends on the message.
	* @param lParam - depends on the message.
	* @return returns whether the message has been processed or not.
	*/
	INT_PTR CALLBACK dialogProcedure(HWND hwnd, UINT msg, WPARAM wParam, LPARAM lParam);
}
#endif PROCEDURE_H