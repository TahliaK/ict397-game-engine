#ifndef EDITORMANAGER_H
#define EDITORMANAGER_H

#include "Window.h"
#include "View.h"
#include "ControllerMain.h"
#include "ControllerGL.h"
#include "DialogWindow.h"

#define DIALOG_COUNTER 8

namespace Editor {

	/*
	* @class EditorManager
	* @brief Allows to create windows for two modes: Editor and Game.
	* Provides a user interface for the developer to easily control the game engine.
	* @author Olesia Kochergina
	* @date 15/02/2017
	*/
	class EditorManager {
	public:

		/*
		* @brief Default c-tor.
		*/
		EditorManager(void);

		/*
		* @brief Destructor for unloading resources.
		*/
		~EditorManager(void);

		/*
		* @brief Creates a new application. And also sets the 
		* application mode to either game or editor.
		* @param editorTitle - the name of the application.
		* @param modeFileName - file name of a text file where the editor mode is stored such as 
		* Resources\\EDITOR_MODE.txt; the editor mode is 1, the game mode is 0.
		*/
		void Initialize(const std::string editorTitle, const std::string modeFileName);
		
		/*
		* @brief Starts the application's message loop.
		*/
		void Start(void);

		/*
		* @brief Returns the exit state of the application.
		* @return Exit code.
		*/
		const int GetExitCode(void) const;

		/*
		* @brief Unloads allocated resources.
		*/
		void Clear();

	private:

		/*
		* @brief Starts a message loop to receive and then
		* deliver messages to specific windows for both the editor and the game modes.
		* @param hAccelTable - accelerator table for defining keyboard shortcuts.
		* @return Returns the exit state.
		*/
		int EnterMessageLoop(HACCEL hAccelTable = 0);

		/*
		* @brief Creates a new 3D/2D graphics window.
		* @param mainWindow - reference to the base window.
		* @param glCtrl - reference to the controller for the graphics window.
		* @param glWindow - reference to the graphics window.
		* @param windowName - a string containing the new window's name.
		*/
		void CreateRenderingWindow(Window& mainWindow, ControllerGL& glCtrl, Window& glWindow, std::string windowName);

		/*
		* @brief Reads the editor mode from an external file.
		* @param fileName - file name where the mode is stored based on the location of the executable.
		*/
		void GetEditorMode(const std::string fileName);

		/*
		* @brief Creates editor mode windows and places them
		* at hard-coded positions within the main window.
		*/
		void InitializeEditorMode(void);

		/*
		* @brief Places the main and graphics windows
		* at hard-coded positions.
		*/
		void InitializeGameMode(void);

		/*
		* @brief Ensures that the common control DLL (Comctrl32.dll) is loaded to use
		* some built-in windows. The method should be invoked before creating windows.
		*/
		void InitCommonControl(void) const;

		/*
		* @brief Switches the editor to the play view 
		* submode when viewing a game in the editor. Hides all windows except 
		* the main window, the main menu bar, the graphics window and the tool bar.
		*/
		void PlayView(void);

		/*
		* @brief Switches the editor to the edit view
		* submode when viewing a game in the editor 
		* by displaying all standard windows.
		*/
		void EditView(void);
		
		///array of dialog windows for the editor mode
		Editor::DialogWindow m_dialogWindow[DIALOG_COUNTER];

		///array of pointers to controllers for the dialog windows
		Editor::Controller* ctrlForm[DIALOG_COUNTER];

		///exit code
		int m_exit;

		///the base window
		Window m_mainWindow;

		///controller of the base window
		ControllerMain m_mainCtrl;

		///graphics set up
		View m_glView;

		///controller of the graphics window
		ControllerGL m_glCtrl;

		///graphics window
		Window m_glWindow;
		
		///true is the app is in the editor play view
		bool m_playView;

		///positions of all standard and dialog windows
		Vector4 m_positions[10];
	};
}
#endif EDITORMANAGER_H
