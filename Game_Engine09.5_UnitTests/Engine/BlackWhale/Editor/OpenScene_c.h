#ifndef OPENSCENE_C_H
#define OPENSCENE_C_H
#include "Controller.h"
namespace Editor {

	/*
	* @class OpenScene_c
	* @brief Used as a controller for a dialog window which allows to load and select scenes.
	* @author Olesia Kochergina
	* @date 20/03/2017
	*/
	class OpenScene_c : public Controller {
	public:

		/*
		* @brief Default c-tor.
		*/
		OpenScene_c();

		/*
		* @brief Destructor.
		*/
		~OpenScene_c() {};

		/*
		* @brief Initializes internal controls.
		* @param info - contains information about the window being created.
		* @return Returns 0 to indicate that the message is processed, if -1 then the window is destroyed.
		*/
		int Create(CREATESTRUCT* info);

		/*
		* @brief Destroys the window and its controls.
		* @return Returns 0 to indicate that the message is processed.
		*/
		int Destroy(void);

		/*
		* @brief Called by WM_NOTIFY message received from the message loop.
		* @param id - ID of the control sending the message.
		* @param lParam - pointer to NMHDR which contains the notification code and other information.
		* @return Can be ignored.
		*/
		int Notify(int id, LPARAM lParam);

		/*
		* @brief Identifies which scene to load and makes it the current one.
		* @param wParam - ID of the control
		* @param msg -
		* @param hwnd - handle to the window
		* @return Returns 0 to indicate that the message is processed.
		*/
		int Command(WPARAM wParam, LPARAM msg, HWND hwnd);

		/*
		* @brief Returns index of the selected scene.
		* @return index of the scene.
		*/
		int GetSelectedScene() const;

		/*
		* @brief Sets index of the selected scene.
		* @param index of the scene.
		*/
		void SetSelectedScene(int index);

		/*
		* @brief Reads all .scene files from the "Scenes\" folder and adds them to the list of scenes.
		*/
		void LoadScenes(void);
	private:

		///used for displaying all available scenes.
		ListBox m_listbox;

		///index of the selected item in the listbox. The variable is set to -1 if nothing is selected
		int m_itemIndex;

		/**
		* @brief Adds components' names to the listbox and filters the list depending on
		* the parameter.
		* The filter is case sensitive.
		* @param name - whole or part name of a specific component to be found.
		*/

		int GetSceneIndex(std::string name);
	};

}
#endif // !OPENSCENE_C_H
