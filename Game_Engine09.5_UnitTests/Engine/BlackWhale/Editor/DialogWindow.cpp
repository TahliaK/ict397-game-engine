#include <sstream>
#include <iostream>
#include <cstring>

#include "DialogWindow.h"
#include "procedure.h"
using namespace Editor;

DialogWindow::DialogWindow(HINSTANCE hInst, WORD id, HWND hParent, Controller* ctrl) : handle(0), instance(hInst), id(id),
                                                                                       parentHandle(hParent), controller(ctrl),
                                                                                       m_rect(0)
{
}

DialogWindow::~DialogWindow(){
}

HWND DialogWindow::Create(){
    handle = ::CreateDialogParam(instance, MAKEINTRESOURCE(id), parentHandle, Editor::dialogProcedure, (LPARAM)controller);
	this->Show(SW_HIDE);             
    return handle;
}

void DialogWindow::Show(bool cmdShow){
    ::ShowWindow(handle, cmdShow);
    ::UpdateWindow(handle);
	controller->Paint();
}

void DialogWindow::SetWindowRect(Vector4 rect){
	m_rect = rect;
}