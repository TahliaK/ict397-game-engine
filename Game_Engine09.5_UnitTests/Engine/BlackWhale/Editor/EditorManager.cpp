#include "EditorManager.h"
#include <commctrl.h>  
#include <fstream>
#include <string>
#pragma comment (lib,"comctl32.lib")

#include "../Engine.h"
#include "../Master.h"
#include "../../resource.h"
#include "../Time.h"
#include "..\Input\Input.h"

#include "Console_c.h"
#include "Project_c.h"
#include "Hierarchy_c.h"
#include "Settings_c.h"
#include "Toolbox_c.h"
#include "Bottom_c.h"
#include "Compmenu_c.h"
#include "OpenScene_c.h"

using namespace Editor;
using namespace std;

EditorManager::EditorManager(void) : m_glCtrl(&m_glView) {
	m_exit = -1;
	m_playView = false;
	InitCommonControl();
	IFORSI(0, DIALOG_COUNTER) {
		ctrlForm[i] = NULL;
	}
}
EditorManager::~EditorManager(void) {
	Clear();
}
void EditorManager::Clear() {
	Editor::Controller::Clear();
	if (Master::EDITOR_MODE) {
		IFORSI(0, DIALOG_COUNTER) {
			if (ctrlForm[i] != NULL)
				delete ctrlForm[i];
			ctrlForm[i] = NULL;
		}
	}
}

void EditorManager::InitCommonControl(void) const {
	/*INITCOMMONCONTROLSEX commonCtrls;
	commonCtrls.dwSize = sizeof(commonCtrls);
	commonCtrls.dwICC = ICC_STANDARD_CLASSES | ICC_BAR_CLASSES | ICC_LINK_CLASS | ICC_UPDOWN_CLASS;
	if (!::InitCommonControlsEx(&commonCtrls))
		cout<<("[ERROR] Common Control DLL could not be loaded")<<endl;*/
}

const int EditorManager::GetExitCode(void) const {
	return m_exit;
}

void EditorManager::Initialize(const std::string p_editorTitle, const std::string p_modeFileName) {
	GetEditorMode(p_modeFileName);

	m_mainWindow = Editor::Window(GetModuleHandle(0), p_editorTitle, 0, &m_mainCtrl);
	if (!m_mainWindow.Create()) {
//		Editor::log("[ERROR] Failed to Create main window: %d", GetLastError());
	}
	m_mainCtrl.SetHandle(m_mainWindow.GetHandle());

	//Editor::View view2;
	//Editor::ControllerGL m_glCtrl(&view2);
	/*	Window glWin2;
	m_glCtrl.setRC(m_glCtrl.getRC(), m_glCtrl.getPixelFormat());*/
	//CreateRenderingWindow(m_mainWindow,m_glCtrl, glWin2,"Window2");
	//int win2id = m_mainCtrl.AddWindow(glWin2.GetHandle());
	//Master::EDITOR_MODE = 1;
	CreateRenderingWindow(m_mainWindow, m_glCtrl, m_glWindow, "Window1");
}

void EditorManager::Start() {
	if (Master::EDITOR_MODE) {
		InitializeEditorMode();
	}
	else {
		InitializeGameMode();
	}
	m_glWindow.Show();
	m_mainWindow.Show();
	Engine::Start();
	Engine::ReshapeFunction((int)Master::s_screen[2], (int)Master::s_screen[3]);
	if (Master::EDITOR_MODE)
		Editor::Hierarchy_c::Update();
	m_exit = EnterMessageLoop(0);
}

void EditorManager::CreateRenderingWindow(Window& m_mainWindow, ControllerGL& glCtrl, Window& glWin, std::string windowName) {
	glWin = Editor::Window((HINSTANCE)GetModuleHandle(0), windowName, m_mainWindow.GetHandle(), &glCtrl);
	glWin.SetWindowStyle(WS_CHILD | WS_VISIBLE | WS_CLIPSIBLINGS | WS_CLIPCHILDREN);
	glWin.SetWindowStyleEx(WS_EX_WINDOWEDGE);
	glWin.SetClassStyle(CS_OWNDC);
	glWin.SetWindowRect(Vector4(0, 0, Master::s_screen[2], Master::s_screen[3]));
	if (!glWin.Create()) {
		//Editor::log("[ERROR] Failed to Create OpenGL window: %d", GetLastError());
	}
}

int EditorManager::EnterMessageLoop(HACCEL p_hAccelTable) {
	MSG l_msg;
	HWND l_activeHandle, l_fps, l_dc;
	if (Master::EDITOR_MODE) {
		l_fps = GetDlgItem(ctrlForm[4]->GetHandle(), IDC_STATIC_FPSCOUNTER);
		l_dc = GetDlgItem(ctrlForm[4]->GetHandle(), IDC_STATIC_DCCOUNTER);
	}
	while (true) {
		l_activeHandle = GetActiveWindow();
		if (::PeekMessage(&l_msg, NULL, 0, 0, PM_REMOVE)) {
			if (l_msg.message == WM_QUIT)
				break;
			if (!::TranslateAccelerator(l_msg.hwnd, p_hAccelTable, &l_msg)) {
				::TranslateMessage(&l_msg);
				::DispatchMessage(&l_msg);
			}
		}
		else {//if no messages
			m_glCtrl.Paint();
			Input::Update();
			if (Master::EDITOR_MODE) {//if in the editor
				if (Editor::Toolbox_c::s_switchMode) {//to switch between edit and play modes in the editor
					m_playView = !m_playView;
					if (!m_playView) {
						EditView();
					}
					else {
						PlayView();
					}
					Engine::ReshapeFunction(Master::s_screen[2], Master::s_screen[3]);
					
					Editor::Toolbox_c::s_switchMode = false;
				}
				//update fps textbox
				SendMessage(l_fps, WM_SETTEXT, 0, (LPARAM)std::to_string((int)Time::GetFPS()).c_str());
				//update draw calls textbox
				SendMessage(l_dc, WM_SETTEXT, 0, (LPARAM)std::to_string(GraphicsEngine::S_DRAW_CALLS).c_str());
			}
		}
	}
	m_exit = (int)l_msg.wParam;// return exit state of PostQuitMessage()
	return m_exit;
}

void EditorManager::GetEditorMode(const std::string p_fileName) {
	char BUF[200];
	GetModuleFileName(NULL, BUF, 200);
	std::string location(BUF);
	location = location.substr(0, location.find_last_of('\\'));
	location.append("\\");
	std::ifstream l_file(location + p_fileName);
	std::string l_mode;
	if (l_file.good()) {
		std::getline(l_file, l_mode);
		try {
			Master::EDITOR_MODE = (bool)atoi(l_mode.c_str());
		}catch (exception e) {
			cout<<("[ERROR] Invalid Editor Mode")<<endl;
		}
	}
}

void EditorManager::InitializeEditorMode() {
	WORD dialogType[DIALOG_COUNTER] = { IDD_FORM_HIERARCHY, IDD_FORM_CONSOLE, IDD_FORM_PROJECT, IDD_FORM_SETTINGS, IDD_FORM_TOOLBOX,IDD_DIALOG_BOTTOM, IDD_FORM_COMPMENU,IDD_DIALOG_OPEN_SCENE };
	int dialogIndex[DIALOG_COUNTER];

	//could use another array of pointers
	ctrlForm[0] = new Editor::Hierarchy_c();
	ctrlForm[1] = new Editor::Console_c();
	ctrlForm[2] = new Editor::Project_c();
	ctrlForm[3] = new Editor::Settings_c();
	ctrlForm[4] = new Editor::Toolbox_c();
	ctrlForm[5] = new Editor::Bottom_c();
	ctrlForm[6] = new Editor::Compmenu_c();
	ctrlForm[7] = new Editor::OpenScene_c();

	IFORSI(0, 1) {
		m_dialogWindow[i] = Editor::DialogWindow(GetModuleHandle(0), dialogType[i], m_mainWindow.GetHandle(), ctrlForm[i]);

		if (!m_dialogWindow[i].Create())
			cout<<("[ERROR] Failed to Create Form dialog: %d", GetLastError())<<endl;
	}
	IFORSI(3, DIALOG_COUNTER) {
		m_dialogWindow[i] = Editor::DialogWindow(GetModuleHandle(0), dialogType[i], m_mainWindow.GetHandle(), ctrlForm[i]);

		if (!m_dialogWindow[i].Create())
			cout<<("[ERROR] Failed to Create Form dialog: %d", GetLastError())<<endl;
	}

	HWND hParent = GetDlgItem(ctrlForm[5]->GetHandle(), IDC_TAB_BO_1);
	IFORSI(1, 3) {
		m_dialogWindow[i] = Editor::DialogWindow(GetModuleHandle(0), dialogType[i], hParent, ctrlForm[i]);

		if (!m_dialogWindow[i].Create()) {
			//cout << ("[ERROR] Failed to Create Form dialog: %d", GetLastError()) << endl;
		}
	}
	vector<HWND> tempv;
	tempv.push_back(m_dialogWindow[1].GetHandle());
	tempv.push_back(m_dialogWindow[2].GetHandle());
	((Editor::Bottom_c*)ctrlForm[5])->SetDialogHandles(tempv);
	tempv.clear();
	((Editor::Settings_c*)ctrlForm[3])->SetCompMenu(&m_dialogWindow[6]);

	((Editor::Compmenu_c*)ctrlForm[6])->SetSettingsHandle(ctrlForm[3]->GetHandle());
	((Editor::Hierarchy_c*)ctrlForm[0])->SetCurrentSettings((Editor::Settings_c*)ctrlForm[3]);
	/* ViewForm viewForm1/*(&modelGL)*//*;
											ControllerForm formCtrl1(/*&modelGL,*/ /*&viewForm1);
											DialogWindow formDialog1(GetModuleHandle(0), IDD_FORM_SETTINGS, m_mainWindow.GetHandle(), &formCtrl1);
											if(formDialog1.Create())
											log("Form dialog is created.");
											else
											log("[ERROR] Failed to Create Form dialog: %d",GetLastError());
											int d1ID = m_mainCtrl.AddDialog(formDialog1.GetHandle());
											// send gl controllers to form controller to draw after any event
											*/
											/*	ViewForm viewForm2/*(&modelGL)*//*;
																					 ControllerForm formCtrl2(/*&modelGL,*//* &viewForm2);
																					 DialogWindow formDialog2(GetModuleHandle(0), IDD_FORM_CONSOLE, m_mainWindow.GetHandle(), &formCtrl2);
																					 if(formDialog2.Create())
																					 log("Form dialog is created.");
																					 else
																					 log("[ERROR] Failed to Create Form dialog: %d",GetLastError());
																					 int d2ID = m_mainCtrl.AddDialog(formDialog2.GetHandle()) */

	Editor::Controller::SetGraphicsController(m_glCtrl);
	Controller::SetSceneDlg(m_dialogWindow[7]);

	BOOL show = true;
	//windows 8 and above, should use "Button" for windows 7
	HWND taskbar = FindWindow(("Shell_TrayWnd"), NULL);
	RECT rect;
	int offset = 0;
	if (taskbar != NULL) {
		ShowWindow(taskbar, show ? SW_SHOW : SW_HIDE);//hide/show the taskbar
		UpdateWindow(taskbar);
		if (taskbar && GetWindowRect(taskbar, &rect))
			offset = rect.bottom - rect.top;
	}


	RECT dialogRect[DIALOG_COUNTER];
	int dimen[DIALOG_COUNTER][2];
	IFORSI(0, DIALOG_COUNTER) {
		::GetWindowRect(m_dialogWindow[i].GetHandle(), &dialogRect[i]);
		dimen[i][0] = (float)(dialogRect[i].right - dialogRect[i].left);
		dimen[i][1] = (float)(dialogRect[i].bottom - dialogRect[i].top);
	}

	float l_yMax = GetSystemMetrics(SM_CYSCREEN) - offset - 60;
	Master::s_screen[3] = l_yMax;
	m_positions[0] = Vector4(0, 0, dimen[0][0], dimen[0][1]);//hierarchy
	m_positions[1] = Vector4(0, 30, dimen[5][0], Master::s_screen[3]);//console
	m_positions[2] = Vector4(0, 30, dimen[5][0], Master::s_screen[3]);//project
	m_positions[3] = Vector4(dimen[5][0] + 60, 0, dimen[3][0] + 60, Master::s_screen[3] + 20);//settings
	m_positions[4] = Vector4(dimen[5][0] + 60 - dimen[4][0], 0, dimen[4][0] + 60, dimen[4][1]);//toolbox
	m_positions[5] = Vector4(0, dimen[0][1], dimen[5][0], Master::s_screen[3]);//bottom tab control
	//m_positions[6] = Vector4(100, 0, dimen[6][0], dimen[6][1]);//component menu
	//m_positions[7] = Vector4(100, 0, dimen[7][0], dimen[7][1]);//open scene menu
	m_positions[8] = Vector4(dimen[0][0], 0, dimen[5][0] - dimen[0][0], dimen[0][1]);//gl window
	m_positions[9] = Vector4(0, 0, 1366, GetSystemMetrics(SM_CYSCREEN) - offset + 10);//main window

	EditView();
}

void EditorManager::InitializeGameMode() {
	m_mainWindow.SetCurrentMenu(NULL);
	//BOOL show = true;
	////windows 8 and above, should use "Button" for windows 7
	//HWND taskbar = FindWindow(("Shell_TrayWnd"), NULL);
	//RECT rect;
	//int offset = 0;
	//if (taskbar != NULL) {
	//	ShowWindow(taskbar, show ? SW_SHOW : SW_HIDE);//hide/show the taskbar
	//	UpdateWindow(taskbar);
	//	if (taskbar && GetWindowRect(taskbar, &rect))
	//		offset = rect.bottom - rect.top;
	//}
	RECT xy;
	BOOL fResult = SystemParametersInfo(SPI_GETWORKAREA, 0, &xy, 0);
	Master::s_screen[3] = xy.bottom - xy.top;
	Master::s_screen[2] = xy.right - xy.left;
	int glOffset = 5;
	::SetWindowPos(m_glWindow.GetHandle(), 0, Master::s_screen[0], Master::s_screen[1], Master::s_screen[2], Master::s_screen[3], SWP_NOZORDER);
	::SetWindowPos(m_mainWindow.GetHandle(), 0, Master::s_screen[0], Master::s_screen[1], Master::s_screen[2], Master::s_screen[3], SWP_NOZORDER);
}

void EditorManager::PlayView() {
	m_mainWindow.SetCurrentMenu(NULL);
	IFORSI(0, DIALOG_COUNTER) {
		m_dialogWindow[i].Show(false);
	}
	m_dialogWindow[4].Show();

	RECT l_toolboxRect;
	::GetWindowRect(m_dialogWindow[4].GetHandle(), &l_toolboxRect);
	int x = (l_toolboxRect.right - l_toolboxRect.left);
	int y = (l_toolboxRect.bottom - l_toolboxRect.top);

	RECT l_mainRect;
	::GetWindowRect(m_mainWindow.GetHandle(), &l_mainRect);
	int mX = (l_mainRect.right - l_mainRect.left);
	int mY = (l_mainRect.bottom - l_mainRect.top);
	Master::s_screen[3] = mY;
	Master::s_screen[2] = mX - x;

	::SetWindowPos(m_dialogWindow[4].GetHandle(), 0, mX - x, 0, mX, mY, SWP_NOZORDER);
	::SetWindowPos(m_glWindow.GetHandle(), 0, 0, 0, Master::s_screen[2], Master::s_screen[3], SWP_NOZORDER);
}

void EditorManager::EditView() {
	m_mainWindow.SetCurrentMenu((HMENU)1);
	IFORSI(0, DIALOG_COUNTER) {
		if (i != 6 && i != 7) {
			::SetWindowPos(m_dialogWindow[i].GetHandle(), 0, m_positions[i][0], m_positions[i][1], m_positions[i][2], m_positions[i][3], SWP_NOZORDER);
		}
	}
	::SetWindowPos(m_glWindow.GetHandle(), 0, m_positions[8][0], m_positions[8][1], m_positions[8][2], m_positions[8][3], SWP_NOZORDER);
	::SetWindowPos(m_mainWindow.GetHandle(), 0, m_positions[9][0], m_positions[9][1], m_positions[9][2], m_positions[9][3], SWP_NOZORDER);

	Master::s_screen[2] = m_positions[1][2] - m_positions[0][2];
	Master::s_screen[3] = m_positions[0][3];
	IFORSI(0, DIALOG_COUNTER - 2) {
		if (i != 1 && i != 2) {
			m_dialogWindow[i].Show(true);
			RedrawWindow(m_dialogWindow[i].GetHandle(), NULL, NULL, RDW_ERASE | RDW_INVALIDATE | RDW_FRAME | RDW_ALLCHILDREN);
		}
	}
}