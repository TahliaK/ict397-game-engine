#ifndef CONTROLLER_MAIN_H
#define CONTROLLER_MAIN_H

#include "Controller.h"

namespace Editor {

	/*
	* @class ControllerMain
	* @brief Controller for the main app window.
	* @author Olesia Kochergina
	* @date 22/02/2017
	*/
	class ControllerMain : public Controller
	{
	public:

		/*
		* @brief Default c-tor.
		*/
		ControllerMain();

		/*
		* @brief Destructor.
		*/
		~ControllerMain() {};

		/*
		* @brief Destroys the window and its children.
		* @return Returns 0 to indicate that the message is processed.
		*/
		int Close();                               

		/*
		* @brief Initializes internal controls.
		* @param info - contains information about the window being created.
		* @return Returns 0 to indicate that the message is processed, if -1 then the window is destroyed.
		*/
		int Create(CREATESTRUCT* info);

		/*
		* @brief Quits the application.
		* @return Returns 0 to indicate that the message is processed.
		*/
		int Destroy();                  

		/*
		* @brief Called by WM_SIZE message received from the message loop.
		* @param width - new width of the client area.
		* @param height - new height of the client area.
		* @param wParam - type of resizing
		* @return Returns 0 to indicate that the message is processed, else 1.
		*/
		int Resize(int w, int h, WPARAM wParam);      
	private:
	};
}
#endif CONTROLLER_MAIN_H
