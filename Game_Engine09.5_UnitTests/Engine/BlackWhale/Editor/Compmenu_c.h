#ifndef COMPMENU_C_H
#define COMPMENU_C_H
#include "Controller.h"
namespace Editor {

	/*
	* @class Compmenu_c
	* @brief Used as a controller for a window with a list of available game components
	* and allows to add the components to the currently selected gameobject.
	* @author Olesia Kochergina
	* @date 15/02/2017
	*/
	class Compmenu_c : public Controller {
	public:

		/*
		* @brief Default c-tor.
		*/
		Compmenu_c();

		/*
		* @brief Destructor.
		*/
		~Compmenu_c() {};

		/*
		* @brief Initializes internal controls.
		* @param info - contains information about the window being created.
		* @return Returns 0 to indicate that the message is processed, if -1 then the window is destroyed.
		*/
		int Create(CREATESTRUCT* info);

		/*
		* @brief Destroys the window and its controls.
		* @return Returns 0 to indicate that the message is processed.
		*/
		int Destroy();

		/*
		* @brief Allows to find and select a component to add to the current go.
		* @param wParam - ID of the control
		* @param msg -
		* @param hwnd - handle to the window
		* @return Returns 0 to indicate that the message is processed.
		*/
		int Command(WPARAM wParam, LPARAM msg, HWND hwnd);

		/*
		* @brief Returns the index of the currently selected component.
		* @return index of the component.
		*/
		int GetSelectedComponent(void) const;

		/*
		* @brief Sets the index of the currently selected component.
		* @param index of the component.
		*/
		void SetSelectedComponent(int index);

		/*
		* @brief Sets a handle to the settings window.
		* @param handle to the settings window.
		*/
		void SetSettingsHandle(HWND handle);

	private:

		///control with a list of available components
		ListBox m_listbox;

		///control for searching for a component
		EditBox m_editbox;

		///handle to the settings window
		HWND m_setHandle;

		///index of the selected item in the listbox. The variable is set to -1 if nothing is selected
		int m_itemIndex;

		/**
		* @brief Adds components' names to the listbox and filters the list depending on
		* the parameter.
		* The filter is case sensitive.
		* @param name - whole or part name of a specific component to be found.
		* @param compare - should be set to true if only specific components have to be added.
		*/
		void AddComponents(std::string name, bool compare);

		/*
		* @brief Finds the index of a component by its name.
		* @param name - name of the component.
		* @return index of the component
		*/
		int GetComponentIndex(std::string name);
	};

}
#endif COMPMENU_C_H