#ifndef CONTROLLER_GL_H
#define CONTROLLER_GL_H

#include "Controller.h"
#include "View.h"

namespace Editor {

	/*
	* @class ControllerGL
	* @brief Controller for OpenGL rendering window.
	* @author Olesia Kochergina
	* @date 22/02/2017
	*/
	class ControllerGL : public Controller
	{
	public:

		/*
		* @brief Default c-tor.
		*/
		ControllerGL() {};

		/*
		* @brief C-tor with the window's view.
		*/
		ControllerGL(View* view);

		/*
		* @brief Destructor.
		*/
		~ControllerGL() {};

		/*
		* @brief Closes the OpenGL window.
		* @return Returns 0 to indicate that the messaged is processed.
		*/
		int Close(void);

		/*
		* @brief Called by WM_COMMAND message received from the message loop.
		* @param wParam - ID of the control
		* @param msg -
		* @param hwnd - handle to the window
		* @return Returns 0 to indicate that the message is processed.
		*/
		int Command(int id, int cmd, LPARAM msg);

		/*
		* @brief Creates OpenGL rendering context.
		* @param info - contains information about the window being created.
		* @return Returns 0 to indicate that the message is processed, if -1 the creation failed.
		*/
		int Create(CREATESTRUCT* info);

		/*
		* @brief Destroys the window as well as its controls and releases DC.
		* @return Returns 0 to indicate that the message is processed.
		*/
		int Destroy();

		/*
		* @brief Updates the game engine.
		*/
		int Paint(void);

		/*
		* @brief Resized the OpenGL window and the engine's internal components.
		* @param width - new width of the client area.
		* @param height - new height of the client area.
		* @param wParam - type of resizing
		* @return Returns 0 to indicate that the message is processed, else 1.
		*/
		int Resize(int width, int height, WPARAM type);

		/*
		* @brief Returns rendering context.
		* @return rendering context.
		*/
		HGLRC GetRC(void) const;

		/*
		* @brief Returns the pixel format.
		* @return pixel format.
		*/
		int GetPixelFormat(void) const;

		/*
		* @brief Sets rendering context.
		* @param rc - rendering context.
		* @param pixelFormat - pixel format.
		*/
		void SetRC(HGLRC rc, int pixelFormat);

	private:

		///rendering context
		HGLRC m_hglrc;

		///pixel format
		int m_pixelFormat;

		///OpenGL view
		View* m_view;

		///true - the app already has a rendering context
		static bool s_isCreatedContext;
	};
}
#endif CONTROLLER_GL_H
