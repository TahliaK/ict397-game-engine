#ifndef Toolbox_c_H
#define Toolbox_c_H
#include "Controller.h"

namespace Editor {

	/*
	* @class Bottom_c
	* @brief Used as a controller for a window which 
	* provides a user-friendly way to manipulate game objects. NOT FULLY IMPLEMENTED
	* @author Olesia Kochergina
	* @date 03/03/2017
	*/
	class Toolbox_c : public Controller {
	public:

		/*
		* @brief Default c-tor.
		*/
		Toolbox_c();

		/*
		* @brief Destructor.
		*/
		~Toolbox_c() {};

		/*
		* @brief Initializes internal controls.
		* @param info - contains information about the window being created.
		* @return Returns 0 to indicate that the message is processed, if -1 then the window is destroyed.
		*/
		int Create(CREATESTRUCT* info);

		/*
		* @brief Destroys the window and its controls.
		* @return Returns 0 to indicate that the message is processed.
		*/
		int Destroy();

		/*
		* @brief Called by WM_NOTIFY message received from the message loop.
		* @param id - ID of the control sending the message.
		* @param lParam - pointer to NMHDR which contains the notification code and other information.
		* @return Can be ignored.
		*/
		int Notify(int id, LPARAM lParam);

		/*
		* @brief Allows to switch between two editor modes as well as the rendering modes (normal/wireframe)
		* @param wParam - ID of the control
		* @param msg -
		* @param hwnd - handle to the window
		* @return Returns 0 to indicate that the message is processed.
		*/
		int Command(WPARAM wParam, LPARAM msg, HWND hwnd);

		///true - the user wants to change the editor mode.
		static bool s_switchMode;
	};
}
#endif Toolbox_c_H