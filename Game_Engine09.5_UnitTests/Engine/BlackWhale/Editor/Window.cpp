#pragma warning(disable : 4996)
#include <sstream>
#include <iostream>
#include <cstring>

#include "../Struct.h"
#include "Window.h"
#include "Procedure.h"
#include "../../resource.h"
#include "../Engine.h"

#include "../GameObject.h"

using std::wstringstream;
using std::wcout;
using std::endl;
using namespace Editor;

Window::Window(HINSTANCE hInst, std::string name, HWND hParent, Controller* ctrl) : m_handle(0), m_instance(hInst), controller(ctrl), m_winStyle(WS_OVERLAPPEDWINDOW | WS_CLIPCHILDREN),
m_winStyleEx(WS_EX_CLIENTEDGE), m_size(CW_USEDEFAULT), m_pHandle(hParent), m_menuHandle(0) {

	m_title = strdup(name.c_str());
	m_className = strdup(name.c_str());
	m_menuHandle = NULL;
	m_winClass.cbSize = sizeof(WNDCLASSEX);
	m_winClass.style = CS_OWNDC | CS_HREDRAW | CS_VREDRAW;                                     // class styles: CS_OWNDC, CS_PARENTDC, CS_CLASSDC, CS_GLOBALCLASS, ...
	m_winClass.lpfnWndProc = Editor::windowProcedure;                  // pointer to window procedure
	m_winClass.cbClsExtra = 0;
	m_winClass.cbWndExtra = 0;
	m_winClass.hInstance = m_instance;                              // owner of this class
	m_winClass.hIcon = LoadIcon(m_instance, MAKEINTRESOURCE(IDI_ICON_MAIN));   // default icon
	m_winClass.hIconSm = 0;
	m_winClass.hCursor = LoadCursor(0, IDC_ARROW);              // default arrow cursor
	m_winClass.hbrBackground = (HBRUSH)GetStockObject(LTGRAY_BRUSH);   // default white brush
	m_winClass.lpszMenuName = NULL;
	m_winClass.lpszClassName = m_className;
	m_winClass.hIconSm = LoadIcon(m_instance, MAKEINTRESOURCE(IDI_ICON_MAIN));   // default small icon

}

Window::~Window()
{
	::UnregisterClass(m_className, m_instance);
}

HWND Window::Create() {
	// register a window class
	if (!::RegisterClassEx(&m_winClass))
		return 0;

	m_handle = ::CreateWindowEx(
		m_winStyleEx,           // window border with a sunken edge
		m_className,            // name of a registered window class
		m_title,                // caption of window
		m_winStyle,             // window style
		m_size[0],              // x position
		m_size[1],              // y position
		m_size[2],              // width
		m_size[3],              // height
		m_pHandle,              // handle to parent window
		m_menuHandle,           // handle to menu
		m_instance,             // application instance
		(LPVOID)controller);    // window controller
	return m_handle;
}

void Window::Show(bool p_cmdShow) {
	::ShowWindow(m_handle, p_cmdShow);
	::UpdateWindow(m_handle);
	controller->Paint();
}

HWND Window::GetHandle() {
	return m_handle;
}


HICON Window::m_LoadIcon(int p_ID) {
	return (HICON)::LoadImage(m_instance, MAKEINTRESOURCE(p_ID), IMAGE_ICON, 0, 0, LR_DEFAULTSIZE);
}


HICON Window::m_LoadCursor(int p_ID) {
	return (HCURSOR)::LoadImage(m_instance, MAKEINTRESOURCE(p_ID), IMAGE_CURSOR, 0, 0, LR_DEFAULTSIZE);
}

void Window::SetClassStyle(UINT style) {
	m_winClass.style = style;
}
void Window::SetIcon(int ID) {
	m_winClass.hIcon = m_LoadIcon(ID);
}
void Window::SetIconSmall(int ID) {
	m_winClass.hIconSm = m_LoadIcon(ID);
}
void Window::SetCursor(int ID) {
	m_winClass.hCursor = m_LoadCursor(ID);
}
void Window::SetBackground(int color) {
	m_winClass.hbrBackground = (HBRUSH)::GetStockObject(color);
}
void Window::SetMenuName(LPCTSTR name) {
	m_winClass.lpszMenuName = name;
}
void Window::SetWindowStyle(DWORD style) {
	m_winStyle = style;
}
void Window::SetWindowStyleEx(DWORD style) {
	m_winStyleEx = style;
}
void Window::SetWindowRect(const Vector4& Resize) {
	m_size = Resize;
}
void Window::SetParent(HWND handle) {
	m_pHandle = handle;
}
void Window::SetCurrentMenu(HMENU menu) {
	if (menu == NULL) {
		SetMenu(m_handle,NULL);
	}
	else if(!m_menuHandle){
		m_menuHandle = LoadMenu(m_instance, MAKEINTRESOURCE(IDR_MENU_MAIN));
		::SetMenu(m_handle,m_menuHandle);
	}
	else
	{
		::SetMenu(m_handle,m_menuHandle);
	}
}