#include <exception>
#include <cstdlib>
#include <string>
#include <sstream>

#include "Project_c.h"
#include "../../resource.h"
using namespace Editor;

Project_c::Project_c() {
}


int Project_c::Destroy() {
	::DestroyWindow(handle);
	return 0;
}

void Project_c::SetDirectory(HTREEITEM p_parent, std::string p_dirName, std::vector<std::string>& p_fileNames, bool p_updateTree) {
	p_fileNames.clear();
	std::string l_path = p_dirName + "/*.*";
	WIN32_FIND_DATA l_fd;
	HANDLE l_hFind = ::FindFirstFile(l_path.c_str(), &l_fd);
	if (l_hFind != INVALID_HANDLE_VALUE) {
		do {
			// read all (real) files in current folder
			// , delete '!' read other 2 default folder . and ..
			if (!(l_fd.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY))
			{
				p_fileNames.push_back(l_fd.cFileName);
			}
			else if (p_updateTree && !(l_fd.cFileName[0] == '.' && l_fd.cFileName[1] == '.' && l_fd.cFileName[2] == '\0') && !(l_fd.cFileName[0] == '.' && l_fd.cFileName[1] == '\0')) {
				LPSTR temp = (LPSTR)(l_fd.cFileName);
				HTREEITEM l_curParent = m_directory.InsertItem(temp, p_parent, TVI_LAST, 0, 0);
				std::string l_curDirectory(p_dirName);
				l_curDirectory.append("\\");
				l_curDirectory.append(l_fd.cFileName);
				SetDirectory(l_curParent, l_curDirectory, p_fileNames, p_updateTree);
			}
		} while (::FindNextFile(l_hFind, &l_fd));
		::FindClose(l_hFind);
	}
}

int Project_c::Create(CREATESTRUCT* info) {
	m_updateTree = false;
	m_addItemFromList = false;
	m_directory = TreeView(handle, IDC_TREE_PR_1, true);
	m_items = ListControl(handle, IDC_LIST_PR_1, true);
	char str[100];
	DWORD size = 100;
	LPTSTR lp = str;
	GetCurrentDirectory(size, lp);
	string l_directory(str);
	//cout<<l_directory<<endl;
	vector<string> dirNames;
	vector<string> fileNames;
	SetDirectory(TVI_ROOT, l_directory, fileNames, true);
	IFORSI(0, fileNames.size()) {
		//cout<<fileNames.at(i)<<endl;
		LPSTR temp = (LPSTR)(fileNames.at(i).c_str());
		m_items.AddItem(temp, i);
	}
	HICON icon = LoadIcon(GetModuleHandle(0), MAKEINTRESOURCE(IDI_ICON_FOLDER));
	HIMAGELIST l_imageList = ImageList_Create(32, 32, ILC_MASK, 1, 1);
	int imageIndex = ImageList_AddIcon(l_imageList, icon);

	//SendMessage(directory->GetHandle(),IDC_TREE_PR_1,TVM_SETIMAGELIST,0,(LPARAM)l_imageList);
	m_directory.SetImageList(l_imageList);
	return 0;
}

int Project_c::Command(WPARAM wParam, LPARAM msg, HWND hwnd) {
	switch (LOWORD(wParam))
	{
	case IDC_TREE_PR_1:
	case IDC_LIST_PR_1:
		break;
	case IDC_EDIT_IN_SZ:
		if (HIWORD(wParam) == EN_KILLFOCUS) {
			char input[100];
			GetDlgItemText((HWND)hwnd, LOWORD(wParam), input, sizeof(input));
			string in(input);
			float l_number = 0;
			bool l_newValue = false;
			try {
				l_number = std::stof(in);
				char l_char[30];
				sprintf(l_char, "%f", l_number);
				l_newValue = true;
			}
			catch (exception e) {
				cout << "[ERROR] Project_c.cpp: " << e.what() << endl;
			}
			char l_char[30];
			sprintf(l_char, "%f", l_number);
			SetDlgItemText(hwnd, LOWORD(wParam), (LPCSTR)l_char);
		}
		break;
	}

	return 0;
}

std::string Project_c::GetDirectory() {
	const DWORD size = 100;
	char str[size];
	LPTSTR lp = str;
	GetCurrentDirectory(size, lp);

	std::string l_return(str);
	return l_return;
}

int Project_c::Notify(int id, LPARAM lParam) {
	// first cast lParam to NMHDR* to know what the control is
	NMHDR* nmhdr = (NMHDR*)lParam;
	HWND from = nmhdr->hwndFrom;
	switch (nmhdr->code) {
	case NM_CLICK:
		m_updateTree = true;
		//cout<<"clicked"<<endl;
		break;
	case NM_DBLCLK:
		m_addItemFromList = true;
		//cout<<"double  click"<<endl;
		//cout<<typeid(directory).name()<<endl;
	case TVN_DELETEITEM:
	case TVN_SELCHANGING:
		//cout<<"TV changing selection"<<endl;
		break;
	case TVN_SELCHANGED:
	{
		if (m_updateTree) {//ensures that NM_CLICK (left mouse button) event was created before this event
			m_updateTree = false;
			//cout<<"TVN changed selection"<<endl;
			const int L_SIZE = 100;
			char str[L_SIZE];
			std::string l_directory = GetDirectory();
			TVITEM l_item;
			l_item.hItem = m_directory.GetSelected();
			string l_temp;
			//start from the selected item and traverse to the root to get all directory names
			do {
				l_item.mask = TVIF_TEXT;
				l_item.cchTextMax = L_SIZE;
				l_item.pszText = str;
				m_directory.GetItem(&l_item);
				l_temp.append("|");
				l_temp.append((CHAR*)l_item.pszText);
				HTREEITEM l_itemParent = m_directory.GetParent(l_item.hItem);
				l_item.hItem = l_itemParent;
			} while (l_item.hItem != NULL);

			//add directory names in the reverse order
			for (int i = l_temp.size() - 1; i >= 0; i--) {
				if (l_temp.at(i) == '|') {
					l_directory.append("\\");
					l_directory.append(l_temp.substr(i + 1));
					l_temp = l_temp.substr(0, i);
				}
			}

			m_items.Clear();
			std::vector<std::string> fileNames;
			SetDirectory(TVI_ROOT, l_directory, fileNames, false);
			IFORSI(0, fileNames.size()) {
				LPSTR temp = (LPSTR)(fileNames.at(i).c_str());
				m_items.AddItem(temp, i);
			}
		}

	}
	break;
	case LVN_ITEMCHANGED:
		//cout<<"LVN changing selection"<<endl;
		break;
	}

	return 0;
}
