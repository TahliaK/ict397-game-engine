#include "ShaderManager.h"
#include "Master.h"
#include <string>
unsigned int ShaderManager::m_activeShader = 0;
unsigned int ShaderManager::m_amount = 0;
std::vector<ShaderProgram> ShaderManager::m_shaders;

ShaderProgram* ShaderManager::ActivateShaderProgram(unsigned int p_ID) {
	//p_ID = 27;
	/*if (m_activeShader == p_ID) {
		return GetActive();
	}
	if (p_ID == 0) {
		m_activeShader = 0;
		Master::gl.UseProgram(m_activeShader);
	}
	else*/ {
		for (unsigned int i = 0; i < m_amount; i++) {
			if (m_shaders.at(i).GetID() == p_ID) {
				m_activeShader = p_ID;
				Master::gl.UseProgram(m_activeShader);
				m_shaders.at(i).Activate();
				return  &m_shaders.at(i);
			}
		}
	}
	return NULL;
}

ShaderProgram* ShaderManager::Get(unsigned int p_ID) {
	for (unsigned int i = 0; i < m_amount; i++) {
		if (m_shaders.at(i).GetID() == p_ID) {
			return  &m_shaders.at(i);
		}
	}
	return NULL;
}

ShaderProgram* ShaderManager::GetActive(void) {
	if (m_activeShader == 0) {
		return NULL;
	}
	else {
		for (unsigned int i = 0; i < m_amount; i++) {
			if (m_shaders.at(i).GetID() == m_activeShader) {
				return  &m_shaders.at(i);
			}
		}
	}
	return NULL;
}

unsigned int ShaderManager::CreateShaderProgram(std::string p_v, std::string p_f) {
	cout << "CREATE NEW SHADER PROGRAM" << endl;
	for (unsigned int i=0; i < m_amount;++i) {
		if (m_shaders.at(i).GetFragmentShader().compare(p_f) == 0 && m_shaders.at(i).GetVertexShader().compare(p_v) == 0) {
			//cout <<"loop, break: "<< p_v << endl;
			return m_shaders.at(i).GetID();
		}			
	}
	m_shaders.push_back(ShaderProgram());
	//Master::InitializeShader(m_shaders.back(), p_v, p_f);
	m_shaders.at(m_amount).SetFragmentShader(p_f);
	m_shaders.at(m_amount).SetVertexShader(p_v);

	m_shaders.at(m_amount).Create();
	m_shaders.at(m_amount).Attach();
	m_shaders.at(m_amount).Link();

	if (m_shaders.at(m_amount).GetID() == -1) {
		std::cout << "SHADER PROGRAM ERROR: " << p_v << "  " << p_f << std::endl;
	}
	else {
		m_shaders.at(m_amount).SetLocations();
	}
	//cout << "ID: " << m_shaders.at(m_amount).GetID() << endl;	
	//if(m_amount==1)
	//cout << "checking " << m_shaders.at(m_amount).GetID()<<"  "<< m_shaders.at(m_amount).GetFragmentShader() << "  " << m_shaders.at(m_amount-1).GetID() << "  " << m_shaders.at(m_amount-1).GetFragmentShader() << endl;
	m_amount++;
	return m_shaders.at(m_amount-1).GetID();
}

void ShaderManager::Clear() {
	for (unsigned int i = 0; i < m_shaders.size(); ++i) {
		m_shaders.at(i).Clear();
	}
}
