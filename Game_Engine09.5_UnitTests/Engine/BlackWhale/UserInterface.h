#ifndef PANEL_H
#define PANEL_H
#include "Component.h"
#include "GameObject.h"
#include "Material_s.h"

/*
* @class UserInterface
* @brief Base class for every 2D game object such as images and labels.
* @version 02
* @date 18/04/2017
* @author Olesia Kochergina
*/
class UserInterface : public Component {
public:

	/*
	* @brief Default c-tor.
	*/
	UserInterface();

	/*
	* @brief Destructor.
	*/
	virtual ~UserInterface();

	/*
	* @brief Initializes the object, called when creating the object.
	*/
	virtual void Initialize();

	/*
	* @brief Called just before the first update.
	*/
	virtual void Start() {};

	/*
	* @brief Updates the object and should be called every frame.
	*/
	virtual void Update();

	/*
	* @brief Clears internal data, should be called when
	* the object needs to be either reused or deleted.
	*/
	virtual void Clear() {};

	/*
	* @brief Creates a new component of this class, should be overriden by children.
	* @return Pointer to the new component or NULL.
	*/
	virtual Component* NewComponent(void) const = 0;

	/*
	* @brief Sets the color of the element.
	* @param color - RGBA variable which holds the color of the object.
	* @return true - if the color is successfully set.
	*/
	bool SetColor(boost::any color);

	/*
	* @brief Sets the order of the element.
	* Note that its NOT IMPLEMENTED
	* @param order - int variable.
	* @return true - if the order is set.
	*/
	bool SetOrder(boost::any order);

	/*
	* @brief Sets the interactability of the element which is used to detect collisions with the mouse.
	* @param interactable - bool variable.
	* @return true - if the order is set.
	*/
	bool SetInteractable(boost::any interactable);

	/*
	* @brief Returns the current color of the object.
	* @return color of the object.
	*/
	RGBA GetColor(void) const;

	/*
	* @brief Returns the current order of the object.
	* @return order of the object.
	*/
	int GetOrder(void) const;

	/*
	* @brief Returns the interactability of the object.
	* @return interactability of the object.
	*/
	bool GetInteractable(void) const;

	/*
	* @brief Returns whether the mouse is over the object or not.
	* @return true if the mouse is over the object.
	*/
	bool IsMouseOver(void) const;

	/*
	* @brief Sets up a 2D camera, should be called when starting the game engine.
	*/
	static void SetUp();
protected:

	/*
	* @brief Checks whether the mouse is over the object or not,
	* should be overriden by children.
	*/
	virtual void Interactivity() = 0;

	///color of the element
	RGBA m_color;

	///order of the element
	int m_order;

	///collision detection with the mouse
	bool m_mouseOver;

	///interactability of the object
	bool m_interactable;

	///static 2D camera for all 2d elements
	static GameObject m_camera;
private:

	/*
	* @brief Adds values to three vectors: variable name,  setter, getter.
	* Used in the Editor to modify components at run-time.
	*/
	void Reflection();

	///serialization
	friend class boost::serialization::access;
	template<class Archive>
	void save(Archive& ar, const unsigned int /*version*/) const {
		ar & SER_BASE_OBJECT_NVP(Component);
		ar& SER_NVP(m_color);
		ar& SER_NVP(m_order);
		ar& SER_NVP(m_interactable);
	}

	template<class Archive>
	void load(Archive& ar, const unsigned int /*version*/) {
		ar & SER_BASE_OBJECT_NVP(Component);
		ar& SER_NVP(m_color);
		ar& SER_NVP(m_order);
		ar& SER_NVP(m_interactable);
	}
	SER_SPLIT_MEMBER()
};

SER_CLASS_VERSION(UserInterface, 0);
SER_ABSTRACT(UserInterface)
SER_CLASS_EXPORT_KEY(UserInterface)
#endif PANEL_H