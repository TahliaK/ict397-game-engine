#include "procedure.h"
#include "Controller.h"
#include "..\Input\Input_Win.h"
#include "Log.h"
#include "Engine.h"
#include "Console_c.h"
#include "Settings_c.h"
#include "Hierarchy_c.h"
#include "Project_c.h"
#include "Toolbox_c.h"
using namespace Editor::Win;

LRESULT CALLBACK Editor::Win::windowProcedure(HWND hwnd, UINT msg, WPARAM wParam, LPARAM lParam){
	LRESULT returnValue = 0;        // return value
	// find controller associated with window handle
	static Controller *ctrl;
	ctrl = (Controller*)::GetWindowLongPtr(hwnd, GWLP_USERDATA);
	if(msg == WM_NCCREATE){
		// WM_NCCREATE message is called before non-client parts(border,
		// titlebar, menu,etc) are created. This message comes with a pointer
		// to CREATESTRUCT in lParam. The lpCreateParams member of CREATESTRUCT
		// actually contains the value of lpPraram of CreateWindowEX().
		// First, retrieve the pointrer to the controller specified when
		// Win::Window is setup.
		ctrl = (Controller*)(((CREATESTRUCT*)lParam)->lpCreateParams);
		ctrl->SetHandle(hwnd);

		// Second, store the pointer to the Controller into GWLP_USERDATA,
		// so, other messege can be routed to the associated Controller.
		::SetWindowLongPtr(hwnd, GWLP_USERDATA, (LONG_PTR)ctrl);

		return  ::DefWindowProc(hwnd, msg, wParam, lParam);
	}

	// check NULL pointer, because GWLP_USERDATA is initially 0, and
	// we store a valid pointer value when WM_NCCREATE is called.
	if(!ctrl)
		return ::DefWindowProc(hwnd, msg, wParam, lParam);

	// route messages to the associated controller
	switch(msg)
	{
	case WM_CREATE:		
		returnValue = ctrl->Create();
		break;

	case WM_SIZE:
		returnValue = ctrl->Resize(LOWORD(lParam), HIWORD(lParam), (int)wParam);    // width, height, type
		break;

	case WM_ENABLE:
		{
			bool flag;
			if(wParam) 
				flag = true;
			else 
				flag = false;
			returnValue = ctrl->enable(flag);  // TRUE or FALSE
			break;
		}

	case WM_PAINT:
		ctrl->paint();
		returnValue = ::DefWindowProc(hwnd, msg, wParam, lParam);
		break;

	case WM_COMMAND:
		cout<<"working"<<endl;
		returnValue =(ctrl)->Command(wParam, lParam, hwnd);
		break;

	case WM_CLOSE:
		returnValue = ctrl->Close();
		break;
	
	case WM_DESTROY:
		returnValue = ctrl->Destroy();
		break;

	case WM_SYSCOMMAND:
		//returnValue = ctrl->sysCommand(wParam, lParam);
		returnValue = ::DefWindowProc(hwnd, msg, wParam, lParam);
		break;
		/*
		// Disable Alt-F4 and screensavers
		switch (wparam & 0xfff0)
		{
		case SC_CLOSE:
		case SC_SCREENSAVE:
		ret = 0;
		break;

		default:
		returnValue = DefWindowProc(hwnd, message, wparam, lparam);
		break;
		}
		break;
		*/

	case WM_CHAR:
		cout<<"wm_char:  "<<(char)wParam<<endl;
		Input_Win::ProcessNormal((int)wParam,true);
		//returnValue = ctrl->char(wParam, lParam);   // route keycode
		break;

	case WM_KEYDOWN:

	case WM_SYSKEYDOWN:
		// returnValue = ctrl->keyDown((int)wParam, lParam);                       // keyCode, keyDetail
		break;
	
	case WM_KEYUP:
	case WM_SYSKEYUP:
		//returnValue = ctrl->keyUp((int)wParam, lParam);                         // keyCode, keyDetail
		break;

	case WM_LBUTTONDOWN:
		SetFocus(ctrl->GetHandle());
		Input_Win::MouseFunction(1,1,LOWORD(lParam), HIWORD(lParam));
		// returnValue = ctrl->lButtonDown(wParam, LOWORD(lParam), HIWORD(lParam)); // state, x, y
		//returnValue = ctrl->lButtonDown(wParam, GET_X_LPARAM(lParam), GET_X_LPARAM(lParam)); // state, x, y
		break;

	case WM_LBUTTONUP:
		Input_Win::MouseFunction(1,0,LOWORD(lParam), HIWORD(lParam));
		// returnValue = ctrl->lButtonUp(wParam, LOWORD(lParam), HIWORD(lParam));   // state, x, y
		break;

	case WM_RBUTTONDOWN:
		Input_Win::MouseFunction(3,1,LOWORD(lParam), HIWORD(lParam));
		//  returnValue = ctrl->rButtonDown(wParam, LOWORD(lParam), HIWORD(lParam)); // state, x, y
		break;

	case WM_RBUTTONUP:
		Input_Win::MouseFunction(3,0,LOWORD(lParam), HIWORD(lParam));
		//returnValue = ctrl->rButtonUp(wParam, LOWORD(lParam), HIWORD(lParam));   // state, x, y
		break;

	case WM_MBUTTONDOWN:
		Input_Win::MouseFunction(2,1,LOWORD(lParam), HIWORD(lParam));
		//returnValue = ctrl->mButtonDown(wParam, LOWORD(lParam), HIWORD(lParam)); // state, x, y
		break;

	case WM_MBUTTONUP:
		Input_Win::MouseFunction(2,0,LOWORD(lParam), HIWORD(lParam));
		//  returnValue = ctrl->mButtonUp(wParam, LOWORD(lParam), HIWORD(lParam));   // state, x, y
		break;

	case WM_MOUSEHOVER:
		returnValue = ctrl->mouseHover((int)wParam, (short)LOWORD(lParam), (short)HIWORD(lParam));   // state, x, y
		break;

	case WM_MOUSELEAVE:
		returnValue = ctrl->mouseLeave();
		break;

	case WM_MOUSEMOVE:

		cout<<"mouse"<<endl;
		Input_Win::PassiveMotionFunction(LOWORD(lParam), HIWORD(lParam));

		//returnValue = ctrl->mouseMove(wParam, LOWORD(lParam), HIWORD(lParam));  // state, x, y
		break;

	case WM_MOUSEWHEEL:
		returnValue = ctrl->mouseWheel((short)LOWORD(wParam), (short)HIWORD(wParam)/WHEEL_DELTA, (short)LOWORD(lParam), (short)HIWORD(lParam));   // state, delta, x, y
		break;

	case WM_HSCROLL:
		returnValue = ctrl->hScroll(wParam, lParam);
		break;

	case WM_VSCROLL:
		returnValue = ctrl->vScroll(wParam, lParam);
		break;

	case WM_TIMER:
		returnValue = ctrl->timer(LOWORD(wParam), HIWORD(wParam));
		break;

	case WM_NOTIFY:
		returnValue = ctrl->notify((int)wParam, lParam);                        // controllerID, lParam
		break;

	case WM_CONTEXTMENU:
		returnValue = ctrl->contextMenu((HWND)wParam, LOWORD(lParam), HIWORD(lParam));    // handle, x, y (from screen coords)
		/*	case WM_CTLCOLORDLG:
		return (LONG)CreateSolidBrush(RGB(0, 0, 0));
		case WM_CTLCOLORSTATIC:
		{
		HDC hdcStatic = (HDC)wParam;
		SetTextColor(hdcStatic, RGB(255, 255, 255));
		SetBkMode(hdcStatic, TRANSPARENT);
		return (LONG)CreateSolidBrush(RGB(0, 0, 0));
		}
		break;*/
		//case WM_ERASEBKGND:
		//    returnValue = ctrl->eraseBkgnd((HDC)wParam);                            // handle to device context
		//    break;

	default:
		returnValue = ::DefWindowProc(hwnd, msg, wParam, lParam);
	}
	return returnValue;
}

INT_PTR CALLBACK Editor::Win::dialogProcedure(HWND hwnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
	// find controller associated with window handle
	static Controller *ctrl;
	ctrl = (Controller*)::GetWindowLongPtr(hwnd, GWLP_USERDATA);
	// WM_INITDIALOG message is called before displaying the dialog box.
	// lParam contains the value of dwInitParam of CreateDialogBoxParam(),
	// which is the pointer to the Controller.
	if(msg == WM_INITDIALOG){
		// First, retrieve the pointrer to the controller associated with
		// the current dialog box.
		ctrl = (Controller*)lParam;
		ctrl->SetHandle(hwnd);

		// Second, store the pointer to the Controller into GWL_USERDATA,
		// so, other messege can be routed to the associated Controller.
		::SetWindowLongPtr(hwnd, GWLP_USERDATA, (LONG_PTR)ctrl);

		// When WM_INITDIALOG is called, all controls in the dialog are created.
		// It is good time to initalize the appearance of controls here.
		// NOTE that we don't handle WM_CREATE message for dialogs because
		// the message is sent before controls have been Create, so you cannot
		// access them in WM_CREATE message handler.
		ctrl->Create();

		return true;
	}

	// check NULL pointer, because GWL_USERDATA is initially 0, and
	// we store a valid pointer value when WM_NCCREATE is called.
	if(!ctrl)
		return false;

	switch(msg)
	{
	case WM_COMMAND:
		cout<<"working dialog"<<endl;
		ctrl->Command(wParam, lParam, hwnd);   // id, code, msg
		return true;

	case WM_TIMER:
		ctrl->timer(LOWORD(wParam), HIWORD(wParam));
		return true;
	case NM_SETFOCUS:
		cout<<"focus set"<<endl;
		break;
	case WM_PAINT:
		ctrl->paint();
		::DefWindowProc(hwnd, msg, wParam, lParam);
		return true;

	case WM_DESTROY:
		ctrl->Destroy();
		return true;

	case WM_CLOSE:
		ctrl->Close();
		return true;

	case WM_HSCROLL:
		ctrl->hScroll(wParam, lParam);
		return true;

	case WM_VSCROLL:
		ctrl->vScroll(wParam, lParam);
		return true;

	case WM_NOTIFY:
		ctrl->notify((int)wParam, lParam);                      // controllerID, lParam
		return true;

	case WM_MOUSEMOVE:
		Input_Win::PassiveMotionFunction(LOWORD(lParam), HIWORD(lParam));
		//        ctrl->mouseMove(wParam, LOWORD(lParam), HIWORD(lParam));
		//ctrl->mouseMove(wParam, (int)GET_X_LPARAM(lParam), (int)GET_Y_LPARAM(lParam));  // state, x, y
		return true;

	case WM_LBUTTONUP:
		Input_Win::MouseFunction(wParam,1,LOWORD(lParam), HIWORD(lParam));
		// ctrl->lButtonUp(wParam, LOWORD(lParam), HIWORD(lParam));    // state, x, y
		return true;

	case WM_CONTEXTMENU:
		ctrl->contextMenu((HWND)wParam, LOWORD(lParam), HIWORD(lParam));    // handle, x, y (from screen coords)
		return true;

	case WM_SIZE:
		ctrl->Resize(LOWORD(lParam), HIWORD(lParam), (int)wParam);    // width, height, type
		return true;
	case WM_CTLCOLORDLG:
	case WM_CTLCOLORSTATIC:
		{
			HDC hdcStatic = (HDC)wParam;
			SetTextColor(hdcStatic, RGB(255, 255, 255));
			SetBkMode(hdcStatic, TRANSPARENT);
			HBRUSH temp = (HBRUSH)GetStockObject(LTGRAY_BRUSH);
			LOGBRUSH lb;
			int r,g,b;
			if(GetObject(temp,sizeof(LOGBRUSH),(LPSTR)&lb))
			{
				r = GetRValue(lb.lbColor);
				g = GetGValue(lb.lbColor);
				b = GetBValue(lb.lbColor);
			}else{
				r=b=g=0;
			}
			return (LONG)CreateSolidBrush(RGB(r,g,b));
		}
		break;
	default:
		return false;
	}
}
