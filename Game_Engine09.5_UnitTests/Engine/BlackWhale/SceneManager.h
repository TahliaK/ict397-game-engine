#ifndef SCENEMANAGER_H
#define SCENEMANAGER_H
#include <string>
#include "Scene.h"

namespace SceneManagement {

	/*
	* @brief Loads, deletes, save and activates scenes.
	* @author Olesia Kochergina
	* @date 01/04/2017
	*/
	class SceneManager {
	public:

		/*
		* @brief Creates a new scene and returns its index.
		* @param sceneName - name of the new scene.
		* @return The index of the scene.
		*/
		static int CreateScene(const std::string sceneName);

		/*
		* @brief Returns the currently active scene.
		* @return The active scene or NULL if there is no scenes loaded.
		*/
		static Scene* GetActive(void);

		/*
		* @brief Returns the currently active scene.
		* @return The active scene's ID or -1 if the current scene is invalid.
		*/
		static int GetActiveID(void);

		/**
		* @param Index of the required scene
		* @return The appropriate scene or NULL if the index is invalid.
		*/
		static Scene* GetAt(const int index);

		/*
		* @brief Loads a scene by its name.
		* @param sceneName - just the name of the scene which will be used as "Scenes/sceneName.scene".
		* @return index of the scene.
		*/
		static int LoadScene(const std::string sceneName);

		/*
		* @brief Returns a pointer to a scene by using its name.
		* @param sceneName - name of the scene
		* @return pointer to the scene
		*/
		static Scene* GetByFilename(const std::string sceneName);

		/*
		* @brief Activates a scene by its index.
		* @param index - index of the scene
		*/
		static void SetActive(const int index);

		/*
		* @brief Saves the active scene and its components.
		*/
		static void SaveActive(void);

		/*
		* @brief Returns the total amount of scenes.
		*/
		static int GetTotalAmount(void);

		/*
		* @brief Deletes all scenes in the project.
		*/
		static void Clear();

		//static void UnloadScene(const int index);
	private:

		///index of the currently active scene
		static int m_activeScene;

		///pointers to all scenes within the "Scenes" folder.
		static std::vector<Scene*> m_scenes;
	};
};
#endif SCENEMANAGER_H