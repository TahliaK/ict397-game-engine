#include "MeshRenderer.h"

#include  "Master.h"
#include "SceneManager.h"
#include "ShaderManager.h"
#include "ModelManager.h"
 
BOOST_CLASS_EXPORT_IMPLEMENT(MeshRenderer)

MeshRenderer::MeshRenderer() : Component() {
	Reflection();
	//Renderer::Renderer();
	Initialize();
	AddToList();
}

MeshRenderer::MeshRenderer(unsigned int p_shaderProgram, Mesh* p_mesh) {
	Reflection();
	Initialize();
	m_shaderProgram = p_shaderProgram;
	SetMesh(p_mesh);
	AddToList();
}


void MeshRenderer::AddToList() {
	static bool l_inList = false;
	if (!l_inList) {
		l_inList = true;
		MeshRenderer* l_pointer = new MeshRenderer;
		m_derivedClasses.push_back(l_pointer);
	}
}

Component* MeshRenderer::NewComponent() const{
	Component* l_pointer = new MeshRenderer;
	return l_pointer;
}

void MeshRenderer::Initialize() {
	m_multiple = false;
	m_mesh = NULL;
	m_fileName = "Resources/Rendering/";
	m_name = "MeshRenderer";
	m_vS = "Resources/Shaders/geom3D.vert";
	m_fS = "Resources/Shaders/geom3D.frag";
	m_shaderProgram = 0;
}
void MeshRenderer::Start() {
	m_shaderProgram =  ShaderManager::CreateShaderProgram(m_vS, m_fS);
	SetMeshFile(m_fileName);
}

MeshRenderer::MeshRenderer(unsigned int p_shaderProgram, std::string p_meshFile) {
	Initialize();
	m_shaderProgram = p_shaderProgram;
	SetMeshFile(p_meshFile);
}

MeshRenderer::~MeshRenderer() {
}

const bool MeshRenderer::SetVertexShader(boost::any var) {
	try {
		std::string l_temp = boost::any_cast<string> (var);
		if (ShaderManager::Get(m_shaderProgram)->GetVertexShader().compare(l_temp) != 0) {
			m_vS = l_temp;
			m_shaderProgram = ShaderManager::CreateShaderProgram(l_temp, ShaderManager::Get(m_shaderProgram)->GetFragmentShader());
			return SetMesh(m_fileName);
		}
		else
			return false;
	}
	catch (boost::bad_any_cast& /*bac*/) {
		return false;
	}
	return false;
}
const bool MeshRenderer::SetFragmentShader(boost::any var) {
	try {
		std::string l_temp = boost::any_cast<string> (var);
		if (ShaderManager::Get(m_shaderProgram)->GetVertexShader().compare(l_temp) != 0) {
			m_fS = l_temp;
			m_shaderProgram = ShaderManager::CreateShaderProgram(ShaderManager::Get(m_shaderProgram)->GetVertexShader(), l_temp);
			return SetMesh(m_fileName);
		}
		else
			return false;
	}
	catch (boost::bad_any_cast& /*bac*/) {
		return false;
	}
	return false;
}

const std::string MeshRenderer::GetVertexShader(void) const {
	return m_vS;
}

const std::string MeshRenderer::GetFragmentShader(void) const{
	return m_fS;
}

const std::string MeshRenderer::GetMeshFile() const {
	return m_fileName;
}

const Mesh* MeshRenderer::GetMesh(void) const {
	return m_mesh;
}

const bool MeshRenderer::SetMesh(boost::any p_mesh) {
	try {
		m_mesh = boost::any_cast<Mesh*> (p_mesh);
	}
	catch (boost::bad_any_cast& /*bac*/) {
		return false;
	}
	return true;
}
void MeshRenderer::Update() {
	if (m_shaderProgram == 0 || !ShaderManager::ActivateShaderProgram(m_shaderProgram))
		return;
	//else if (m_shaderProgram == ShaderManager::GetActive()->GetID()) {//should change to != later on when add other shaders
	//	
	//	Master::gl.UniformMatrix4fv(ShaderManager::GetActive()->GetLocation("model"), 1, GRAPHICS_FALSE, &SceneManagement::SceneManager::GetActive()->GetMainCamera()->GetModelMatrix().Get()[0]);
	//	Master::gl.UniformMatrix4fv(ShaderManager::GetActive()->GetLocation("proj"), 1, GRAPHICS_FALSE, &SceneManagement::SceneManager::GetActive()->GetMainCamera()->GetProjectionMatrix().Get()[0]);
	//	Master::gl.UniformMatrix4fv(ShaderManager::GetActive()->GetLocation("view"), 1, GRAPHICS_FALSE, &SceneManagement::SceneManager::GetActive()->GetMainCamera()->GetViewMatrix().Get()[0]);
	//}
	Master::gl.UniformMatrix4fv(ShaderManager::GetActive()->GetLocation("model"), 1, GRAPHICS_FALSE, &SceneManagement::SceneManager::GetActive()->GetMainCamera()->GetModelMatrix().Get()[0]);
	Master::gl.UniformMatrix4fv(ShaderManager::GetActive()->GetLocation("proj"), 1, GRAPHICS_FALSE, &SceneManagement::SceneManager::GetActive()->GetMainCamera()->GetProjectionMatrix().Get()[0]);
	Master::gl.UniformMatrix4fv(ShaderManager::GetActive()->GetLocation("view"), 1, GRAPHICS_FALSE, &SceneManagement::SceneManager::GetActive()->GetMainCamera()->GetViewMatrix().Get()[0]);

	if (m_mesh && m_gameObject->GetVisible()) {
		Matrix4 l_modelMatrix = GraphicsEngine::BuildModelMatrix(SceneManagement::SceneManager::GetActive()->GetMainCamera()->GetModelMatrix(), m_gameObject->GetTransform()->position, m_gameObject->GetTransform()->rotation, m_gameObject->GetTransform()->scale);
		Master::gl.UniformMatrix4fv(ShaderManager::GetActive()->GetLocation("model"), 1, GRAPHICS_FALSE, l_modelMatrix);
	}
	if (m_mesh && m_gameObject->GetVisible()) {
		m_mesh->Draw();
	}
}



const bool MeshRenderer::SetMeshFile(boost::any p_meshFile) {
	try {
		std::string l_temp = boost::any_cast<string> (p_meshFile);
		if (ModelManager::AddMesh(l_temp, m_shaderProgram) != NULL) {
			m_mesh = ModelManager::GetMesh(l_temp);
			m_fileName = l_temp;
		}
		else {
			return false;
		}
	}
	catch (boost::bad_any_cast& /*bac*/) {
		return false;
	}
	return true;
}


void MeshRenderer::Reflection() {
	m_Variables.push_back("Mesh file");
	m_Setters.push_back(boost::bind(&MeshRenderer::SetMeshFile, this, _1));
	m_Getters.push_back(boost::bind(&MeshRenderer::GetMeshFile, this));

	m_Variables.push_back("Vertex Shader");
	m_Setters.push_back(boost::bind(&MeshRenderer::SetVertexShader, this, _1));
	m_Getters.push_back(boost::bind(&MeshRenderer::GetVertexShader, this));

	m_Variables.push_back("Fragment Shader");
	m_Setters.push_back(boost::bind(&MeshRenderer::SetFragmentShader, this, _1));
	m_Getters.push_back(boost::bind(&MeshRenderer::GetFragmentShader, this));
}