#include "Component.h"
#include "Transform.h"
BOOST_CLASS_EXPORT_IMPLEMENT(Component)
std::vector<Component*> Component::m_derivedClasses;

Component::Component() {
	m_gameObject = NULL;
	m_multiple = false;
	m_name = "Component";
}

Component::~Component() {
	m_Setters.clear();
	m_Getters.clear();
	m_Variables.clear();
}

Component::Component(const Component& component) {
	m_multiple = false;
	m_gameObject = component.m_gameObject;
}

void Component::SetGameObject(GameObject* p_gameObject) {
	m_gameObject = p_gameObject;
}

GameObject* Component::GetGameObject() const {
	return m_gameObject;
}

Component* Component::NewComponent() const {
	return NULL;
}

bool Component::IsMultipleOccurrence(void) const {
	return m_multiple;
}

const std::vector<boost::function<bool(boost::any)>> Component::GetSetters() const {
	return m_Setters;
}

const std::vector<boost::function<boost::any(void)>> Component::GetGetters() const {
	return m_Getters;
}
const std::vector<std::string> Component::GetVariables(void) const {
	return m_Variables;
}

std::vector<Component*> Component::GetDerivedClasses() {
	return m_derivedClasses;
}

void Component::ClearDerivedClasses() {
	IFORSI(0, m_derivedClasses.size()) {
		delete m_derivedClasses.at(i);
		m_derivedClasses.at(i) = NULL;
	}
	m_derivedClasses.clear();
}