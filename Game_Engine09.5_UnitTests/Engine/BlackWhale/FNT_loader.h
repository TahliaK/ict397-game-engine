#ifndef FNT_LOADER_H
#define FNT_LOADER_H

#include <map>
#include <vector>
#include <iostream>

#include "Character.h"
namespace Text {
	/*
	* @class FNT_LOADER
	* @brief Reads in character definitions from .fnt files.
	* @author Olesia Kochergina
	* @date 02/03/2017
	*/
	class FNT_loader {
	public:

		/*
		* @brief Default c-tor.
		*/
		FNT_loader();

		/*
		* @brief c-tor.
		* @param p_fileName - description of a font
		*/
		FNT_loader(std::string p_fileName);

		/*
		* @brief Destructor.
		*/
		virtual ~FNT_loader();

		/*
		* @brief Clears internal data, should be called when
		* the object needs to be either reused or deleted.
		*/
		void Clear();

		/*
		* @brief Returns a character based on its ascii value.
		* @param ASCII - index
		* @return character at the given position.
		*/
		Character GetCharacter(int ASCII);

		/*
		* @brief Returns the space width.
		* @return space width
		*/
		float GetSpaceWidth(void) const;

	private:

		/*
		* @brief Loads padding into an array.
		*/
		void LoadPadddingData(void);

		/*
		* @brief Loads line sizes.
		*/
		void LoadLineSizes(void);

		/*
		* @brief Loads a character into the second parameter.
		* @param imageSize - size of the whole texture
		* @param character - object to store the new character
		*/
		void LoadCharacter(int imageSize, Character& character);

		/*
		* @brief Loads all characters.
		* @param imageSize - size of the whole texture.
		*/
		void LoadCharacterData(int imageSize);

		/*
		* @brief Reads a map with values of specific measurements for a single current character,
		* converts the values to integers and returns them.
		* @param var - measurement type such as scaleW, count and id.
		* @return the value under specifc variable.
		*/
		int GetValue(std::string var) const;

		/*
		* @brief Returns a vector of values for array measurements.
		* @param var - name of a particular array such as padding
		* @return vector of values primarily based on the parameter.
		*/
		std::vector<int> GetValues(std::string var) const;

		///padding width
		int m_paddingWidth;

		///padding height
		int m_paddingHeight;

		float m_spaceWidth;

		///vertical per pixel Resize
		float m_vSize;

		///horizontal per pixel Resize
		float m_hSize;

		///width/height
		float m_aspect;

		///padding at four sides
		int padding[4];

		///padding type
		enum PAD { TOP, LEFT, BOTTOM, RIGHT };

		///ASCII indices to characters
		std::map<int, Character> m_collection;

		///stores information about a single character, where variable names map to values.
		std::map<std::string, std::string> values;

		///the desired padding for all fonts
		static int DESIRED_PADDING;

		///the standard delimiter
		static char DELIM;

		///delimiter between numeric values
		static char NUMBER_SEPARATOR;
	};
}
#endif FNT_LOADER_H