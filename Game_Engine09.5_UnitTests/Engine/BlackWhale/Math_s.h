#ifndef MATH_S_H
#define MATH_S_H
#include <iostream>
#include <ostream>
#include "Interfaces\Serialization.h"
using namespace std;


#define MINIMUM_INIT_INT 1000000
#define MAXIMUM_INIT_INT -1000000
//short int from -32768 to 32767 
#define SI short int
#define UNINT unsigned int
//for loop starting from min to max using i where i is a short int
#define IFORSI(min,max) for(UNINT i = min; i < max; ++i)
//for loop starting from min to max using j where j is a short int
#define JFORSI(min,max) for(UNINT j = min; j < max; ++j)
#define PI 	3.1415926535897932384626433832795f
#define RADIANS(degrees) degrees *= ( PI / 180.0f)
#define DEGREES(radians) radians *= (180.0f / PI)


struct Vector2 {
	float x, y;

	Vector2() {
		x = y = 0;
	}

	Vector2(const Vector2& original) {
		this->x = original.x;
		this->y = original.y;
	}

	Vector2(float p_x, float p_y) {
		this->x = p_x;
		this->y = p_y;
	}

	Vector2(float p_value) {
		x = y = p_value;
	}

	float& operator [](SI p_index) {
		if (p_index == 0)
			return x;
		else
			return y;
	}

	Vector2 operator - (const Vector2& vector) {
		return Vector2(x - vector.x, y - vector.y);
	}

	Vector2 operator + (const Vector2& vector) {
		return Vector2(x + vector.x, y + vector.y);
	}

	Vector2 operator = (const Vector2& vector) {
		x = vector.x;
		y = vector.y;
		return Vector2(x, y);
	}

	friend ostream & operator <<(ostream & os, const Vector2& vector) {
		os << vector.x << "  " << vector.y;
		return os;
	}
private:
	///serialization
	friend class boost::serialization::access;
	template<class Archive>
	void save(Archive& ar, const unsigned int /*version*/) const {
		ar& SER_NVP(x);
		ar& SER_NVP(y);
	}
	template<class Archive>
	void load(Archive& ar, const unsigned int /*version*/) {
		ar& SER_NVP(x);
		ar& SER_NVP(y);
	}
	BOOST_SERIALIZATION_SPLIT_MEMBER()
};
BOOST_CLASS_VERSION(Vector2, 0)

/// Contains all the operations required by Vector3 variables
#ifndef VECTOR3_H
#define VECTOR3_H
struct Vector3
{
public:
	//x-axis value
	float x;
	//y-axis value
	float y;
	//z-axis value
	float z;

	/**
	* @brief A contructor to define a 3D vector.
	* @param x - x-axis value
	* @param y - y-axis value
	* @param z - z-axis value
	*/
	Vector3(float x, float y, float z) {
		Vector3::x = x;
		Vector3::y = y;
		Vector3::z = z;
	};

	/**
	* @brief A default constructor to set the
	* vector's variables to 0.
	*/
	Vector3() {
		x = y = z = 0;
	};

	/**
	* @brief A constructor to set all variables to the parameter's value.
	* @param num - the number to be set to.
	*/
	Vector3(float num) {
		x = y = z = num;
	}

	/**
	* @brief Overloads the equality operator to copy vectors.
	* @param A vector to be copied from.
	* @return a bew vector
	*/
	Vector3 operator = (const Vector3 original) {
		x = original.x;
		y = original.y;
		z = original.z;
		return Vector3(x, y, z);
	};

	bool operator == (const Vector3 original) const{
		return x == original.x && y == original.y && z == original.z;
	};

	/**
	* @brief Uses IO to display one vector.
	* @param os - reference to an output stream
	* @param vector - a vector to be displayed.
	*/
	friend ostream & operator <<(ostream & os, const Vector3 & vector) {
		os << vector.x << "  " << vector.y << "  " << vector.z;
		return os;
	}

	/*
	* @brief Multiplies this vector by the parameter.
	* @param the scalar
	* @return the result of multiplication.
	*/
	Vector3 operator * (const float scalar) {
		Vector3 temp(x, y, z);
		temp.x *= scalar;
		temp.y *= scalar;
		temp.z *= scalar;
		return temp;
	};

	/*
	* @brief Adds two vectors together.
	* @param second - a vector which has to be added
	* @return the result of addition.
	*/
	Vector3 operator + (const Vector3 second) {
		Vector3 temp(x, y, z);
		temp.x += second.x;
		temp.y += second.y;
		temp.z += second.z;
		return temp;
	}

	/*
	* @brief Subtracts the parameter from this vector.
	* @param second - a vector which has to be subtracted from this vector.
	* @return the result of subtraction.
	*/
	Vector3 operator - (const Vector3 second) {
		Vector3 temp(x, y, z);
		temp.x -= second.x;
		temp.y -= second.y;
		temp.z -= second.z;
		return temp;
	}

	/**
	* @brief An overlaoded array access operator to get one of the vector's values.
	* @param index - 0 - x value, 1 - y value, 2 - z value
	* @return the value of a particular variable or -1000000
	*/
	float operator[](const int index)  const {
		if (index == 0)
			return x;
		else if (index == 1)
			return y;
		else if (index == 2)
			return z;
		else
			return -10000000;
	}

	/**
	* @brief Changes one of the variables of this vector.
	* @param index - 0 - x value, 1 - y value, 2 - z value
	* @param value - a new value to be assigned
	*/
	void Set(int index, float value) {
		if (index == 0)
			x = value;
		else if (index == 1)
			y = value;
		else if (index == 2)
			z = value;
	}

	/**
	* @brief Divides this vector by a scalar
	* @param a float to be divided by
	* @return the result of division
	*
	*/
	Vector3 operator / (const float copy) {
		Vector3 temp(x, y, z);
		temp.x /= copy;
		temp.y /= copy;
		temp.z /= copy;
		return temp;
	}

	/**
	* @brief The cross product of two vectors.
	* @param v1 - the first vector
	* @param v2 - the second vector
	* @return the cross product
	*/
	static Vector3 Cross(const Vector3 p_v1, const Vector3 p_v2) {
		return Vector3(p_v1.y*p_v2.z - p_v1.z*p_v2.y, p_v1.z*p_v2.x - p_v1.x*p_v2.z, p_v1.x*p_v2.y - p_v1.y*p_v2.x);
	}
	/**
	* @brief The normal vector of a face.
	* @param v1 - the first point
	* @param v2 - the second point
	* @param v3 - the third point
	* @return SurfaceNormal vector
	*/
	static Vector3 SurfaceNormal(const Vector3 p_v1, const Vector3 p_v2, const Vector3 p_v3) {
		Vector3 l_t1 = p_v1;
		Vector3 l_t3 = p_v3;
		return Cross(l_t1 - p_v2, l_t3 - p_v2);
	}
	/**
	* @brief Calculates the distance between to 3D points.
	* @param v1 - the first point
	* @param v2 - the second point
	* @return the distance between the points.
	*/
	static float Distance(const Vector3 p_v1, const Vector3 p_v2) {
		return sqrt((p_v2.x - p_v1.x)*(p_v2.x - p_v1.x) +
			(p_v2.y - p_v1.y)*(p_v2.y - p_v1.y) +
			(p_v2.z - p_v1.z)*(p_v2.z - p_v1.z));
	}
	/**
	* @brief Returns the dot product of two vectors.
	* 1 - the vectors are pointing in the direction
	* 0 - perpendicular
	* @param v1 - the first vector
	* @param v2 - the second vector
	* @return a scalar value
	*/
	static float Dot(const Vector3 p_v1, const Vector3 p_v2) {
		return p_v1.x*p_v2.x + p_v1.y*p_v2.y + p_v1.z *p_v2.z;
	}
	
	/*
	* @brief Calculates the length of this vector.
	* @param v1 - the initial vector
	* @return the length of the vector.
	*/
	static float Length(const Vector3 p_v1) {
		return sqrt((p_v1.x*p_v1.x) + (p_v1.y*p_v1.y) + (p_v1.z*p_v1.z));
	}

	/**
	* @brief Uses barycentric equation to calculate the height of a point
	* within a triangle.
	* @param p1 - the first 3D vertex
	* @param p2 - the second 3D vertex
	* @param p3 - the third 3D vertex
	* @param pos - 2D position(x,z) within the triangle
	* @return the height of the position.
	*/
	static float BaryCentric(Vector3 p1, Vector3 p2, Vector3 p3, Vector2 pos) {
		float det = (p2.z - p3.z) * (p1.x - p3.x) + (p3.x - p2.x) * (p1.z - p3.z);
		float l1 = ((p2.z - p3.z) * (pos.x - p3.x) + (p3.x - p2.x) * (pos.y - p3.z)) / det;
		float l2 = ((p3.z - p1.z) * (pos.x - p3.x) + (p1.x - p3.x) * (pos.y - p3.z)) / det;
		float l3 = (1.0f - l1 - l2);
		return l1 * p1.y + l2 * p2.y + l3 * p3.y;
	}

	/**
	* @brief Calculates a normalized version of the vector.
	* @param v1 - the initial vectors
	* @return the normalized vector.
	*/
	static Vector3 Normalize(const Vector3 p_v1) {
		float l_length = Length(p_v1);
		if (l_length == 0)
			return Vector3(0);
		else {
			return Vector3(p_v1.x / l_length, p_v1.y / l_length, p_v1.z / l_length);
		}
	}

private:
	///serialization
	friend class boost::serialization::access;
	template<class Archive>
	void save(Archive& ar, const unsigned int /*version*/) const {
		ar& SER_NVP(x);
		ar& SER_NVP(y);
		ar& SER_NVP(z);
	}

	template<class Archive>
	void load(Archive& ar, const unsigned int /*version*/) {
		ar& SER_NVP(x);
		ar& SER_NVP(y);
		ar& SER_NVP(z);
	}
	BOOST_SERIALIZATION_SPLIT_MEMBER()
};
BOOST_CLASS_VERSION(Vector3, 0)
#endif VECTOR3_H

struct Vector4 {
	float x, y, z, w;

	Vector4() {
		x = y = z = w = 0;
	}

	Vector4(const Vector4& original) {
		x = original.x;
		y = original.y;
		z = original.z;
		w = original.w;
	}

	Vector4(float x, float y, float z, float w) {
		this->x = x;
		this->y = y;
		this->z = z;
		this->w = w;
	}
	Vector4(float value) {
		x = y = z = w = value;
	}

	float* GetAll() {
		v4[0] = this->x;
		v4[1] = this->y;
		v4[2] = this->z;
		v4[3] = this->w;
		return v4;
	}

	/**
	* @brief An overlaoded array access operator to get one of the vector's values.
	* @param index - 0 - x value, 1 - y value, 2 - z value, 3 - w value
	* @return the value of a particular variable or -1000000
	*/
	float& operator[](int index) {
		if (index == 0)
			return x;
		else if (index == 1)
			return y;
		else if (index == 2)
			return z;
		else if (index == 3)
			return w;
		else
			return invalid;
	}

	/**
	* @brief Overloads the equality operator to copy vectors.
	* @param A vector to be copied from.
	* @return a new vector
	*/
	Vector4& operator = (const Vector4& original) {
		x = original.x;
		y = original.y;
		z = original.z;
		w = original.w;
		return *this;
	};

	/**
	* @brief Uses IO to display one vector.
	* @param os - reference to an output stream
	* @param vector - a vector to be displayed.
	*/
	friend ostream & operator <<(ostream & os, const Vector4 & vector) {
		os << "[" << vector.x << ", " << vector.y << ",  " << vector.z << ", " << vector.w << "]";
		return os;
	}

	/*
	* @brief Multiplies this vector by the parameter.
	* @param the scalar
	* @return the result of multiplication.
	*/
	Vector4 operator * (const float scalar) {
		Vector4 temp(x, y, z, w);
		temp.x *= scalar;
		temp.y *= scalar;
		temp.z *= scalar;
		temp.w *= scalar;
		return temp;
	};

	/*
	* @brief Adds two vectors together.
	* @param second - a vector which has to be added
	* @return the result of addition.
	*/
	Vector4 operator + (const Vector4& second) {
		Vector4 temp(x, y, z, w);
		temp.x += second.x;
		temp.y += second.y;
		temp.z += second.z;
		temp.w += second.w;
		return temp;
	}

	/*
	* @brief Subtracts the parameter from this vector.
	* @param second - a vector which has to be subtracted from this vector.
	* @return the result of subtraction.
	*/
	Vector4 operator - (const Vector4& second) {
		Vector4 temp(x, y, z, w);
		temp.x -= second.x;
		temp.y -= second.y;
		temp.z -= second.z;
		temp.w -= second.w;
		return temp;
	}



	/**
	* @brief Changes one of the variables of this vector.
	* @param index - 0 - x value, 1 - y value, 2 - z value, 3 - w value
	* @param value - a new value to be assigned
	*/
	void Set(int index, float value) {
		switch (index) {
		case 0:
			x = value;
			break;
		case 1:
			y = value;
			break;
		case 2:
			z = value;
			break;
		case 3:
			w = value;
			break;
		default:
			break;
		}
	}

	/**
	* @brief Divides this vector by a scalar
	* @param a float to be divided by
	* @return the result of division
	*
	*/
	Vector4 operator / (const float copy) {
		Vector4 temp(x, y, z, w);
		temp.x /= copy;
		temp.y /= copy;
		temp.z /= copy;
		temp.w /= copy;
		return temp;
	}

private:
	float v4[4];
	float invalid;
	///serialization
	friend class boost::serialization::access;
	template<class Archive>
	void save(Archive& ar, const unsigned int /*version*/) const {
		ar& SER_NVP(x);
		ar& SER_NVP(y);
		ar& SER_NVP(z);
		ar& SER_NVP(w);
	}

	template<class Archive>
	void load(Archive& ar, const unsigned int /*version*/) {
		ar& SER_NVP(x);
		ar& SER_NVP(y);
		ar& SER_NVP(z);
		ar& SER_NVP(w);
	}
	BOOST_SERIALIZATION_SPLIT_MEMBER()
};
BOOST_CLASS_VERSION(Vector4, 0)

struct Matrix4 {
	Vector4 rows[4];
	Matrix4(float value) {
		IFORSI(0, 4) {
			JFORSI(0, 4)
				rows[i].Set(j, value);
		}
	}

	Matrix4(const Vector4& v1, const Vector4 v2, const Vector4& v3, const Vector4& v4) {
		rows[0] = v1;
		rows[1] = v2;
		rows[2] = v3;
		rows[3] = v4;
	}

	Matrix4(const Matrix4& original) {
		IFORSI(0, 4)
			rows[i] = original.rows[i];
	}

	Matrix4(Vector4 rows[4]) {
		IFORSI(0, 4)
			rows[i] = rows[i];
	}

	Matrix4() {
		Identity();
	}

	Matrix4 operator = (const Matrix4& original) {
		IFORSI(0, 4)
			rows[i] = original.rows[i];
		return Matrix4(rows);
	}


	Matrix4 operator + (const Matrix4& m) {
		Matrix4 temp;
		IFORSI(0, 4) {
			temp.rows[i] = rows[i] + m.rows[i];
		}
		return temp;
	}

	Matrix4 operator - (const Matrix4& m) {
		Matrix4 temp;
		IFORSI(0, 4) {
			temp.rows[i] = rows[i] - m.rows[i];
		}
		return temp;
	}

	Matrix4 operator * (const float scalar) {
		Matrix4 temp;
		IFORSI(0, 4) {
			temp.rows[i] = rows[i] * scalar;
		}
		return temp;
	}

	Matrix4 Transpose() {
		Matrix4 temp;
		IFORSI(0, 4) {
			JFORSI(0, 4)
				temp.rows[j].Set(i, rows[i][j]);
		}
		return temp;
	}
	Matrix4 operator * (const Matrix4& matrix) {
		Matrix4 m(matrix);
		Matrix4 temp(.0f);
		IFORSI(0, 4) {
			JFORSI(0, 4) {
				for (SI k = 0; k < 4; ++k) {
					float value = temp.rows[j][i] + (rows[j][k] * m.rows[k][i]);
					temp.rows[j].Set(i, value);
				}
			}
		}
		return temp;
	}

	Vector4 operator * (const Vector4& vector) {
		Vector4 v(vector);
		Vector4 temp(0);
		IFORSI(0, 4) {
			JFORSI(0, 4) {
				float value = temp[i] + (rows[i][j] * v[j]);
				temp.Set(i, value);
			}
		}
		return temp;
	}

	void Set(SI index, float value) {
		rows[index / 4].Set(index - index * 4, value);
	}

	float& operator [](SI index) {
		if (index < 16)
			return rows[index / 4][index - ((index / 4) * 4)];
		else
			return rows[0][0];
	}

	/**
	* @brief Sets the current matrix to Identity Matrix.
	*
	*/
	void Identity() {
		IFORSI(0, 4) {
			JFORSI(0, 4)
				rows[i].Set(j, 0);
		}
		IFORSI(0, 4)
			rows[i].Set(i, 1);
	}

	friend ostream & operator <<(ostream & os, const Matrix4 & matrix) {
		IFORSI(0, 4)
			os << matrix.rows[i] << endl;
		return os;
	}

	float* Get() {
		IFORSI(0, 16) {
			m_matrix[i] = this->operator[](i);
		}
		return m_matrix;
	}

	static Matrix4 Translate(const Matrix4& model, const Vector4& translation) {
		Matrix4 m(model);
		/*Matrix4 t(
			Vector4(1,0,0,translation.x),
			Vector4(0,1,0,translation.y),
			Vector4(0,0,1,translation.z),
			Vector4(0,0,0,1));*/
		Matrix4 t(
			Vector4(1, 0, 0, 0),
			Vector4(0, 1, 0, 0),
			Vector4(0, 0, 1, 0),
			Vector4(translation.x, translation.y, translation.z, 1));
		m = m * t;
		return m;
	}

	static Matrix4 Scale(const Matrix4& model, const Vector4& scale) {
		Matrix4 s(
			Vector4(scale.x, 0, 0, 0),
			Vector4(0, scale.y, 0, 0),
			Vector4(0, 0, scale.z, 0),
			Vector4(0, 0, 0, 1));
		Matrix4 m(model);
		m = m * s;
		return m;
	}

	static Matrix4 Rotate(const Matrix4& model, const Vector4& rotation) {
		Matrix4 m(model);
		Vector4 v(rotation);

		v.x = RADIANS(v.x);
		v.y = RADIANS(v.y);
		v.z = RADIANS(v.z);

		Matrix4 rots[3];//xyz
		rots[0] = Matrix4(
			Vector4(1, 0, 0, 0),
			Vector4(0, cos(v.x), -sin(v.x), 0),
			Vector4(0, sin(v.x), cos(v.x), 0),
			Vector4(0, 0, 0, 1));

		rots[1] = Matrix4(
			Vector4(cos(v.y), 0, sin(v.y), 0),
			Vector4(0, 1, 0, 0),
			Vector4(-sin(v.y), 0, cos(v.y), 0),
			Vector4(0, 0, 0, 1));

		rots[2] = Matrix4(
			Vector4(cos(v.z), -sin(v.z), 0, 0),
			Vector4(sin(v.z), cos(v.z), 0, 0),
			Vector4(0, 0, 1, 0),
			Vector4(0, 0, 0, 1));

		Vector4 temp(rotation);
		IFORSI(0, 3) {
			if (temp[i] != 0) {
				m = m * rots[i];
			}
		}
		return m;
	}

	static Vector4 Rotate(const Vector4& rot, const Vector4& vector) {
		Matrix4 m;
		m = Scale(m, rot);
		return m * vector;
	}


	static Vector4 Translate(const Vector4& translation, const Vector4& vector) {
		Matrix4 m;
		m = Translate(m, translation);
		return m * vector;
	}

	static Vector4 Scale(const Vector4& scale, const Vector4&  vector) {
		Matrix4 m;
		m = Scale(m, scale);
		return m * vector;
	}

private:
	float m_matrix[16];

	///serialization
	friend class boost::serialization::access;
	template<class Archive>
	void save(Archive& ar, const unsigned int /*version*/) const {
		//IFORSI(0, 4)
			ar& SER_NVP(rows);
	}
	template<class Archive>
	void load(Archive& ar, const unsigned int /*version*/) {
		//IFORSI(0, 4)
		ar& SER_NVP(rows);
	}
	BOOST_SERIALIZATION_SPLIT_MEMBER()
};
BOOST_CLASS_VERSION(Matrix4, 0)




/* NOT IMPLEMENTED
struct Quaternion{
float x, y, z, w;
Vector3 RotationAxis;
float RotationAngle;

Quaternion(){
w=1;
y=0;
z=0;
x=0;
}

Quaternion(float x, float y, float z, float w){
this->x = x;
this->y = y;
this->z = z;
this->w = w;
//Identity = Quaternion(0,0,0,1);
}
Vector3 EulerAngles(Vector3 angle){
}

*
*@param RotationAxis - unit vector
*

Quaternion Rotate(Vector3 RotationAxis, float angle){
x = RotationAxis.x * sin(angle/2);
y = RotationAxis.y * sin(angle/2);
z = RotationAxis.z * sin(angle/2);
w = cos(angle/2);
RotationAngle = angle;
return *this;
}
static float Dot(Quaternion one, Quaternion two){
matching
if(abs(matching-1.0) <0.001)

}
};*/

#endif MATH_S_H