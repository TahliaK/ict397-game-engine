#ifndef MASTER_H
#define MASTER_H
#include "Camera.h"
#include "OpenGL.h"
#include "ShaderProgram.h"
#include "FrameBuffer.h"

//not used but can be for compatibility
#define IS_FIXED_PIPELINE false
//
#define IS_OPENGL_INPUT false
#define PRINTNAME(x) #x
//#define PRINTFUNCTIONNAME() __PRETTY_FUNCTION__
#define PRINTFUNCTIONNAME() __FUNCTION__
#define PRINTFILE() __FILE__
#define DEBUG_MODE true

//works faster than std::endl
#define ENDL '\n'
/*
* @brief General class with data that needs to be accessed everywhere.
* SHADERS SHOULD BE REMOVED FROM THIS PLACE LATER ON
* @author Olesia Kochergina
* @date 10/03/2017
*/
class Master{
public:

	///rendering mode: wireframe or normal
	static bool IS_FRAME_RENDER_MODE;

	///true if the engine has had the first update
	static bool STARTED;

	///editor mode or game mode
	static bool EDITOR_MODE;

	///instance of the gl class.
	static OpenGL gl;
	static GameObject *s_guiCamera;
	static RGBA  s_worldColor;
	static ShaderProgram s_TextShader;
	static ShaderProgram s_WaterShader;
	static ShaderProgram s_TerrainShader;
	static ShaderProgram s_SkyboxShader;

	static ShaderProgram s_GeomPassShader;
	static ShaderProgram s_LightPassShader;

	static ShaderProgram s_WaterDRShader;

	static std::string s_fontFolder;
	static std::string s_modelFolder;
	static std::string s_guiFolder;
	static std::string s_soundFolder;

	static FrameBuffer s_reflectionFBO;
	static FrameBuffer s_refractionFBO;
	static FrameBuffer s_shadowFBO;

	static unsigned int NULL_TEXTURE;
	
	///FORMULA to calculate vertices - v` = P * V * M * v

	///Contains all translations, rotations or scaling applied to vertices
	//static Matrix4 m_model;
	///Controls the way a scene is viewed similar to glLookAt(position,view,up)
	//static Matrix4 m_view;

	///Orthographic - wont modify the Resize of objects - useful for CAD and 2D games
	/// right,left,top,bottom represent positions of clipping planes
	///|	2/(right-left)			0				0			-(right+left)/(right-left)	|
	///|			0		2/(top-bottom)			0			-(top+bottom)/(top-bottom)	|
	///|			0				0			-2/(far-near)	-(far+near)/(far-near)		|
	///|			0				0				0						1				|
	///Perspective - projecs the world coordinates to the unit cube
	///specified by viewing angle(FOV), aspect, near and far
	///near and far respesent positions of near and far clipping planes
	/// top = near * tan(PI/180 * FOV/2); bottom = - top; right = top * aspect; left = -right
	///|	2*near/(right-left)				0				right+left/right-left				0			|
	///|			0				2*near/(top-bottom)		(top+bottom)/(top-bottom)			0			|
	///|			0						0				-(far+near)/(far-near)		-2*far*near/far-near|
	///|			0						0							-1						0			|
	//static Matrix4 m_projection;

	///[0] - xMin; [1] - yMin; [2] - width; [3] - height
	static Vector4 s_screen;

	/*
	* @brief Called just before the first update.
	*/
	static void Start();

	/*
	* @brief Called when the user changes the app's width/height
	*/
	static void Reshape();

	/*
	* @brief Clears internal data, should be called when
	* the object needs to be either reused or deleted.
	*/
	static void Clear();
	
private:
	
	/*
	* @brief Initializes a single shader
	* @param shader - reference to a shader program container
	* @param vertexShader - file name of the vertex shader
	* @param fragmentShader - file name of the fragment shader
	*/
	static void InitializeShader(ShaderProgram& shader, const std::string vertexShader, const std::string fragmentShader);

	static void ModifyShaders();
};
#endif
