/**
* @brief Identifies a single framebuffer with one color texture and one depth texture/buffer.
* @author Olesia Kochergina
* @version 02
* @date 11/02/2017
*/

#ifndef FRAMEBUFFER_H
#define FRAMEBUFFER_H

enum DEPTH_TYPE { DEPTH_BUFFER, DEPTH_TEXTURE };

class FrameBuffer {
public:

	/*
	* @brief Default c-tor.
	*/
	FrameBuffer();

	/*
	* @brief Destructor.
	*/
	~FrameBuffer();

	/**
	* @brief Creates a new framebuffer.
	* @param type - DEPTH_BUFFER or DEPTH_TEXTURE
	* @param screen - dimensions of the buffer
	*/
	const unsigned int CreateBuffer(DEPTH_TYPE type, const int screen[4]);

	/**
	* @brief Binds this framebuffer.
	* @param targetBuffer - GRAPHICS_FRAMEBUFFER, GRAPHICS_DRAW_FRAMEBUFFER, GRAPHICS_READ_FRAMEBUFFER
	*/
	void BindBuffer(unsigned int targetBuffer) const;

	/**
	* @brief Unbinds this framebuffer.
	* @param targetBuffer - GRAPHICS_FRAMEBUFFER, GRAPHICS_DRAW_FRAMEBUFFER, GRAPHICS_READ_FRAMEBUFFER
	*/
	void UnbindBuffer(unsigned int targetBuffer) const;

	/*
	* @brief Deletes the framebuffer and its content.
	*/
	void Clear(void);

	/*
	* @brief Returns the color texture.
	* @return ID of the current color texture.
	*/
	const unsigned int GetColor(void) const;

	/*
	* @brief Returns the depth texture/buffer.
	* @return ID of the current depth texture/buffer.
	*/
	const unsigned int GetDepth(void) const;

	/*
	* @brief Creates a new depth texture.
	* @return ID of the texture.
	*/
	unsigned int CreateDepthTexture(void);

	/*
	* @brief Creates a new depth buffer.
	* @return ID of the buffer.
	*/
	unsigned int CreateDepthBuffer(void);

	/*
	* @brief Creates a new color texture.
	* @param attachmentIndex - index of the color attachment
	* @param internalFormat - internal format of the texture:  GRAPHICS_ALPHA, GRAPHICS_LUMINANCE, GRAPHICS_LUMINANCE_ALPHA, GRAPHICS_RGB, GRAPHICS_RGBA
	* @param format - format of texel data:   GRAPHICS_ALPHA, GRAPHICS_LUMINANCE, GRAPHICS_LUMINANCE_ALPHA, GRAPHICS_RGB, GRAPHICS_RGBA
	* @param type - the data type of the texel data: GRAPHICS_UNSIGNED_BYTE, GRAPHICS_UNSIGNED_SHORT_5_6_5, GRAPHICS_UNSIGNED_SHORT_4_4_4_4, and GRAPHICS_UNSIGNED_SHORT_5_5_5_1.
	* @return ID of the texture.
	*/
	unsigned int CreateColorTexture(int attachmentIndex, int internalFormat, int format, int type);
private:

	/*
	* @brief Generates a new buffer.
	* @return index of the buffer.
	*/
	unsigned int CreateFrameBuffer();

	///index of the framebuffer
	unsigned int m_fbo;

	///index of the depth texture/buffer
	unsigned int m_depth;

	///index of the color texture
	unsigned int m_color;

	///dimensions of the depth texture and color texture
	int m_screen[4];

	///Depth type
	DEPTH_TYPE m_depthType;
};
#endif FRAMEBUFFER_H
