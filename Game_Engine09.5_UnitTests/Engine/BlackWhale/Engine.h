#ifndef ENGINE_H
#define ENGINE_H
#include "Camera.h"
#include "Light.h"

/*
* @brief The heart of the game engine which combines all components together
* and manipulates them
* @author Olesia Kochergina
* @date 19/04/2017
* @version 03
*/
class Engine {
public:

	/*
	* @brief Initializes internal data and should be
	* called before starting anything else.
	*/
	static void Initialize();

	/*
	* @brief Starts the engine and should be called before
	* the first update.
	*/
	static void Start();

	/*
	* @brief Updates the engine and should be called every frame.
	*/
	static void Update();

	/*
	* @brief Updates the virtual screen dimensions and should
	* be called when the user the size of the application window.
	*/
	static void ReshapeFunction(int width, int height);

	/*
	* @brief Deletes all dynamic data and should be called before
	* quitting the application.
	*/
	static void Clear();

	/*
	* @brief Returns a default 3D camera if a particular scene
	* does not have its own camera.
	*/
	static Camera* GetMainCamera(void);

	/*
	* @brief Returns a default light if a particular scene
	* does not have its own light.
	*/
	static Light* GetMainLight(void);
private:

	/*
	* @brief NOT USED.
	* Used to update water refraction when forward rendering was used.
	*/
	static void PrepareWaterRefraction();

	/*
	* @brief NOT USED.
	* Used to update water reflection when forward rendering was used.
	*/
	static void PrepareWaterReflection();

	///generic game object that represents a default light
	static GameObject s_light;

	///generic game object that represents a default camera
	static GameObject s_camera;
};

#endif