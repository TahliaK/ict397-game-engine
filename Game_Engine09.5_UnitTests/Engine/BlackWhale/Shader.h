#ifndef SHADER_H
#define SHADER_H
#define ERROR_STATE -1
class Shader{
public:
	Shader();
	~Shader();
	/**
	* @param shaderType - GL_FRAGMENT_SHADER or GL_VERTEX_SHADER or GL_GEOMETRY_SHADER
	*/
	bool CreateShader(unsigned int shaderType);
	void AttachSource(int numOfStrings, const char **strings, int *lenOfStrings);
	void CompileShader();
	unsigned int GetShader();
	bool IsCompiled();
	void PrintInfoLog();
private:
	unsigned int m_shader;
	int m_compiled;
};
#endif