#include "FrameBuffer.h"
#include "Master.h"
#include "Debug.h"

FrameBuffer::FrameBuffer() {
	m_fbo = 0;
	m_depth = 0;
	m_color = 0;
}

const unsigned int FrameBuffer::CreateBuffer(DEPTH_TYPE type, const int screen[4]) {
	Clear();
	for (unsigned i = 0; i < 4; i++)
		this->m_screen[i] = screen[i];
	CreateFrameBuffer();
	CreateColorTexture(0, GRAPHICS_RGB, GRAPHICS_RGB, GRAPHICS_UNSIGNED_BYTE);
	if (type == DEPTH_BUFFER)
		CreateDepthBuffer();
	else
		CreateDepthTexture();
	Debug::RunFramebuffer();
	UnbindBuffer(GRAPHICS_FRAMEBUFFER);
	return m_fbo;
}

unsigned int FrameBuffer::CreateFrameBuffer() {
	Master::gl.GenFB(1, &m_fbo);
	BindBuffer(GRAPHICS_FRAMEBUFFER);
	//glDrawBufferss(GL_COLOR_ATTACHMENT0);
	return m_fbo;
}

const unsigned int FrameBuffer::GetColor() const {
	return m_color;
}

const unsigned int FrameBuffer::GetDepth() const {
	return m_depth;
}

FrameBuffer::~FrameBuffer() {
	Clear();
}

void FrameBuffer::Clear() {
	if (m_fbo != 0) {
		Master::gl.DeleteFB(1, &m_fbo);
		m_fbo = 0;
	}
	if (m_color != 0) {
		Master::gl.DeleteTextures(1, &m_color);
		m_color = 0;
	}
	if (m_depth != 0) {
		if (m_depthType == DEPTH_BUFFER)
			Master::gl.DeleteRB(1, &m_depth);
		else
			Master::gl.DeleteTextures(1, &m_depth);
		m_depth = 0;
	}
}

void FrameBuffer::BindBuffer(unsigned int p_target) const {
	Master::gl.BindTexture(GRAPHICS_TEXTURE_2D, 0);
	Master::gl.BindFB(p_target, m_fbo);

	//glViewport(screen[0],screen[1],screen[2],screen[3]);
}

void FrameBuffer::UnbindBuffer(unsigned int p_target) const {
	Master::gl.BindFB(p_target, 0);
	//glViewport(Master::screen[0][0],Master::screen[0][1],Master::screen[1][0],Master::screen[1][1]);
}

unsigned int FrameBuffer::CreateDepthTexture() {
	Master::gl.GenTexture(1, &m_depth);
	Master::gl.BindTexture(GRAPHICS_TEXTURE_2D, m_depth);
	Master::gl.TexImage2D(GRAPHICS_TEXTURE_2D, 0, GRAPHICS_DEPTH_COMPONENT32, m_screen[2] - m_screen[0], m_screen[3] - m_screen[1], 0, GRAPHICS_DEPTH_COMPONENT, GRAPHICS_FLOAT, (void*)NULL);
	Master::gl.TexParami(GRAPHICS_TEXTURE_2D, GRAPHICS_TEXTURE_MAG_FILTER, GRAPHICS_LINEAR);
	Master::gl.TexParami(GRAPHICS_TEXTURE_2D, GRAPHICS_TEXTURE_MIN_FILTER, GRAPHICS_LINEAR);
	Master::gl.FBtexture(GRAPHICS_FRAMEBUFFER, GRAPHICS_DEPTH_ATTACHMENT, m_depth, 0);
	return m_depth;
}

unsigned int FrameBuffer::CreateColorTexture(int p_index, int p_intFormat, int p_format, int p_type) {
	Master::gl.GenTexture(1, &m_color);
	Master::gl.BindTexture(GRAPHICS_TEXTURE_2D, m_color);
	Master::gl.TexImage2D(GRAPHICS_TEXTURE_2D, 0, p_intFormat, m_screen[2] - m_screen[0], m_screen[3] - m_screen[1], 0, p_format, p_type, (void*)NULL);
	Master::gl.TexParami(GRAPHICS_TEXTURE_2D, GRAPHICS_TEXTURE_MAG_FILTER, GRAPHICS_LINEAR);
	Master::gl.TexParami(GRAPHICS_TEXTURE_2D, GRAPHICS_TEXTURE_MIN_FILTER, GRAPHICS_LINEAR);
	//Master::gl.FBtexture(GRAPHICS_FRAMEBUFFER, GRAPHICS_COLOR_ATTACHMENT0 + p_index, color, 0);
	Master::gl.FbTexture2D(GRAPHICS_FRAMEBUFFER, GRAPHICS_COLOR_ATTACHMENT0 + p_index, GRAPHICS_TEXTURE_2D, m_color, 0);
	return m_color;
}

unsigned int FrameBuffer::CreateDepthBuffer() {
	Master::gl.GenRB(1, &m_depth);
	Master::gl.BindRB(GRAPHICS_RENDERBUFFER, m_depth);
	Master::gl.RBstorage(GRAPHICS_RENDERBUFFER, GRAPHICS_DEPTH_COMPONENT, m_screen[2] - m_screen[0], m_screen[3] - m_screen[1]);
	Master::gl.FBRB(GRAPHICS_FRAMEBUFFER, GRAPHICS_DEPTH_ATTACHMENT, GRAPHICS_RENDERBUFFER, m_depth);
	return m_depth;
}
