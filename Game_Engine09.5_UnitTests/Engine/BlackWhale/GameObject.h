#ifndef GAMEOBJECT_H
#define GAMEOBJECT_H
#include "Struct.h"
#include "InObject.h"
#include "Math_s.h"

//forward declaration
class Transform;

//forward declaration
class Component;

/*
* @class GameObject
* @brief Represents a single game object with one to many components.
* @version 01
* @date 18/02/2017
* @author Olesia Kochergina
*/
class GameObject : public InObject {
public:

	/*
	* @brief Default c-tor.
	*/
	GameObject();

	/*
	* @brief Copy c-tor.
	*/
	GameObject(const GameObject& go);

	/*
	* @brief D-tor.
	*/
	virtual ~GameObject();

	/*
	* @brief Called just before the first update.
	*/
	virtual void Start();

	/*
	* @brief Updates the object and should be called every frame.
	*/
	virtual void Update();

	/*
	* @brief Creates a new instance of the class.
	* @return pointer to the new instance
	*/
	GameObject* NewObject(void) const;

	/*
	* @brief Adds a component the game object.
	* @param component - component to be added
	* @return true - the component is added.
	*/
	bool AddComponent(Component* component);

	/*
	* @brief Updates the visibility of the object.
	* @param visibility - if true then the object will be rendered.
	*/
	void SetVisible(bool visiblility);

	/*
	* @brief NOT IMPLEMENTED
	* @param isStatic - if true then the object's transform wont be updated while playing.
	*/
	void SetStatic(bool isStatic);

	/*
	* @brief Sets the object's layer
	* @param layer - layer of the object such as 0 - default, 1 - UI
	*/
	void SetLayer(int layer);

	/*
	* @brief Returns the object's visibility.
	* @return visibility
	*/
	const bool GetVisible(void) const;

	/*
	* @brief Returns the object's staticity.
	* @return staticity
	*/
	const bool GetStatic(void) const;

	/*
	* @brief Returns the object's layer.
	* @return layer
	*/
	const int GetLayer(void) const;

	/*
	* @brief Returns the object's transform.
	* @return the transform of the object.
	*/
	Transform* GetTransform(void) const;

	/*
	* @brief Returns the first occurrence of a particular component type or NULL.
	* @param type - component that identifies the type of component to look for.
	* @return the component or NULL.
	*/
	Component* GetComponent(Component* type)const;

	/*
	* @brief Returns all occurrences of a particular component type.
	* @param type - component that identifies the type of components to look for.
	* @return vector of components.
	*/
	std::vector<Component*> GetComponents(Component* type) const;

	/*
	* @brief Returns all components.
	* @return vector of components.
	*/
	std::vector<Component*> GetComponents() const;
	//static GameObject* Find(std::string name);
	//GameObject(std::string name);
protected:

	///transform of this object
	Transform* m_transform;

	///staticity of the object
	bool m_static;

	///visibility of the object
	bool m_visible;

	///0 - default; 1 - UI
	int m_layer;

	///the object's scene
	int m_scene;

	///array of all components including the transform.
	vector<Component*> m_components;
private:

	///serialization
	friend class boost::serialization::access;
	template<class Archive>
	void save(Archive& ar, const unsigned int /*version*/) const {
		ar& SER_BASE_OBJECT_NVP(InObject);
		ar& SER_NVP(m_static);
		ar& SER_NVP(m_visible);
		ar& SER_NVP(m_layer);
		ar& SER_NVP(m_scene);
		ar& SER_NVP(m_transform);
		ar& SER_NVP(m_components);

	}
	template<class Archive>
	void load(Archive& ar, const unsigned int /*version*/) {
		ar& SER_BASE_OBJECT_NVP(InObject);
		ar& SER_NVP(m_static);
		ar& SER_NVP(m_visible);
		ar& SER_NVP(m_layer);
		ar& SER_NVP(m_scene);
		ar& SER_NVP(m_transform);
		ar& SER_NVP(m_components);

	}
	SER_SPLIT_MEMBER()
};
SER_CLASS_VERSION(GameObject, 0);
BOOST_CLASS_EXPORT_KEY(GameObject)
#endif GAMEOBJECT_H
