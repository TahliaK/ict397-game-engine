/**
* @class Header file for material structs
* @brief
* @author Olesia Kochergina
* @version 01
* @date 15/02/2017
*/

#ifndef MATERIAL_S_H
#define MATERIAL_S_H
#include <string>
#include <iostream>

#include "InObject.h"
#include "ShaderProgram.h"

struct RGBA {
	float r;
	float g;
	float b;
	float a;

	RGBA() {
		r = 1;
		g = 1;
		b = 1;
		a = 1;
	};

	RGBA(const RGBA& original) {
		r = original.r;
		g = original.g;
		b = original.b;
		a = original.a;
	};

	RGBA(float value) {
		r = g = b = a = value;
	}

	RGBA(float r, float g, float b, float a) {
		this->r = r;
		this->g = g;
		this->b = b;
		this->a = a;
	}

	RGBA(float r, float g, float b) {
		this->r = r;
		this->g = g;
		this->b = b;
		this->a = 1;
	}

	float* GetColor() {
		rgba[0] = r;
		rgba[1] = g;
		rgba[2] = b;
		rgba[3] = a;
		return rgba;
	}

	void Set(int i, float value) {
		if (i == 0)
			r = value;
		else if (i == 1)
			g = value;
		else if (i == 2)
			b = value;
		else if (i == 3)
			a = value;
	}

	RGBA operator * (const RGBA color) {
		r *= color.r;
		g *= color.g;
		b *= color.b;
		a *= color.a;
		return RGBA(r, g, b, a);
	}

	float operator [](const int index)const {
		if (index == 0)
			return r;
		else if (index == 1)
			return g;
		else if (index == 2)
			return b;
		else if (index == 3)
			return a;
		else
			return -1000000;
	}
private:
	float rgba[4];

	///serialization
	friend class boost::serialization::access;
	template<class Archive>
	void save(Archive& ar, const unsigned int /*version*/) const {
		ar& SER_NVP(r);
		ar& SER_NVP(g);
		ar& SER_NVP(b);
		ar& SER_NVP(a);
	}
	template<class Archive>
	void load(Archive& ar, const unsigned int /*version*/) {
		ar& SER_NVP(r);
		ar& SER_NVP(g);
		ar& SER_NVP(b);
		ar& SER_NVP(a);
	}
	BOOST_SERIALIZATION_SPLIT_MEMBER()
};
BOOST_CLASS_VERSION(RGBA, 0)

///a structure which holds the first/last faces to be used when identifying texture/Vector3/normal vertices as well as a buffer ID
struct matF {
	std::string mtlname;
	int fFace, lFace;
	std::string name[3];
	matF() {
		name[0] = "position";
		name[1] = "normal";
		name[2] = "texCoord";
	}
	~matF() {
	}
private:
	///serialization
	friend class boost::serialization::access;
	template<class Archive>
	void save(Archive& ar, const unsigned int /*version*/) const {
		ar& SER_NVP(mtlname);
		ar& SER_NVP(fFace);
		ar& SER_NVP(lFace);
		ar& SER_NVP(name);
	}
	template<class Archive>
	void load(Archive& ar, const unsigned int /*version*/) {
		ar& SER_NVP(mtlname);
		ar& SER_NVP(fFace);
		ar& SER_NVP(lFace);
		ar& SER_NVP(name);
	}
	BOOST_SERIALIZATION_SPLIT_MEMBER()
};
BOOST_CLASS_VERSION(matF, 0)

///Defines a Material 
class Material{
public:

	///The shader used by the material
	ShaderProgram shader;
	RGBA ambient;
	RGBA diffuse;
	RGBA specular;
	RGBA emission;
	float shininess;

	///Allows the specific colors to pass through the object
	float transFilter;

	std::string mtlname;
	std::string path;
	///The material's texture
	unsigned int texture;

	/*
	Illumination    Properties that are turned on in the
	model           Property Editor

	0		Color on and Ambient off
	1		Color on and Ambient on
	2		Highlight on
	3		Reflection on and Ray trace on
	4		Transparency: Glass on
	Reflection: Ray trace on
	5		Reflection: Fresnel on and Ray trace on
	6		Transparency: Refraction on
	Reflection: Fresnel off and Ray trace on
	7		Transparency: Refraction on
	Reflection: Fresnel on and Ray trace on
	8		Reflection on and Ray trace off
	9		Transparency: Glass on
	Reflection: Ray trace off
	10		Casts shadows onto invisible surfaces*/
	float illum;

	/*
	* @brief  A default constructor to initialize arrays.
	*/
	Material() {
		transFilter = 1;
		shininess = 30;
		illum = 0;
		for (int i = 0; i < 3; i++) {
			diffuse.Set(i, 0.5);
			ambient.Set(i, 0.5);
			specular.Set(i, 1);
			emission.Set(i, 0);
		}
		diffuse.Set(3, 1);
		ambient.Set(3, 1);
		specular.Set(3, 1);
		emission.Set(3, 1);
		texture = 0;
	}

	Material(const Material& original)  {
		transFilter = original.transFilter;
		shininess = original.shininess;
		illum = original.illum;
		for (int i = 0; i < 3; i++) {
			diffuse.Set(i, 0.5);
			ambient.Set(i, 0.5);
			specular.Set(i, 1);
			emission.Set(i, 0);
		}
		diffuse = original.diffuse;
		ambient = original.ambient;
		specular = original.specular;
		emission = original.emission;
		texture = original.texture;
		path = original.path;
		mtlname = original.mtlname;
		shader = original.shader;
	}
	/*float*  GetColors(){
	IFORSI(0,4){
	mat[0+i] = ambient[i];
	mat[4+i] = diffuse[i];
	mat[8+i] = specular[i];
	mat[12+i] = emission[i];
	}
	return mat;
	}*/



	void Clear() {
		if (/*texture != Master::NULL_TEXTURE ||*/ texture != 0) {
			//Master::gl.DeleteTextures(1, &texture);
			texture = 0;
		}
	}
	~Material() {	}

	/*
	* @brief Copies the parameter's data to this instance.
	* @param original - a Material to be copied from
	*/
	void Copy(const Material& original) {
		path = original.path;
		texture = original.texture;
		mtlname = original.mtlname;
		illum = original.illum;
		shininess = original.shininess;
		transFilter = original.transFilter;
		for (int i = 0; i < 3; i++) {
			ambient.Set(i, original.ambient[i]);
			specular.Set(i, original.specular[i]);
			diffuse.Set(i, original.diffuse[i]);
			emission.Set(i, original.emission[i]);
		}
	}

	const float* GetVector(void){
		IFORSI(0, 4) {
			m_material[i] = ambient[i];
			m_material[i + 4] = diffuse[i];
			m_material[i + 8] = specular[i];
			m_material[i + 12] = emission[i];
			
		}
		return m_material;
	}

private:
	float m_material[16];

	///serialization
	friend class boost::serialization::access;
	template<class Archive>
	void save(Archive& ar, const unsigned int /*version*/) const {
		ar& SER_NVP(ambient);
		ar& SER_NVP(diffuse);
		ar& SER_NVP(specular);
		ar& SER_NVP(emission);
		ar& SER_NVP(shininess);
		ar& SER_NVP(illum);
		ar& SER_NVP(mtlname);
		ar& SER_NVP(path);
		//ar& shader;
		//ar& texture;
	}
	template<class Archive>
	void load(Archive& ar, const unsigned int /*version*/) {
		ar& SER_NVP(ambient);
		ar& SER_NVP(diffuse);
		ar& SER_NVP(specular);
		ar& SER_NVP(emission);
		ar& SER_NVP(shininess);
		ar& SER_NVP(illum);
		ar& SER_NVP(mtlname);
		ar& SER_NVP(path);
		//ar& shader;
		//ar& texture;
	}
	BOOST_SERIALIZATION_SPLIT_MEMBER()
};
BOOST_CLASS_VERSION(Material, 0)

#endif MATERIAL_S_H