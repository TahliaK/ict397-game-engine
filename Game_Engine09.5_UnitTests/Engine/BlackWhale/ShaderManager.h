#ifndef  SHADER_MANAGER_H
#define SHADER_MANAGER_H
#include "ShaderProgram.h"
#include <vector>
class ShaderManager {
public:
	static ShaderProgram* ActivateShaderProgram(unsigned int ID);
	static ShaderProgram* Get(unsigned int ID);
	static unsigned int CreateShaderProgram(std::string vertexShader, std::string fragmentShader);
	static ShaderProgram* GetActive(void);
	static void Clear();
private:
	static std::vector<ShaderProgram> m_shaders;
	static unsigned int m_activeShader;
	static unsigned int m_amount;
};
#endif // ! SHADER_MANAGER_H
