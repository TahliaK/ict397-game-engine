#ifndef WATER_H
#define WATER_H
#include "Component.h"
#include "Struct.h"
#include "Material_s.h"

/**
* @class Water
* @brief uses forward rendering to create a polygon with reflection 
* and refraction textures and simulate the behaviour of water.
* Note that it also works with forward rendering which has been removed.
* @Olesia Kochergina
* @date 11/02/2017
*/
class Water: public Component{
public:

	/*
	* @brief Default c-tor.
	*/
	Water(void);

	/*
	* @brief Copy c-tor.
	*/
	Water(const Water& original);

	/*
	* @brief Destructor.
	*/
	~Water();

	/*
	* @brief Initializes the object, called when creating the object.
	*/
	void Initialize();

	/*
	* @brief Called just before the first update.
	*/
	void Start();

	/*
	* @brief Updates the object and should be called every frame.
	*/
	void Update();

	/*
	* @brief Clears internal data, should be called when
	* the object needs to be either reused or deleted.
	*/
	void Clear();

	/*
	* @brief Creates a new component of this class.
	* @return Pointer to the new component or NULL.
	*/
	Component* NewComponent(void) const;

	/*
	* @brief Sets the speed of the waves.
	* @param waveSpeed - float variable
	* @return true - the speed is set.
	*/
	bool SetWaveSpeed(boost::any waveSpeed);
	
	/*
	* @brief Returns the current wave speed.
	* @return the speed
	*/
	float GetWaveSpeed(void) const;

	//bool SetMoveFactor(boost::any moveFactor);
	//float GetMoveFactor(void);
private:
	
	/*
	* @brief Adds values to three vectors: variable name,  setter, getter.
	* Used in the Editor to modify components at run-time.
	*/
	void Reflection();

	///wave speed
	float m_waveSpeed;

	///movement at every frame
	float m_moveFactor;

	///mesh information
	vBuffer m_data;

	///material of the mesh
	Material m_material;

	///normal map for the waves' illusion
	unsigned int normalMap;

	///color of the water
	RGBA m_color;

	///serialization
	friend class boost::serialization::access;
	template<class Archive>
	void save(Archive& ar, const unsigned int /*version*/) const {
		ar & SER_BASE_OBJECT_NVP(Component);
	}

	template<class Archive>
	void load(Archive& ar, const unsigned int /*version*/) {
		ar & SER_BASE_OBJECT_NVP(Component);
	}
	SER_SPLIT_MEMBER()
};
SER_CLASS_VERSION(Water, 0);
BOOST_CLASS_EXPORT_KEY(Water)
#endif WATER_H