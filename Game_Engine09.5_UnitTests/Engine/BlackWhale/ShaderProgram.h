#ifndef SHADERPROGRAM_H
#define SHADERPROGRAM_H
#include "Shader.h"
#include <iostream>
#include <map>

class ShaderProgram{
public:
	ShaderProgram();
	//ShaderProgram(const ShaderProgram& original);
	~ShaderProgram();
	void Create();
	/**
	* @brief Attaches fragment and Vector3 shaders.
	*/
	void Attach();
	bool Link();
	bool Activate();
	void PrintInfoLog();
	int GetID();
	void SetFragmentShader(std::string fileName);
	void SetVertexShader(std::string fileName);
	std::string GetVertexShader(void);
	std::string GetFragmentShader(void);
	void Clear();

	void SetULocation(std::string shaderVariable);
	int GetLocation(std::string shaderVariable);
	void SetLocations(void);
	void SetALocation(std::string shaderVariable);
private:
	unsigned int m_program;
	int m_linked;
	Shader m_fs;
	Shader m_vs;
	std::string m_fsName;
	std::string m_vsName;
	std::map<std::string,int> m_object_key;
};
#endif