#include "TerrainHeightmap.h"
#include "../Master.h"
#include "../VertexBuffer.h"
BOOST_CLASS_EXPORT_IMPLEMENT(TerrainHeightmap)

TerrainHeightmap::TerrainHeightmap():Terrain(){
	Reflection();
	m_fileName = "Resources/Rendering/heightmap128.jpg";
	m_width = 0;
	m_height = 0;
	m_heightmap = NULL;
	m_name = "TerrainHeightmap";
	static bool l_inList = false;
	if (!l_inList) {
		l_inList = true;
		TerrainHeightmap* l_pointer = new TerrainHeightmap;
		m_derivedClasses.push_back(l_pointer);
	}
}
TerrainHeightmap::~TerrainHeightmap() {
	Clear();
}
Component* TerrainHeightmap::NewComponent() const {
	Component* l_pointer = new TerrainHeightmap;
	return l_pointer;
}

void TerrainHeightmap::Clear() {
	Terrain::Clear();
	//FIX: throws an exception
	if (m_heightmap != NULL)
		delete m_heightmap;
	m_heightmap = NULL;
}
void TerrainHeightmap::Start() {
	Terrain::Start();
	if (Load())
		Generate();
}

bool TerrainHeightmap::SetHeightmap(boost::any p_var) {
	try {
		m_fileName = boost::any_cast<std::string> (p_var);
		if (!Load())
			return false;
		else {
			Generate();
		}
	}
	catch (boost::bad_any_cast& /*bac*/) {
		return false;
	}
	return true;
}

std::string TerrainHeightmap::GetHeightmap() {
	return m_fileName;
}

void TerrainHeightmap::Generate() {
	m_heights.clear();
	m_data.Clear();
	// Size of the terrain in world units
	float l_terrainWidth = (m_width - 1);
	float l_terrainHeight = (m_height - 1);

	float halfl_terrainWidthidth = l_terrainWidth * 0.5f;
	float halfTerrainHeight = l_terrainHeight * 0.5f;

	unsigned long int l_amount = m_height * m_width;
	
	vector<Vector3> l_vertices(l_amount);
	vector<Vector3> l_tCoords(l_amount);
	vector<Vector3> l_normals(l_amount);

	for (int x = 0; x < m_width; x++) {
		for (int y = 0; y < m_height; y++) {
			unsigned int index = (x * m_width) + y;
			
			float heightValue = m_heightmap[index];
			float S = (x / (float)l_terrainWidth);
			float T = (y / (float)l_terrainHeight);

			float X = (S * l_terrainWidth) - halfl_terrainWidthidth;
			float Y = heightValue;
			float Z = (T * l_terrainHeight) - halfTerrainHeight;

			// Blend 3 textures depending on the height of the terrain
			//	float tex0Contribution = 1.0f - FuzzyTwo( heightValue, 0.0f, 0.75f );
			//	float tex2Contribution = 1.0f - FuzzyTwo( heightValue, 0.75f, 1.0f );
			l_vertices[index] = Vector3(X, Y, Z);
			m_heights.push_back(Y);
			l_tCoords[index] = Vector3(S, T, 0);
		}
	}
	
	l_normals.resize(l_vertices.size());
	m_data.data.vertex = l_vertices;
	
	Smooth();
	l_vertices = m_data.data.vertex;
	m_data.data.vertex.clear();

	GenerateMesh(l_vertices, l_tCoords, l_normals, m_data);
	m_data.data.normal = GraphicsEngine::GenerateNormals(m_data.data.vertex);
	VertexBuffer::CreateVBO(&m_data, m_shaderProgram);
}

bool TerrainHeightmap::Load() {
	Clear();
	if (!GraphicsEngine::LoadHeightmap(m_fileName, m_heightmap, m_width, m_height)) {
		return false;
	}
	return true;
}

void TerrainHeightmap::GenerateMesh(std::vector<Vector3> p_vertexPoints, std::vector<Vector3> p_texturePoints, std::vector<Vector3> p_normalPoints, vBuffer& p_buffer) {
	const int l_neighbourhood = 8;
	//vertex compression where 1 - all pixels are considered and if step > 1 - only part of pixels
	//should never be 0 or negative
	float step = 1;
	/*
	vector<Vector3> tV;
	vector<Vector3> tN;
	vector<Vector3> tT;
	while(step < 1){
	for(int x=0;x<width-1;x+=1){
	for(int y=0;y<height-1; y+=1){
	int index = width*x+y;
	tV.push_back((p_vertexPoints.at(index) + p_vertexPoints.at(index+1))/2);
	tT.push_back((p_texturePoints.at(index) + p_texturePoints.at(index+1))/2);
	tN.push_back((p_normalPoints.at(index) + p_normalPoints.at(index+1))/2);
	}
	}
	step*=2;
	width*=2;
	height*=2;
	}
	p_vertexPoints = tV;
	p_normalPoints = tN;
	p_texturePoints = tT;*/
	//Moore neighbourhood
	int first[l_neighbourhood][2] = {
		{ -step,0 },{ -step,step },
		{ 0,step },{ step,step },
		{ step,0 },{ step,-step },
		{ 0,-step },{ -step,-step },
	};

	//Sobel neighbourhood
	/*int first[l_neighbourhood][2] = {
	{-step,step},{step,step},
	{step,-step},{-step,-step}
	};*/

	for (int x = step; x < m_width - step; x += step * 2) {
		for (int y = step; y < m_height - step; y += step * 2) {
			for (int i = 0; i < l_neighbourhood; i += 1) {//local array counter
														//first point of a triangle
				if (i != l_neighbourhood - 1) {//not the last one in an array
					p_buffer.data.vertex.push_back(p_vertexPoints.at(m_width*(x + first[i + 1][0]) + (y + first[i + 1][1])));
					p_buffer.data.normal.push_back(p_normalPoints.at(m_width*(x + first[i + 1][0]) + (y + first[i + 1][1])));
					p_buffer.data.texture.push_back(p_texturePoints.at(m_width*(x + first[i + 1][0]) + (y + first[i + 1][1])));
				}
				else {//take the first one
					p_buffer.data.vertex.push_back(p_vertexPoints.at(m_width*(x + first[0][0]) + (y + first[0][1])));
					p_buffer.data.texture.push_back(p_texturePoints.at(m_width*(x + first[0][0]) + (y + first[0][1])));
					p_buffer.data.normal.push_back(p_normalPoints.at(m_width*(x + first[0][0]) + (y + first[0][1])));
				}
				//middle point - the center of a l_neighbourhood
				p_buffer.data.vertex.push_back(p_vertexPoints.at(m_width*(x)+(y)));
				p_buffer.data.normal.push_back(p_normalPoints.at(m_width*(x)+(y)));
				p_buffer.data.texture.push_back(p_texturePoints.at(m_width*(x)+(y)));
				//third point
				p_buffer.data.vertex.push_back(p_vertexPoints.at(m_width*(x + first[i][0]) + (y + first[i][1])));
				p_buffer.data.normal.push_back(p_normalPoints.at(m_width*(x + first[i][0]) + (y + first[i][1])));
				p_buffer.data.texture.push_back(p_texturePoints.at(m_width*(x + first[i][0]) + (y + first[i][1])));
			}
		}
	}
}

void TerrainHeightmap::Reflection() {
	m_Variables.push_back("Heightmap file");
	m_Setters.push_back(boost::bind(&TerrainHeightmap::SetHeightmap, this, _1));
	m_Getters.push_back(boost::bind(&TerrainHeightmap::GetHeightmap, this));
}