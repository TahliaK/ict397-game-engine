#ifndef TERRRAINHEIGHTMAP_H
#define TERRAINHEIGHTMAP_H
#include "Terrain.h"

/*
* @brief Generates a 3d terrain from a heightmap.
* @author Olesia Kochergina
* @version 01
* @date 12/03/2017
*/
class TerrainHeightmap: public Terrain{
public:

	/*
	* @brief Default constructor.
	*/
	TerrainHeightmap();

	/*
	* @brief The destructor.
	*/
	~TerrainHeightmap();

	/*
	* @brief Called just before the first update.
	*/
	void Start();

	/*
	* @brief Deletes the dynamically allocated heighmap array.
	*/
	void Clear();

	/*
	* @brief Creates a new component of this class.
	* @return Pointer to the new component or NULL.
	*/
	Component* NewComponent(void) const;

	/*
	* @brief Sets a heightmap file name and loads a heightmap from the file.
	* @param fileName - string containing the name of the heightmap
	* @return true - the file name is valid
	*/
	bool SetHeightmap(boost::any fileName);

	/*
	* @brief Returns the current heightmap file name.
	* @return string containing the file name.
	*/
	std::string GetHeightmap();

private:

	///pointer to a dynamic array with height values from a texture
	int* m_heightmap;

	///file name of the current heightmap
	std::string m_fileName;

	/*
	* @brief Loads a heightmap.
	* @return true if the heightmap is valid.
	*/
	bool Load();

	/*
	* @brief Reads texture values, converts them into data vectors and creates a VBO.
	*/
	void Generate();
	
	/**
	* @brief Generates a 3D representation of a heightmap.
	* @param p_vertexPoints - sequentially stored vertex points
	* @param p_normalPoints - sequentially stored normal points
	* @param p_texturePoints - sequentially stored texture coordinates
	* @param p_buffer - reference to the current vertex buffer
	*/
	void GenerateMesh(std::vector<Vector3> p_vertexPoints, std::vector<Vector3> p_texturePoints, std::vector<Vector3> p_normalPoints, vBuffer& p_buffer);
	
	/*
	* @brief Adds values to three vectors: variable name,  setter, getter.
	* Used in the Editor to modify components at run-time.
	*/
	void Reflection();

	///serialization
	friend class boost::serialization::access;
	template<class Archive>
	void save(Archive& ar, const unsigned int /*version*/) const {
		ar & SER_BASE_OBJECT_NVP(Terrain);
		ar & SER_NVP(m_fileName);
	}

	template<class Archive>
	void load(Archive& ar, const unsigned int /*version*/) {
		ar & SER_BASE_OBJECT_NVP(Terrain);
		ar & SER_NVP(m_fileName);
	}
	SER_SPLIT_MEMBER()
};
SER_CLASS_VERSION(TerrainHeightmap, 0);
SER_CLASS_EXPORT_KEY(TerrainHeightmap)
#endif TERRAINHEIGHTMAP_H