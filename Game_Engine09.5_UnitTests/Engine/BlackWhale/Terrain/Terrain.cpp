#include "Terrain.h"
#include <assert.h>
#include <iostream>
#include <fstream>
#include "../Master.h"
#include "../VertexBuffer.h"
#include "../GraphicsEngine.h"
#include "../SceneManager.h"
#include "../ShaderManager.h"

BOOST_CLASS_EXPORT_IMPLEMENT(Terrain)

Terrain::Terrain() : Component() {
	Reflection();
	Initialize();
	m_name = "Terrain";
}

void Terrain::Initialize() {
	//default set up
	/*m_vS = "Resources/Shaders/TerrainDR.vert";
	m_fS = "Resources/Shaders/TerrainDR.frag";
	m_shaderProgram = 0;*/
	m_curTextureIndex = -1;
	m_smoothStep = 0;
	m_height = 0;
	m_width = 0;
	m_noiseMap = 0;
	m_noiseMapFile = "Resources/Rendering/TerrainNoiseSeamless.jpg";
	m_detailMap = 0;
	m_detailMapFile = "Resources/Rendering/DetailMap.jpg";

	SetTextureIndex(0);
	SetTextureName("Resources/Rendering/Grass.jpg");
	SetTextureMinHeight(-10);
	SetTextureMaxHeight(3);

	SetTextureIndex(1);
	SetTextureName("Resources/Rendering/GrassRock.jpg");
	SetTextureMinHeight(3);
	SetTextureMaxHeight(12);

	SetTextureIndex(2);
	SetTextureName("Resources/Rendering/Rock.jpg");
	SetTextureMinHeight(12);
	SetTextureMaxHeight(20);

	SetTextureIndex(3);
	SetTextureName("Resources/Rendering/Snow.jpg");
	SetTextureMinHeight(20);
	SetTextureMaxHeight(30);
}

void Terrain::Clear() {
	m_data.Clear();
}

Terrain::~Terrain() {
	Clear();
}

void Terrain::Start() {
	SetDetailMap(m_detailMapFile);
	SetNoiseMap(m_noiseMapFile);
	/*m_vS = "Resources/Shaders/geom3D.vert";
	m_fS = "Resources/Shaders/geom3D.frag";
	*/

	//ShaderManager::Get(m_shaderProgram)->SetULocation("height");
	m_shaderProgram = ShaderManager::CreateShaderProgram("Resources/Shaders/geom3D.vert", "Resources/Shaders/geom3D.frag");
	if (m_shaderProgram == 0)
		return;
	else {
		m_data.attribLocation[0] = ShaderManager::Get(m_shaderProgram)->GetLocation("position");
		m_data.attribLocation[1] = ShaderManager::Get(m_shaderProgram)->GetLocation("texCoord");
		m_data.attribLocation[2] = ShaderManager::Get(m_shaderProgram)->GetLocation("normal");
	}
	if (m_textures.size() == 0) {
		IFORSI(0, m_nameTextures.size()) {
			m_textures.push_back(GraphicsEngine::LoadTexture(m_nameTextures.at(i), false));
		}
	}
	else {
		IFORSI(0, m_nameTextures.size()) {
			m_textures.at(i) = GraphicsEngine::LoadTexture(m_nameTextures.at(i), false);
		}
	}
}

void Terrain::Update() {
	ShaderManager::ActivateShaderProgram(m_shaderProgram);
	if (!m_gameObject->GetVisible())
		return;
	Matrix4 l_modelMatrix = GraphicsEngine::BuildModelMatrix(SceneManagement::SceneManager::GetActive()->GetMainCamera()->GetModelMatrix(), m_gameObject->GetTransform()->position, m_gameObject->GetTransform()->rotation, m_gameObject->GetTransform()->scale);
	GraphicsEngine::EnableTexture(0, m_noiseMap, ShaderManager::Get(m_shaderProgram)->GetLocation("texture"));
	GraphicsEngine::EnableTexture(1, m_detailMap, ShaderManager::Get(m_shaderProgram)->GetLocation("DetailMap"));


	Master::gl.Uniform3fv(ShaderManager::Get(m_shaderProgram)->GetLocation("u_cameraPosition"), 1, SceneManagement::SceneManager::GetActive()->GetMainCamera()->GetGameObject()->GetTransform()->position);
	Master::gl.Uniform1i(ShaderManager::Get(m_shaderProgram)->GetLocation("u_shader"), 1);
	Master::gl.UniformMatrix4fv(ShaderManager::GetActive()->GetLocation("model"), 1, GRAPHICS_FALSE, l_modelMatrix);


	if (m_textures.size() > 0) {
		int arrayT[10] = { 0 };
		for (int i = 0; i < m_textures.size(); i++)
			arrayT[i] = i + 2;
		Master::gl.Uniform1i(ShaderManager::Get(m_shaderProgram)->GetLocation("u_amountTextures"), m_textures.size());
		Master::gl.Uniform1iv(ShaderManager::Get(m_shaderProgram)->GetLocation("multitexture"), m_textures.size(), arrayT);
		Master::gl.Uniform1fv(ShaderManager::Get(m_shaderProgram)->GetLocation("maxH"), m_textures.size(), &m_maxTextures[0]);
		Master::gl.Uniform1fv(ShaderManager::Get(m_shaderProgram)->GetLocation("minH"), m_textures.size(), &m_minTextures[0]);
	}
	for (unsigned int i = 0; i < m_textures.size(); i++) {
		Master::gl.ActiveTexture(GRAPHICS_TEXTURE2 + i);
		Master::gl.BindTexture(GRAPHICS_TEXTURE_2D, m_textures[i]);
	}

	Master::gl.TexEnvi(GRAPHICS_TEXTURE_ENV, GRAPHICS_TEXTURE_ENV_MODE, GRAPHICS_MODULATE);
	VertexBuffer::RenderVBO(&m_data, 0, m_data.data.vertex.size());

	for (unsigned i = 0; i < m_textures.size() + 2; i++)
		GraphicsEngine::DisableTexture(i);
	Master::gl.ActiveTexture(GRAPHICS_TEXTURE0);
	Master::gl.BindTexture(GRAPHICS_TEXTURE_2D, Master::NULL_TEXTURE);
}

float Terrain::GetHeight(float p_worldX, float p_worldZ) {
	if (!m_gameObject)
		return 0;
	//world coordinates
	float terrainW = (m_width - 1) * m_gameObject->GetTransform()->scale.x;
	float terrainH = (m_height - 1) * m_gameObject->GetTransform()->scale.z;

	//beginning of the grid
	float l_x = p_worldX - (-terrainW*0.5) - m_gameObject->GetTransform()->position.x;
	float l_z = p_worldZ - (-terrainH*0.5) - m_gameObject->GetTransform()->position.z;

	//width/Height of the terrain divided by the amount of grid squares
	float squareSizeW = (terrainW) / (m_width - 1);
	float squareSizeH = (terrainH) / (m_height - 1);

	//find a grid square
	int gridX = (int)std::floor(l_x / squareSizeW);
	int gridZ = (int)std::floor(l_z / squareSizeH);

	//if not on the terrain
	if (gridX >= m_width - 1 || gridZ >= m_height - 1 || gridX < 0 || gridZ < 0) {
		return 0;
	}

	//Player's XZ coordinates within one grid square
	float xCoord = fmod(l_x, squareSizeW) / squareSizeW;
	float zCoord = fmod(l_z, squareSizeH) / squareSizeH;

	/*if (m_transform->rotation.y == 180) {
		int t = gridX;
		gridX = gridZ;
		gridZ = t;
	}*/

	float answer;
	//find a specific triangle in the grid square
	if (xCoord <= (1 - zCoord)) {
		answer = Vector3::BaryCentric(Vector3(0, m_heights[(gridX*m_width + gridZ)], 0),
			Vector3(1, m_heights[((gridX + 1)*m_width + gridZ)], 0),
			Vector3(0, m_heights[(gridX*m_width + gridZ + 1)], 1),
			Vector2(xCoord, zCoord));
	}
	else {
		answer = Vector3::BaryCentric(Vector3(1, m_heights[((gridX + 1)*m_width + gridZ)], 0),
			Vector3(1, m_heights[((gridX + 1)*m_width + gridZ + 1)], 1),
			Vector3(0, m_heights[(gridX*m_width + gridZ + 1)], 1),
			Vector2(xCoord, zCoord));
	}

	return answer * m_gameObject->GetTransform()->scale.y + m_gameObject->GetTransform()->position.y;
}

void Terrain::Smooth() {
	for (int k = 0; k < m_smoothStep; k++) {
		m_heights.clear();
		std::vector<Vector3> copy(m_data.data.vertex);
		int temp = 0;
		IFORSI(0, m_width) {
			JFORSI(0, m_height) {
				copy.at(m_width*i + j).y = ChangeHeight(i, j, m_width, m_height, m_data.data.vertex.at(m_width*i + j).y);
				m_heights.push_back((copy.at(m_width*i + j).y));
			}
		}
		m_data.data.vertex = copy;
	}
}

bool Terrain::SetSmoothStep(boost::any var) {
	try {
		int l_temp = boost::any_cast<int> (var);
		if (l_temp >= 0) {
			m_smoothStep = l_temp;
			Generate();
		}
		else
			return false;
	}
	catch (boost::bad_any_cast& /*bac*/) {
		return false;
	}
	return true;
}

int Terrain::GetSmoothStep() const {
	return m_smoothStep;
}


float Terrain::ChangeHeight(const int p_x, const int p_y, const int maxX, const int maxY, const float p_height) const {
	float total = p_height;
	int l_counter = 1;//amount of neighbours + this point
	for (int h = -1; h < 2; h++) {
		for (int v = -1; v < 2; v++) {
			if (!(v == 0 && h == 0)) {
				if (p_x + h >= 0 && p_x + h < maxX && (p_y + v) >= 0 && (p_y + v) < maxY &&
					(maxX * (p_x + h) + p_y + v) < maxX * maxY && maxX * maxY * (p_x + h) + p_y + v >= 0) {

					total += m_data.data.vertex.at(maxX*(p_x + h) + p_y + v).y;
					l_counter++;
				}

			}
		}
	}
	return total / l_counter;
}
//
//const bool Terrain::SetVertexShader(boost::any var) {
//	try {
//		std::string l_temp = boost::any_cast<string> (var);
//		if (ShaderManager::Get(m_shaderProgram)->GetVertexShader().compare(l_temp) != 0) {
//			m_vS = l_temp;
//			m_shaderProgram = ShaderManager::CreateShaderProgram(l_temp, ShaderManager::Get(m_shaderProgram)->GetFragmentShader());
//			Generate();
//		}
//		else
//			return false;
//	}
//	catch (boost::bad_any_cast& /*bac*/) {
//		return false;
//	}
//	return false;
//}
//const bool Terrain::SetFragmentShader(boost::any var) {
//	try {
//		std::string l_temp = boost::any_cast<string> (var);
//		if (ShaderManager::Get(m_shaderProgram)->GetVertexShader().compare(l_temp) != 0) {
//			m_fS = l_temp;
//			m_shaderProgram = ShaderManager::CreateShaderProgram(ShaderManager::Get(m_shaderProgram)->GetVertexShader(), l_temp);
//			Generate();
//		}
//		else
//			return false;
//	}
//	catch (boost::bad_any_cast& /*bac*/) {
//		return false;
//	}
//	return false;
//}
//
//std::string Terrain::GetVertexShader(void)const {
//	return m_vS;
//}
//std::string Terrain::GetFragmentShader(void)const {
//	return m_fS;
//}

const bool Terrain::SetTextureIndex(boost::any p_var) {
	try {
		int l_temp = boost::any_cast<int> (p_var);
		if (l_temp >= 0 && l_temp < m_textures.size()) {
			m_curTextureIndex = l_temp;
		}
		else if (l_temp == m_textures.size()) {
			m_textures.push_back(Master::NULL_TEXTURE);
			m_maxTextures.push_back(0);
			m_minTextures.push_back(0);
			m_nameTextures.push_back("Resources/Rendering/");
			m_curTextureIndex = l_temp;
		}
		else {
			return false;
		}
	}
	catch (boost::bad_any_cast& /*bac*/) {
		return false;
	}
	return true;
}

const bool Terrain::SetTextureName(boost::any p_var) {
	if (m_curTextureIndex == -1)
		return false;
	try {
		std::string l_temp = boost::any_cast<string> (p_var);
		if (l_temp.compare("") == 0) {
			m_nameTextures.at(m_curTextureIndex) = "";
			m_textures.at(m_curTextureIndex) = Master::NULL_TEXTURE;
			return true;
		}
		int l_index = GraphicsEngine::LoadTexture(l_temp, false);
		if (l_index != 0) {
			m_textures.at(m_curTextureIndex) = l_index;
			m_nameTextures.at(m_curTextureIndex) = l_temp;
		}
		else {
			return false;
		}

	}
	catch (boost::bad_any_cast& /*bac*/) {
		return false;
	}
	return true;
}

const bool Terrain::SetTextureMinHeight(boost::any p_var) {
	if (m_curTextureIndex == -1)
		return false;
	try {
		m_minTextures.at(m_curTextureIndex) = boost::any_cast<float> (p_var);
	}
	catch (boost::bad_any_cast& /*bac*/) {
		return false;
	}
	return true;
}

const bool Terrain::SetTextureMaxHeight(boost::any p_var) {
	if (m_curTextureIndex == -1)
		return false;
	try {
		m_maxTextures.at(m_curTextureIndex) = boost::any_cast<float> (p_var);
	}
	catch (boost::bad_any_cast& /*bac*/) {
		return false;
	}
	return true;
}

int Terrain::GetTextureIndex(void) const {
	return m_curTextureIndex;
}

const bool Terrain::SetDetailMap(boost::any p_var) {
	try {
		std::string l_temp = boost::any_cast<string> (p_var);
		if (l_temp.compare("") == 0) {
			m_detailMapFile = "";
			m_detailMap = Master::NULL_TEXTURE;
			return true;
		}
		int l_index = GraphicsEngine::LoadTexture(l_temp, false);
		if (l_index != 0) {
			m_detailMap = l_index;
			m_detailMapFile = l_temp;
		}
		else {
			return false;
		}
	}
	catch (boost::bad_any_cast& /*bac*/) {
		return false;
	}
	return true;
}

const bool Terrain::SetNoiseMap(boost::any p_var) {
	try {
		std::string l_temp = boost::any_cast<string> (p_var);
		if (l_temp.compare("") == 0) {
			m_noiseMapFile = "";
			m_noiseMap = Master::NULL_TEXTURE;
			return true;
		}
		int l_index = GraphicsEngine::LoadTexture(l_temp, false);
		if (l_index != 0) {
			m_noiseMap = l_index;
			m_noiseMapFile = l_temp;
		}
		else {
			return false;
		}
	}
	catch (boost::bad_any_cast& /*bac*/) {
		return false;
	}
	return true;
}

std::string Terrain::GetDetailMap(void) const {
	return m_detailMapFile;
}

std::string Terrain::GetNoiseMap(void) const {
	return m_noiseMapFile;
}

std::string Terrain::GetTextureName(void) const {
	if (m_curTextureIndex > -1) {
		return m_nameTextures.at(m_curTextureIndex);
	}
	return "Resources/Rendering/";
}

float Terrain::GetTextureMinHeight(void) const {
	if (m_curTextureIndex > -1) {
		return m_minTextures.at(m_curTextureIndex);
	}
	return -1000000;
}

float Terrain::GetTextureMaxHeight(void) const {
	if (m_curTextureIndex > -1) {
		return m_maxTextures.at(m_curTextureIndex);
	}
	return -1000000;
}

void Terrain::Reflection() {
	m_Variables.push_back("Smooth step");
	m_Setters.push_back(boost::bind(&Terrain::SetSmoothStep, this, _1));
	m_Getters.push_back(boost::bind(&Terrain::GetSmoothStep, this));

	m_Variables.push_back("Noise Map");
	m_Setters.push_back(boost::bind(&Terrain::SetNoiseMap, this, _1));
	m_Getters.push_back(boost::bind(&Terrain::GetNoiseMap, this));

	m_Variables.push_back("Detail Map");
	m_Setters.push_back(boost::bind(&Terrain::SetDetailMap, this, _1));
	m_Getters.push_back(boost::bind(&Terrain::GetDetailMap, this));

	m_Variables.push_back("Texture Index");
	m_Setters.push_back(boost::bind(&Terrain::SetTextureIndex, this, _1));
	m_Getters.push_back(boost::bind(&Terrain::GetTextureIndex, this));

	m_Variables.push_back("File Name");
	m_Setters.push_back(boost::bind(&Terrain::SetTextureName, this, _1));
	m_Getters.push_back(boost::bind(&Terrain::GetTextureName, this));

	m_Variables.push_back("Minimum Height");
	m_Setters.push_back(boost::bind(&Terrain::SetTextureMinHeight, this, _1));
	m_Getters.push_back(boost::bind(&Terrain::GetTextureMinHeight, this));

	m_Variables.push_back("Maximum Height");
	m_Setters.push_back(boost::bind(&Terrain::SetTextureMaxHeight, this, _1));
	m_Getters.push_back(boost::bind(&Terrain::GetTextureMaxHeight, this));
	//m_Variables.push_back("Vertex Shader");
	//m_Setters.push_back(boost::bind(&Terrain::SetVertexShader, this, _1));
	//m_Getters.push_back(boost::bind(&Terrain::GetVertexShader, this));

	//m_Variables.push_back("Fragment Shader");
	//m_Setters.push_back(boost::bind(&Terrain::SetFragmentShader, this, _1));
	//m_Getters.push_back(boost::bind(&Terrain::GetFragmentShader, this));
}

//
//float Terrain::FuzzyTwo(float value, float min, float max){
//	float result;
//
//	if (value <= min)
//		result = 0;//false
//	else if (value >= max)
//		result = 1;//true
//	else 
//		result = (value / (max - min)) - (min / (max - min));
//	return result;
//}
//

//int Terrain::GetFileLength( std::istream& file )
//{
//	int start = file.tellg();//starting position
//	file.seekg(0, std::ios::end ); //go to the end
//	int length = file.tellg(); //get the ending position
//	file.seekg(0, std::ios::beg); // Restore the position of the get pointer
//
//	return length - start;
//}

//
//void Terrain::Compress(vector<Vertex3>& m_vArray,vector<Vertex3>& m_tArray, vector<Vertex3>& m_nArray){
//	//quadric error metric
//	//each vertex - error metric, 4x4 matrix, quadric error
//	//each edge - new vertex with minimum error value is found and used
//	//for selecting an edge to be collapsed
//	//selecting edge:
//	//two vertices (p,q) if form an edge(pg) or (|p-q|<e)
//	//e - user defined constant -> should be 0
//	int agressiveness = 1;
//	int target_count = 10000;
//	vector<Ref> refs;
//	vector<Triangle3> trigV(m_vArray.size()/3);
//	int deleted_trig = 0;
//	vector<int> deleted0,deleted1;
//	int trig_count = trigV.size();
//
//
//	for(int i=0;i<m_vArray.size()-2;i+=3){
//		trigV.at(i/3) = (Triangle3(i, i+1,i+2));
//		trigV.at(i/3).deleted = false;
//	}
//	for(int iteration=0;iteration<100;iteration++){	
//		//if target number of triangles reached then break
//		if(trig_count - deleted_trig <= target_count){
//			break;
//		}
//		//update mesh once in a while
//		if(iteration % 5 == 0){
//			Update_Mesh(iteration,m_vArray,trigV,refs);
//		}
//
//		cout<<"done5"<<endl;
//		//clear dirty flag
//		for(int i=0; i< trigV.size();i++){
//			trigV[i].dirty = false;
//		}
//		//remove trigs below threshold
//
//		double threshold = 00.000000001*pow(double(iteration+3),agressiveness);
//		for(int i=0;i<trigV.size();i++){
//			Triangle3 &t = trigV[i];
//			if(t.error[3] > threshold || t.deleted || t.dirty)
//				continue;
//			for(int j=0;j<3;j++){
//				if(t.error[j]<threshold){
//					int i0 = t.v[j];
//					Vertex3 &v0 = m_vArray[i0];
//					int i1 = t.v[(j+1)%3];
//					Vertex3 &v1 = m_vArray[i1];
//
//					//border check
//					if(v0.border != v1.border)
//						continue;
//
//					//compute vertex to collapse to
//					Vector3 p;
//					calculate_error(i0,i1,p,m_vArray,trigV,refs);
//
//					deleted0.resize(v0.tcount);
//					deleted1.resize(v1.tcount);
//
//					if(flipped(p,i0,i1,v0,v1,deleted0,m_vArray,trigV,refs))
//						continue;
//					if(flipped(p,i1,i0,v1,v0,deleted1,m_vArray,trigV,refs))
//						continue;
//
//					//not flipped then remove
//					v0 = p;
//					v0.q = v1.q + v0.q;
//					int tstart = refs.size();
//
//					update_triangles(i0,v0,deleted0,deleted_trig,m_vArray,trigV,refs);
//					update_triangles(i0,v1,deleted1,deleted_trig,m_vArray,trigV,refs);
//					int tcount = refs.size() - tstart;
//					if(tcount <= v0.tcount){
//						if(tcount)
//							memcpy(&refs[v0.tstart],&refs[tstart],tcount*sizeof(Ref));
//					}else{//append
//						v0.tstart = tstart;
//					}
//					v0.tcount = tcount;
//					break;
//				}
//			}
//			//done?
//			if(trig_count- deleted_trig <= target_count)
//				break;
//		}
//	}
//
//	//clean up mesh
//	compact_mesh(m_vArray,trigV,refs);
//}
//
//void Terrain::Update_Mesh(int iteration, vector<Vertex3> m_vArray, vector<Triangle3> trigV, vector<Ref> refs){
//	//compact triangles, compute edge error and build reference list
//	if(iteration > 0){//compact triangles
//		int dst = 0;
//		for(int i=0;i<trigV.size();i++){
//			if(!trigV[i].deleted){
//				trigV[dst++] = trigV[i];
//			}
//		}
//		trigV.resize(dst);
//	}
//	cout<<"done1"<<endl;
//	//init quadrics by plane & edge errors
//	if(iteration == 0){
//		for(int i=0; i < m_vArray.size();i++){
//			m_vArray[i].q = SymmetricMatrix(0.0);
//		}
//
//		for(int i=0;i<trigV.size();i++){
//			Triangle3 &t = trigV.at(i);
//			Vector3 n,p[3];
//
//			for(int j = 0; j<3;j++){
//				p[j] = m_vArray[t.v[j]].p;
//			}
//
//			n = Vector3::Cross(p[1]-p[0],p[2]-p[0]);
//			t.normal = Vector3::Normalize(n);
//			for(int j = 0; j<3;j++){
//				m_vArray[t.v[j]].q = m_vArray[t.v[j]].q + SymmetricMatrix(t.normal.x,t.normal.y,t.normal.z, - Vector3::Dot(t.normal,p[0]));
//			}
//		}
//		cout<<"done 1.5"<<endl;
//		for(int i=0;i<trigV.size();i++){
//			Triangle3 &t = trigV.at(i);
//			Vector3 p;
//			for(int j=0;j<3;j++){
//				t.error[j] = calculate_error(t.v[j],t.v[(j+1)%3],p,m_vArray,trigV,refs);
//			}
//			t.error[3] = t.error[0];
//			if(t.error[3] > t.error[1])
//				t.error[3] = t.error[1];
//			if(t.error[3] > t.error[2])
//				t.error[3] = t.error[2];
//		}
//	}
//	cout<<"done2"<<endl;
//	//init reference id list
//
//	for(int i=0;i<m_vArray.size();i++){
//		m_vArray[i].tstart = 0;
//		m_vArray[i].tcount = 0;
//	}
//
//	for(int i=0;i<trigV.size();i++){
//		Triangle3 &t=trigV[i];
//		for(int j=0;j<3;j++){
//			m_vArray[t.v[j]].tcount++;
//		}
//	}
//	int tstart = 0;
//	for(int i=0;i<m_vArray.size();i++){
//		Vertex3 &v = m_vArray[i];
//		v.tstart = tstart;
//		tstart +=v.tcount;
//		v.tcount = 0;
//	}
//	cout<<"done3"<<endl;
//	//write references
//	refs.resize(trigV.size()*3);
//	for(int i=0;i<trigV.size();i++){
//		Triangle3 &t = trigV[i];
//		for(int j=0;j<3;j++){
//			Vertex3 &v = m_vArray[t.v[j]];
//			refs[v.tstart+v.tcount].tid = i;
//			refs[v.tstart+v.tcount].tvertex = j;
//			v.tcount++;
//		}
//	}
//	cout<<"done4"<<endl;
//	//identify boundary : m_vArray[].border =0,1
//	if(iteration==0){
//		vector<int> vcount,vids;
//
//		for(int i=0;i<m_vArray.size();i++){
//			m_vArray[i].border = 0;
//		}
//		for(int i=0;i<m_vArray.size();i++){
//			Vertex3 &v = m_vArray[i];
//			vcount.clear();
//			vids.clear();
//			for(int j=0;j<v.tcount;j++){
//				int kk = refs[v.tstart+j].tid;
//				Triangle3 &t = trigV[kk];
//				for(int k = 0;k<3;k++){
//					int ofs =0,id=t.v[k];
//					while(ofs < vcount.size()){
//						if(vids[ofs] == id)
//							break;
//						ofs++;
//					}
//					if(ofs == vcount.size()){
//						vcount.push_back(1);
//						vids.push_back(id);
//					}else
//						vcount[ofs]++;
//				}
//			}
//			for(int j=0;j<vcount.size();j++){
//				if(vcount[j]==1)
//					m_vArray[vids[j]].border = 1;
//			}
//		}
//	}
//}
//
//bool Terrain::flipped(Vector3 p, int i0, int i1, Vertex3 &v0, Vertex3 &v1,vector<int> &deleted, vector<Vertex3> m_vArray, vector<Triangle3> trigV, vector<Ref> refs){
//	int bordercount=0;
//	for(int k=0;k<v0.tcount;k++)
//	{
//		Triangle3 &t=trigV[refs[v0.tstart+k].tid]; 
//		if(t.deleted)continue;
//
//		int s=refs[v0.tstart+k].tvertex;
//		int id1=t.v[(s+1)%3];
//		int id2=t.v[(s+2)%3];
//
//		if(id1==i1 || id2==i1) // delete ?
//		{
//			bordercount++;
//			deleted[k]=1;
//			continue;
//		}
//		Vector3 d1 = m_vArray[id1].p-p;
//		Vector3 d2 = m_vArray[id2].p-p;
//		if(fabs(Vector3::Dot(Vector3::Normalize(d1),Vector3::Normalize(d2)))>0.999) 
//			return true;
//		deleted[k]=0;	
//		if(Vector3::Dot(Vector3::Normalize(Vector3::Cross(Vector3::Normalize(d1),Vector3::Normalize(d2))),t.normal)<0.2)
//			return true;
//	}
//	return false;
//}
//
//void Terrain::update_triangles(int i0,Vertex3 &v,std::vector<int> &deleted,int &deleted_triangles, vector<Vertex3> m_vArray, vector<Triangle3> trigV, vector<Ref> refs){
//	Vector3 p;
//	for(int k=0;k<v.tcount;k++)
//	{
//		Ref &r=refs[v.tstart+k];
//		Triangle3 &t=trigV[r.tid]; 
//		if(t.deleted)continue;
//		if(deleted[k]) 
//		{
//			t.deleted=1;
//			deleted_triangles++;
//			continue;
//		}
//		t.v[r.tvertex]=i0;
//		t.dirty=1;
//		t.error[0]=calculate_error(t.v[0],t.v[1],p,m_vArray,trigV,refs);
//		t.error[1]=calculate_error(t.v[1],t.v[2],p,m_vArray,trigV,refs);
//		t.error[2]=calculate_error(t.v[2],t.v[0],p,m_vArray,trigV,refs);
//
//		t.error[3] = t.error[0];
//		if(t.error[3] > t.error[1])
//			t.error[3] = t.error[1];
//		if(t.error[3] > t.error[2])
//			t.error[3] = t.error[2];
//		refs.push_back(r);
//	}
//}
//
//void Terrain::compact_mesh(vector<Vertex3> m_vArray, vector<Triangle3> trigV, vector<Ref> refs){
//	int dst=0;
//	for(int i=0;i<m_vArray.size();i++)
//	{
//		m_vArray[i].tcount=0;
//	}
//	for(int i =0; i< trigV.size();i++)
//		if(!trigV[i].deleted)
//		{
//			Triangle3 &t=trigV[i];
//			trigV[dst++]=t;
//			for(int j=0;j<3;j++)
//				m_vArray[t.v[j]].tcount=1;
//		}
//		trigV.resize(dst);
//		dst=0;
//		for(int i=0;i<m_vArray.size();i++)
//			if(m_vArray[i].tcount)
//			{
//				m_vArray[i].tstart=dst;
//				m_vArray[dst]=m_vArray[i];
//				dst++;
//			}
//
//			for(int i=0;trigV.size();i++)
//			{
//				Triangle3 &t=trigV[i];
//
//				for(int j=0;j<3;j++)
//					t.v[j]=m_vArray[t.v[j]].tstart;
//			}
//			m_vArray.resize(dst);
//}
//
//double Terrain::vertex_error(SymmetricMatrix q, double x, double y, double z){
//	return   q[0]*x*x + 2*q[1]*x*y + 2*q[2]*x*z + 2*q[3]*x + q[4]*y*y
//		+ 2*q[5]*y*z + 2*q[6]*y + q[7]*z*z + 2*q[8]*z + q[9];
//}
//
//double Terrain::calculate_error(int id_v1, int id_v2, Vector3 &p_result, vector<Vertex3> m_vArray, vector<Triangle3> trigV, vector<Ref> refs)
//{
//	// compute interpolated vertex 
//
//	SymmetricMatrix q = m_vArray[id_v1].q + m_vArray[id_v2].q;
//	bool   border = m_vArray[id_v1].border & m_vArray[id_v2].border;
//	double error=0;
//	double det = q.det(0, 1, 2, 1, 4, 5, 2, 5, 7);
//
//	if ( det != 0 && !border )
//	{
//		// q_delta is invertible
//		p_result.x = -1/det*(q.det(1, 2, 3, 4, 5, 6, 5, 7 , 8));	// vx = A41/det(q_delta) 
//		p_result.y =  1/det*(q.det(0, 2, 3, 1, 5, 6, 2, 7 , 8));	// vy = A42/det(q_delta) 
//		p_result.z = -1/det*(q.det(0, 1, 3, 1, 4, 6, 2, 5,  8));	// vz = A43/det(q_delta) 
//		error = vertex_error(q, p_result.x, p_result.y, p_result.z);
//	}
//	else
//	{
//		// det = 0 -> try to find best result
//		Vector3 p1 = m_vArray[id_v1].p;
//		Vector3 p2 = m_vArray[id_v2].p;
//		Vector3 p3 = (p1+p2)/2;
//
//		double error1 = vertex_error(q, p1.x,p1.y,p1.z);
//		double error2 = vertex_error(q, p2.x,p2.y,p2.z);
//		double error3 = vertex_error(q, p3.x,p3.y,p3.z);
//		error = min(error1, min(error2, error3));
//		if (error1 == error) 
//			p_result=p1;
//		if (error2 == error) 
//			p_result=p2;
//		if (error3 == error) 
//			p_result=p3;
//	}
//	return error;
//}
