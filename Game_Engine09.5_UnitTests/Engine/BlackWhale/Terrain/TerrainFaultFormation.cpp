﻿#include "TerrainFaultFormation.h"
#include "../Master.h"
#include "../VertexBuffer.h"
BOOST_CLASS_EXPORT_IMPLEMENT(TerrainFaultFormation)

TerrainFaultFormation::TerrainFaultFormation() : Terrain() {
	Reflection();
	m_iterations = 1;
	m_name = "TerrainFaultFormation";
	m_width = 128;
	m_height = 128;
	static bool l_inList = false;
	if (!l_inList) {
		l_inList = true;
		TerrainFaultFormation* l_pointer = new TerrainFaultFormation;
		m_derivedClasses.push_back(l_pointer);
	}
}
TerrainFaultFormation::~TerrainFaultFormation() {
	Clear();
}
Component* TerrainFaultFormation::NewComponent() const {
	Component* l_pointer = new TerrainFaultFormation;
	return l_pointer;
}

void TerrainFaultFormation::Clear() {
	Terrain::Clear();
}

void TerrainFaultFormation::Start() {
	Terrain::Start();
	Generate();
}

void TerrainFaultFormation::Generate() {
	if (m_iterations == 0 || !m_gameObject)
		return;
	m_data.Clear();

	float l_terrainWidth = (m_width - 1);
	float l_terrainHeight = (m_height - 1);

	float halfl_terrainWidthidth = l_terrainWidth * 0.5f;
	float halfTerrainHeight = l_terrainHeight * 0.5f;

	unsigned long int l_amount = m_width*m_height;
	vector<Vector3> l_tCoords(l_amount);
	std::vector<Vector3> l_vertices(l_amount);
	m_data.data.vertex.resize(l_amount);

	for (int x = 0; x < m_width; x++) {
		for (int z = 0; z < m_height; z++) {
			m_data.data.vertex[x*m_width + z].x = x;
			m_data.data.vertex[x*m_width + z].z = z;
			float S = (x / (float)l_terrainWidth);
			float T = (z / (float)l_terrainHeight);
			l_tCoords[x*m_width + z] = Vector3(S, T, 0);
		}
	}
	for (int k = 0; k < m_iterations; k++) {
		float l_x1 = (rand() % (m_width));
		float l_z1 = (rand() % (m_height));
		float l_x2 = 0, l_z2 = 0;
		do {
			float l_x2 = (rand() % (m_width));
			float l_z2 = (rand() % (m_height));
		} while (l_x1 == l_x2  && l_z1 == l_z2);

		float l_displacement = ((float)(rand() % 100)) / 500.0f;
		//if true - then from min to max x and z; else from max to min
		bool l_toMaxValue = rand() % 2;
		int l_y = 0;
		IFORSI(0, m_width) {
			JFORSI(0, m_height) {
				unsigned int l_cell = i*m_width + j;
				if (l_toMaxValue)
					l_y = (l_z2 - l_z1)* (i - l_x1) - (l_x2 - l_x1) * (j - l_z1);
				else
					l_y = (l_z1 - l_z2)* (i - l_x2) - (l_x1 - l_x2) * (j - l_z2);
				if (l_y > 0) {
					if (rand() % 2 == 1) {
						m_data.data.vertex[l_cell].y += l_displacement;
					}
					else {
						m_data.data.vertex[l_cell].y -= l_displacement;
					}
				}
			}
		}
	}

	Smooth();
	l_vertices = m_data.data.vertex;
	m_data.data.vertex.clear();
	vector<Vector3> l_normals;
	l_normals.resize(l_vertices.size());

	GenerateMesh(l_vertices, l_tCoords, l_normals, m_data);
	m_data.data.normal = GraphicsEngine::GenerateNormals(m_data.data.vertex);
	VertexBuffer::CreateVBO(&m_data, m_shaderProgram);
}

//copied from terrainHeightmap; need to change the structure

void TerrainFaultFormation::GenerateMesh(std::vector<Vector3> p_vertexPoints, std::vector<Vector3> p_texturePoints, std::vector<Vector3> p_normalPoints, vBuffer& p_buffer) {
	const int l_neighbourhood = 8;
	//vertex compression where 1 - all pixels are considered and if step > 1 - only part of pixels
	//should never be 0 or negative
	float step = 1;
	/*
	vector<Vector3> tV;
	vector<Vector3> tN;
	vector<Vector3> tT;
	while(step < 1){
	for(int x=0;x<width-1;x+=1){
	for(int y=0;y<height-1; y+=1){
	int index = width*x+y;
	tV.push_back((p_vertexPoints.at(index) + p_vertexPoints.at(index+1))/2);
	tT.push_back((p_texturePoints.at(index) + p_texturePoints.at(index+1))/2);
	tN.push_back((p_normalPoints.at(index) + p_normalPoints.at(index+1))/2);
	}
	}
	step*=2;
	width*=2;
	height*=2;
	}
	p_vertexPoints = tV;
	p_normalPoints = tN;
	p_texturePoints = tT;*/
	//Moore neighbourhood
	int first[l_neighbourhood][2] = {
		{ -step,0 },{ -step,step },
		{ 0,step },{ step,step },
		{ step,0 },{ step,-step },
		{ 0,-step },{ -step,-step },
	};

	//Sobel neighbourhood
	/*int first[l_neighbourhood][2] = {
	{-step,step},{step,step},
	{step,-step},{-step,-step}
	};*/
	if (!m_gameObject)
		return;
	for (int x = step; x < m_width - step; x += step * 2) {
		for (int y = step; y < m_height - step; y += step * 2) {
			for (int i = 0; i < l_neighbourhood; i += 1) {//local array counter
														  //first point of a triangle
				if (i != l_neighbourhood - 1) {//not the last one in an array
					p_buffer.data.vertex.push_back(p_vertexPoints.at(m_width*(x + first[i + 1][0]) + (y + first[i + 1][1])));
					p_buffer.data.normal.push_back(p_normalPoints.at(m_width*(x + first[i + 1][0]) + (y + first[i + 1][1])));
					p_buffer.data.texture.push_back(p_texturePoints.at(m_width*(x + first[i + 1][0]) + (y + first[i + 1][1])));
				}
				else {//take the first one
					p_buffer.data.vertex.push_back(p_vertexPoints.at(m_width*(x + first[0][0]) + (y + first[0][1])));
					p_buffer.data.texture.push_back(p_texturePoints.at(m_width*(x + first[0][0]) + (y + first[0][1])));
					p_buffer.data.normal.push_back(p_normalPoints.at(m_width*(x + first[0][0]) + (y + first[0][1])));
				}
				//middle point - the center of a l_neighbourhood
				p_buffer.data.vertex.push_back(p_vertexPoints.at(m_width*(x)+(y)));
				p_buffer.data.normal.push_back(p_normalPoints.at(m_width*(x)+(y)));
				p_buffer.data.texture.push_back(p_texturePoints.at(m_width*(x)+(y)));
				//third point
				p_buffer.data.vertex.push_back(p_vertexPoints.at(m_width*(x + first[i][0]) + (y + first[i][1])));
				p_buffer.data.normal.push_back(p_normalPoints.at(m_width*(x + first[i][0]) + (y + first[i][1])));
				p_buffer.data.texture.push_back(p_texturePoints.at(m_width*(x + first[i][0]) + (y + first[i][1])));
			}
		}
	}
}

bool TerrainFaultFormation::SetNoIterations(boost::any p_var) {
	try {
		float l_temp = boost::any_cast<int> (p_var);
		if (l_temp > 0)
			m_iterations = l_temp;
		Generate();
	}
	catch (boost::bad_any_cast& /*bac*/) {
		return false;
	}
	return true;
}

int TerrainFaultFormation::GetNoIterations() {
	return m_iterations;
}

void TerrainFaultFormation::Reflection() {
	m_Variables.push_back("Iterations");
	m_Setters.push_back(boost::bind(&TerrainFaultFormation::SetNoIterations, this, _1));
	m_Getters.push_back(boost::bind(&TerrainFaultFormation::GetNoIterations, this));
}