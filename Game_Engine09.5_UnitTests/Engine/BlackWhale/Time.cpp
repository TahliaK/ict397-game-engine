#include "Time.h"
#include <iostream>
#include <time.h>
using namespace std;

float Time::m_beginFrame = 0;
float Time::m_deltaTime = 0;
float Time::m_endFrame = 0;
float Time::m_fps = 0;
float Time::m_startTime = 0;

float Time::m_fpsSamples[NUM_FPS_SAMPLES] = {0};

int Time::m_frameCount = 0;

void Time::Start(){
	m_startTime = clock();
}

void Time::UpdateTime(bool p_frameStart){
	if(p_frameStart){//start of frame
		m_beginFrame = clock();//current ticks
		m_frameCount++;
	}else{//end of frame
		m_endFrame = clock();
		m_deltaTime = (m_endFrame - m_beginFrame);
	}	
}

float Time::GetFPS(){
	//standard fps
	//m_fps = 1.0 / m_deltaTime * CLOCKS_PER_SEC;

	//average fps 
	/*m_fpsSamples[m_frameCount % NUM_FPS_SAMPLES] = 1.0f / m_deltaTime * CLOCKS_PER_SEC;
	
	m_fps = 0;
	for(int i=0;i<NUM_FPS_SAMPLES;i++){
		m_fps += m_fpsSamples[i];
	}
	m_fps /= NUM_FPS_SAMPLES;*/
	if(m_deltaTime > 0){
		m_fps = CLOCKS_PER_SEC / m_deltaTime;
	}
	return m_fps;
}

float Time::GetDeltaTime(){
	return m_deltaTime;// / CLOCKS_PER_SEC;
}

float Time::GetCurrent(){
	float l_temp = m_beginFrame / CLOCKS_PER_SEC;
	return l_temp;
}