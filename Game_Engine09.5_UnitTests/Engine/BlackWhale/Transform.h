#ifndef TRANSFORM_H
#define TRANSFORM_H
#include "Math_s.h"
#include "Component.h"

/*
* @brief Represents each game object's transformation in 
* the game world.
* Note that every game object should have one transform component.
* @author Olesia Kochergina
* @version 02
* @date 12/03/2017
*/
class Transform : public Component {
public:

	/*
	* @brief Default constructor.
	*/
	Transform();

	/*
	* @brief Copy constructor.
	*/
	Transform(const Transform& original);

	/*
	* @brief Destructor.
	*/
	~Transform();

	/*
	* @brief Assigns the value of the parameter to the current transform.
	* @param original - the original transform
	* @return the current transform.
	*/
	Transform operator = (const Transform& original);

	/*
	* @brief Sets the transform's parent to the parameter's value.
	* @param parent - the new parent of the transform.
	*/
	const void SetParent(Transform* parent);

	/*
	* @brief Returns the parent of the transform.
	* @return pointer to the parent
	*/
	const Transform* GetParent(void) const;

	/*
	* @brief Returns the total amount of children transforms this transform has.
	* @return number of children
	*/
	const int GetChildrenCount(void) const;

	/*
	* @brief Returns the transform's child based on the index.
	* @param index - position of the child.
	* @return pointer to the child.
	*/
	Transform* GetChild(int index) const;

	/*
	* @brief Returns all children of the transform.
	* @return vector with all children transforms.
	*/
	std::vector<Transform*> GetChildren(void) const;

	/*
	* @brief Detaches a child from the list of children.
	* The child is detached only if it can be found in
	* the list.
	* @param child - transform of the child game object.
	*/
	const void DetachChild(Transform* child);

	/*
	* @brief Deletes a child from the list of children.
	* The child game object is completely deleted - its components and its
	* own reference are no longer valid. Trying to access the child or its
	* components is likely to cause errors.
	* @param child - transform of the child game object.
	*/
	const void DeleteChild(Transform* child);

	/*
	* @brief Adds a new child to this tranform.
	* @param child - pointer to the child transform.
	*/
	const void AddChild(Transform* child);

	/*
	* @brief Checks whether a transform is a child of this transform.
	* @param child - pointer to the potential child transform.
	*/
	const bool IsChildOf(const Transform* parent) const;

	///position 
	Vector3 position;

	///scale
	Vector3 scale;

	///rotation in degrees
	Vector3 rotation;
private:

	///parent transform
	Transform* m_parent;

	///vector of all children
	vector<Transform*> m_children;

	///serialization
	friend class boost::serialization::access;
	template<class Archive>
	void save(Archive& ar, const unsigned int /*version*/) const {
		ar & SER_BASE_OBJECT_NVP(Component);

		//ar& m_children;
		/*IFORSI(0, m_children.size()) {
			ar& m_children.at(i)->m_gameObject;
		}*/
		ar& SER_NVP(m_parent);
		ar& SER_NVP(position);
		ar& SER_NVP(rotation);
		ar& SER_NVP(scale);
	}
	template<class Archive>
	void load(Archive& ar, const unsigned int /*version*/) {
		ar & BOOST_SERIALIZATION_BASE_OBJECT_NVP(Component);

		/*IFORSI(0, m_children.size()) {
			ar& m_children.at(i)->m_gameObject;
		}*/
		ar& SER_NVP(m_parent);
		ar& SER_NVP(position);
		ar& SER_NVP(rotation);
		ar& SER_NVP(scale);
	}
	SER_SPLIT_MEMBER()
};
SER_CLASS_VERSION(Transform, 0);
SER_CLASS_EXPORT_KEY(Transform)
#endif TRANSFORM_H
