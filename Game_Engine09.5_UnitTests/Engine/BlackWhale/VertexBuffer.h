/*
* @brief Used for creating vertex buffer objects and rendering graphics, only the first two methods are valid.
* Others are either deprecated or invalid.
* @author Olesia Kochergina
* @version 03
* @date 15/02/2017
*/
#ifndef VERTEXBUFFER_H
#define VERTEXBUFFER_H
#include "Struct.h"
#include <vector>
#define BUFFER_OFFSET(i) ((char*)NULL +(i))
/**
* VBO FORMATS
* (VVVV) (NNNN) (CCCC) - for each batch allocate a separate VBO per vertex attribute
* (VVVVNNNNCCCC) - storing blocks in a batch
* (VNCVNCVNCVNC) - interleaved
*
*/
class VertexBuffer {
public:

	/**
	* @brief Creates a new vertex buffer and adds data to it.
	* Accepts vertex attribute blocks only that are tightly packed -> (VVVVTTTTCCCC) thus
	* the new VBO contains all information.
	* @param info - vertex, normal, texture data
	* @param shaderProgram - ID of the shader program
	*/
	static bool CreateVBO(vBuffer* info, int shaderProgram);

	/**
	* @brief Renders the contents of a vertex buffer object specified by the first parameter.
	* To render all data from the current buffer fIndex needs to be equal to 0 and lIndex has to be
	* the Resize of the buffer. Accepts vertex attribute blocks only that are tightly packed -> (VVVVTTTTCCCC)
	* @param info - contains required information to bind buffers, arrays and calculate the offset
	* @param fIndex - specifies the first vertex to be rendered.
	* @param lIndex - specifies the last vertex to be rendered.
	*
	*/
	static void RenderVBO(const vBuffer* info, int fIndex, int lIndex);

	/**
	* @brief Creates a Vector3 buffer object based on vertices: doesnt work with normals and tex coords
	* @param VBO - Vertex Buffer Object
	* @param dataV - Vertex data
	* @param dataT - Coordinate data
	* @param dataN - SurfaceNormal data
	* @param NoOfCoordinates - 2 - when only x and y are defined and z is 0; 3 - for 3D points
	*/
	static void CreateVBO(unsigned int *VAO, unsigned int *VBO, vector<Vector3> dataV, vector<Vector3> dataT, vector<Vector3> dataN, int NoOfCoordinates, int shaderProgram);
	static void RenderVBO(unsigned int *VAO, unsigned int *VBO, int position, int textureCoord, int normal, float first, float last);



	static void CreateIBO(unsigned int *IBO, unsigned int *VBO, unsigned int *VAO, vector<unsigned int> indices, vector<Vector3> data, char* name, int shaderProgram);
	static void CreateIBO(unsigned int *IBO, unsigned int *VBO, unsigned int *VAO, vector<unsigned int> indices, vector<Vector3> dataV, vector<Vector3> dataT, vector<Vector3> dataN, int shaderProgram);

	static void CreateIBO(iBuffer* info, int shaderProgram);
	static void RenderIBO(iBuffer* info, int fFace, int lFace);
	/**
	* @brief Displays an array of vertices from an Index Buffer Object
	*/
	static void RenderIBO(unsigned int *IBO, unsigned int *VAO, unsigned int *VBO, vector<unsigned int> indices, int position, int textureCoord, int normal, int first, int last, int shaderProgram);


	/**
	* @brief Creates a Vector3 buffer object based on vertices.
	* @param VBO - Vertex Buffer Object
	* @param dataV - Vertex data
	* @param dataT - Coordinate data
	* @param dataN - SurfaceNormal data
	* @param NoOfCoordinates - 2 - when only x and y are defined and z is 0; 3 - for 3D points
	*/
	static void DeprecatedCreateVBO(unsigned int *VBO, vector<Vector3> dataV, vector<Vector3> dataT, vector<Vector3> dataN, int NoOfCoordinates);
	static void DeprecatedRenderVBO(unsigned int *VBO, float Resize);
private:

	///GRAPHICS_STREAM_DRAW, GRAPHICS_STREAM_READ, GRAPHICS_STREAM_COPY, GRAPHICS_STATIC_DRAW, GRAPHICS_STATIC_READ, GRAPHICS_STATIC_COPY, GRAPHICS_DYNAMIC_DRAW, GRAPHICS_DYNAMIC_READ, or GRAPHICS_DYNAMIC_COPY
	static unsigned int s_bufferState;

};
#endif VERTEXBUFFER_H