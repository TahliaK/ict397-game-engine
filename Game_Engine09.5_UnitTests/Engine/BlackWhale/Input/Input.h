#ifndef Input_H
#define Input_H
#include <map>
#include "../Math_s.h"

///available keys
enum KeyCode {
	NONE, BACKSPACE_KEY, DELETE_KEY, TAB_KEY, ESCAPE_KEY, SPACE_KEY,
	A_KEY, B_KEY, C_KEY, D_KEY, E_KEY, F_KEY, G_KEY, H_KEY, I_KEY, J_KEY, K_KEY, L_KEY, M_KEY, N_KEY, O_KEY, P_KEY, Q_KEY, R_KEY, S_KEY, T_KEY, U_KEY, V_KEY, W_KEY, X_KEY, Y_KEY, Z_KEY,
	a_KEY, b_KEY, c_KEY, d_KEY, e_KEY, f_KEY, g_KEY, h_KEY, i_KEY, j_KEY, k_KEY, l_KEY, m_KEY, n_KEY, o_KEY, p_KEY, q_KEY, r_KEY, s_KEY, t_KEY, u_KEY, v_KEY, w_KEY, x_KEY, y_KEY, z_KEY,
	UP_KEY, DOWN_KEY, RIGHT_KEY, LEFT_KEY,
	F1, F2, F3, F4, F5, F6, F7, F8, F9, F10, F11, F12,
	PAGE_DOWN_KEY, PAGE_UP_KEY, INSERT_KEY, ALT_RIGHT_KEY, ALT_LEFT_KEY, SHIFT_RIGHT_KEY, SHIFT_LEFT_KEY, HOME_KEY, END_KEY,
	LEFT_BUTTON, MIDDLE_BUTTON, RIGHT_BUTTON
};

/*
* @brief Allows to read info from the keyboard and mouse every frame. 
* Has a generic structure which allows to easily switch between 
* input types (GL and Windows) by just a changing the class's internal Input variable.
* @author Olesia Kochergina
* @version 02
* @date 19/04/2017
*/
class Input {
public:
	//	enum mouse_up{RIGHT,LEFT,MIDDLE};
	//	enum mouse_down{RIGHT,LEFT,MIDDLE};

	/*
	* @brief Initializes internal variables and should be 
	* called just before the first frame.
	*/
	static void Initialize(void);

	/*
	* @brief Updates internal information and 
	* should be called every frame.
	*/
	static void Update();

	/*
	* @brief Destroys internal dynamic data and should be
	* called just before quitting the program.
	*/
	static void Clear();

	static void SpecialFunction(int keyCode, int width, int height);
	static void SpecialUpFunction(int keyCode, int width, int height);
	static void KeyboardFunction(unsigned char keyCode, int width, int height);
	static void KeyboardUpFunction(unsigned char keyCode, int width, int height);

	static void MouseFunction(int button, int state, int width, int height);
	static void PassiveMotionFunction(int pos_x, int pos_y);
	static void ActiveMotionFunction(int pos_x, int pos_y);

	static void ProcessSpecial(int keyCode, bool value);
	static void ProcessNormal(int keyCode, bool value);
	static Vector2 GetMousePosition();
	static Vector2 GetPrevMousePosition();
	static bool GetKey(KeyCode key);
	static bool GetKeyUp(KeyCode key);
	static bool GetKeyDown(KeyCode key);
protected:

	virtual void InitializeF() = 0;
	virtual void SpecialF(int keyCode, int width, int height) = 0;
	virtual void SpecialUpF(int keyCode, int width, int height) = 0;
	virtual void KeyboardF(unsigned char keyCode, int width, int height) = 0;
	virtual void KeyboardUpF(unsigned char keyCode, int width, int height) = 0;

	virtual void MouseF(int button, int state, int width, int height) = 0;
	virtual void PassiveMotionF(int pos_x, int pos_y) = 0;
	virtual void ActiveMotionF(int pos_x, int pos_y) = 0;

	virtual void ProcessSpecialF(int keyCode, bool value) = 0;
	virtual void ProcessNormalF(int keyCode, bool value) = 0;

	static std::map<int, bool> input;

	///current mouse position
	static Vector2 mouse_position;

	///previous mouse position
	static Vector2 prev_mouse_position;

private:
	//virtual std::map<int,bool> prevInput;

	///Windows or GL
	static Input* s_input;
};
#endif INPUT_H