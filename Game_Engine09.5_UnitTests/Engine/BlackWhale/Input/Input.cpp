#include "Input.h"
#include "../Master.h"
#include "../Engine.h"
#include "Input_GL.h"
#include "Input_Win.h"

Vector2 Input::mouse_position;
Vector2 Input::prev_mouse_position;
Input* Input::s_input = new Input_Win;
map<int,bool> Input::input;

void Input::Clear() {
	input.clear();
	delete s_input;
}

void Input::Initialize() {
	s_input->InitializeF();
}

bool Input::GetKeyDown(KeyCode key) {
	return input.at(key);
}

bool Input::GetKeyUp(KeyCode key) {
	return input.at(key);
}

bool Input::GetKey(KeyCode key) {
	return input.at(key);
}

Vector2 Input::GetMousePosition(){
	return mouse_position;
}
Vector2 Input::GetPrevMousePosition(){
	return prev_mouse_position;
}

void Input::Update(){
	for(int i=0;i<127;i++){
		input.at(i)=false;
	}
}


void Input::SpecialFunction(int p_key, int p_width, int p_height){
	s_input->SpecialF(p_key, p_width, p_height);
}

void Input::SpecialUpFunction(int p_key, int p_width, int p_height){
	s_input->SpecialUpF(p_key, p_width, p_height);
}

void Input::KeyboardFunction(unsigned char p_key, int p_width, int p_height){
	cout << "key0" << endl;
	s_input->KeyboardF(p_key, p_width, p_height);
}

void Input::KeyboardUpFunction(unsigned char p_key, int p_width, int p_height){
	s_input->SpecialF(p_key, p_width, p_height);
}

void Input::MouseFunction(int p_button, int p_state, int p_width, int p_height){
	s_input->MouseF( p_button,  p_state,  p_width,  p_height);
}

void Input::PassiveMotionFunction(int p_posX, int p_posY){
	s_input->PassiveMotionF(p_posX,p_posY);
}

void Input::ActiveMotionFunction(int p_posX, int p_posY){
	s_input->ActiveMotionF(p_posX, p_posY);
}

void Input::ProcessSpecial(int p_keyCode, bool p_value){
	s_input->ProcessSpecialF(p_keyCode, p_value);
}

void Input::ProcessNormal(int p_keyCode, bool p_value){
	s_input->ProcessNormalF(p_keyCode, p_value);
}
