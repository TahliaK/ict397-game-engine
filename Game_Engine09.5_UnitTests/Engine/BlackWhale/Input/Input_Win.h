#ifndef Input_WIN_H
#define Input_WIN_H
#include "Input.h"

/*
* @brief Extends the behaviour of the Input class and processes 
* data from the Windows window callback procedure.
* @author Olesia Kochergina
* @version 02
* @date 19/04/2017
*/
class Input_Win : public Input {
public:
	void InitializeF();

	//	enum mouse_up{RIGHT,LEFT,MIDDLE};
	//	enum mouse_down{RIGHT,LEFT,MIDDLE};

	void SpecialF(int keyCode, int width, int height);
	void SpecialUpF(int keyCode, int width, int height);
	void KeyboardF(unsigned char keyCode, int width, int height);
	void KeyboardUpF(unsigned char keyCode, int width, int height);

	void MouseF(int button, int state, int width, int height);
	void PassiveMotionF(int pos_x, int pos_y);
	void ActiveMotionF(int pos_x, int pos_y) {};

	void ProcessSpecialF(int keyCode, bool value);
	void ProcessNormalF(int keyCode, bool value);
};
#endif Input::WIN_H
