#ifndef Input_GL_H
#define Input_GL_H
#include "Input.h"

/*
* @brief Extends the behaviour of the Input class and processes
* data from the GL callback procedures.
* @author Olesia Kochergina
* @version 02
* @date 19/04/2017
*/
class Input_GL : public Input {
public:
	void InitializeF();

	void SpecialF(int keyCode, int width, int height);
	void SpecialUpF(int keyCode, int width, int height);
	void KeyboardF(unsigned char keyCode, int width, int height);
	void KeyboardUpF(unsigned char keyCode, int width, int height);

	void MouseF(int button, int state, int width, int height);
	void PassiveMotionF(int pos_x, int pos_y);
	void ActiveMotionF(int pos_x, int pos_y) {};

	void ProcessSpecialF(int keyCode, bool value);
	void ProcessNormalF(int keyCode, bool value);
};
#endif Input_GL_H
