#ifndef MODEL_MANAGERR_H
#define MODEL_MANAGER_H
#include <map>

#include "Mesh.h"
#include "OBJ_model.h"
/*
* @class ModelManager
* @brief Manages 3D models
* @author Olesia Kochergina
* @date 18/04/2017
*/
class ModelManager {
public:

	/*
	* @brief Adds a new mesh to the collection of meshes.
	* @param fileName - the file name of the mesh
	* @param shaderProgram - shader program of the model.
	* @return pointer to the newly created mesh or an existing one.
	*/
	static Mesh* AddMesh(std::string fileName, int shaderProgram);

	/*
	* @brief Searches for a mesh by its file name and returns a pointer to it.
	* @param fileName - file name of the mesh
	* @return pointer to the mesh or NULL.
	*/
	static Mesh* GetMesh(std::string fileName);

	/*
	* @brief Deletes all 3D meshes.
	*/
	static void Clear(void);
private:

	/*
	* @brief Loads a single OBJ model.
	* @param model - reference to the model
	* @param fileName - file name of the model.
	* @return true  - loaded
	*/
	static bool LoadOBJFile(OBJ_model& model, std::string filename);

	///map for file names to meshes
	static std::map<std::string, Mesh*> s_collection;
};

#endif MODEL_MANAGER_H