#include "ColliderAABB.h"
#include "CollisionEngine.h"
BOOST_CLASS_EXPORT_IMPLEMENT(ColliderAABB)
bool ColliderAABB::s_inList = false;

ColliderAABB::ColliderAABB(): Collider() {
	Reflection();
	Initialize();
	//m_tree.Insert(AABB(((ColliderAABB*)m_minBox)->GetMin(), ((ColliderAABB*)m_minBox)->GetMax()),m_LLPtr);
	if (!s_inList) {
		s_inList = true;
		ColliderAABB* l_pointer = new ColliderAABB;
		m_derivedClasses.push_back(l_pointer);
	}
}

void ColliderAABB::Initialize(void) {
	m_LLPtr = NULL;
	//	m_minBox = new ColliderAABB;
	m_name = "AABB Collider";
}

Component* ColliderAABB::NewComponent() const {
	Component* l_pointer = new ColliderAABB;
	//assumed that the component will definitely have a GO
	CollisionEngine::AddCollider((ColliderAABB*)l_pointer);
	return l_pointer;
}

ColliderAABB::ColliderAABB(const ColliderAABB& original): Collider(original) {
	Reflection();
	Initialize();
	m_min = original.m_min;
	m_max = original.m_max;
}

ColliderAABB::ColliderAABB(const Vector3 min, const Vector3 max): Collider() {
	Reflection();
	Initialize();
	m_min = min;
	m_max = max;
	//CalculateCenter();
	SetMinAABB(this);
	if(this->m_gameObject)
		CollisionEngine::AddCollider(this);
	if (!s_inList) {
		s_inList = true;
		ColliderAABB* l_pointer = new ColliderAABB;
		m_derivedClasses.push_back(l_pointer);
	}
}

ColliderAABB::~ColliderAABB() {
	CollisionEngine::RemoveCollider(this);
	if (m_LLPtr) {
		delete m_LLPtr;
		m_LLPtr = NULL;
	}
}

DataStructures::Node<ColliderAABB*>*& ColliderAABB::GetLinkedListPointer(void){
	return m_LLPtr;
}

ColliderAABB* ColliderAABB::operator = (const ColliderAABB& original) {
	m_min = original.m_min;
	m_max = original.m_max;
	m_gameObject = original.m_gameObject;
	//CalculateCenter();
	return this;
}

bool ColliderAABB::operator == (const ColliderAABB& original) {
	return (m_min == original.m_min && m_max == original.m_max);
}

bool ColliderAABB::Contains(const ColliderAABB* box) const{
	return (m_min.x <= box->m_min.x && m_min.y <= box->m_min.y &&
		m_min.z <= box->m_min.z && m_max.x >= box->m_max.x &&
		m_max.y >= box->m_max.y && m_max.z >= box->m_max.z);
}

bool ColliderAABB::Intersect(const ColliderAABB* box) const{
	if (m_max.x < box->m_min.x || m_min.x > box->m_max.x)
		return false;
	if (m_max.y < box->m_min.y || m_min.y > box->m_max.y)
		return false;
	if (m_max.z < box->m_min.z || m_min.z > box->m_max.z)
		return false;
	return true;
}

void ColliderAABB::Update() {
	Vector3 l_change = m_gameObject->GetTransform()->position - m_pTransform.position;
	if (l_change.x != 0 || l_change.y != 0 || l_change.z != 0) {
		//Vector3 curMin = ((ColliderAABB*)m_minBox)->GetMin();
		//Vector3 curMax = ((ColliderAABB*)m_minBox)->GetMax();
		//((ColliderAABB*)m_minBox)->SetMin(curMin + l_change);
		m_min = m_min + l_change;
		m_max = m_max + l_change;
		//((ColliderAABB*)m_minBox)->SetMax(curMax + l_change);
		//AABB l_box = AABB(((ColliderAABB*)m_minBox)->GetMin(), ((ColliderAABB*)m_minBox)->GetMax());
		CollisionEngine::AddCollider(this);
	}

	//only dynamic objects can have collision responses atm
	if (!m_gameObject->GetStatic()) {		
		vector<GameObject*> t = CollisionEngine::Intersect(this);
		//should be replaced with a function to response to collisions
		for (int i = 0; i<t.size(); i++) {
			//cout << t.at(i)->GetName() << " ";
		}
	}
	else {

	}
	m_pTransform = *m_gameObject->GetTransform();
}

Vector3 ColliderAABB::Center() const {
	Vector3 center = center = Vector3((m_min.x + m_max.x) / 2, (m_min.y + m_max.y) / 2, (m_min.z + m_max.z) / 2);
	return center;
}
bool ColliderAABB::SetMin(boost::any var) {
	try {
		m_min = boost::any_cast<Vector3> (var);
	}
	catch (boost::bad_any_cast& /*bac*/) {
		return false;
	}
	return true;
}

bool ColliderAABB::SetMax(boost::any var) {
	try {
		m_max = boost::any_cast<Vector3> (var);
	}
	catch (boost::bad_any_cast& /*bac*/) {
		return false;
	}
	return true;
}

const Vector3 ColliderAABB::GetMin(void)const {
	return m_min;
}
const Vector3 ColliderAABB::GetMax(void)const {
	return m_max;
}

ColliderAABB* ColliderAABB::FindMinAABB(const std::vector<Vector3>& p_points) {
	ColliderAABB* tempBox = new ColliderAABB;
	tempBox->m_min = MINIMUM_INIT_INT;//reset 
	tempBox->m_max = MAXIMUM_INIT_INT;

	for (unsigned j = 0; j<p_points.size(); j++) {
		for (unsigned i = 0; i<3; i++) {
			if (tempBox->m_min[i] > p_points[j][i])
				tempBox->m_min.Set(i, p_points[j][i]);
			if (tempBox->m_max[i] < p_points[j][i])
				tempBox->m_max.Set(i, p_points[j][i]);
		}
	}
	int counter = 0;
	for (int i = 0; i<3; i++) {
		if (tempBox->m_min[i] > tempBox->m_max[i])
			counter++;
	}
	if (counter == 3) {
		Vector3 minimum = tempBox->m_max;
		tempBox->m_max = tempBox->m_min;
		tempBox->m_min = tempBox->m_max;
	}
	return tempBox;
}

void ColliderAABB::Reflection() {
	m_Variables.push_back("Minimum");
	m_Setters.push_back(boost::bind(&ColliderAABB::SetMin, this, _1));
	m_Getters.push_back(boost::bind(&ColliderAABB::GetMin, this));

	m_Variables.push_back("Maximum");
	m_Setters.push_back(boost::bind(&ColliderAABB::SetMax, this, _1));
	m_Getters.push_back(boost::bind(&ColliderAABB::GetMax, this));
}