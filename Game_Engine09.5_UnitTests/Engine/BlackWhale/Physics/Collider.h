/*
* @author Olesia Kochergina
* @version 01
* @date 17/03/2017
*/
#ifndef COLLIDER_H
#define COLLIDER_H
#include "../Component.h"
#include "../Math_s.h"
#include "../Transform.h"

/*
* @class Collider
* @brief Base class for all colliders
* @version 01
* @date 03/04/2017
* @author Olesia Kochergina
*/
class Collider: public Component {
public:

	/*
	* @brief Default c-tor.
	*/
	Collider();

	/*
	* @brief Copy c-tor.
	*/
	Collider(const Collider& original);

	/*
	* @brief Destructor.
	*/
	~Collider();
	
	/**
	* @brief The method is called when this object is being created.
	*/
	virtual void Initialize();

	/**
	* @brief The method is called when the script
	* is enabled just before the Update method.
	*/
	virtual void Start();

	/**
	* @brief The method is called every frame.
	*/
	virtual void Update();

	/**
	* @brief The method is called before destroying the script.
	*/
	virtual void Clear();

	/*
	* @brief Creates a new component of this class.
	* @return Pointer to the new component or NULL.
	*/
	virtual Component* NewComponent(void) const = 0;

	virtual void SetMinAABB(Collider* box);
	virtual const Collider* GetMinAABB();
	
	virtual Collider* operator = (const Collider* /*original*/) { return this; };
	virtual bool operator == (const Collider* /*original*/) { return false; }
	//static void SetTreeAABB(AABB gridAABB);
	bool SetLayer(boost::any layer);
	int GetLayer(void)const;
protected:
	///previous transform
	Transform m_pTransform;

	///ColliderAABB which stores the minimum bounding box
	//Collider* m_minBox;
private:
	void Reflection();

	///serialization
	friend class boost::serialization::access;
	template<class Archive>
	void save(Archive& ar, const unsigned int /*version*/) const {
		ar & SER_BASE_OBJECT_NVP(Component);
	}
	template<class Archive>
	void load(Archive& ar, const unsigned int /*version*/) {
		ar & SER_BASE_OBJECT_NVP(Component);
	}
	SER_SPLIT_MEMBER()
};

SER_CLASS_VERSION(Collider, 0);
SER_ABSTRACT(Collider)
SER_CLASS_EXPORT_KEY(Collider)
#endif COLLIDER_H