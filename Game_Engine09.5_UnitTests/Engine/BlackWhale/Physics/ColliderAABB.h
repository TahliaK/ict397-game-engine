/*
* @author Olesia Kochergina
* @version 01
* @date 17/03/2017
*/
#ifndef COLLIDERAABB_H
#define COLLIDERAABB_H
#include "Collider.h"
#include "LinkedList.h"
class ColliderAABB: public Collider{
public:
	ColliderAABB();
	/**
	* @brief A constructor to set min and max points.
	* @param min - the minimum point of the box
	* @param max - the maximum point of the box
	*/
	ColliderAABB(const Vector3 min, const Vector3 max);
	ColliderAABB(const ColliderAABB& original);
	void Initialize();
	void Update();
	~ColliderAABB();
	bool SetMin(boost::any minimum);
	bool SetMax(boost::any maximum);
	const Vector3 GetMin(void)const;
	const Vector3 GetMax(void)const;
	ColliderAABB* operator = (const ColliderAABB& original);
	bool operator == (const ColliderAABB& original);
	Component* NewComponent(void) const;
	static ColliderAABB* FindMinAABB(const std::vector<Vector3>& points);

	bool Contains(const ColliderAABB* collider) const;
	bool Intersect(const ColliderAABB* collider) const;
	
	Vector3 Center() const;
	DataStructures::Node<ColliderAABB*>*& GetLinkedListPointer(void);
private:
	static bool s_inList;

	///minimum 3D point of the box
	Vector3 m_min;

	///maximum 3D point of the box
	Vector3 m_max;

	///pointer to a node which holds this collider
	DataStructures::Node<ColliderAABB*>* m_LLPtr;
	void Reflection();
	///serialization
	friend class boost::serialization::access;
	template<class Archive>
	void save(Archive& ar, const unsigned int /*version*/) const {
		ar & SER_BASE_OBJECT_NVP(Collider);
		ar & SER_NVP(m_min);
		ar & SER_NVP(m_max);
	}
	template<class Archive>
	void load(Archive& ar, const unsigned int /*version*/) {
		ar & SER_BASE_OBJECT_NVP(Collider);
		ar & SER_NVP(m_min);
		ar & SER_NVP(m_max);
	}
	SER_SPLIT_MEMBER()
};
SER_CLASS_VERSION(ColliderAABB, 0);
SER_CLASS_EXPORT_KEY(ColliderAABB)
#endif // !COLLIDERAABB_H
