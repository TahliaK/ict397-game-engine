#include "CollisionEngine.h"
ColliderAABB CollisionEngine::m_gridCollider(Vector3(-100, -100, -100), Vector3(100, 100, 100));
DataStructures::Quadtree<ColliderAABB*> CollisionEngine::m_tree(CollisionEngine::m_gridCollider);

void CollisionEngine::AddCollider(ColliderAABB* p_min) {
	RemoveCollider(p_min);
	m_tree.Insert(p_min, p_min->GetLinkedListPointer());
}

void CollisionEngine::RemoveCollider(ColliderAABB* p_min) {
	if (p_min->GetLinkedListPointer()) {
		m_tree.Delete(p_min->GetLinkedListPointer());
		p_min->GetLinkedListPointer() = NULL;
	}
}

std::vector<GameObject*> CollisionEngine::Intersect(ColliderAABB* p_collider) {
	vector<ColliderAABB*> l_range = m_tree.QueryRange(*p_collider);
	vector<GameObject*> l_go;
	for (int i = 0; i < l_range.size(); i++) {
		//if attached to different game objects
		if (l_range.at(i)->GetGameObject() != p_collider->GetGameObject()) {
			//check for collision
			if(p_collider->Intersect(l_range.at(i)))
				l_go.push_back(l_range.at(i)->GetGameObject());
			
		}
	}
	return l_go;
}