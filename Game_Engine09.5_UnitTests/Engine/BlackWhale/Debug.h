#ifndef DEBUG_H
#define DEBUG_H
class Debug{
public:
	static void RunGeneral();
	static bool RunFramebuffer();
};
#endif DEBUG_H