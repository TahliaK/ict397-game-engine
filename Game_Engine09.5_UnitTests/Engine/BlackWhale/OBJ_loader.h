#ifndef OBJ_LOADER_H
#define OBJ_LOADER_H

#include <string>
#include "OBJ_model.h"

/*
* @brief Reads .obj files and .mtl files to create 3d models.
* @author Olesia Kochergina
* @version 02
* @date 13/03/2017
*/
class OBJ_loader
{
public:

	/**
	* @brief Default constructor.
	*/
	OBJ_loader(void);

	/**
	* @brief Deconstructor
	*/
	~OBJ_loader(void);

	/**
	* @brief Processes the matlib (Material library) for for the obj
	* @param file - the directory and file (including .mtl) of the target Material library
	* @return true if the Material library has been fully processed
	*/
	bool ParseMatLib(const std::string file);


	/**
	* @brief Returns an OBJ model being read
	* @return pointer to the current OBJ model
	*/
	OBJ_model* GetCurrentModel(void) const;

	/**
	* @brief Sets the parameter's value to the current
	* OBJ model in this object.
	* @param pointer to the new OBJ model to be read
	*/
	void SetCurrentModel(OBJ_model* model);

	/*
	* @brief Reads a single number from a stream based on a delimiter.
	* @param stream - string stream containing part of a line from either .obj or .mtl
	* @param delim - separator between numbers.
	* @return the number
	*/
	float ReadNumber(std::stringstream& stream, const char delim) const;

	/*
	* @brief Returns the minimum AABB box of this instance.
	* @return This instance's bounding box
	*/
	// AABB& GetBoundingBox();

private:
	///Reference to the current OBJ model being read
	OBJ_model* m_currentModel;

	///saves the min and max points of this instance to use them for creating a minimum bounding box
	//AABB minBB;
};

/// Reads a files contents and converts into the triangles, textures, faces, etc of an object
std::istream & operator >>(std::istream & input, OBJ_loader &self);
#endif
