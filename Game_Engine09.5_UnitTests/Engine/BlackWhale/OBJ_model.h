#ifndef OBJ_MODEL_H
#define OBJ_MODEL_H
#include <map>

#include "Struct.h"
#include "Material_s.h"
#include "Mesh.h"

/**
* @class OBJ_model
* @brief Represents a single .obj model.
* @date 10/02/2017
* @author Olesia Kochergina
* @version 02
*/
class OBJ_model: public Mesh{
public:

	/**
	* @brief Default constructor to initialize member variables.
	*/
	OBJ_model(unsigned int shader);

	/**
	* @brief Copy constructor.
	*/
	OBJ_model(const OBJ_model& original);

	/**
	* @brief Deletes the buffers when the object is destroyed.
	*/
	~OBJ_model();

	/**
	* @brief Display this model.
	*/
	const void Draw();

	/**
	* @brief Adds the parameter to a list of texture coordinates' vertices.
	* @param v - a single 3D point which defines a texture coordinate Vector3
	*/
	void AddVertexT(Vector3 v);

	/**
	* @brief Adds the parameter to a list of geometric vertices.
	* @param v - a single 3D point which defines a geometric Vector3
	*/
	void AddVertexV(Vector3 v);

	/**
	* @brief Adds the parameter to a list of normal vertices.
	* @param v - a signle 3D point which  defines a normal Vector3
	*/
	void AddVertexN(Vector3 v);

	/**
	* @brief Adds the parameter to a list of geometric faces.
	* @param f - holds three values: x,y,z
	*/
	void AddVFace(Vector3 f);

	/**
	* @brief Adds the parameter to a list of texture faces.
	* @param f - triangular face element
	*/
	void AddTFace(Vector3 f);

	/**
	* @brief Adds the parameter's value to a list of normal faces.
	* @param f - one face element
	*/
	void AddNFace(Vector3 f);

	/**
	* @brief Adds a matF object to the matkey data structure 
	* @param m - the matF holds the name of the Material, its assocated first and last face and the Vector3 buffer object which is outputed from that information
	*/
	void AddMatKey(matF& m);

	/**
	* @brief adds a Material to the Material library with the key being set to the name of that Material
	* @param m - the Material to be added
	*/
	void AddMat(Material& m);

	/*
	* @brief Returns all vertices.
	* @return all points, texture coordinates and normals of the model.
	*/
	MeshData* GetVertices(void);

	/*
	* @brief Returns all faces.
	* @return faces of the model.
	*/
	MeshData* GetFaces(void);

	/*
	* @brief Returns all materials of the model.
	* @return map with materials 
	*/
	std::map<std::string, Material>* GetMaterialLibrary(void);

	/*
	* @brief Returns all material keys
	* @return vector of keys
	*/
	std::vector<matF>* GetMaterialKeys(); 

	/**
	* @brief Generates a buffer depending on the amount of faces. 
	*/
	void GenerateVBO();

	/*
	* @brief Adds vertices based on a materual key to produce a mesh later on.
	* @param mat - specific material key. 
	*/
	void AssignVertices(matF* mat);

private:

	/*
	* @brief Sends material information to the model's shader. 
	* @param index - index of the material
	*/
	const void SetMaterial(unsigned index) const;
		
	///storage of the model's data
	vBuffer m_data;

	//iBuffer m_iData;

	///Stores 3 vectors with vertex data
	MeshData m_vertInfo;

	///Stores 3 vectors with faces data
	MeshData m_faceInfo;

	/// Map holding information for the Materials of an object
	std::map<std::string, Material> m_matlib;

	/// Holds the first and last faces which a particular Material apply to
	std::vector<matF> m_matKey; 
};
#endif OBJ_MODEL_H