/**
* @brief NOT USED
* @author Olesia Kochergina
* @date 05/03/2017
* 
*/

#ifndef RENDERER_H
#define RENDERER_H

#include "Component.h"
#include "Math_s.h"
#include "Material_s.h"

class Renderer: public Component{

public:
	Renderer();
	~Renderer();
	const bool GetIsEnabled(void) const;
	const bool GetIsVisible(void) const;
	const bool GetReceiveShadows(void) const;
	const bool GetCastShadows(void) const;
	const std::vector<Material> GetMaterials(void) const;

	/**
	* @param bool
	*/
	const bool SetIsEnabled(boost::any enabled);

	/**
	* @param bool
	*/
	const bool SetIsVisible(boost::any visible);

	/**
	* @param bool
	*/
	const bool SetReceiveShadows(boost::any receive);

	/**
	* @param bool
	*/
	const bool SetCastShadows(boost::any cast);

	/**
	* @param std::vector<Material>
	*/
	const bool SetMaterials(boost::any materials);

	/**
	* @param Material
	*/
	const bool AddMaterial(boost::any material);

private:
	//AABB bounds;

	///makes the game object appear if enabled
	bool m_enabled;

	///true - visible to at least one camera
	bool m_visible;

	///true - the object can receive shadows
	bool m_receiveShadows;

	///true - the object can cast shadows
	bool m_castShadows;

	///all materials of this object
	std::vector<Material> m_materials;
	void Reflection(void);
};
#endif RENDERER_H