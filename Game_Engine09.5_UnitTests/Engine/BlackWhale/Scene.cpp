#include "Scene.h"
#include "Input\Input.h"
#include "Master.h"
#include "MeshRenderer.h"
#include "Terrain\TerrainHeightmap.h"
#include "Water.h"
#include "Light.h"
#include "Physics\ColliderAABB.h"
#include "Physics\CollisionEngine.h"
#include "Engine.h"
#include "ShaderManager.h"
using namespace SceneManagement;
BOOST_CLASS_EXPORT_IMPLEMENT(SceneManagement::Scene)

Scene::Scene() : InObject() {
	Initialize();
}

Scene::~Scene() {
	Clear();
}

void Scene::DeleteGameObject(unsigned int p_index) {
	if (p_index < m_gameObjects.size()) {
		IFORSI(0, m_gameObjects.at(p_index)->GetComponents().size()) {
			if (typeid(*m_gameObjects.at(p_index)->GetComponents().at(i)) == typeid(Camera)) {
				if (m_cameraIndex[0] == p_index && m_cameraIndex[1] == i) {
					m_cameraIndex[0] = -1;
					m_cameraIndex[1] = -1;
					break;
				}
			}
			if (typeid(*m_gameObjects.at(p_index)->GetComponents().at(i)) == typeid(Light)) {
				if (m_lightIndex[0] == p_index && m_lightIndex[1] == i) {
					m_lightIndex[0] = -1;
					m_lightIndex[1] = -1;
					break;
				}
			}
		}
		delete m_gameObjects.at(p_index);
		m_gameObjects.erase(m_gameObjects.begin() + p_index);
	}
}

bool Scene::AddComponent(GameObject* p_go, Component* p_comp) {
	if (!p_go || !p_comp)
		return false;
	if (m_cameraIndex[0] == -1) {
		if (typeid(*p_comp) == typeid(Camera)) {
			IFORSI(0, m_gameObjects.size()) {
				if (m_gameObjects.at(i) == p_go) {
					if (m_gameObjects.at(i)->AddComponent(p_comp)) {
						m_cameraIndex[0] = i;
						m_cameraIndex[1] = m_gameObjects.at(i)->GetComponents().size() - 1;
						((Camera*)m_gameObjects.back())->SetLockUp(true);
						return true;
					}
					else {
						delete p_comp;
						return false;
					}
				}
			}
		}
	}
	else if (m_lightIndex[0] == -1) {
		IFORSI(0, m_gameObjects.size()) {
			if (m_gameObjects.at(i) == p_go) {
				if (m_gameObjects.at(i)->AddComponent(p_comp)) {
					m_lightIndex[0] = i;
					m_lightIndex[1] = m_gameObjects.at(i)->GetComponents().size() - 1;
					return true;
				}
				else {
					delete p_comp;
					return false;
				}
			}
		}
	}
	else {
		if (p_go->AddComponent(p_comp))
			return true;
		else {
			delete p_comp;
			return false;
		}
	}
	delete p_comp;
	return false;
}

void Scene::Initialize() {
	m_cameraIndex[0] = -1;
	m_lightIndex[0] = -1;
	m_cameraIndex[1] = -1;
	m_lightIndex[1] = -1;

	m_index = 0;
	m_modified = 0;
	m_loaded = 1;
	m_name = "Level";
	m_path = "/";
}

void Scene::Clear() {
	IFORSI(0, m_gameObjects.size()) {
		if (m_gameObjects.at(i)) {
			delete m_gameObjects.at(i);
			m_gameObjects.at(i) = NULL;
		}
	}
	m_gameObjects.clear();
}

void Scene::Reshape() {

}

void Scene::Start() {
	IFORSI(0, m_gameObjects.size()) {
		JFORSI(0, m_gameObjects.at(i)->GetComponents().size()) {
			if (typeid(*m_gameObjects.at(i)->GetComponents().at(j)) == typeid(Camera)) {
				m_cameraIndex[0] = i;
				m_cameraIndex[1] = j;
				break;
			}
		}
	}
	if (m_cameraIndex[0] != -1)
		((Camera*)m_gameObjects.at(m_cameraIndex[0])->GetComponents().at(m_cameraIndex[1]))->SetLockUp(true);

	/*m_gameObjects.push_back(new GameObject);
	m_gameObjects.back()->AddComponent(new ColliderAABB(Vector3(0, 0, 0), Vector3(20, 20, 20)));
	m_gameObjects.push_back(new GameObject);
	m_gameObjects.back()->AddComponent(new ColliderAABB(Vector3(50, 10, 10), Vector3(60, 20, 20)));
	m_gameObjects.push_back(new GameObject);
	m_gameObjects.back()->AddComponent(new ColliderAABB(Vector3(-10, 0, 0), Vector3(20, 20, 20)));
	*/
	IFORSI(0, m_gameObjects.size()) {
		m_gameObjects.at(i)->Start();
	}
	GraphicsEngine::Reshape();
}

Camera* Scene::GetMainCamera() {
	if (m_cameraIndex[0] == -1) {
		//create warning message
		return Engine::GetMainCamera();
	}
	else {
		return ((Camera*)m_gameObjects.at(m_cameraIndex[0])->GetComponents().at(m_cameraIndex[1]));
	}
}

Light* Scene::GetMainLight() {
	if (m_lightIndex[0] == -1) {
		//create a warning message
		return Engine::GetMainLight();
	}
	else {
		return ((Light*)m_gameObjects.at(m_lightIndex[0])->GetComponents().at(m_lightIndex[1]));
	}
}

std::vector<GameObject*>& Scene::GetRootGameObjects() {
	return m_gameObjects;
}
int Scene::GetID() const {
	return m_index;
}

void Scene::SetID(const int index) {
	m_index = index;
}

void Scene::Update() {
	Master::gl.UseProgram(0);

	if (GetMainCamera() && GetMainCamera()->GetGameObject()) {
		Vector3 l_position(0);
		IFORSI(0, m_gameObjects.size()) {
			JFORSI(0, m_gameObjects.at(i)->GetComponents().size()) {
				if (typeid(*m_gameObjects.at(i)->GetComponents().at(j)) == typeid(TerrainHeightmap)) {
					l_position = ((TerrainHeightmap*)m_gameObjects.at(i)->GetComponents().at(j))->
						GetHeight(GetMainCamera()->GetGameObject()->GetTransform()->position.x, GetMainCamera()->GetGameObject()->GetTransform()->position.z);

				}
			}
		}
		GetMainCamera()->GetGameObject()->GetTransform()->position.y = l_position.y + 3;
		//Collider
		//CollisionEngine::Intersect(GetMainCamera()->GetPosition());

		//should not be a part of the engine
		//should be defined using lua scripts
		Input();

		Master::gl.Enable(GRAPHICS_CULL_FACE);
		Deferred();
	}
}

void Scene::Deferred(void) {
	GraphicsEngine::SetDeferredGeometry();

	//[0] - transform 
	//TransformedVector = TranslationMatrix * RotationMatrix * ScaleMatrix * OriginalVector;
	int startIndex = 1;
	ShaderManager::ActivateShaderProgram(30);
	IFORSI(0, m_gameObjects.size()) {
		if (m_gameObjects.at(i)->GetLayer() != 1) {
			m_gameObjects.at(i)->Update();
		}
	}
	//m_tree.DrawAABB(m_tree.GetAABB());
	int lightCount = 1;

	//ShaderManager::ActivateShaderProgram(s_lightShader);
	GraphicsEngine::SetDeferredLight();
	Master::s_LightPassShader.Activate();
	//sends data about all lights at the same time
	Light::UpdateLight();
	GraphicsEngine::ResetDeferred();

	//renders GUI
	IFORSI(0, m_gameObjects.size()) {
		if (m_gameObjects.at(i)->GetLayer() == 1) {
			m_gameObjects.at(i)->Update();
		}
	}
}


void Scene::Input() {
	//cout << Input::GetMousePosition() << endl;
	/*if (Input::GetKey(KeyCode::ESCAPE_KEY))
		exit(0);*/
	int cX = (Master::s_screen[2] + Master::s_screen[0] * 2) / 2;
	int cY = (Master::s_screen[3] + Master::s_screen[1] * 2) / 2;
	//cout<<Input::mouse_position[0]<<" "<<Input::mouse_position[1]<<endl;
	if ((Input::GetMousePosition()[0] != Input::GetPrevMousePosition()[0] || Input::GetMousePosition()[1] != Input::GetPrevMousePosition()[1]) && Input::GetKey(KeyCode::LEFT_BUTTON)) {
		//glutWarpPointer(cX, cY); 
		//glutWarpPointer((GetMainCamera()->GetWindowSize()[0] + GetMainCamera()->GetWindowSize()[2])/2,(GetMainCamera()->GetWindowSize()[1] + GetMainCamera()->GetWindowSize()[3])/2); 
		Vector2 temp = Input::GetMousePosition() - Input::GetPrevMousePosition();
		((Camera*)GetMainCamera())->Rotate(Vector3(temp.y / 100 * Master::s_screen[2] / Master::s_screen[3], temp.x / -100, 0));
	}
	float speed = 0.5f;
	float rotSpeed = 0.05f;
	if (Input::GetKey(KeyCode::W_KEY) || Input::GetKey(KeyCode::w_KEY)) {
		((Camera*)GetMainCamera())->Move(Vector3(-speed, 0, 0));
	}
	if (Input::GetKey(KeyCode::S_KEY) || Input::GetKey(KeyCode::s_KEY)) {
		((Camera*)GetMainCamera())->Move(Vector3(speed, 0, 0));
	}
	if (Input::GetKey(KeyCode::A_KEY) || Input::GetKey(KeyCode::a_KEY)) {
		((Camera*)GetMainCamera())->Rotate(Vector3(0, rotSpeed, 0));
	}
	if (Input::GetKey(KeyCode::D_KEY) || Input::GetKey(KeyCode::d_KEY)) {
		((Camera*)GetMainCamera())->Rotate(Vector3(0, -rotSpeed, 0));
	}
	if (Input::GetKey(KeyCode::Q_KEY) || Input::GetKey(KeyCode::q_KEY)) {
		((Camera*)GetMainCamera())->Move(Vector3(0, speed, 0));
	}
	if (Input::GetKey(KeyCode::E_KEY) || Input::GetKey(KeyCode::e_KEY)) {
		((Camera*)GetMainCamera())->Move(Vector3(0, -speed, 0));
	}
	if (Input::GetKey(KeyCode::Z_KEY) || Input::GetKey(KeyCode::z_KEY)) {
		((Camera*)GetMainCamera())->Rotate(Vector3(speed, 0, 0));
	}
	if (Input::GetKey(KeyCode::X_KEY) || Input::GetKey(KeyCode::x_KEY)) {
		((Camera*)GetMainCamera())->Rotate(Vector3(-speed, 0, 0));
	}
	if (Input::GetKey(KeyCode::SHIFT_LEFT_KEY)) {
		((Camera*)GetMainCamera())->InvertPitch();
	}
}