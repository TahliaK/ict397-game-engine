#ifndef SOUND_H
#define SOUND_H
#include "GameObject.h"
class Sound: public GameObject{
public:
	Sound();
	~Sound();
	void Initialize();
	void Start();
	void Update();
	void Clear();
private:
};
#endif