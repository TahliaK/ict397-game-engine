#include "Shader.h"
#include <iostream>
#include "Master.h"
#include <GL\glew.h>
Shader::Shader(){
	m_shader=ERROR_STATE;
	m_compiled=false;
}

Shader::~Shader(){
	if(m_shader!=ERROR_STATE)
		Master::gl.DeleteShader(m_shader);
}

bool Shader::CreateShader(unsigned int shaderType){
	if(shaderType == GL_FRAGMENT_SHADER || shaderType == GL_VERTEX_SHADER || shaderType == GL_GEOMETRY_SHADER){
		m_shader = Master::gl.CreateShader(shaderType);
		if(m_shader!=ERROR_STATE)
			return true;
	}
	return false;
}
void Shader::AttachSource(int numOfStrings, const char **strings, int *lenOfStrings){
	m_compiled = GL_FALSE;
	if(m_shader!=ERROR_STATE){
		Master::gl.ShaderSource(m_shader,numOfStrings,strings,lenOfStrings);
	}
}

void Shader::CompileShader(){
	if(m_shader!=ERROR_STATE){
		Master::gl.CompileShader(m_shader);
		Master::gl.GetShaderiv(m_shader, GL_COMPILE_STATUS,&m_compiled);
		return;
	}
	m_compiled = GL_FALSE;
}

unsigned int Shader::GetShader(){
	return m_shader;
}

bool Shader::IsCompiled(){
	return (bool)m_compiled;
}

void Shader::PrintInfoLog(){
	std::cout<<Master::gl.GetShaderInfoLog(m_shader)<<std::endl;
}