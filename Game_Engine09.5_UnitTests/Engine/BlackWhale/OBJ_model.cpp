#include "OBJ_model.h"
#include "GraphicsEngine.h"
#include "VertexBuffer.h"
#include "Master.h"
#include <string>
#include <vector>
#include "ShaderManager.h"

OBJ_model::OBJ_model(unsigned int p_program) : Mesh(p_program) {
	
}

OBJ_model::~OBJ_model() {
	m_matlib.clear();
	m_matKey.clear();
	m_vertInfo.Clear();
	m_faceInfo.Clear();
	m_data.Clear();
//	m_iData.vertexBuffer.Clear();
}

OBJ_model::OBJ_model(const OBJ_model& p_original): Mesh(p_original.m_shaderProgram) {
	int j = 0;
	while (true)
	{
		if (p_original.m_matKey.size() == j) {
			break;
		}
		Vector3 temp;
		for (int i = 0; i < p_original.m_vertInfo.vertex.size(); i++) {
			for (int p = 0; p < 3; p++)
				temp.Set(p, p_original.m_vertInfo.vertex.at(i)[p]);
			AddVertexV(temp);
		}
		for (int i = 0; i < p_original.m_vertInfo.texture.size(); i++) {
			for (int p = 0; p < 3; p++)
				temp.Set(p, p_original.m_vertInfo.texture.at(i)[p]);
			AddVertexT(temp);
		}
		for (int i = 0; i < p_original.m_vertInfo.normal.size(); i++) {
			for (int p = 0; p < 3; p++)
				temp.Set(p, p_original.m_vertInfo.normal.at(i)[p]);
			AddVertexN(temp);
		}
		for (int i = 0; i < p_original.m_faceInfo.vertex.size(); i++) {
			for (int p = 0; p < 3; p++)
				temp.Set(p, p_original.m_faceInfo.texture.at(i)[p]);
			AddTFace(temp);
			for (int p = 0; p < 3; p++)
				temp.Set(p, p_original.m_faceInfo.vertex.at(i)[p]);
			AddVFace(temp);
			for (int p = 0; p < 3; p++)
				temp.Set(p, p_original.m_faceInfo.normal.at(i)[p]);
			AddNFace(temp);
		}
		Material newMat1;
		newMat1.Copy(p_original.m_matlib.at(p_original.m_matKey.at(j).mtlname));
		AddMat(newMat1);

		matF tempM;
		tempM.fFace = p_original.m_matKey.at(j).fFace;
		tempM.lFace = p_original.m_matKey.at(j).lFace;
		tempM.mtlname = p_original.m_matKey.at(j).mtlname;
		AssignVertices(&tempM);
		AddMatKey(tempM);
		j++;
	}
	if (j != 0)
		GenerateVBO();
	/*minBB.min = original.minBB.min;
	minBB.max = original.minBB.max;
	minBB.layer = original.minBB.layer;*/
}

MeshData* OBJ_model::GetVertices(){
	return &m_vertInfo;
}

MeshData* OBJ_model::GetFaces(){
	return &m_faceInfo;
}

std::map<string, Material>* OBJ_model::GetMaterialLibrary() {
	return &m_matlib;
}

std::vector<matF>* OBJ_model::GetMaterialKeys() {
	return &m_matKey;
}

void OBJ_model::AddVertexV(Vector3 v)
{
	m_vertInfo.vertex.push_back(v);
}

void OBJ_model::AddVertexT(Vector3 v)
{
	m_vertInfo.texture.push_back(v);
}

void OBJ_model::AddVertexN(Vector3 v)
{
	m_vertInfo.normal.push_back(v);
}

void OBJ_model::AddVFace(Vector3 f)
{
	m_faceInfo.vertex.push_back(f);
}

void OBJ_model::AddTFace(Vector3 f)
{
	m_faceInfo.texture.push_back(f);
}

void OBJ_model::AddNFace(Vector3 f)
{
	m_faceInfo.normal.push_back(f);
}

void OBJ_model::AddMat(Material& m)
{
	//matlib[m.mtlname] = m;
	m_matlib.insert(pair<string, Material>(m.mtlname, m));
}

void OBJ_model::AddMatKey(matF& m)
{
	m_matKey.push_back(m);
}

const void OBJ_model::Draw() {
	int j = 0;
	bool endOfModel = false;
	while (!endOfModel)
	{
		if (m_matKey.size() == j)
		{
			break;
		}
		SetMaterial(j);
		int tempSize = (m_matKey.at(j).lFace - m_matKey.at(j).fFace) * 3 + 3;
		/*	for(int k=0;k<3;k++){
		glBindVertexArray(m_matKey.at(j).VAO[k]);
		glBindBuffer(GL_ARRAY_BUFFER, m_matKey.at(j).VBO[k]);
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_matKey.at(j).IBO[k]);

		//glDrawRangeElements(GL_TRIANGLES,0,3,tempSize,GL_FLOAT,0);
		glEnableVertexAttribArray(glGetAttribLocation(1,m_matKey.at(j).name[k]));
		glVertexAttribPointer(glGetAttribLocation(1,m_matKey.at(j).name[k]),sizeof(Vector3)*tempSize/3,GL_FLOAT,GL_FALSE,0,0);
		glDrawArrays(GL_TRIANGLES, 0, tempSize);
		}*/
		Master::gl.Uniform1i(ShaderManager::Get(m_shaderProgram)->GetLocation("u_shader"), 0);
		Master::gl.Uniform1i(ShaderManager::Get(m_shaderProgram)->GetLocation("texture"), 0);
		Master::gl.UniformMatrix4fv(ShaderManager::Get(m_shaderProgram)->GetLocation("Material"), 1, GRAPHICS_FALSE, m_matlib.at(m_matKey.at(j).mtlname).GetVector());
		Master::gl.Uniform1f(ShaderManager::Get(m_shaderProgram)->GetLocation("shininess"), m_matlib.at(m_matKey.at(j).mtlname).shininess);
		/*if (FORWARD_SHADING) {
			Master::gl.Uniform1i(Master::s_3DShader.GetLocation("texture"), 0);
			Master::gl.UniformMatrix4fv(Master::s_3DShader.GetLocation("Material"), 1, GRAPHICS_FALSE, m_matlib.at(m_matKey.at(j).mtlname).GetVector());
			Master::gl.Uniform1f(Master::s_3DShader.GetLocation("shininess"), m_matlib.at(m_matKey.at(j).mtlname).shininess);

		}
		else {
			Master::gl.Uniform1i(Master::s_GeomPassShader.GetLocation("texture"), 0);
			Master::gl.UniformMatrix4fv(Master::s_GeomPassShader.GetLocation("Material"), 1, GRAPHICS_FALSE, m_matlib.at(m_matKey.at(j).mtlname).GetVector());
			Master::gl.Uniform1f(Master::s_GeomPassShader.GetLocation("shininess"), m_matlib.at(m_matKey.at(j).mtlname).shininess);
		}*/
		VertexBuffer::RenderVBO(&m_data, m_matKey.at(j).fFace * 3, m_matKey.at(j).lFace * 3 + 3);
	//	VertexBuffer::RenderIBO(&m_iData, m_matKey.at(j).fFace * 3, m_matKey.at(j).lFace * 3 + 3);
		j++;
		Master::gl.BindTexture(GRAPHICS_TEXTURE_2D, Master::NULL_TEXTURE);
	}
}

const void OBJ_model::SetMaterial(unsigned p_index) const {
	if (strcmp(m_matlib.at(m_matKey.at(p_index).mtlname).path.c_str(), "") != 0) {
		GraphicsEngine::SwitchToRGB(true);
		Master::gl.ActiveTexture(GRAPHICS_TEXTURE0);
		Master::gl.BindTexture(GRAPHICS_TEXTURE_2D, m_matlib.at(m_matKey.at(p_index).mtlname).texture);
		Master::gl.TexEnvi(GRAPHICS_TEXTURE_ENV, GRAPHICS_TEXTURE_ENV_MODE, GRAPHICS_MODULATE);
	}else {
		GraphicsEngine::ColorSettings();
		Master::gl.BindTexture(GRAPHICS_TEXTURE_2D, Master::NULL_TEXTURE);
	}
}

void OBJ_model::AssignVertices(matF* mat) {
	Vector3 vert;
	for (int i = mat->fFace; i <= mat->lFace; i++)
	{
		for (int k = 0; k < 3; k++) {
			vert.x = m_vertInfo.vertex[m_faceInfo.vertex[i][k]].x;
			vert.y = m_vertInfo.vertex[m_faceInfo.vertex[i][k]].y;
			vert.z = m_vertInfo.vertex[m_faceInfo.vertex[i][k]].z;
			m_data.data.vertex.push_back(vert);
			if (m_vertInfo.texture.size() > 0 && m_vertInfo.texture.size() > m_faceInfo.texture[i][k]) {
				vert.x = m_vertInfo.texture[m_faceInfo.texture[i][k]].x;
				vert.y = m_vertInfo.texture[m_faceInfo.texture[i][k]].y;
				vert.z = m_vertInfo.texture[m_faceInfo.texture[i][k]].z;
				m_data.data.texture.push_back(vert);
			}
			if (m_vertInfo.normal.size() > 0 && m_vertInfo.normal.size() > m_faceInfo.normal[i][k]) {
				vert.x = m_vertInfo.normal[m_faceInfo.normal[i][k]].x;
				vert.y = m_vertInfo.normal[m_faceInfo.normal[i][k]].y;
				vert.z = m_vertInfo.normal[m_faceInfo.normal[i][k]].z;
				m_data.data.normal.push_back(vert);
			}
		}
	}
	//vector<Vector3> l_unique;
	//for (int i = mat->fFace; i <= mat->lFace; i++)
	//{
	//	for (int k = 0; k < 3; k++) {
	//		vert.x = m_faceInfo.vertex[i][k];
	//		{
	//			vert.y = m_faceInfo.texture[i][k];
	//		}
	//		{
	//			vert.z = m_faceInfo.normal[i][k];
	//		}
	//		bool added = false;
	//		unsigned int j = 0;
	//		for (j = 0; j < l_unique.size(); j++) {//add to l_unique if face does not exist
	//			if (vert.x == l_unique[j].x && vert.y == l_unique[j].y && vert.z == l_unique[j].z) {
	//				added = true;
	//				break;
	//			}
	//		}
	//		if (!added) {
	//			l_unique.push_back(vert);
	//			break;
	//		}
	//		else {
	//			vert.y = j;//position of the face
	//			vert.x = -1;//to indicate that face is added
	//			l_unique.push_back(vert);
	//		}
	//	}
	//}

	//for (unsigned int i = 0; i < l_unique.size(); i++) {
	//	if (l_unique[i].x == -1)//already exists
	//	{
	//		m_iData.indices.push_back(l_unique[l_unique[i].y].x);
	//	}
	//	else {
	//		m_iData.indices.push_back(m_iData.vertexBuffer.data.vertex.size());
	//		vert.x = m_vertInfo.vertex[l_unique[i].x].x;
	//		vert.y = m_vertInfo.vertex[l_unique[i].x].y;
	//		vert.z = m_vertInfo.vertex[l_unique[i].x].z;
	//		l_unique[i].x = m_iData.vertexBuffer.data.vertex.size();//set to size to remember the position of the original
	//		m_iData.vertexBuffer.data.vertex.push_back(vert);
	//		{
	//			vert.x = m_vertInfo.texture[l_unique[i].y].x;
	//			vert.y = m_vertInfo.texture[l_unique[i].y].y;
	//			vert.z = m_vertInfo.texture[l_unique[i].y].z;
	//			m_iData.vertexBuffer.data.texture.push_back(vert);
	//		}
	//		{
	//			vert.x = m_vertInfo.normal[l_unique[i].z].x;
	//			vert.y = m_vertInfo.normal[l_unique[i].z].y;
	//			vert.z = m_vertInfo.normal[l_unique[i].z].z;
	//			m_iData.vertexBuffer.data.normal.push_back(vert);
	//		}
	//	}
	//}
}


void OBJ_model::GenerateVBO() {
	if (m_shaderProgram == 0)
		return;
	else {

		m_data.attribLocation[0] = ShaderManager::Get(m_shaderProgram)->GetLocation("position");
		m_data.attribLocation[1] = ShaderManager::Get(m_shaderProgram)->GetLocation("texCoord");
		m_data.attribLocation[2] = ShaderManager::Get(m_shaderProgram)->GetLocation("normal");
		VertexBuffer::CreateVBO(&m_data, m_shaderProgram);
		
		/*m_iData.vertexBuffer.attribLocation[0] = ShaderManager::Get(m_shaderProgram)->GetLocation("position");
		m_iData.vertexBuffer.attribLocation[1] = ShaderManager::Get(m_shaderProgram)->GetLocation("texCoord");
		m_iData.vertexBuffer.attribLocation[2] = ShaderManager::Get(m_shaderProgram)->GetLocation("normal");
		VertexBuffer::CreateIBO(&m_iData, m_shaderProgram);*/
	}
	/*if (FORWARD_SHADING) {
		m_data.attribLocation[0] = Master::gl.GetALoc(Master::s_3DShader.GetID(), "position");
		m_data.attribLocation[1] = Master::gl.GetALoc(Master::s_3DShader.GetID(), "texCoord");
		m_data.attribLocation[2] = Master::gl.GetALoc(Master::s_3DShader.GetID(), "normal");
		VertexBuffer::CreateVBO(&m_data, Master::s_3DShader.GetID());
	}
	else {
		m_data.attribLocation[0] = Master::gl.GetALoc(Master::s_GeomPassShader.GetID(), "position");
		m_data.attribLocation[1] = Master::gl.GetALoc(Master::s_GeomPassShader.GetID(), "texCoord");
		m_data.attribLocation[2] = Master::gl.GetALoc(Master::s_GeomPassShader.GetID(), "normal");
		VertexBuffer::CreateVBO(&m_data, Master::s_GeomPassShader.GetID());
	}*/
	//VertexBuffer::CreateIBO(&m_data,Master::s_3DShader.GetID());
	//gc.CreateIBO(&mat->ibo,&mat->vbo,&mat->vao,faceV,vertInfo.vertex, vertInfo.texture, vertInfo.normal);
	/*	vector<Vector3> data[6];
	data[0] = faceV;
	data[1] = vertInfo.vertex;
	data[2] = faceN;
	data[3] = vertInfo.normal;
	data[4] = faceT;
	data[5] = vertInfo.texture;
	for(int i=0;i<3;i++)
	gc.CreateIBO(&mat->IBO[i],&mat->VBO[i],&mat->VAO[i],data[i],data[i+1],mat->name[i]);*/
}
