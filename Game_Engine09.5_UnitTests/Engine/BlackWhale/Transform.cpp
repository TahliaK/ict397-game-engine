#include "Transform.h"
#include "GameObject.h"
//BOOST_CLASS_EXPORT(Transform)
BOOST_CLASS_EXPORT_IMPLEMENT(Transform)

Transform Transform::operator = (const Transform& p_original) {
	position = p_original.position;
	scale = p_original.scale;
	rotation = p_original.rotation;
	m_children.clear();
	IFORSI(0, p_original.m_children.size())
		m_children.at(i) = p_original.m_children.at(i);
	m_gameObject = p_original.m_gameObject;
	m_parent = p_original.m_parent;
	return *this;
}


Transform::Transform(const Transform& p_original) : Component(p_original) {
	position = p_original.position;
	scale = p_original.scale;
	rotation = p_original.rotation;
	IFORSI(0, p_original.m_children.size())
		m_children.at(i) = p_original.m_children.at(i);
	m_parent = p_original.m_parent;
}

Transform::~Transform() {
	IFORSI(0, m_children.size()) {
		if (m_children.at(i)) {
			delete m_children.at(i)->GetGameObject();
		}
	}
	m_children.clear();
}


std::vector<Transform*> Transform::GetChildren(void) const {
	return m_children;
}

const void Transform::SetParent(Transform* p_parent) {
	m_parent = p_parent;
}

Transform::Transform() : Component() {
	m_name = "Transform";
	m_parent = NULL;
	scale = Vector3(1);
	position = Vector3(0);
	rotation = Vector3(0);
}


const void Transform::AddChild(Transform* p_child) {
	m_children.push_back(p_child);
	m_children.back()->m_parent = this;
}

const Transform* Transform::GetParent(void) const {
	return m_parent;
}

const void Transform::DetachChild(Transform* p_child) {
	auto iterator = std::find(std::begin(m_children), std::end(m_children), p_child);
	if (iterator != std::end(m_children)) {
		//delete 	l_temp;
		m_children.erase(iterator);
	}
}

const bool Transform::IsChildOf(const Transform* p_parent) const {
	return p_parent == m_parent;
}

const void Transform::DeleteChild(Transform* p_child) {
	auto iterator = std::find(std::begin(m_children), std::end(m_children), p_child);
	if (iterator != std::end(m_children)) {
		delete 	p_child->GetGameObject();
		p_child = NULL;
		m_children.erase(iterator);
	}
}

const int Transform::GetChildrenCount(void) const {
	return m_children.size();
}

Transform* Transform::GetChild(int p_index) const {
	if (m_children.size() > p_index)
		return m_children.at(p_index);
	return NULL;
}