#include "SceneManager.h"
#include <fstream>

using namespace SceneManagement;
int SceneManager::m_activeScene = 0;
std::vector<Scene*> SceneManager::m_scenes;

int SceneManager::CreateScene(const std::string p_name) {
	Scene* l_pointer = new Scene;
	l_pointer->SetID(m_scenes.size());
	m_scenes.push_back(l_pointer);
	m_scenes.back()->SetName(p_name);
	return m_scenes.size() - 1;
}

Scene* SceneManager::GetActive(void) {
	if (m_scenes.size() > m_activeScene)
		return m_scenes.at(m_activeScene);
	else
		return NULL;
}

int SceneManager::GetActiveID(void) {
	if (m_scenes.size() > m_activeScene)
		return m_activeScene;
	else
		return -1;
}

Scene* SceneManager::GetAt(const int p_index) {
	if (p_index < m_scenes.size()) {
		return m_scenes.at(p_index);
	}
	return NULL;
}

void SceneManager::SaveActive() {
	std::string format(".scene");
	std::string filename("Scenes/" + m_scenes.at(m_activeScene)->GetName() + format);
	std::ofstream ofs(filename);
	if (ofs.good()) {
		Scene* l_scene = GetActive();
		if (OUTPUT_FORMAT == 1) {
			boost::archive::xml_oarchive oa(ofs);
			oa << SER_NVP(l_scene);
		}
		else {
			boost::archive::text_oarchive oa(ofs);
			oa << SER_NVP(l_scene);
		}
	}
	ofs.close();
}

int SceneManager::LoadScene(const std::string p_name) {
	std::string format(".scene");
	std::ifstream ifs("Scenes/" + p_name + format);
	Scene* l_existing = GetByFilename(p_name);
	if (l_existing) {
		SetActive(l_existing->GetID());
		return l_existing->GetID();
	}
	if (ifs.good()) {
		int p_index = CreateScene(p_name);
		if (OUTPUT_FORMAT == 1) {
			boost::archive::xml_iarchive ia(ifs);
			ia >> SER_NVP(*m_scenes.back());
		}
		else {
			boost::archive::text_iarchive ia(ifs);
			ia >> SER_NVP(*m_scenes.back());
		}
		ifs.close();
		return p_index;
	}
	else {
		ifs.close();
		return -1;
	}
	/*if (remove("Storage/Level.txt") != 0) {
	cout << "Could not delete a scene." << endl;
	}*/
}

void SceneManager::SetActive(int p_index) {
	if (p_index < m_scenes.size())
		m_activeScene = p_index;
}

int SceneManager::GetTotalAmount(void) {
	return m_scenes.size();
}

Scene* SceneManager::GetByFilename(const std::string p_name) {
	IFORSI(0, m_scenes.size()) {
		if (m_scenes.at(i)->GetName().compare(p_name) == 0)
			return m_scenes.at(i);
	}
	return NULL;
}

void SceneManager::Clear() {
	IFORSI(0, m_scenes.size()) {
		if (m_scenes.at(i)) {
			delete m_scenes.at(i);
		}
	}
	m_scenes.clear();
}