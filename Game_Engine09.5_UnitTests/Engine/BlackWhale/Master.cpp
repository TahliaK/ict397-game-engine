#include "Master.h"
#include "ShaderManager.h"
bool Master::EDITOR_MODE = true;
OpenGL Master::gl;
RGBA  Master::s_worldColor(0.6f, 0.6f, 0.8f, 1.0f);
ShaderProgram Master::s_TextShader;
ShaderProgram Master::s_WaterShader;
ShaderProgram Master::s_TerrainShader;
ShaderProgram Master::s_SkyboxShader;

ShaderProgram Master::s_GeomPassShader;
ShaderProgram Master::s_LightPassShader;
ShaderProgram Master::s_WaterDRShader;

GameObject* Master::s_guiCamera;

std::string Master::s_fontFolder = "Resources/GUI/Font/";
std::string Master::s_modelFolder = "Resources/Rendering/";
std::string Master::s_guiFolder = "Resources/GUI/";
std::string Master::s_soundFolder = "Resources/Sound/";

FrameBuffer Master::s_reflectionFBO;
FrameBuffer Master::s_refractionFBO;
FrameBuffer Master::s_shadowFBO;

unsigned int Master::NULL_TEXTURE = 0;

//1024x768 1366x768 1920x1080 1280x1024
//1280 x 720
Vector4 Master::s_screen(0, 0, 752, 440);
//Vector4 Master::s_screen(0,0,1280,720);
bool Master::STARTED = false;
bool Master::IS_FRAME_RENDER_MODE = false;


void Master::Reshape() {
	s_reflectionFBO.Clear();
	s_refractionFBO.Clear();
	s_shadowFBO.Clear();
	//have to change to have proper resizing
	//int res1[] = {0,0,1280,696};//window res
	int res1[] = { s_screen[0],s_screen[1],((int)s_screen[2]) / 8 * 8,((int)s_screen[3]) / 8 * 8 };
	//int res1[] = { 0,0,752,440 };
	//int res1[] = {200,200,840,560};
	//int res1[] = {30,200, screen[1][0]-330, screen[1][1]-30-200};
	//int res1[] = {0,0,1360,696};
	s_reflectionFBO.CreateBuffer(DEPTH_BUFFER, res1);
	s_refractionFBO.CreateBuffer(DEPTH_TEXTURE, res1);
	s_shadowFBO.CreateBuffer(DEPTH_TEXTURE, res1);
}
void Master::Start() {
	STARTED = true;
	while (NULL_TEXTURE == 0) {
		NULL_TEXTURE = GraphicsEngine::LoadTexture("Resources/Rendering/NULL.jpg", false);
	}
	s_guiCamera = NULL;
	ModifyShaders();
	Reshape();
}


void Master::InitializeShader(ShaderProgram& shader, const std::string vertexShader, const std::string fragmentShader) {
	shader.SetFragmentShader(fragmentShader);
	shader.SetVertexShader(vertexShader);

	shader.Create();
	shader.Attach();

	shader.Link();
}

void Master::ModifyShaders() {

	InitializeShader(s_TextShader, "Resources/Shaders/UI2D.vert", "Resources/Shaders/UI2D.frag");
	InitializeShader(s_WaterShader, "Resources/Shaders/Water.vert", "Resources/Shaders/Water.frag");
	InitializeShader(s_TerrainShader, "Resources/Shaders/Terrain.vert", "Resources/Shaders/Terrain.frag");
	InitializeShader(s_SkyboxShader, "Resources/Shaders/Skybox.vert", "Resources/Shaders/Skybox.frag");
	InitializeShader(s_LightPassShader, "Resources/Shaders/light.vert", "Resources/Shaders/light.frag");

	InitializeShader(s_WaterDRShader, "Resources/Shaders/WaterDR.vert", "Resources/Shaders/WaterDR.frag");

	InitializeShader(s_GeomPassShader, "Resources/Shaders/TerrainDR.vert", "Resources/Shaders/TerrainDR.frag");

	if (DEBUG_MODE) {
		if (s_WaterShader.GetID() == -1) {
			cout << "SHADER ERROR: " << PRINTNAME(s_WaterShader) << endl;
		}
		if (s_TextShader.GetID() == -1) {
			cout << "SHADER ERROR: " << PRINTNAME(s_TextShader) << endl;
		}
		if (s_TerrainShader.GetID() == -1) {
			cout << "SHADER ERROR: " << PRINTNAME(s_TerrainShader) << endl;
		}
		if (s_SkyboxShader.GetID() == -1) {
			cout << "SHADER ERROR: " << PRINTNAME(s_SkyboxShader) << endl;
		}
		/*if(s_GeomPassShader.GetID() == -1){
			cout<<"SHADER ERROR: "<< PRINTNAME(s_GeomPassShader)<<endl;
		}*/
		if (s_LightPassShader.GetID() == -1) {
			cout << "SHADER ERROR: " << PRINTNAME(s_LightPassShader) << endl;
		}
		if (s_WaterDRShader.GetID() == -1) {
			cout << "SHADER ERROR: " << PRINTNAME(s_WaterDRShader) << endl;
		}
	}

	std::string s1[] = { "texture","model","view", "proj", "lightCount", "cameraPos", "lightPosition", "Light", "Material", "shininess", "attenuation", "lightDirection", "lightCutOff" };
	//std::string s2[] = {"isSDF", "color","texture"};
	std::string s2[] = { "model", "isSDF", "color","texture" };
	std::string s3[] = { "texture","model","view", "proj", "cameraPos", "lightPosition", "Light", "Material","attenuation", "lightDirection", "lightCutOff" };
	std::string s4[] = { "model","view", "proj", "cameraPos", "lightPosition", "LightAmbient", "moveFactor", "reflectionT", "refractionT", "dudvMap", "normalMap" };
	std::string s5[] = { "model","view", "proj", "skybox" };
	//std::string s6[] = {"texture","model","view","proj","Material","plane"};
	std::string s7[] = { "proj","model","view","u_position","u_color","u_type","u_quadratic","u_radius","u_cameraPosition","u_tPosition","u_tColor","u_tNormal","u_amount" };
	std::string s8[] = { "model","view", "proj", "cameraPos", "lightPosition", "LightAmbient", "moveFactor", "reflectionT", "refractionT", "dudvMap", "normalMap","Material" };

	IFORSI(0, 4)
		Master::s_TextShader.SetULocation(s2[i]);
	IFORSI(0, 11)
		Master::s_TerrainShader.SetULocation(s3[i]);
	IFORSI(0, 11)
		Master::s_WaterShader.SetULocation(s4[i]);
	IFORSI(0, 4)
		Master::s_SkyboxShader.SetULocation(s5[i]);
	/*IFORSI(0,6)
		Master::s_GeomPassShader.SetULocation(s6[i]);*/
	IFORSI(0, 13)
		Master::s_LightPassShader.SetULocation(s7[i]);
	IFORSI(0, 12)
		Master::s_WaterDRShader.SetULocation(s8[i]);

	//s_LightPassShader = *ShaderManager::Get(ShaderManager::CreateShaderProgram("Resources/Shaders/Light.vert", "Resources/Shaders/Light.frag"));
}

void Master::Clear() {
	s_TextShader.Clear();
	s_WaterShader.Clear();
	s_TerrainShader.Clear();
	s_SkyboxShader.Clear();
	//s_GeomPassShader.Clear();
	s_LightPassShader.Clear();
	s_WaterDRShader.Clear();
	s_reflectionFBO.Clear();
	s_refractionFBO.Clear();
	//	s_shadowFBO().Clear();
}