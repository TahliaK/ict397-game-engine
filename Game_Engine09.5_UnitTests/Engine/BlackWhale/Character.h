/**
* @brief Data Structure to hold information about each glyph in the 
* fint texture atlas.
*
*/
#ifndef CHARACTER_H
#define CHARACTER_H
#include <iostream>
namespace Text{
class Character{
public:
	///ASCII value
	int id;

	///x texture coordinate
	float x;

	///y texture coordinate
	float y;

	///the width of the character
	float width;

	///the height of the character
	float height;

	///distance from the curser to the left edge of the character's quad
	float xOffset;

	///distance from the curser to the top edge of the character's quad
	float yOffset;

	///width of the quad
	float xSize;

	///height of the quad
	float ySize;

	///how far the cursor should advance after adding the character
	float xAdvance;

	Character();

	Character(int id);

	Character(int id, float xTextureCoord, float yTextureCoord, float xTexSize, float yTexSize,
		float xOffset, float yOffset, float sizeX, float sizeY, float xAdvance);
};

std::istream & operator >>(std::istream & input,Character & C );
std::ostream & operator <<(std::ostream & output, const Character & C);
}
#endif CHARACTER_H