#ifndef SCRIPTFUNC_H
#define SCRIPTFUNC_H
#include "BlackWhale/Input/Input.h"
#include "BlackWhale/Camera.h"
#include "BlackWhale/Math_s.h"
#include "BlackWhale/GameObject.h"
#include "BlackWhale/Scene.h"
#include "BlackWhale/SceneManager.h" //??

/**@class ScriptFunctions
*@brief defines all the functions and behaviours that scripting will have access to in the engine
*@date 22/04/2017
*@author Tahlia Kowald
*@version 1
*/
class ScriptFunctions
{
public:
	ScriptFunctions(void);
	~ScriptFunctions(void);

	
	typedef enum objectFeatures {VISIBLE = 0, STATIC = 1, COLLISION = 3} objectFeatures;

	// Scripting functions for object creation
	/**@brief Makes a 3D game object.
	*@param name the name of the 3D object
	*@param isVisible true if it is visible, false if invisible
	*@param isStatic true if this object does not move/get updated, false if it does
	*@param pos the object's position (x, y, z)
	*@param rot the object's rotation (x, y, z)
	*@param scale the object's scale (x, y, z)
	*@return A game object
	*/
	static GameObject * make3DObject(std::string name, bool isVisible, bool isStatic, Vector3 pos, Vector3 rot, Vector3 scale);
	/**@brief Loads a 3D model to associate it with a game object
	* Replaces any existing 3D model with that object.
	*@param go the gameobject in use
	*@param filename the filename of the 3D model
	*@return true/false success status
	*/
	static bool load3DModel(GameObject * go, std::string filename);
	/**@brief Sets 3D collisions for a game object
	*@param hasCollisions true if it should be tested for collisions
	*@param collisionMin the minimum (x, y, z) point of the AABB collision box
	*@param collisionMax the maximum (x, y, z) points of the AABB collision box
	*@return true if succeeded, false if not
	*/
	static bool set3DCollisions(GameObject * go, bool hasCollisions, Vector3 collisionMin, Vector3 collisionMax); //likely to change, this would use AABB collisions
	/**@brief a general function for changing game object status
	*@param go the gameobject to edit
	*@param f the feature to edit
	*@param state the true/false state of the feature
	*@return true/false indicator of success
	*/
	static bool setObjectFeature(GameObject * go, objectFeatures f, bool state);
	/**@brief moves a gameObject in 3D space
	*@param go the gameobject to be moved
	*@param direction the direction vector to move
	*@param rotation the rotation vector to move
	*@return true/false indicator of success
	*/
	static bool moveObject(GameObject * go, Vector3 direction, Vector3 rotation);
	/**@brief Destroys a 3D object
	*@param go 3D object to be destroyed
	*@return true/false measure of success
	*/
	static bool destroy3DObject(GameObject * go);

	/* TODO: Scripting functions for UI creation */

	// Scripting functions for object management
	/**@brief gives an indicator if two objects are in a collision
	*@param g1 game object #1 to test
	*@param g2 game object #2 to test
	*@return true if in collision, false if not
	*/
	static bool areObjectsColliding(GameObject * g1, GameObject * g2);

	// Script functions for camera creation & editing
	/**@brief creates a Perspective 3D camera
	*@param p_fov the camera's field of view angle
	*@param p_aspect the camera's aspect ratio
	*@param p_near the camera's near clip plane distance
	*@param p_far the camera's far clip plane distance
	*@param pos the camera's position (x, y, z)
	*@param up the camera's up vector (x, y, z)
	*@return a camera object with the given parameters
	*/
	static Camera * createPerspectiveCamera(float p_fov, float p_aspect, float p_near, float p_far, Vector3 pos, Vector3 at, Vector3 up); //creates a camera
	/**@brief Edits the camera to have the given perspective qualities
	* This can be applied to an ortho camera too - it just has a perspective camera as the output
	*@param p_fov the camera's field of view angle
	*@param p_aspect the camera's aspect ratio
	*@param p_near the camera's near clip plane distance
	*@param p_far the camera's far clip plane distance
	*@param pos the camera's position (x, y, z)
	*@param up the camera's up vector (x, y, z)
	*@return a true/false indicator of success
	*/
	static bool editCameraPerspective(Camera & c, float p_fov, float p_aspect, float p_near, float p_far, Vector3 pos, Vector3 at, Vector3 up); //edits a camera to perspective
	/**@brief creates an orthographic 3d camera object
	*@param p_left the left clip plane distance
	@param p_right the right clip plane distance
	@param p_bottom the bottom clip plane distance
	@param p_top the top clip plane distance
	@param p_near the near clip plane distance
	@param p_far the far clip plane distance
	@param pos the camera's position (x, y, z)
	@param up the camera's up vector (x, y, z)
	@return an orthographic perspective camera
	*/
	static Camera * createOrthoCamera(float p_left, float p_right, float p_bottom, float p_top, float p_near, float p_far, Vector3 pos, Vector3 at, Vector3 up); //creates an ortho camera
	/**@brief Edits a camera to have the given orthographic qualities
	* Can be applied to a perspective camera, it just means the output will be an orthographic camera
	*@param p_left the left clip plane distance
	@param p_right the right clip plane distance
	@param p_bottom the bottom clip plane distance
	@param p_top the top clip plane distance
	@param p_near the near clip plane distance
	@param p_far the far clip plane distance
	@param pos the camera's position (x, y, z)
	@param up the camera's up vector (x, y, z)
	@return a true/false indicator of success
	*/
	static bool editCameraOrtho(Camera & c, float p_left, float p_right, float p_bottom, float p_top, float p_near, float p_far, Vector3 pos, Vector3 at, Vector3 up); //edits a camera to ortho

	// Script functions for camera editing
	/**@brief moves the camera a given direction and/or rotation
	*@param c the camera to move
	*@param direction the direction vector for movement
	*@param rotation the rotation vector for movement
	*/
	static void moveCamera(Camera * c, Vector3 direction, Vector3 rotation); //moves the camera
	/**@brief Gives indication if camera's future movement will cause a collision with a given object
	*@param c the camera to test against
	*@param direction the direction that the camera will move
	*@param rotation the rotation that the camera will move
	*@param go the game object to test with*/
	static bool movementWillCollide(Camera * c, Vector3 direction, Vector3 rotation, GameObject & go); //returns true if camera will collide with something
	/**@brief Gives indication if camera's future movement will cause a collision with a given scene's worth of objects
	*@param c the camera to test against
	*@param direction the direction that the camera will move
	*@param rotation the rotation that the camera will move
	*@param s the scene object to test with*/
	static bool movementWillCollide(Camera * c, Vector3 direction, Vector3 rotation, SceneManagement::Scene s);

	// Script functions for input registration
	/**@brief gives the change vector of mouse movement on the screen
	*@param x the output variable of the mouse's x movement
	*@param y the output variable of the mouse's y movement
	*/
	static void mouseChangeVec(float &x, float &y); //gives vector of mouse's movement in last turn
	/**@brief gives mouse location on the window/screen
	*@param x the mouse's x location output
	*@param y the mouse's y location output
	*/
	static void mouseLocation(float &x, float &y); //gives mouse's actual location on the 2D screen
	/**@brief checks whether a particular key is pressed down
	*@param k the key to check for
	*@return true if pressed, false if not pressed
	*/
	static bool keyPressed(enum KeyCode k); //checks if a particular key is pressed

	/* TODO: Functions for the game engine to be aware of what camera is in use right now */

	//Time monitoring control
	/**@brief gives the time in ms since last checked the time
	*@return time in miliseconds
	*/
	static float timeDif(); //gives time since started, or time since last checked, in miliseconds.

	//Scene management
	static int newScene(std::string sceneName); //creates a new scene in sceneManager w/ this sceneName
	static int loadScene(std::string sceneName); //loads an existing scene in sceneManager w/ this sceneName
	static bool swapSceneTo(int sceneIndex); //swaps to a scene with this sceneName
	
};

#endif