//16 bit floating point,  UBO
#version 330 core

const int MAX_LIGHTS = 1;

uniform int lightCount;
uniform vec3 lightPosition[MAX_LIGHTS];
uniform vec3 lightColor[MAX_LIGHTS];


uniform vec3 attenuation;//[0] - constant, [1] - linear, [2] - quadratic;


/*uniform struct LightSourceParameters{
	vec4 ambient;
	vec4 diffuse;
	vec4 specular;
	vec4 position;
	vec4 helfVector;

	vec3 spotDirection;
	float spotExponent;
	float spotCutoff;
	float  spotCosCutoff;
} Light[MAX_LIGHTS];
*/

uniform vec4 plane;
uniform mat4 proj;
uniform	mat4 model;
uniform mat4 view;

uniform vec3 cameraPos;
uniform float fps;

in vec3 position;
in vec3 normal;
in vec3 texCoord;


out vec2 uv;
out float atten;//used to reduce the light's intensity

out ToFrag{
	vec3 normal;
	vec4 viewDir;

	vec4 reflectDir;
	vec4 lightDir;
}Frag;

void DeterminePosition();
void SendToFragmentShader();

void main(){
	DeterminePosition();
	SendToFragmentShader();
}

void DeterminePosition(){
//	v.z = sin(1.0 * v.z + fps * 0.1);
	//v.z +=fps*5;

	vec4 worldPos = model * vec4(position,1);
	gl_ClipDistance[0] = dot(worldPos,plane);
	gl_Position = proj * view * worldPos;
}

void SendToFragmentShader(){
	uv= texCoord.st;
	mat3 normal_matrix = transpose(inverse(mat3(model)));
	Frag.normal = normal_matrix * normal;
	
	int amount = min(MAX_LIGHTS,lightCount);
	vec3 lightDirection[MAX_LIGHTS]; 
	
	for(int i = 0; i< amount; i++){
	vec4 lightPos = vec4(lightPosition[i],0);
		lightDirection[i] = vec3(/*modelview */ lightPos);
	}
	vec4 light_position = view * vec4(lightPosition[0],1);
	if(light_position.w == 1){//should be 0
		light_position.w = 0;
		Frag.lightDir = normalize(-light_position);
	}else{
		Frag.lightDir = normalize(light_position - (model * vec4(position,1)));//model only for position
	}

	Frag.viewDir = vec4(normalize(cameraPos - position),1);//view direction
	
	Frag.reflectDir = reflect(-Frag.lightDir, vec4(Frag.normal,1));//reflection direction
	
	//POINT LIGHTS
	float dist = distance(light_position, vec4(position,1));
	atten = 1.0f / (attenuation.x + attenuation.y * dist + attenuation.z * (dist * dist));

	//atten = exp(-9 * distance(gl_Position, light_position) * distance(gl_Position, light_position));
}
