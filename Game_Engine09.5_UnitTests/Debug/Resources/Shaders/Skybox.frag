#version 330 core

uniform samplerCube skybox;

in vec3 texCoords;

out vec4 outputF;

void main(){
	outputF = texture(skybox, texCoords);
}