#version 330 core
 
uniform vec4 color;
uniform sampler2D texture;
uniform int isSDF = 0; //Signed Distance Field

in vec2 uv;

const float width = 0.5;
const float edge = 0.1;

out vec4 outputF;

void main()
{
	if(isSDF == 1){
		float distance = 1.0 - texture2D(texture, uv).a;
		float alpha = 1.0 - smoothstep(width, width + edge, distance);
		outputF = vec4(texture2D(texture, uv).xyz * color.xyz,alpha);
	}else{
		outputF = texture2D(texture,uv)* color;
	}
}

/*
#version 330 core
 
uniform vec4 color;
uniform sampler2D texture;
uniform int isSDF = 0; //Signed Distance Field

uniform float borderWidth = 0;
uniform float borderEdge = 0;

uniform float width = 0;
uniform float edge = 0;
uniform vec3 offset;
uniform vec3 outlineColor;

in vec2 uv;

out vec4 outputF;

void main()
{
	if(isSDF == 1){
		float distance = 1.0 - texture2D(texture, uv).a;
		float alpha = 1.0 - smoothstep(width, width + edge, distance);

		float distance2 = 1.0 - texture2D(texture, vec3(uv)+offset).a;
		float alpha2 = 1.0 - smoothstep(borderWidth, borderWidth + borderEdge, distance2);
		
		float alpha3 =alpha + (1.0 - alpha) * alpha2;
		vec3 overall = mix(outlineColor, color, alpha /alpha3);

		outputF = vec4(texture2D(texture, uv).xyz * overall, alpha);
	}else{
		outputF = texture2D(texture,uv)* color;
	}


	//invert texture vertically
	//vec2 upDownUV = vec2(uv.x, -1 *uv.y);
	//outputF = texture2D(texture,upDownUV)* color;
}
*/