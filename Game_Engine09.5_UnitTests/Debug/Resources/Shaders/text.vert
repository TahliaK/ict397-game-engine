#version 330 core

//uniform	mat4 proj;
//uniform	mat4 modelview;
//uniform vec4 viewport;
 
//uniform vec3 translate;
in vec3 position;
in vec3 texCoord;
in vec3 normal;

out vec2 uv;

void main(void){
	gl_Position = vec4(position,1);
	uv = vec2(texCoord.st);
}


