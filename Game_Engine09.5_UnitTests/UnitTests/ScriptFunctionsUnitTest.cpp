#include "stdafx.h"
#include "CppUnitTest.h"
#include "../Engine/ScriptFunctions.h"

using namespace Microsoft::VisualStudio::CppUnitTestFramework;

namespace UnitTests
{		
	TEST_CLASS(UnitTest1)
	{
	public:
		
		TEST_METHOD(TestMethod1)
		{
			// TODO: Your test code here
		}

		TEST_METHOD(Make3DObject_Test)
		{
			GameObject * go = ScriptFunctions::make3DObject("name", true, false,
				Vector3(0, 0, 0), Vector3(0, 1, 0), Vector3(1, 1, 1));

			Assert::IsTrue(go->GetName() == "name", L"Error: Name incorrect.");
			Assert::IsTrue(go->GetVisible() == true, L"Error: Visibility is not correct.");
			Assert::IsTrue(go->GetStatic() == false, L"Error: Static is not correct.");
			Assert::IsTrue(go->GetTransform()->position == Vector3(0, 0, 0), L"Error: position is not correct.");
			Assert::IsTrue(go->GetTransform()->rotation == Vector3(0, 1, 0), L"Error: rotation is not correct.");
			Assert::IsTrue(go->GetTransform()->scale == Vector3(1, 1, 1), L"Error: scale is not correct.");

		}

	};
}